﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model.LD
{
    public class Dannie : INotifyPropertyChanged  //Личные данные
    {
        public event PropertyChangedEventHandler PropertyChanged;
        string lnumber_ = ".";
        string fio_ = ".";
        string pol_ = ".";
        string mesto_ = ".";
        string date_ = ".";
        string nac_ = ".";
        string gr_ob_ = ".";
        string vo_ob_ = ".";
        string card_ = ".";
        string vod_pr_ = ".";
        string voen_ = ".";
        string home_ = ".";
        string pac_ = ".";
        string types_ = ".";
        public Dannie() { }
        public Dannie(XElement element)
        {
            if (element.Attribute("lnumber").Value != null) Lnumber = element.Attribute("lnumber").Value;
            if (element.Attribute("fio").Value != null) FIO = element.Attribute("fio").Value;
            if (element.Attribute("pol").Value != null) POL = element.Attribute("pol").Value;
            if (element.Attribute("mesto_brd").Value != null) Mesto_brd = element.Attribute("mesto_brd").Value;
            if (element.Attribute("date_brd").Value != null) Date_brd = element.Attribute("date_brd").Value;
            if (element.Attribute("nac").Value != null) Nac = element.Attribute("nac").Value;
            if (element.Attribute("gr_obr").Value != null) Gr_obr = element.Attribute("gr_obr").Value;
            if (element.Attribute("voen_obr").Value != null) Voen_obr = element.Attribute("voen_obr").Value;
            if (element.Attribute("bank_card").Value != null) Bank_card = element.Attribute("bank_card").Value;
            if (element.Attribute("vod_prava").Value != null) Vod_prava = element.Attribute("vod_prava").Value;
            if (element.Attribute("voenkomat").Value != null) Voenkomat = element.Attribute("voenkomat").Value;
            if (element.Attribute("home_adres").Value != null) Home_adres = element.Attribute("home_adres").Value;
            if (element.Attribute("pac").Value != null) PAC = element.Attribute("pac").Value;
            if (element.Attribute("types").Value != null) Types = element.Attribute("types").Value;
        }
        public string Lnumber
        {
            get { return lnumber_; }
            set
            {
                if (lnumber_ == value)
                    return;
                lnumber_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Lnumber"));
            }
        }
        public string FIO
        {
            get { return fio_; }
            set
            {
                if (fio_ == value)
                    return;
                fio_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FIO"));
            }
        }
        public string POL
        {
            get { return pol_; }
            set
            {
                if (pol_ == value)
                    return;
                pol_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("POL"));
            }
        }
        public string Mesto_brd
        {
            get { return mesto_; }
            set
            {
                if (mesto_ == value)
                    return;
                mesto_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Mesto_brd"));
            }
        }
        public string Date_brd
        {
            get { return date_; }
            set
            {
                if (date_ == value)
                    return;
                date_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date_brd"));
            }
        }
        public string Nac
        {
            get { return nac_; }
            set
            {
                if (nac_ == value)
                    return;
                nac_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Nac"));
            }
        }
        public string Gr_obr
        {
            get { return gr_ob_; }
            set
            {
                if (gr_ob_ == value)
                    return;
                gr_ob_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Gr_obr"));
            }
        }
        public string Voen_obr
        {
            get { return vo_ob_; }
            set
            {
                if (vo_ob_ == value)
                    return;
                vo_ob_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Voen_obr"));
            }
        }
        public string Bank_card
        {
            get { return card_; }
            set
            {
                if (card_ == value)
                    return;
                card_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Bank_card"));
            }
        }
        public string Vod_prava
        {
            get { return vod_pr_; }
            set
            {
                if (vod_pr_ == value)
                    return;
                vod_pr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Vod_prava"));
            }
        }
        public string Voenkomat
        {
            get { return voen_; }
            set
            {
                if (voen_ == value)
                    return;
                voen_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Voenkomat"));
            }
        }
        public string Home_adres
        {
            get { return home_; }
            set
            {
                if (home_ == value)
                    return;
                home_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Home_adres"));
            }
        }
        public string PAC
        {
            get { return pac_; }
            set
            {
                if (pac_ == value)
                    return;
                pac_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PAC"));
            }
        }
        public string Types
        {
            get { return types_; }
            set
            {
                if (types_ == value)
                    return;
                types_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public void ToXml(string LOGIN)
        {
            string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + LOGIN + ".xml";
            XDocument doc;
            if (File.Exists(fileName) == false)
            {
                doc = new XDocument(
                new XElement("base",
                    new XElement("track_dan",
                        new XAttribute("lnumber", Lnumber),
                        new XAttribute("fio", FIO),
                        new XAttribute("pol", POL),
                        new XAttribute("mesto_brd", Mesto_brd),
                        new XAttribute("date_brd", Date_brd),
                        new XAttribute("nac", Nac),
                        new XAttribute("gr_obr", Gr_obr),
                        new XAttribute("voen_obr", Voen_obr),
                        new XAttribute("bank_card", Bank_card),
                        new XAttribute("vod_prava", Vod_prava),
                        new XAttribute("voenkomat", Voenkomat),
                        new XAttribute("home_adres", Home_adres),
                        new XAttribute("types", Types),
                        new XAttribute("pac", PAC))));
                doc.Save(fileName);
            }
            else
            {
                doc = XDocument.Load(fileName);
                XElement track = new XElement("track_dan");
                track.Add(new XAttribute("lnumber", Lnumber));
                track.Add(new XAttribute("fio", FIO));
                track.Add(new XAttribute("pol", POL));
                track.Add(new XAttribute("mesto_brd", Mesto_brd));
                track.Add(new XAttribute("date_brd", Date_brd));
                track.Add(new XAttribute("nac", Nac));
                track.Add(new XAttribute("gr_obr", Gr_obr));
                track.Add(new XAttribute("voen_obr", Voen_obr));
                track.Add(new XAttribute("bank_card", Bank_card));
                track.Add(new XAttribute("vod_prava", Vod_prava));
                track.Add(new XAttribute("voenkomat", Voenkomat));
                track.Add(new XAttribute("home_adres", Home_adres));
                track.Add(new XAttribute("types", Types));
                track.Add(new XAttribute("pac", PAC));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }
    }
}
