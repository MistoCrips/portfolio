﻿using ActionMailer.Net.Mvc;
using Proba2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Proba2.Controllers
{
    public class EmailController : MailerBase
    {
        // GET://EmailResult Email
        public void SendEmail(EmailModel model)
        {
                       MailMessage mail = new MailMessage();
            mail.To.Add(model.To);
            mail.From = new MailAddress(model.From);
            mail.Subject = model.Subject;
            mail.Body = model.Body;
            mail.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Credentials = new System.Net.NetworkCredential
                 ("address@gmail.com", "pas"); 
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.Send(mail);
        }
    }
}