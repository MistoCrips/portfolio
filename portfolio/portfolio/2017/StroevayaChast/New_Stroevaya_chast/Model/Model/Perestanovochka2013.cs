﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model
{
    public class Perest : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        string lnumber_ = ".";
        string fio_ = ".";
        string zvanie_ = ".";
        string types_ = ".";
        string oldPodr_ = ".";
        string oldDolg_ = ".";
        string newDolg_ = ".";
        string newPodr_ = ".";
        string vus_ = ".";
        string kod_ = ".";
        int id_ = 0;
        public int Id
        {
            get { return id_; }
            set
            {
                if (id_ == value)
                    return;
                id_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Id"));
            }
        }
        public string Lnumber
        {
            get { return lnumber_; }
            set
            {
                if (lnumber_ == value)
                    return;
                lnumber_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Lnumber"));
            }
        }
        public string Zvanie
        {
            get { return zvanie_; }
            set
            {
                if (zvanie_ == value)
                    return;
                zvanie_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Zvanie"));
            }
        }
        public string FIO
        {
            get { return fio_; }
            set
            {
                if (fio_ == value)
                    return;
                fio_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FIO"));
            }
        }
        public string Types
        {
            get { return types_; }
            set
            {
                if (types_ == value)
                    return;
                types_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public string Old_podr
        {
            get { return oldPodr_; }
            set
            {
                if (oldPodr_ == value)
                    return;
                oldPodr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Old_podr"));
            }
        }
        public string Old_dolg
        {
            get { return oldDolg_; }
            set
            {
                if (oldDolg_ == value)
                    return;
                oldDolg_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Old_dolg"));
            }
        }
        public string New_dolg
        {
            get { return newDolg_; }
            set
            {
                if (newDolg_ == value)
                    return;
                newDolg_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("New_dolg"));
            }
        }
        public string New_podr
        {
            get { return newPodr_; }
            set
            {
                if (newPodr_ == value)
                    return;
                newPodr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("New_podr"));
            }
        }
        public string VUS
        {
            get { return vus_; }
            set
            {
                if (vus_ == value)
                    return;
                vus_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("VUS"));
            }
        }
        public string Kod
        {
            get { return kod_; }
            set
            {
                if (kod_ == value)
                    return;
                kod_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Kod"));
            }
        }
        public Perest()
        {
        }
        public Perest(XElement element)
        {
            Lnumber = element.Attribute("lnumber").Value;
            FIO = element.Attribute("fio").Value;
            Zvanie = element.Attribute("tek_zvanie").Value;
            Types = element.Attribute("type").Value;
            Old_dolg = element.Attribute("old_dolg").Value;
            Old_podr = element.Attribute("old_podr").Value;
            newDolg_ = element.Attribute("new_dolg").Value;
            New_podr = element.Attribute("new_podr").Value;
            VUS = element.Attribute("vus").Value;
            Kod = element.Attribute("kod").Value;
        }
    }// Перестановки военнослужащих с одной должности на другую
}
