package com.example.user.proba;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.nio.channels.FileChannel;
import java.security.spec.ECField;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import android.net.Uri;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.IBinder;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.Context;

import com.example.user.proba.MusicService.MusicBinder;
import android.widget.MediaController.MediaPlayerControl;

public class MainActivity extends Activity {//implements MediaPlayerControl {
    private static final int MY_PERMISSION_REQUEST = 1;
    final String LOG_TAG = "myLogs";
    private MusicIntentReceiver myReceiver;
    BoxAdapter boxAdapter;
    static Track Songer, redact_track, track_create_playlist;
    static int totalTime, active_track_int, redacter, status_box, value_box, counter_playlist = 0, playlist_type_vibor = 0;
    Spinner  Sp_sort_value;
    static String Janr, Lang_, PodJanr, Nastr, uri_create, types_playlist_teg;
    static int Track_Teg, Playlist_key;
    EditText editText, editText2;
    ArrayAdapter<String> adapter;
    Track TMP, track_to_del;
    GridLayout Into_Track_layout;
    LinearLayout Create_track_list, Nastroyki,
            Active_layout_track, Create_New_track_list,
            Create_New_track_list_artist, Create_New_track_list_janr,
            Create_New_track_list_name, Create_New_track_list_podjanr,
            Create_New_track_list_lang, Create_New_track_list_nastr,
            Player_list, Playlists, Playlists_create,
            Create_Playlist_menu, Create_Playlist_menu_teg_type,
            Create_Playlist_menu_teg_value, Create_Playlist_menu_classic_name;
    CheckBox Classic, Meloman;
    Button Nav_trackBTN, playBtn, pustishka, repeat, loveBtn;
    SeekBar positionBar;
    TextView elapsedTimeLabel, remainingTimeLabel, artist_box, artist_box1, artist_box2, artist_box3, artist_box4, artist_box5,
            name_box, LabelInfoArtist1, LabelInfoArtist2, LabelInfoArtist3, LabelInfoArtist4, LabelInfoTitle, LabelInfoJanr, LabelInfoPodJanr, LabelInfoLang,
            LabelInfoNastr,LabelInfoDate, name_classic_playlist;
    ArrayList<CheckBoxClassInit> arraySpinner, arraySpinner_lang, arraySpinner_podjanr, arraySpinnerTeg_2,
        arraySpiner_create_list, arrayNastroenie_list, Language, Janr_list, Podjanr_list;
    String[] arraySpiner_sort_collection = {"По исполнителю", "По названию", "По дате добавления"};
    int repeat_;
    private ArrayList<Track>  AllTrackList, ActiveTrackList, PlayTrackList,
            CollectionTrackList, ListCollectionTeg, loveTrackCollection, Create_List,
            track_list1, track_list2, track_list3, track_list4, track_list5;

    static private  ArrayList<Track>  Clone;
    private ArrayList<Playlist> NamesPlaylist, PlaylistsValue;
    DBHelper dbHelper;
    private ListView AllTrackListView, ActiveTrackListView, list_playlist, Janr_sp, podjanr_box,
            Lang_tb, nastr_box, Sp_teg_type, Sp_teg_value, list_playlist_create;
    private MusicService musicSrv;
    private Intent playIntent;
    private boolean musicBound=false, love= false, hantlerbool = false;
    //private MusicController controller;
    private boolean paused=false, playbackPaused=false;
    TextView titleBox;
    private int pauseSong = 0;
    private ServiceConnection musicConnection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            setTheme(R.style.AppTheme);
            myReceiver = new MusicIntentReceiver();
            Playlist_key = 0;
            //Log.d(LOG_TAG,"плеер включен");
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            Playlists = (LinearLayout)findViewById(R.id.Playlists);
            final  TextView Create_Playlist = (TextView) findViewById(R.id.Create_Playlist);
            loveTrackCollection = new ArrayList<Track>();
            ListCollectionTeg = new ArrayList<Track>();
            Clone = new ArrayList<Track>();
            track_list1  = new ArrayList<Track>();
            track_list2  = new ArrayList<Track>();
            track_list3  = new ArrayList<Track>();
            track_list4  = new ArrayList<Track>();
            track_list5  = new ArrayList<Track>();
            NamesPlaylist = new ArrayList<Playlist>();
            PlaylistsValue = new ArrayList<Playlist>();
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            //setController();
            Perem();
            status_box = 0;
            final LinearLayout bg = (LinearLayout)findViewById(R.id.Player);
            Into_Track_layout = (GridLayout)findViewById(R.id.Into_Track_layout);
            registerForContextMenu(bg);
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            repeat_ = 0;
            try {
                CollectionTrackList = read("mytable");
            }
            catch (Exception e){
                Log.d(LOG_TAG,"Ошибка при чтении базы коллекции ");
            }
            try {
                    loveTrackCollection = read("mytable_1");
            }
            catch (Exception e){
                Log.d(LOG_TAG,"Ошибка при чтении базы Избранных " + e.getMessage());
            }
            getSongList();
            Collections.sort(AllTrackList, new Comparator<Track>(){
                public int compare(Track a, Track b){
                    return b.getDate().compareTo(a.getDate());
                }
            });
            positionBar = (SeekBar) findViewById(R.id.possitionBar);
            bg.setOnTouchListener(new OnSwipeTouchListener(this) {
                public void onSwipeTop() {

                }
                public void onSwipeRight() throws IOException {
                    musicSrv.playPrev();
                    playBtn.setBackgroundResource(R.drawable.stop);
                    players();
                }
                public void onSwipeLeft() throws IOException {
                    musicSrv.Nexting_(true);
                    musicSrv.playNext();
                    playBtn.setBackgroundResource(R.drawable.stop);
                    players();
                }
                public void onSwipeBottom() {

                }
            });
            try {
                if(CollectionTrackList.size()>0){
                    ActiveTrackList = CollectionTrackList;
                    //ActiveTrackList = AllTrackList;
                }
                else {
                    ActiveTrackList = AllTrackList;
                }
            }catch (Exception e){
                Log.d(LOG_TAG, "Ошибка в передаче активному листу: " + e.getMessage());
            }
            SongAdapter songAllAdt = new SongAdapter(this, AllTrackList);
            AllTrackListView.setAdapter(songAllAdt);
            SongAdapterActiveList songActAdt = new SongAdapterActiveList(this, ActiveTrackList);
            ActiveTrackListView.setAdapter(songActAdt);

            NamesPlaylist = readPlaylist();
            for(Playlist t: NamesPlaylist){
                PlaylistsValue.add(t);
            }
            //PlaylistsValue = NamesPlaylist;
            SongList();
            Playlist t = new Playlist("Все песни",AllTrackList);
            NamesPlaylist.add(t);
            t = new Playlist("Коллекция",CollectionTrackList);
            t.setName_table("mytable");
            NamesPlaylist.add(t);
            t = new Playlist("Избранные", loveTrackCollection);
            t.setName_table("mytable_1");
            NamesPlaylist.add(t);
            Log.d(LOG_TAG, "Размер массива с плейлистами: "+NamesPlaylist.size());
            PlayListAdapter playAdatper = new  PlayListAdapter(this, NamesPlaylist);
            list_playlist.setAdapter(playAdatper);
            Log.d(LOG_TAG, "Размер массива с плейлистами 2: "+PlaylistsValue.size());
            PlayListAdapter playAdatper_create = new  PlayListAdapter(this, PlaylistsValue);
            list_playlist_create.setAdapter(playAdatper_create);
            musicConnection = new ServiceConnection(){
                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {
                    MusicBinder binder = (MusicBinder)service;
                    //get service
                    musicSrv = binder.getService();
                    //pass list
                    musicSrv.setList(ActiveTrackList);
                    musicBound = true;
                    Log.d(LOG_TAG, "musicBound Запущен");

                }

                @Override
                public void onServiceDisconnected(ComponentName name) {
                    musicBound = false;
                }
            };

        }
        catch (Exception e){
            Log.d(LOG_TAG, "Ошибка в Крите: " + e.getMessage());
        }

    }
    private void Perem(){
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //navigationView.setNavigationItemSelectedListener(this);
        titleBox = (TextView) findViewById(R.id.TitleTrack);
        LabelInfoArtist1= (TextView) findViewById(R.id.LabelInfoArtist1);
        LabelInfoArtist2= (TextView) findViewById(R.id.LabelInfoArtist2);
        LabelInfoArtist3= (TextView) findViewById(R.id.LabelInfoArtist3);
        LabelInfoArtist4= (TextView) findViewById(R.id.LabelInfoArtist4);
        LabelInfoTitle= (TextView) findViewById(R.id.LabelInfoTitle);
        name_classic_playlist= (TextView) findViewById(R.id.name_classic_playlist);
        LabelInfoJanr= (TextView) findViewById(R.id.LabelInfoJanr);
        LabelInfoPodJanr= (TextView) findViewById(R.id.LabelInfoPodJanr);
        LabelInfoLang= (TextView) findViewById(R.id.LabelInfoLang);
        LabelInfoNastr= (TextView) findViewById(R.id.LabelInfoNastr);
        LabelInfoDate= (TextView) findViewById(R.id.LabelInfoDate);
        editText=(EditText)findViewById(R.id.txtsearch);
        editText2 =(EditText)findViewById(R.id.txtsearch2);
        Classic = (CheckBox) findViewById(R.id.checkBoxClassic);
        Meloman =  (CheckBox) findViewById(R.id.checkBoxMeloman);
        playBtn = (Button) findViewById(R.id.playBtn);
        elapsedTimeLabel = (TextView) findViewById(R.id.elapsedTimeLabel);
        remainingTimeLabel = (TextView) findViewById(R.id.remainingTimeLabel);
        Nav_trackBTN = (Button) findViewById(R.id.nav_track);
        repeat = (Button) findViewById(R.id.repeatBtn);
        pustishka = (Button) findViewById(R.id.btn0);
        Create_track_list = (LinearLayout) findViewById(R.id.Layout_track);
        Player_list = (LinearLayout) findViewById(R.id.Player);
        Playlists_create = (LinearLayout) findViewById(R.id.Playlists_create);
        AllTrackListView = (ListView) findViewById(R.id.list_track);
        ActiveTrackListView = (ListView) findViewById(R.id.active_list_track);
        list_playlist_create = (ListView) findViewById(R.id.list_playlist_create);
        list_playlist = (ListView) findViewById(R.id.list_playlist);
        Janr_sp = (ListView) findViewById(R.id.Janr_sp);
        podjanr_box = (ListView) findViewById(R.id.podjanr_box);
        Lang_tb = (ListView) findViewById(R.id.Metka_Lang_tb);
        nastr_box = (ListView) findViewById(R.id.Metka_Nastr_tb);
        Sp_teg_type= (ListView) findViewById(R.id.Sp_teg_type);
        Sp_teg_value= (ListView) findViewById(R.id.Sp_teg_value);
        Active_layout_track = (LinearLayout) findViewById(R.id.Active_Layout_track);
        Create_New_track_list = (LinearLayout) findViewById(R.id.Menu_Create_track);
        Create_New_track_list_artist = (LinearLayout) findViewById(R.id.Menu_Create_track_Artist);
        Create_New_track_list_name = (LinearLayout) findViewById(R.id.Menu_Create_track_Name);
        Create_New_track_list_janr = (LinearLayout) findViewById(R.id.Menu_Create_track_Janr);
        Create_New_track_list_podjanr = (LinearLayout) findViewById(R.id.Menu_Create_track_PodJanr);
        Create_New_track_list_lang = (LinearLayout) findViewById(R.id.Menu_Create_track_Lang);
        Create_New_track_list_nastr = (LinearLayout) findViewById(R.id.Menu_Create_track_Nastr);
        Create_Playlist_menu = (LinearLayout) findViewById(R.id.Create_Playlist_menu);
        Create_Playlist_menu_teg_type = (LinearLayout) findViewById(R.id.Create_Playlist_menu_teg_type);
        Create_Playlist_menu_teg_value = (LinearLayout) findViewById(R.id.Create_Playlist_menu_teg_value);
        Create_Playlist_menu_classic_name = (LinearLayout) findViewById(R.id.Create_Playlist_menu_classic_name);
        Nastroyki = (LinearLayout) findViewById(R.id.Menu_nastroyki);
        AllTrackList = new ArrayList<>();
        ActiveTrackList = new ArrayList<>();
        PlayTrackList = new ArrayList<>();
        CollectionTrackList = new ArrayList<>();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        dbHelper = new DBHelper(this);
        redact_track = new Track(-1,".",".");
        track_to_del = new Track(-1,".",".");
        //Текстовые поля при добавлении песни
        artist_box = (TextView) findViewById(R.id.Artist_tb); //Метка АРТИСТ
        artist_box1 = (TextView) findViewById(R.id.Artist_tb2); //Метка АРТИСТ2
        artist_box2 = (TextView) findViewById(R.id.Artist_tb3); //Метка АРТИСТ3
        artist_box3 = (TextView) findViewById(R.id.Artist_tb4); //Метка АРТИСТ4
        artist_box4 = (TextView) findViewById(R.id.Artist_tb5); //Метка АРТИСТ5
        artist_box5 = (TextView) findViewById(R.id.Artist_tb6); //Метка АРТИСТ6
        artist_box.post(new Runnable() {
            @Override
            public void run() {
                InputMethodManager inputMethodManager =
                        (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.toggleSoftInputFromWindow(
                        artist_box.getApplicationWindowToken(),InputMethodManager.SHOW_IMPLICIT, 0);
                artist_box.requestFocus();
            }
        });
        name_box = (TextView) findViewById(R.id.Name_tb); //Метка НАЗВАНИЕ
        name_box.post(new Runnable() {
            @Override
            public void run() {
                InputMethodManager inputMethodManager =
                        (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.toggleSoftInputFromWindow(
                        name_box.getApplicationWindowToken(),InputMethodManager.SHOW_IMPLICIT, 0);
                name_box.requestFocus();
            }
        });
        loveBtn = (Button) findViewById(R.id.loveBtn);
        Sp_sort_value = (Spinner) findViewById(R.id.Sp_sort_value);
        Sp_sort_value.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // показываем позиция нажатого элемента
                try {
                    switch(position) {
                        case 0:
                            Collections.sort(ActiveTrackList, new Comparator<Track>() {
                                public int compare(Track a, Track b) {
                                    return a.getArtist().compareTo(b.getArtist()); }
                            });
                            break;
                        case 1:
                            Collections.sort(ActiveTrackList, new Comparator<Track>() {
                                public int compare(Track a, Track b) {
                                    return a.getTitle().compareTo(b.getTitle()); }
                            });
                            break;
                        case 2:
                            Collections.sort(ActiveTrackList, new Comparator<Track>() {
                                public int compare(Track a, Track b) {
                                    return b.getDate().compareTo(a.getDate()); }
                            });

                            break;

                    }
                    Reset_active_list();
                }
                catch (Exception e){

                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                try{

                }catch (Exception e){}
            }
        });
        Sp_sort_value.setAdapter(adapter(arraySpiner_sort_collection));
        Sp_sort_value.setPrompt("Сортировка");
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if(s.toString().equals("")){
                        // reset listview
                        initList(1);
                    } else {
                        // perform search
                        searchItem(s.toString(),1);
                    }
                }catch (Exception e){
                    Log.d(LOG_TAG, "Ошибка в ЭДИТЕ поиска: " + e.getMessage());
                }

            }
            @Override
            public void afterTextChanged(Editable s) {
            }

        });
        editText2.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if(s.toString().equals("")){
                        // reset listview
                        initList(2);
                    } else {
                        // perform search
                        searchItem(s.toString(),2);
                    }
                }catch (Exception e){
                    Log.d(LOG_TAG, "Ошибка в ЭДИТЕ поиска: " + e.getMessage());
                }

            }
            @Override
            public void afterTextChanged(Editable s) {
            }

        });
    }
    public void CloseActiveLayotClick(View view){
        Active_layout_track.setVisibility(LinearLayout.GONE);
        Player_list.setVisibility(LinearLayout.VISIBLE);
        ActiveTrackList = new ArrayList<Track>();
        ActiveTrackList = Clone;
        SongAdapterActiveList songActAdt = new SongAdapterActiveList(this, ActiveTrackList);
        ActiveTrackListView.setAdapter(songActAdt);
    }//закрытие активного плейлиста
    @Override
    protected void onStart() {
        super.onStart();
        if(playIntent==null){
            playIntent = new Intent(this, MusicService.class);
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            startService(playIntent);
        }
    }
    public void CreatePlaylist(View view){
        try {
            Playlist p;
            switch(playlist_type_vibor){
                case 1:
                    p = NamesPlaylist.get(Integer.parseInt(view.getTag().toString()));
                    ActiveTrackList = new ArrayList<Track>();
                    for (Track T : p.getSongs()) {
                        ActiveTrackList.add(T);
                    }
                    musicSrv.setList(ActiveTrackList);
                    SongAdapterActiveList songActAdt = new SongAdapterActiveList(this, ActiveTrackList);
                    ActiveTrackListView.setAdapter(songActAdt);
                    //Log.d(LOG_TAG, "Готово, выбор прошел успешно");
                    Playlists.setVisibility(LinearLayout.GONE);
                    Player_list.setVisibility(LinearLayout.VISIBLE);
                    //ActiveTrackList = p.getSongs();
                    break;
                case 2:
                    p = PlaylistsValue.get(Integer.parseInt(view.getTag().toString()));
                    Log.d(LOG_TAG,"Добавление в плейлист песни: "+ track_create_playlist.getTitle()+ ". в таблицу: "+ p.getZvan() + " | " + p.getName_table());
                    if(p.getName_table() == "track_list1"){
                        track_list1.add(track_create_playlist);
                        newBaseTrack(track_create_playlist, p.getZvan());
                    }
                    if(p.getName_table() == "track_list2")
                    {
                        track_list2.add(track_create_playlist);
                        newBaseTrack(track_create_playlist, p.getZvan());
                    }
                    if(p.getName_table() == "track_list3")
                    {
                        track_list3.add(track_create_playlist);
                        newBaseTrack(track_create_playlist, p.getZvan());
                    }
                    if(p.getName_table() == "track_list4")
                    {
                        track_list4.add(track_create_playlist);
                        newBaseTrack(track_create_playlist, p.getZvan());
                    }
                    if(p.getName_table() == "track_list5")
                    {
                        track_list5.add(track_create_playlist);
                        newBaseTrack(track_create_playlist, p.getZvan());
                    }
                    Playlists_create.setVisibility(LinearLayout.GONE);
                    Playlists.setVisibility(LinearLayout.GONE);
                    Player_list.setVisibility(LinearLayout.VISIBLE);
                    break;
            }

        }catch (Exception e){
            Log.d(LOG_TAG, e.getMessage());
        }
    } //Выбор плейлиста
    public void Next_create_song(View view) throws IOException{
        switch(view.getId()) {
            case R.id.Add_song_next1: // идентификатор "@+id/Add_song_next1"
                Create_New_track_list_artist.setVisibility(LinearLayout.GONE);
                Create_New_track_list_name.setVisibility(LinearLayout.VISIBLE);
                break;
            case R.id.Add_song_next2: // идентификатор "@+id/Add_song_next2"
                Create_New_track_list_name.setVisibility(LinearLayout.GONE);
                Create_New_track_list_janr.setVisibility(LinearLayout.VISIBLE);
                Types_Janr_value();
                try {
                    status_box = 5;
                    //Log.d(LOG_TAG, "Количество элементов в Janr_list = "+ Janr_list.size());
                    boxAdapter = new BoxAdapter(this, Janr_list);
                    Janr_sp.setAdapter(boxAdapter);
                }
                catch (Exception e){
                    Log.d(LOG_TAG, "Ошибка при формировании листа: "+e.getMessage());
                }
                break;

            case R.id.NextBtn:
                musicSrv.Nexting_(true);
                musicSrv.playNext();
                players();
                break;
            case R.id.playBtn:
                if(!musicSrv.isPng()){
                    //Stoping
                    if(pauseSong == 0) {
                        pauseSong = 1;
                        //Log.d(LOG_TAG, "Тут ошибка?");
                        musicSrv.playSong();
                        //Log.d(LOG_TAG, "Тут ошибка???");
                    } else {
                        if(pauseSong == 2) {
                            musicSrv.go();
                        }
                    }
                    playBtn.setBackgroundResource(R.drawable.stop);
                    players();

                }
                else {
                    //Starting
                    musicSrv.pauseSong();
                    pauseSong = 2;
                    playBtn.setBackgroundResource(R.drawable.play);
                }
                break;
            case R.id.previousBtn:
                musicSrv.playPrev();
                players();
                break;
            case R.id.Red_track: //Кнопка редактирования трека
                Into_Track_layout.setVisibility(LinearLayout.GONE);
                Create_New_track_list.setVisibility(LinearLayout.VISIBLE);
                Create_track_list.setVisibility(LinearLayout.GONE);
                Create_New_track_list_artist.setVisibility(LinearLayout.VISIBLE);
                artist_box.setText(redact_track.getArtist());
                //Log.d(LOG_TAG, redact_track.getArtist3());
                if(redact_track.getArtist2() != "."  && redact_track.getArtist2() != ""){
                    artist_box1.setVisibility(View.VISIBLE);
                    artist_box1.setText(redact_track.getArtist2());
                }
                else artist_box1.setVisibility(View.GONE);
                if(redact_track.getArtist3() != "." && redact_track.getArtist3() != ""){
                    artist_box2.setVisibility(View.VISIBLE);
                    artist_box2.setText(redact_track.getArtist3());
                } else artist_box2.setVisibility(View.GONE);
                if(redact_track.getArtist4() != "." && redact_track.getArtist4() != ""){
                    artist_box3.setVisibility(View.VISIBLE);
                    artist_box3.setText(redact_track.getArtist4());
                }else artist_box3.setVisibility(View.GONE);
                if(redact_track.getArtist5() != "." && redact_track.getArtist5() != ""){
                    artist_box4.setVisibility(View.VISIBLE);
                    artist_box4.setText(redact_track.getArtist5());
                }else artist_box4.setVisibility(View.GONE);
                if(redact_track.getArtist6() != "." && redact_track.getArtist6() != ""){
                    artist_box5.setVisibility(View.VISIBLE);
                    artist_box5.setText(redact_track.getArtist6());
                }else artist_box5.setVisibility(View.GONE);
                name_box.setText(redact_track.getTitle());
                Clone = new ArrayList<Track>();
                for(Track t:AllTrackList){
                    Clone.add(t);
                }
                redacter = 2;
                break;
            case R.id.Close_info: //Закрыть информацию о треке
                Into_Track_layout.setVisibility(View.GONE);
                Active_layout_track.setVisibility(View.VISIBLE);
                break;
            case R.id.loveBtn: //Кнопка Лайк
                if(love == false)
                {
                    newBaseTrack(ActiveTrackList.get(musicSrv.SongPosn()), "mytable_1");
                    loveBtn.setBackgroundResource(R.drawable.btn_like_active);
                    loveTrackCollection.add(ActiveTrackList.get(musicSrv.SongPosn()));
                    love = true;
                }else {
                    loveBtn.setBackgroundResource(R.drawable.btn_like);
                    delTrackBase(ActiveTrackList.get(musicSrv.SongPosn()), "mytable_1");
                    love = false;
                }

                break;
            case R.id.repeatBtn: //Кнопка повтора
                if(repeat_ == 0){
                    repeat_ = 1;
                    repeat.setBackgroundResource(R.drawable.repeat_one_btn);
                    musicSrv.loop(true);
                }else
                if(repeat_ == 1){
                    repeat_ = 2;
                    repeat.setBackgroundResource(R.drawable.random_btn);
                    musicSrv.loop(false);
                    musicSrv.setShuffle(true);
                } else
                if(repeat_ == 2){
                    repeat_ = 0;
                    repeat.setBackgroundResource(R.drawable.repeat_btn);
                    musicSrv.setShuffle(false);
                }
                break;
            case R.id.listBtn:   //Открытие активного плейлиста
                Player_list.setVisibility(LinearLayout.GONE);
                Active_layout_track.setVisibility(LinearLayout.VISIBLE);
                Clone = new ArrayList<Track>();
                for(Track t:ActiveTrackList){
                    Clone.add(t);
                }
                break;
            case R.id.Create_Playlist:

                Playlists.setVisibility(LinearLayout.GONE);
                Create_Playlist_menu.setVisibility(LinearLayout.VISIBLE);
                break;
            case R.id.Create_playlist_menu_close:
                Playlists.setVisibility(LinearLayout.VISIBLE);
                Create_Playlist_menu.setVisibility(LinearLayout.GONE);
                break;
            case R.id.Create_playlist_menu_teg_type_close:
                Playlists.setVisibility(LinearLayout.VISIBLE);
                Create_Playlist_menu_teg_type.setVisibility(LinearLayout.GONE);
                break;
            case R.id.Create_playlist_menu_teg_value_close:
                Playlists.setVisibility(LinearLayout.VISIBLE);
                Create_Playlist_menu_teg_value.setVisibility(LinearLayout.GONE);
                break;
            case R.id.Create_playlist_menu_classic_name_close:
                Playlists.setVisibility(LinearLayout.VISIBLE);
                Create_Playlist_menu_classic_name.setVisibility(LinearLayout.GONE);
                break;
            case R.id.Create_playlist_menu_teg:
                status_box = 1;
                Types_();
                boxAdapter = new BoxAdapter(this, arraySpiner_create_list);
                Sp_teg_type.setAdapter(boxAdapter);
                Create_Playlist_menu.setVisibility(LinearLayout.GONE);
                Create_Playlist_menu_teg_type.setVisibility(LinearLayout.VISIBLE);
                break;

            case R.id.Create_playlist_menu_clas:

                Create_Playlist_menu.setVisibility(LinearLayout.GONE);
                Create_Playlist_menu_classic_name.setVisibility(LinearLayout.VISIBLE);
                break;
            case R.id.Create_playlist_menu_teg_value_create:

                break;
            case R.id.addButton:
                try {
                    active_track_int = Integer.parseInt(view.getTag().toString());
                    //Log.d(LOG_TAG, "Прикинь, я нажал на "+ view.getTag().toString());
                    final CharSequence[] items = {"Добавить в плейлист", "Информация о треке", "Удалить"};

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Выбор действий");
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            if(item == 0){
                                track_create_playlist = ActiveTrackList.get(active_track_int);
                                playlist_type_vibor = 2;
                                Playlists_create.setVisibility(View.VISIBLE);
                                Active_layout_track.setVisibility(View.GONE);
                            }else if(item == 1){
                                InfoTrack(ActiveTrackList.get(active_track_int));
                                Into_Track_layout.setVisibility(View.VISIBLE);
                                Active_layout_track.setVisibility(View.GONE);
                            }
                            else if (item == 2){
                                track_to_del = ActiveTrackList.get(active_track_int);
                                DialogDelToTrack();
                            }

                            //Toast.makeText(getApplicationContext(), items[item], Toast.LENGTH_SHORT).show();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                    //openContextMenu(ActiveTrackListView);
                }catch (Exception e){
                    Log.d(LOG_TAG, "Ошибка в триточки "+e.getMessage());
                }
                break;
            case R.id.Create_playlist_menu_classic_name_create:
                playlist_type_vibor = 1;
                //Log.d(LOG_TAG, String.valueOf(track_list1.size()));
                for(Playlist T: NamesPlaylist){
                    if(T.getNames() == name_classic_playlist.getText().toString()) {
                        Create_Playlist_menu_classic_name.setVisibility(View.GONE);
                        Playlists.setVisibility(View.VISIBLE);
                        return;
                   }
                    if(T.getName_table() == "track_list1" & counter_playlist == 0 || T.getName_table() == "track_list2" & counter_playlist == 1 ||
                    T.getName_table() == "track_list3" & counter_playlist == 2 || T.getName_table() == "track_list4" & counter_playlist == 3 ||
                            T.getName_table() == "track_list5" & counter_playlist == 4 ){
                        Create_Playlist_menu_classic_name.setVisibility(View.GONE);
                        Playlists.setVisibility(View.VISIBLE);
                        return;
                    }
                }
                if(counter_playlist == 0){
                    track_list1 = new ArrayList<Track>();
                    Playlist t = new Playlist(name_classic_playlist.getText().toString(),track_list1);
                    t.setName_table("track_list1");
                    t.setZvan("mytable_4");
                    newBasePlaylist(t);
                    Log.d(LOG_TAG, "Добавлен в 1 трек лист");
                    NamesPlaylist.add(t);
                    counter_playlist ++;
                } else
                if(counter_playlist == 1){
                    track_list2 = new ArrayList<Track>();
                    Playlist t = new Playlist(name_classic_playlist.getText().toString(),track_list2);
                    t.setName_table("track_list2");
                    t.setZvan("mytable_5");
                    newBasePlaylist(t);
                    Log.d(LOG_TAG, "Добавлен в 2 трек лист");
                    NamesPlaylist.add(t);
                    counter_playlist ++;
                }else
                if(counter_playlist == 2){
                    track_list3 = new ArrayList<Track>();
                    Playlist t = new Playlist(name_classic_playlist.getText().toString(),track_list3);
                    t.setName_table("track_list3");
                    t.setZvan("mytable_6");
                    newBasePlaylist(t);
                    Log.d(LOG_TAG, "Добавлен в 3 трек лист");
                    NamesPlaylist.add(t);
                    counter_playlist ++;
                }else
                if(counter_playlist == 3){
                    track_list4 = new ArrayList<Track>();
                    Playlist t = new Playlist(name_classic_playlist.getText().toString(),track_list4);
                    t.setName_table("track_list4");
                    t.setZvan("mytable_8");
                    newBasePlaylist(t);
                    Log.d(LOG_TAG, "Добавлен в 4 трек лист");
                    NamesPlaylist.add(t);
                    counter_playlist ++;
                }else
                if(counter_playlist == 4){
                    track_list5 = new ArrayList<Track>();
                    Playlist t = new Playlist(name_classic_playlist.getText().toString(),track_list5);
                    t.setName_table("track_list5");
                    t.setZvan("mytable_9");
                    newBasePlaylist(t);
                    Log.d(LOG_TAG, "Добавлен в 5 трек лист");
                    NamesPlaylist.add(t);
                    counter_playlist ++;
                } else return;
                Create_Playlist_menu_classic_name.setVisibility(View.GONE);
                Playlists.setVisibility(View.VISIBLE);
                break;
            case R.id.MenuButtonPlaylist:
                try {
                    //active_track_int = Integer.parseInt(view.getTag().toString());
                    Log.d(LOG_TAG, "Прикинь, я нажал на "+ view.getTag().toString());
                    final CharSequence[] items = {"Переименовать", "Информация о плейлисте", "Удалить"};

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Выбор действий");
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            if (item == 0) {

                            } else if (item == 1) {

                            } else if (item == 2) {
                                if (NamesPlaylist.get(active_track_int).getNames() != "Все песни" & NamesPlaylist.get(active_track_int).getNames() != "Коллекция" &
                                        NamesPlaylist.get(active_track_int).getNames() != "Избранные")
                                    delPlaylistBase(NamesPlaylist.get(active_track_int));
                            }

                            //Toast.makeText(getApplicationContext(), items[item], Toast.LENGTH_SHORT).show();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                catch (Exception e){
                    Log.d(LOG_TAG, "Ошибка при создании меню"+ e.getMessage());
                }
                PlayListAdapter playAdatper = new  PlayListAdapter(this, NamesPlaylist);
                list_playlist.setAdapter(playAdatper);
                    break;
        }
    } //Нажатия кнопок
    private void DialogDelToTrack(){
        new AlertDialog.Builder(this)
                .setTitle("Удалить песню?")
                .setMessage("Вы действительно хотите удалить песню?")
                .setNegativeButton(Html.fromHtml("<font color='#FF7F27'>Нет</font>"), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        return;
                    }
                })
                .setPositiveButton(Html.fromHtml("<font color='#FF7F27'>Да</font>"), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        DialogDelToTrack2();

                    }
                }).create().show();
    } //Еще есть путь назад и можно не удалять песню
    private void DialogDelToTrack2(){
        new AlertDialog.Builder(this)
                .setTitle("Только из коллекции?")
                .setMessage("Вы хотите удалить песню только из коллекции?")
                .setNegativeButton(Html.fromHtml("<font color='#FF7F27'>Да</font>"), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        ActiveTrackList.remove(track_to_del);
                        delTrackBase(track_to_del, "mytable");
                        for(Track r: loveTrackCollection){
                            if(track_to_del == r){
                                delTrackBase(track_to_del, "mytable_1");
                            }
                        }

                        //Дальше удаление со всех имеющихся плейлистов
                    }
                })
                .setPositiveButton(Html.fromHtml("<font color='#FF7F27'>Совсем</font>"), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        NeGOTOVOE_();

                    }
                }).create().show();
        SongAdapterActiveList songActAdt = new SongAdapterActiveList(this, ActiveTrackList);
        ActiveTrackListView.setAdapter(songActAdt);
    } //Фича для удаления трека (либо с коллекции, либо совсем)
    public void NeGOTOVOE_(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Важное сообщение!")
                .setMessage("Тут пока ничего нет!")
                .setIcon(R.drawable.ic_android_cat)
                .setCancelable(false)
                .setNegativeButton("ОК, посмотрю после обновления",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    } //Сообщение о том, что это пока не готово
    public void songPicked(View view) throws IOException {
        musicSrv.setSong(Integer.parseInt(view.getTag().toString()));
        //Log.d(LOG_TAG,view.getTag().toString() );
        musicSrv.playSong();
        playBtn.setBackgroundResource(R.drawable.stop);
        players();

    } //Запуск песни
    public void CheckBox_value(){
        try {
            //Log.d(LOG_TAG, "status_box = "+status_box+" value_box: "+value_box);
            switch (status_box){
                case 1:
                    // //Создание плейлиста - выбор по чему создавать
                    Track_Teg = value_box;
                    status_box = 6;
                    boxAdapter = new BoxAdapter(this, Types_value(value_box));
                    Sp_teg_value.setAdapter(boxAdapter);
                    Create_Playlist_menu_teg_type.setVisibility(LinearLayout.GONE);
                    Create_Playlist_menu_teg_value.setVisibility(LinearLayout.VISIBLE);
                    //boxAdapter = new BoxAdapter(this, products);
                    break;
                case 2:
                    Nastr = arrayNastroenie_list.get(value_box).getNamed_(); //Вывод листа с настроением
                    NextCreateSong2();
                    break;
                case 3:
                    Lang_ = Language.get(value_box).getNamed_();
                    Create_New_track_list_lang.setVisibility(LinearLayout.GONE);
                    Create_New_track_list_nastr.setVisibility(LinearLayout.VISIBLE);
                    Types_Nastr();
                    status_box = 2;
                    boxAdapter = new BoxAdapter(this, arrayNastroenie_list);
                    nastr_box.setAdapter(boxAdapter);
                    break;
                case 4:
                    PodJanr = arraySpinner_podjanr.get(value_box).getNamed_();
                    Create_New_track_list_podjanr.setVisibility(LinearLayout.GONE);
                    Create_New_track_list_lang.setVisibility(LinearLayout.VISIBLE);
                    Types_Lang_value();
                    status_box = 3;
                    boxAdapter = new BoxAdapter(this, Language);
                    Lang_tb.setAdapter(boxAdapter);
                    break;
                case 5:
                    Janr = Janr_list.get(value_box).getNamed_();
                    //Log.d(LOG_TAG, Janr);
                    Podjanr_value(Janr);
                    Create_New_track_list_janr.setVisibility(LinearLayout.GONE);
                    Create_New_track_list_podjanr.setVisibility(LinearLayout.VISIBLE);
                    status_box = 4;
                    boxAdapter = new BoxAdapter(this, arraySpinner_podjanr);
                    podjanr_box.setAdapter(boxAdapter);
                    break;
                case 6:
                    try {
                        types_playlist_teg = arraySpinner.get(value_box).getNamed_();
                        if(Playlist_key == 3)
                            return;
                        ListCollectionTeg =  new ArrayList<Track>();
                        switch (Track_Teg) {
                            case 0:
                                for(Playlist t: NamesPlaylist){
                                    if(t.getZvan() == types_playlist_teg){
                                        NamesPlaylist.remove(t);
                                        break;
                                    }
                                }
                                for (Track t : CollectionTrackList) {
                                    if (t.getArtist().equals(types_playlist_teg)  || t.getArtist2().equals(types_playlist_teg)  || t.getArtist3().equals(types_playlist_teg)  || t.getArtist4().equals(types_playlist_teg)  || t.getArtist5().equals(types_playlist_teg)  || t.getArtist6().equals(types_playlist_teg)) {
                                        ListCollectionTeg.add(t);
                                    }
                                }
                                Playlist tt = new Playlist("По исполнителю: "+types_playlist_teg, ListCollectionTeg);
                                NamesPlaylist.add(tt);
                                Playlist_key++;
                                break;
                            case 1:
                                for (Track t : CollectionTrackList) {
                                    if (t.getJanr().equals(types_playlist_teg) ) {
                                        ListCollectionTeg.add(t);
                                    }
                                }
                                tt = new Playlist("По Жанру: "+types_playlist_teg, ListCollectionTeg);
                                NamesPlaylist.add(tt);Playlist_key++;
                                break;
                            case 2:
                                for (Track t : CollectionTrackList) {
                                    if (t.getPodJanr().equals(types_playlist_teg) ) {
                                        ListCollectionTeg.add(t);
                                    }
                                }
                                tt = new Playlist("По поджанру: "+types_playlist_teg, ListCollectionTeg);
                                tt.setZvan(types_playlist_teg);
                                NamesPlaylist.add(tt);Playlist_key++;
                                break;
                            case 3:
                                for (Track t : CollectionTrackList) {
                                    if (t.getMetka1().equals(types_playlist_teg) ) {
                                        ListCollectionTeg.add(t);
                                    }
                                }
                                tt = new Playlist("По языку вокала: "+types_playlist_teg, ListCollectionTeg);
                                NamesPlaylist.add(tt);Playlist_key++;
                                break;
                            case 4:
                                for (Track t : CollectionTrackList) {
                                    if (t.getMetka2().equals(types_playlist_teg) ) {
                                        ListCollectionTeg.add(t);
                                    }
                                }
                                tt = new Playlist("По вызываемому настроению: "+types_playlist_teg, ListCollectionTeg);
                                NamesPlaylist.add(tt);Playlist_key++;
                                break;
                        }

                    }catch (Exception e){
                        Log.d(LOG_TAG, e.getMessage());
                    }
                    playlist_type_vibor = 1;
                    Create_Playlist_menu_teg_value.setVisibility(LinearLayout.GONE);
                    Playlists.setVisibility(LinearLayout.VISIBLE);
                    PlayListAdapter playAdatper = new  PlayListAdapter(this, NamesPlaylist);
                    list_playlist.setAdapter(playAdatper);
                    break;
            }
        }
        catch (Exception e){
            Log.d(LOG_TAG, "Ошибка в CheckBox_value: "+e.getMessage());
        }

    }
    public void Nike_like(View view) {
        try{
            value_box = Integer.parseInt(view.getTag().toString());
            CheckBox_value();
        }
        catch (Exception e){
            Log.d(LOG_TAG, "Ошибка в Nike_like: "+e.getMessage());
        }
    }
    public void NextCreateSong(View view){
        NextCreateSong2();
    }
    public void NextCreateSong2(){
        if(redacter == 1){
            TMP = new Track(CollectionTrackList.size()+1, Songer.getTitle(), Songer.getArtist());
            if(TMP.getArtist() != artist_box.getText().toString())
                TMP.setArtist(artist_box.getText().toString());
            if(TMP.getTitle() != name_box.getText().toString())
                TMP.setTitle(name_box.getText().toString());
            if(artist_box1.getText().toString() != ""){
                TMP.setArtist2(artist_box1.getText().toString());
            }
            if(artist_box2.getText().toString() != ""){
                TMP.setArtist3(artist_box2.getText().toString());
            }
            if(artist_box3.getText().toString() != ""){
                TMP.setArtist4(artist_box3.getText().toString());
            }
            if(artist_box4.getText().toString() != ""){
                TMP.setArtist5(artist_box4.getText().toString());
            }
            if(artist_box5.getText().toString() != ""){
                TMP.setArtist6(artist_box5.getText().toString());
            }
            if(name_box.getText().toString() != ""){
                TMP.setTitle(name_box.getText().toString());
            }
            if(Janr != ""){
                TMP.setJanr(Janr);
            }
            if(PodJanr != ""){
                TMP.setPodJanr(PodJanr);
            }
            if(Lang_ != ""){
                TMP.setMetka1(Lang_);
            }
            if(Nastr!= ""){
                TMP.setMetka2(Nastr);
            }
            TMP.setMetka7(Songer.getMetka7());
            TMP.setMetka8(Songer.getMetka8());
            TMP.setURL(uri_create);
            TMP.setDate(Calendar.getInstance().getTime());
            CollectionTrackList.add(TMP);
            newBaseTrack(TMP, "mytable");
            Create_New_track_list_nastr.setVisibility(LinearLayout.GONE);
            Create_New_track_list.setVisibility(LinearLayout.GONE);
            Create_track_list.setVisibility(LinearLayout.GONE);
            Player_list.setVisibility(LinearLayout.VISIBLE);
            SongAdapterActiveList songActAdt = new SongAdapterActiveList(this, ActiveTrackList);
            ActiveTrackListView.setAdapter(songActAdt);
        }
        if(redacter == 2){

            if(artist_box.getText().toString() != ""){
                redact_track.setArtist(artist_box.getText().toString());
            }
            if(artist_box1.getText().toString() != ""){
                redact_track.setArtist2(artist_box1.getText().toString());
            }
            if(artist_box2.getText().toString() != ""){
                redact_track.setArtist3(artist_box2.getText().toString());
            }
            if(artist_box3.getText().toString() != ""){
                redact_track.setArtist4(artist_box3.getText().toString());
            }
            if(artist_box4.getText().toString() != ""){
                redact_track.setArtist5(artist_box4.getText().toString());
            }
            if(artist_box5.getText().toString() != ""){
                redact_track.setArtist6(artist_box5.getText().toString());
            }
            if(name_box.getText().toString() != ""){
                redact_track.setTitle(name_box.getText().toString());
            }
            if(Janr != ""){
                redact_track.setJanr(Janr);
            }
            if(PodJanr != ""){
                redact_track.setPodJanr(PodJanr);
            }
            if(Lang_ != ""){
                redact_track.setMetka1(Lang_);
            }
            if(Nastr!= ""){
                redact_track.setMetka2(Nastr);
            }
            redact_track.setDate(Calendar.getInstance().getTime());
            delTrackBase(redact_track, "mytable");
            newBaseTrack(redact_track, "mytable");
            Create_New_track_list_nastr.setVisibility(LinearLayout.GONE);
            Create_New_track_list.setVisibility(LinearLayout.GONE);
            Create_track_list.setVisibility(LinearLayout.GONE);
            Player_list.setVisibility(LinearLayout.VISIBLE);
            SongAdapterActiveList songActAdt = new SongAdapterActiveList(this, ActiveTrackList);
            ActiveTrackListView.setAdapter(songActAdt);
        }
    } //Добавление трека в коллекцию
    public void CreateTrack(View view){
        try{
            Create_New_track_list.setVisibility(LinearLayout.VISIBLE);
            Create_track_list.setVisibility(LinearLayout.VISIBLE);
            Create_New_track_list_artist.setVisibility(LinearLayout.VISIBLE);
            //Поиск песни
            Songer = AllTrackList.get(Integer.parseInt(view.getTag().toString()));
            //Поиск по ID
            long currSong = Songer.getID();
            //Установка пути
            Uri trackUri = ContentUris.withAppendedId(
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    currSong);

            //Toast.makeText(getApplicationContext(), newSong.getTitle(), Toast.LENGTH_SHORT).show();
            artist_box.setText(Songer.getArtist());
            name_box.setText(Songer.getTitle());
            Songer.setMetka7(Songer.getArtist());
            Songer.setMetka8(Songer.getTitle());
            uri_create = trackUri.toString();
            //Toast.makeText(getApplicationContext(), trackUri.toString(), Toast.LENGTH_SHORT).show();
            //Toast.makeText(getApplicationContext(), Adt.getName(), Toast.LENGTH_SHORT).show();
            //Toast.makeText(getApplicationContext(), mediaStorageDir.toString()+"/"+Adt.getName(), Toast.LENGTH_SHORT).show();
            return;
        }catch (Exception e){

            Log.d(LOG_TAG, "Ошибка при считывании полей");
        }
    } //Выбор песни для добавления в коллекцию
    public void Add_artist(View view){
        if(artist_box1.getVisibility()==View.GONE){
            artist_box1.setVisibility(View.VISIBLE);
        }else if(artist_box2.getVisibility()==View.GONE){
            artist_box2.setVisibility(View.VISIBLE);
        }else if(artist_box3.getVisibility()==View.GONE){
            artist_box3.setVisibility(View.VISIBLE);
        }else if(artist_box4.getVisibility()==View.GONE){
            artist_box4.setVisibility(View.VISIBLE);
        }else if(artist_box5.getVisibility()==View.GONE){
            artist_box5.setVisibility(View.VISIBLE);
        }

    } //Добавления артистов при добавлении песни в коллекцию
    public void getSongList() {
        String filePath = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String[] column = {android.provider.MediaStore.Audio.Media.DATA};
        ContentResolver contentResolver = getContentResolver();
        Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        //Toast.makeText(getApplicationContext(), uri.toString(), Toast.LENGTH_SHORT).show();
        //Log.d(LOG_TAG, uri.toString());
        Cursor musicCursor = contentResolver.query(uri, null, null, null, null);
        int columnIndex = musicCursor.getColumnIndex(column[0]);
            Date date = null;
            if(musicCursor!=null && musicCursor.moveToFirst()){
            int titleColumn =  musicCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int idColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media._ID);
            int artistColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int dateColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.DATA);
            do {
                filePath = musicCursor.getString(dateColumn);
                long thisId = musicCursor.getLong(idColumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                Track Tr = new Track(thisId, thisTitle, thisArtist);
                try {
                    date = sdf.parse(dater(filePath));
                    //Log.d(LOG_TAG,date.toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //Log.d(LOG_TAG, filePath);
                Tr.setURL(""+filePath);
                Tr.setDate(date);
                AllTrackList.add(Tr);
            } while (musicCursor.moveToNext());
        }

    } //Сбор всех песен с памяти телефона
    private String dater(String DateFile) {
        File file = new File(DateFile);
        if(file.exists()){
            Long lastModified = file.lastModified();
            Date date = new Date(lastModified);
            return DateFormat.format("dd/MM/yyyy",
                    date).toString();
        }
        return "";
    } //Получение даты последнего изменения файла
    private void SongList(){
        try {
            profit(CollectionTrackList,"mytable");
            profit(loveTrackCollection,"mytable_1");
            Log.d(LOG_TAG, "Размер Track_list1 = "+ track_list1.size());
            if(track_list1.size() > 0)
                profit(track_list1, "mytable_4");
            if(track_list2.size() > 0)
                profit(track_list2, "mytable_5");
            if(track_list3.size() > 0)
                profit(track_list3, "mytable_6");
            if(track_list4.size() > 0)
                profit(track_list4, "mytable_8");
            if(track_list5.size() > 0)
                profit(track_list5, "mytable_9");
        }catch (Exception e)
        {
            Log.d(LOG_TAG, "Ошибка в SongList: "+ e.getMessage());
        }
    } //Проверка на совпадения в коллекции и добавлении треков
    public void profit(ArrayList<Track> list, String table){
        Iterator<Track> iter = list.iterator();
        while (iter.hasNext()) {
            Track s = iter.next();
            Log.d(LOG_TAG, s.getTitle());
            for(Track tt :AllTrackList){
                if(s.getMetka7().equals(tt.getArtist()) == true || s.getArtist().equals(tt.getArtist()) == true) {
                    if (s.getMetka8().equals(tt.getTitle()) == true || s.getTitle().equals(tt.getTitle()) == true) {
                        Log.d(LOG_TAG, "ЗАРАБОТАЛО");
                        s.setID(tt.getID());

                        break;
                    }
                }
            }
            if(s.getID() == 0){
                Log.d(LOG_TAG, "При передаче номер песни в коллекции2: " + s.getID());
                delTrackBase(s, table);
                iter.remove();
            }

        }
    } //Проверка на совпадения в коллекции и добавлении треков
    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                //Здесь тупо кнопки залиты белым цветом
                .setTitle("Выйти из приложения?")
                .setMessage("Вы действительно хотите выйти?")
                .setNegativeButton(Html.fromHtml("<font color='#FF7F27'>Нет</font>"), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        return;
                    }
                })
                .setPositiveButton(Html.fromHtml("<font color='#FF7F27'>Да</font>"), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        MainActivity.super.onBackPressed();
                    }
                }).create().show();
    } //Выход из приложения
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        //Случайный порядок
        //case R.id.action_shuffle:
        //musicSrv.setShuffle();
        //break;

        return super.onOptionsItemSelected(item);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_track) {
            Player_list.setVisibility(LinearLayout.GONE);
            Create_track_list.setVisibility(LinearLayout.VISIBLE);
            Nastroyki.setVisibility(LinearLayout.GONE);
            Playlists.setVisibility(LinearLayout.GONE);
            Active_layout_track.setVisibility(View.GONE);
            Create_New_track_list_artist.setVisibility(View.GONE);
            Create_New_track_list_name.setVisibility(View.GONE);
            Create_New_track_list_janr.setVisibility(View.GONE);
            Create_New_track_list_podjanr.setVisibility(View.GONE);
            Create_New_track_list_lang.setVisibility(View.GONE);
            Create_New_track_list_nastr.setVisibility(View.GONE);
            Playlists_create.setVisibility(LinearLayout.GONE);
            Clone = new ArrayList<Track>();
            for(Track t:AllTrackList){
                Clone.add(t);
            }
            redacter = 1;
        } else if (id == R.id.nav_gallery) {
            Player_list.setVisibility(LinearLayout.VISIBLE);
            Create_track_list.setVisibility(LinearLayout.GONE);
            Playlists_create.setVisibility(LinearLayout.GONE);
            Nastroyki.setVisibility(LinearLayout.GONE);
            Playlists.setVisibility(LinearLayout.GONE);
            Active_layout_track.setVisibility(View.GONE);
            Create_New_track_list_artist.setVisibility(View.GONE);
            Create_New_track_list_name.setVisibility(View.GONE);
            Create_New_track_list_janr.setVisibility(View.GONE);
            Create_New_track_list_podjanr.setVisibility(View.GONE);
            Create_New_track_list_lang.setVisibility(View.GONE);
            Create_New_track_list_nastr.setVisibility(View.GONE);
            AllTrackList = new ArrayList<Track>();
            AllTrackList = Clone;
            SongAdapter songAllAdt = new SongAdapter(this, AllTrackList);
            AllTrackListView.setAdapter(songAllAdt);
            //controller.setVisibility(controller.GONE);
            //musicSrv.stop();
        //} else if (id == R.id.nav_collection) {
        //    ActiveTrackList.clear();
        //    ActiveTrackList = CollectionTrackList;

        } else if (id == R.id.nav_slideshow) {
            NeGOTOVOE_();

        } else if (id == R.id.nav_playlist) {
            playlist_type_vibor = 1;
            Player_list.setVisibility(LinearLayout.GONE);
            Playlists_create.setVisibility(LinearLayout.GONE);
            Create_track_list.setVisibility(LinearLayout.GONE);
            Nastroyki.setVisibility(LinearLayout.GONE);
            Playlists.setVisibility(LinearLayout.VISIBLE);
            Active_layout_track.setVisibility(View.GONE);
            Create_New_track_list_artist.setVisibility(View.GONE);
            Create_New_track_list_name.setVisibility(View.GONE);
            Create_New_track_list_janr.setVisibility(View.GONE);
            Create_New_track_list_podjanr.setVisibility(View.GONE);
            Create_New_track_list_lang.setVisibility(View.GONE);
            Create_New_track_list_nastr.setVisibility(View.GONE);
            AllTrackList = new ArrayList<Track>();
            AllTrackList = Clone;
            SongAdapter songAllAdt = new SongAdapter(this, AllTrackList);
            AllTrackListView.setAdapter(songAllAdt);
        } else if (id == R.id.nav_manage) {
            Nastroyki.setVisibility(LinearLayout.VISIBLE);
            Player_list.setVisibility(LinearLayout.GONE);
            Create_track_list.setVisibility(LinearLayout.GONE);
            Active_layout_track.setVisibility(View.GONE);
            Playlists.setVisibility(LinearLayout.GONE);
            Playlists_create.setVisibility(LinearLayout.GONE);
            Create_New_track_list_artist.setVisibility(View.GONE);
            Create_New_track_list_name.setVisibility(View.GONE);
            Create_New_track_list_janr.setVisibility(View.GONE);
            Create_New_track_list_podjanr.setVisibility(View.GONE);
            Create_New_track_list_lang.setVisibility(View.GONE);
            Create_New_track_list_nastr.setVisibility(View.GONE);
            AllTrackList = new ArrayList<Track>();
            AllTrackList = Clone;
            SongAdapter songAllAdt = new SongAdapter(this, AllTrackList);
            AllTrackListView.setAdapter(songAllAdt);
        } else if (id == R.id.nav_share) {
            NeGOTOVOE_();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    } //Выбор навигации
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            //if(totalTime == 0){
                totalTime = musicSrv.getDur();
            //}
            int currentPosition = msg.what;
            //Update Position Bar
            positionBar.setProgress(currentPosition);
            String elapsedTime = createTimeLabel(currentPosition);
            elapsedTimeLabel.setText(elapsedTime);
            String renautingTime = createTimeLabel(totalTime);
            remainingTimeLabel.setText(renautingTime);
            int totalTime1 = totalTime / 2;
            //Log.d(LOG_TAG, "Полтрека = "+createTimeLabel(totalTime1));
            if(currentPosition >= totalTime1){
                //Log.d(LOG_TAG, "currentPosition = "+ elapsedTimeLabel.getText() + ", totalTime = "+ createTimeLabel(totalTime - 60));
                musicSrv.Nexting_(true);

            }

        }
    };
    @Override
    public void onResume(){
        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(myReceiver, filter);
        super.onResume();
    }
    public String createTimeLabel(int time){
        String TimeLabel = "";
        int min = time / 1000 / 60;
        int sec = time / 1000 % 60;
        if(min==0 && sec==0) {
            titleBox.setText(musicSrv.SetTitle());
            for(Track tt : loveTrackCollection){
                    if(titleBox.getText() == tt.getTitle() ){
                        loveBtn.setBackgroundResource(R.drawable.btn_like_active);
                    }
            }

        }
        TimeLabel = min + ":";
        if(sec<10) TimeLabel += "0";
        TimeLabel += sec;

        return TimeLabel;
    }
    public  void players(){
        //totalTime = musicSrv.setTime();
        totalTime = musicSrv.getDur();
        //Toast toast = Toast.makeText(getApplicationContext(),
        //        musicSrv.SetTitle() , Toast.LENGTH_SHORT);
        //toast.show();
        //titleBox.setText(musicSrv.SetTitle());
        for(Track i: loveTrackCollection){
            if(i.getTitle() == ActiveTrackList.get(musicSrv.SongPosn()).getTitle()){
                loveBtn.setBackgroundResource(R.drawable.btn_like_active);
                love = true;
            }
            else {
                loveBtn.setBackgroundResource(R.drawable.btn_like);
                love = false;
            }
        }
        positionBar.setMax(musicSrv.setTime());
        positionBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            musicSrv.seek(progress);
                            positionBar.setProgress(progress);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (musicSrv != null) {
                    try {
                        Message msg = new Message();
                        msg.what = musicSrv.getPosn();
                        handler.sendMessage(msg);

                        //handler.postDelayed(new Runnable() {
                        //    @Override
                        //    public void run() {
                        //        playBtn.setBackgroundResource(R.drawable.stop);
                        //    }
                        //}, 2000);
                    } catch (IndexOutOfBoundsException e) {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Ошибочка в Thread", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }
            }
        }).start();
    }
    public void newChecked(View view){
        int id = view.getId();
        if(id == R.id.checkBoxClassic){
            Meloman.setChecked(false);
        }
        if(id == R.id.checkBoxMeloman){
            Classic.setChecked(false);
        }
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {

// TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.Player:
                super.onCreateContextMenu(menu, v, menuInfo);
                menu.add(0, 1, 0, "Коллекция");
                menu.add(0, 2, 0, "Все песни");
                menu.add(0, 3, 0, "Удалить песню");
                menu.add(0, 4, 0, "Редактировать теги");
                menu.add(0, 5, 0, "Сменить стиль");
                menu.add(0, 6, 0, "Пройти тест");
                break;
            //case R.id.tvSize:
            //    menu.add(0, MENU_SIZE_22, 0, "22");
            //    menu.add(0, MENU_SIZE_26, 0, "26");
            //    menu.add(0, MENU_SIZE_30, 0, "30");
            //    break;
        }
    } //Контекстное меню пока не нашло свою реализацию
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.edit_track_menu:

                return true;
            case R.id.info_track_menu:

                return true;
            case R.id.create_track_menu:

                return true;
            default:
                return super.onContextItemSelected(item);
        }

}
    public void Reset_active_list(){
        SongAdapterActiveList songActAdt = new SongAdapterActiveList(this, ActiveTrackList);
        ActiveTrackListView.setAdapter(songActAdt);
    }
    public void searchItem(String textToSearch, int i){
        try {
            String textToSearch1 = textToSearch.toLowerCase();
            if(i == 1){

                //Log.d(LOG_TAG, "1");
                Iterator<Track> iter = ActiveTrackList.iterator();
                while (iter.hasNext()) {
                    Track s = iter.next();
                    if(!s.getArtist().toLowerCase().contains(textToSearch1) & !s.getTitle().toLowerCase().contains(textToSearch1)){
                   // if(!s.getArtist().toLowerCase().contains(textToSearch1)){
                        //Log.d(LOG_TAG, "2");
                        iter.remove();
                        //Log.d(LOG_TAG, "3");
                    }
                }
                //ActiveTrackList = new ArrayList<Track>();
                //while (iter.hasNext()) {
                //    ActiveTrackList.add(iter.next());
                //}
                //Log.d(LOG_TAG, "Размер клона: "+Clone.size());
                SongAdapterActiveList songActAdt = new SongAdapterActiveList(this, ActiveTrackList);
                //Log.d(LOG_TAG, "5");
                ActiveTrackListView.setAdapter(songActAdt);
                //adapter.notifyDataSetChanged();
            }
            if( i == 2){
                Iterator<Track> iter = AllTrackList.iterator();
                while (iter.hasNext()) {
                    Track s = iter.next();
                    if(!s.getArtist().toLowerCase().contains(textToSearch1) & !s.getTitle().toLowerCase().contains(textToSearch1)){
                        // if(!s.getArtist().toLowerCase().contains(textToSearch1)){
                        //Log.d(LOG_TAG, "2");
                        iter.remove();
                        //Log.d(LOG_TAG, "3");
                    }
                }
                SongAdapter songAllAdt = new SongAdapter(this, AllTrackList);
                AllTrackListView.setAdapter(songAllAdt);
                //Log.d(LOG_TAG, "Размер клона: "+Clone.size());
                //adapter.notifyDataSetChanged();
            }
        }catch (Exception e){
            Log.d(LOG_TAG, "Ошибка в поиске:" + e.getMessage());
        }
    } //Поиск в ListView
    public void initList(int i){
        try {
            if(i == 1){
                ActiveTrackList = new ArrayList<Track>();
                for (Track t:Clone){
                    ActiveTrackList.add(t);
                }
                //Log.d(LOG_TAG, "Размер клона: "+Clone.size());
                SongAdapterActiveList songActAdt = new SongAdapterActiveList(this, ActiveTrackList);
                ActiveTrackListView.setAdapter(songActAdt);
            }
            if(i == 2 ){
                AllTrackList = new ArrayList<Track>();
                for (Track t:Clone){
                    AllTrackList.add(t);
                }
                //Log.d(LOG_TAG, "Размер клона: "+Clone.size());
                SongAdapter songAllAdt = new SongAdapter(this, AllTrackList);
                AllTrackListView.setAdapter(songAllAdt);
            }
        }
       catch (Exception e){
            Log.d(LOG_TAG, "Ошибка в возврате коллекции: " + e.getMessage());
       }
    } //Инициализация после поиска в listView
    private ArrayAdapter<String> adapter(String[] list){
        ArrayAdapter<String> _adapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, list);
        _adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        return _adapter;
    }

    private void InfoTrack(Track tab){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String string_date = sdf.format(tab.getDate());
            redact_track = new Track(-1,".",".");
            redact_track = tab;
            LabelInfoArtist1 = (TextView) findViewById(R.id.LabelInfoArtist1);
            LabelInfoArtist2 = (TextView) findViewById(R.id.LabelInfoArtist2);
            LabelInfoArtist3 = (TextView) findViewById(R.id.LabelInfoArtist3);
            LabelInfoArtist4 = (TextView) findViewById(R.id.LabelInfoArtist4);
            LabelInfoTitle = (TextView) findViewById(R.id.LabelInfoTitle);
            LabelInfoJanr = (TextView) findViewById(R.id.LabelInfoJanr);
            LabelInfoPodJanr = (TextView) findViewById(R.id.LabelInfoPodJanr);
            LabelInfoLang = (TextView) findViewById(R.id.LabelInfoLang);
            LabelInfoNastr = (TextView) findViewById(R.id.LabelInfoNastr);
            LabelInfoDate = (TextView) findViewById(R.id.LabelInfoDate);
            if (tab.getArtist() != "") {
                LabelInfoArtist1.setText(tab.getArtist());
            } else {
                LabelInfoArtist1.setText("нет данных");
            }
            if (tab.getArtist2() != "") {
                LabelInfoArtist2.setText(tab.getArtist2());
            } else {
                LabelInfoArtist2.setText("нет данных");
            }
            if (tab.getArtist3() != "") {
                LabelInfoArtist3.setText(tab.getArtist3());
            } else {
                LabelInfoArtist3.setText("нет данных");
            }
            if (tab.getArtist4() != "") {
                LabelInfoArtist4.setText(tab.getArtist4());
            } else {
                LabelInfoArtist4.setText("нет данных");
            }
            if (tab.getTitle() != "") {
                LabelInfoTitle.setText(tab.getTitle());
            } else {
                LabelInfoTitle.setText("нет данных");
            }
            if (tab.getJanr() != "") {
                LabelInfoJanr.setText(tab.getJanr());
            } else {
                LabelInfoJanr.setText("нет данных");
            }
            if (tab.getPodJanr() != "") {
                LabelInfoPodJanr.setText(tab.getPodJanr());
            } else {
                LabelInfoPodJanr.setText("нет данных");
            }
            if (tab.getMetka1() != "") {
                LabelInfoLang.setText(tab.getMetka1());
            } else {
                LabelInfoLang.setText("нет данных");
            }
            if (tab.getMetka2() != "") {
                LabelInfoNastr.setText(tab.getMetka2());
            } else {
                LabelInfoNastr.setText("нет данных");
            }
            if (tab.getDate().toString() != "") {
                LabelInfoDate.setText(string_date);
            } else {
                LabelInfoDate.setText("нет данных");
            }
        }catch (Exception e){
            Log.d(LOG_TAG,"Ошибка в информации о треке: "+e.getMessage());
        }
    } //Информация о песне


    //Работа с базами данных
    public long newBaseTrack(Track tr, String table){
        Log.d(LOG_TAG, ""+ tr.getJanr()+ ", "+ tr.getMetka1()+ ", "+ tr.getPodJanr() + ", "+ tr.getMetka2());
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            cv.put("artist", tr.getArtist());
            cv.put("artist2", tr.getArtist2());
            cv.put("artist3", tr.getArtist3());
            cv.put("artist4", tr.getArtist4());
            cv.put("artist5", tr.getArtist5());
            cv.put("artist6", tr.getArtist6());
            cv.put("janr", tr.getJanr());
            cv.put("metka1", tr.getMetka1());
            cv.put("metka2", tr.getMetka2());
            cv.put("metka3", tr.getMetka3());
            cv.put("metka4", tr.getMetka4());
            cv.put("metka5", tr.getMetka5());
            cv.put("metka6", tr.getMetka6());
            cv.put("metka7", tr.getMetka7());
            cv.put("metka8", tr.getMetka8());
            cv.put("podjanr", tr.getPodJanr());
            cv.put("title", tr.getTitle());
            cv.put("date", new Date(tr.getDate().toString()).getTime());
            Log.d(LOG_TAG, "Data при записи = " + new Date(tr.getDate().toString()));
            cv.put("url", tr.getURL());
        }
        catch (Exception e){
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String string_date = sdf.format(Calendar.getInstance().getTime());
            Date date = null;
            try {
                date = sdf.parse(string_date);
            } catch (ParseException ee) {
                ee.printStackTrace();
            }
            cv.put("date",date.getTime());
            cv.put("url", tr.getURL());
        }
        // вставляем запись и получаем ее ID
        long rowID = db.insert(table, null, cv);
        Log.d(LOG_TAG, "row inserted, ID = " + rowID);
        dbHelper.close();
        return rowID;
    }
    public ArrayList<Track> read(String table){
        ArrayList<Track> Tmp = new ArrayList<Track>();
        Track tr = new Track(-1,"","");
        try {
            //Log.d(LOG_TAG, "Начало считывания: ");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            //ContentValues cv = new ContentValues();
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            Cursor c = db.query(table, null, null, null, null, null, null);
            if (c.moveToFirst()) {

                // определяем номера столбцов по имени в выборке
                int idColIndex = c.getColumnIndex("id");
                int artistColIndex = c.getColumnIndex("artist");
                int artist2ColIndex = c.getColumnIndex("artist2");
                int artist3ColIndex = c.getColumnIndex("artist3");
                int artist4ColIndex = c.getColumnIndex("artist4");
                int artist5ColIndex = c.getColumnIndex("artist5");
                int artist6ColIndex = c.getColumnIndex("artist6");
                int janrColIndex = c.getColumnIndex("janr");
                int metka1ColIndex = c.getColumnIndex("metka1");
                int metka2ColIndex = c.getColumnIndex("metka2");
                int metka3ColIndex = c.getColumnIndex("metka3");
                int metka4ColIndex = c.getColumnIndex("metka4");
                int metka5ColIndex = c.getColumnIndex("metka5");
                int metka6ColIndex = c.getColumnIndex("metka6");
                int metka7ColIndex = c.getColumnIndex("metka7");
                int metka8ColIndex = c.getColumnIndex("metka8");
                int podjanrColIndex = c.getColumnIndex("podjanr");
                int titleColIndex = c.getColumnIndex("title");
                int dateColIndex = c.getColumnIndex("date");
                int urlColIndex = c.getColumnIndex("url");
                do {
                    tr = new Track(0,  c.getString(titleColIndex) , c.getString(artistColIndex));
                    tr.setCounter(c.getLong(idColIndex));
                    tr.setArtist2(c.getString(artist2ColIndex));
                    tr.setArtist3(c.getString(artist3ColIndex));
                    tr.setArtist4(c.getString(artist4ColIndex));
                    tr.setArtist5(c.getString(artist5ColIndex));
                    tr.setArtist6(c.getString(artist6ColIndex));
                    tr.setJanr(c.getString(janrColIndex));
                    tr.setMetka1(c.getString(metka1ColIndex));
                    tr.setMetka2(c.getString(metka2ColIndex));
                    tr.setMetka3(c.getString(metka3ColIndex));
                    tr.setMetka4(c.getString(metka4ColIndex));
                    tr.setMetka5(c.getString(metka5ColIndex));
                    tr.setMetka6(c.getString(metka6ColIndex));
                    tr.setMetka7(c.getString(metka7ColIndex));
                    tr.setMetka8(c.getString(metka8ColIndex));
                    tr.setTitle(c.getString(titleColIndex));
                    tr.setPodJanr(c.getString(podjanrColIndex));
                    try {
                        java.sql.Date dt = new java.sql.Date(c.getLong(dateColIndex));
                        tr.setDate(dt);
                        //tr.setDate(sdf.parse(c.getString(dateColIndex)));
                        //Log.d(LOG_TAG, ""+tr.getJanr() + tr.getPodJanr());
                        tr.setURL(c.getString(urlColIndex));
                    } catch (Exception e) {
                        Log.d(LOG_TAG, "Ошибка при чтении даты");
                    }
                    //Toast.makeText(getApplicationContext(), tr.getMetka7()+" "+tr.getMetka8(), Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getApplicationContext(), tr.getArtist()+" "+ tr.getTitle() + " " + tr.getURL(), Toast.LENGTH_SHORT).show();
                    // получаем значения по номерам столбцов и пишем все в лог
                    // переход на следующую строку
                    // а если следующей нет (текущая - последняя), то false - выходим из цикла
                    //Вот тут надо добавить строку с добавлением песни в коллекцию!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    if(tr.getID() != -1)
                        Tmp.add(tr);

                } while (c.moveToNext());
            } else
                //Log.d(LOG_TAG, "0 rows");
            dbHelper.close();

        }catch (Exception e){
            Log.d(LOG_TAG, "Ошибка при чтении базы "+table+ " :"+ e.getMessage());
        }
        return Tmp;
    }
    public void delTrackBase(Track tr, String table){
        if (tr.getCounter()==0) {
            return;
        }
        ContentValues cv = new ContentValues();
        // подключаемся к БД
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int delCount = db.delete(table, "id = " + String.valueOf(tr.getCounter()), null);
        dbHelper.close();
    }
    public void updBase(Track tr, String table){
        if (tr.getCounter()==0) {
            Log.d(LOG_TAG, "В updBase нулевое значение");
            return;
        }
        ContentValues cv = new ContentValues();
        // подключаемся к БД
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // подготовим значения для обновления
        try {
            cv.put("artist", tr.getArtist());
            cv.put("artist2", tr.getArtist2());
            cv.put("artist3", tr.getArtist3());
            cv.put("artist4", tr.getArtist4());
            cv.put("artist5", tr.getArtist5());
            cv.put("artist6", tr.getArtist6());
            cv.put("janr", tr.getJanr());
            cv.put("metka1", tr.getMetka1());
            cv.put("metka2", tr.getMetka2());
            cv.put("metka3", tr.getMetka3());
            cv.put("metka4", tr.getMetka4());
            cv.put("metka5", tr.getMetka5());
            cv.put("metka6", tr.getMetka6());
            cv.put("metka7", tr.getMetka7());
            cv.put("metka8", tr.getMetka8());
            cv.put("podjanr", tr.getPodJanr());
            cv.put("title", tr.getTitle());
            Log.d(LOG_TAG, tr.getDate().toString());
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String string_date = sdf.format(Calendar.getInstance().getTime());
            cv.put("date", string_date);
            cv.put("url", tr.getURL());
        }catch (Exception e){
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String string_date = sdf.format(Calendar.getInstance().getTime());
            Date date = null;
            try {
                date = sdf.parse(string_date);
            } catch (ParseException ee) {
                ee.printStackTrace();
            }
            cv.put("date",date.getTime());
            cv.put("url", tr.getURL());
        }
        // обновляем по id
        int updCount = db.update(table, cv, "id = ?",
                new String[] { String.valueOf(tr.getCounter()) });
        dbHelper.close();
    }
    public long newBasePlaylist(Playlist tr){
        Log.d(LOG_TAG, ""+ tr.getNames()+ ", "+ tr.getZvan());
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            cv.put("name", tr.getNames());
            cv.put("table_name", tr.getZvan());
        }
        catch (Exception e){
              Log.d(LOG_TAG, "Ошибка в записи плейлиста в базу: "+e.getMessage() );
        }
        // вставляем запись и получаем ее ID
        long rowID = db.insert("table_playlist", null, cv);
        Log.d(LOG_TAG, "row inserted, ID = " + rowID);
        dbHelper.close();
        return rowID;
    }
    public ArrayList<Playlist> readPlaylist(){
        ArrayList<Playlist> Tmp = new ArrayList<Playlist>();
        Playlist tr;
        try {
            //ArrayList<Track> track_list = new ArrayList<Track>();
            //Log.d(LOG_TAG, "Начало считывания: ");
            //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            //ContentValues cv = new ContentValues();
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            Cursor c = db.query("table_playlist", null, null, null, null, null, null);
            if (c.moveToFirst()) {

                // определяем номера столбцов по имени в выборке
                //int idColIndex = c.getColumnIndex("id");
                int artistColIndex = c.getColumnIndex("name");
                int artist2ColIndex = c.getColumnIndex("table_name");
                int artist3ColIndex = c.getColumnIndex("id");
                do {

                    if(counter_playlist == 0){
                        //Log.d(LOG_TAG, "Начало считывания0: "+ c.getString(artist2ColIndex));
                        track_list1 = new ArrayList<Track>();
                        track_list1 = read(c.getString(artist2ColIndex));
                        Log.d(LOG_TAG, "Размер при считывании track-list1 = "+ track_list1.size());
                        tr = new Playlist(  c.getString(artistColIndex), track_list1);
                        tr.setID(c.getInt(artist3ColIndex));
                        tr.setName_table("track_list1");
                        tr.setZvan(c.getString(artist2ColIndex));
                        counter_playlist ++;
                    }
                    else if(counter_playlist == 1){
                        //Log.d(LOG_TAG, "Начало считывания1: ");
                        track_list2 = new ArrayList<Track>();
                        track_list2 = read(c.getString(artist2ColIndex));
                        tr = new Playlist(  c.getString(artistColIndex), track_list2);
                        tr.setID(c.getInt(artist3ColIndex));
                        tr.setName_table("track_list2");
                        tr.setZvan(c.getString(artist2ColIndex));
                        counter_playlist ++;
                    }else if(counter_playlist == 2){
                        //Log.d(LOG_TAG, "Начало считывания2: ");
                        track_list3 = new ArrayList<Track>();
                        track_list3 = read(c.getString(artist2ColIndex));
                        tr = new Playlist(  c.getString(artistColIndex), track_list3);
                        tr.setID(c.getInt(artist3ColIndex));
                        tr.setName_table("track_list3");
                        tr.setZvan(c.getString(artist2ColIndex));
                        counter_playlist ++;
                    }else if(counter_playlist == 3){
                        //Log.d(LOG_TAG, "Начало считывания3: ");
                        track_list4 = new ArrayList<Track>();
                        track_list4 = read(c.getString(artist2ColIndex));
                        tr = new Playlist(  c.getString(artistColIndex), track_list4);
                        tr.setID(c.getInt(artist3ColIndex));
                        tr.setName_table("track_list4");
                        tr.setZvan(c.getString(artist2ColIndex));
                        counter_playlist ++;
                    }else if(counter_playlist == 4){
                        //Log.d(LOG_TAG, "Начало считывания4: ");
                        track_list5 = new ArrayList<Track>();
                        track_list5 = read(c.getString(artist2ColIndex));
                        tr = new Playlist(  c.getString(artistColIndex), track_list5);
                        tr.setID(c.getInt(artist3ColIndex));
                        tr.setName_table("track_list5");
                        tr.setZvan(c.getString(artist2ColIndex));
                        counter_playlist ++;
                    } else {
                        dbHelper.close();
                        break;
                    }
                    //tr.setName_table(c.getString(artist2ColIndex));
                    //if(tr.getID() != -1)
                    //Log.d(LOG_TAG, "tr.id = "+tr.getID());
                    Tmp.add(tr);
                    //else Log.d(LOG_TAG, String.valueOf(tr.getID()));

                } while (c.moveToNext());
                //if(counter_playlist == 0) {
                //    track_list1 = new ArrayList<Track>();
                //    track_list1 = track_list;
                //}
            } else
                //Log.d(LOG_TAG, "0 rows");
                dbHelper.close();

        }catch (Exception e){
            Log.d(LOG_TAG, "Ошибка при чтении базы c плейлистами"+ e.getMessage());
        }
        return Tmp;
    }
    public void delPlaylistBase(Playlist tr){
        if (tr.getID()==0) {
            return;
        }
        ContentValues cv = new ContentValues();
        // подключаемся к БД
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int delCount = db.delete("table_playlist", "id = " + String.valueOf(tr.getID()), null);
        dbHelper.close();
    }


    //Загрузка данных для формирования плейлистов
    private ArrayList<CheckBoxClassInit> Podjanr_value(String Janr){
        arraySpinner_podjanr = new ArrayList<CheckBoxClassInit>();
        if(Janr == "Народная музыка"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Центральная Азия", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Восточная и Южная Азия", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Кавказ и Закавказье", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Ближний Восток", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Восточная Европа", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Западная Европа", false));
        } else if(Janr == "Духовная музыка"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Каббалистическая музыка", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Апостольская музыка", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Католическая музыка", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Православная музыка", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Афроамериканская духовная музыка", false));
        } else if(Janr == "Классическая музыка"){
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Музыка карнатака", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Арабская классическая музыка", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Европейская классическая музыка", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Средневековье", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Возрождение", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Барокко", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Классицизм", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Романтизм", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Салонная музыка", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Модернизм", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Постмодернизм", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Неоклассицизм", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Электронная европейская классическая музыка", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Оркестровая музыка", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Вокальная, хоровая", false));
        } else if(Janr == "Фолк-музыка"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Этническая музыка", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Прогрессив-фолк", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Фолк-барок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit( "Филк", false));
        } else if(Janr == "Кантри"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Блюграсс", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Кантри-поп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Альт-кантри", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Хонки-тонк", false));
        } else if(Janr == "Латиноамериканская музыка"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Бачата", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Зук", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Кумбия", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Ламбада", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Мамбо", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Меренге", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Пачанга", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Румба", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Сальса", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Самба", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Сон", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Танго", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Форро", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Ча-ча-ча", false));
        } else if(Janr == "Блюз"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Сельский блюз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Харп-блюз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Техасский блюз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Электро-блюз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Вест-сайд-блюз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Вест-кост-блюз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Дельта-блюз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Чикагский блюз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Свомп-блюз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Зайдеко", false));
        } else if(Janr == "Ритм-н-блюз"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Ду-воп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Соул", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Фанк", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Нью-джек-свинг", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Современный ритм-н-блюз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Неосоул", false));
        } else if(Janr == "Джаз"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Новоорлеанский или традиционный джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Хот-джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Диксиленд", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Свинг", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Бибоп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Биг-бэнд", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Мейнстрим-джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Буги-вуги", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Северо-Восточный джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Страйд", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Джаз Канзас-сити", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Джаз Западного побережья", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Кул-джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Босса-нова", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Ворлд-джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Прогрессив-джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Хард-боп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Модальный джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Соул-джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Грув-джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Фри-джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Авангардный джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Криэйтив", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Пост-боп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Джаз-фьюжн", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Джаз-фанк", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Эйсид-джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Смут-джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Ню-джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Кроссовер-джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Афро-кубинский джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Азербайджанский джаз", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Джаз-мануш", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Латиноамериканский джаз", false));
        } else if(Janr == "Электронная музыка"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Академическая электронная музык", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Индастриал", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Нью-эйдж", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Эмбиент", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Синти-поп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Синт-фанк", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Этереал", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Электро", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Хаус", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Техно", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Техно-транс", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Spacesynth", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Чиптюн", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Дарквэйв", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Вэйпорвэйв", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("New Beat", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Брейкбит", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Драм-н-бейс", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Даунтемпо", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Инди-электроника", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("IDM", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Хардкор", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Транс", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Евродэнс", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Гэридж", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Глитч", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Lento Violento", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Фриформ", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Lowercase", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Этно-электроника", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Чиллвейв", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Альтернативная танцевальная музыка", false));
        } else if(Janr == "Рок"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Альтернативный метал", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Альтернативный рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Анатолийский рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Арт-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Бард-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Барокко-поп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Бит", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Блюз-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Брит-поп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Виолончельный метал", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Гаражный рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Глэм-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Гранж", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Дезерт-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Джаз-фьюжн", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Дэнс-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Инструментальный рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Индастриал-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Инди-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Камеди-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Кантри-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Келтик-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Краут-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Латин-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Математический рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Метал", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Нойз-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Паб-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Пауэр-поп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Панк-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Поп-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Постпанк", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Построк", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Прогрессивный рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Прото-панк", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Психоделический рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Регги-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Рокабилли", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Рок-н-ролл", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Рэп-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Рэпкор", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Сатерн-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Свомп-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Сёрф-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Софт-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Спейс-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Стадионный рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Стоунер-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Трип-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Фанк-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Фермер-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Фолк-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Хард-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Хартленд-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Христианский рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Хеви-метал", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Чикано-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Экспериментальный рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Электроник-рок", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Эйсид-рок", false));
        } else if(Janr == "Шансон"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Русский шансон", false));
        } else if(Janr == "Романс"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Цыганский романс", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Жестокий романс", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Русский романс", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Городской романс", false));
        } else if(Janr == "Авторская песня"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Авторская песня", false));
        } else if(Janr == "Поп"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Европоп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Латина", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Синтипоп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Диско", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Танцевальная музыка", false));
        } else if(Janr == "Хип-хоп"){
            arraySpinner_podjanr.add(new CheckBoxClassInit("Олдскул", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Ньюскул", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Гангста-рэп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Политический хип-хоп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Альтернативный хип-хоп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Джи-фанк", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Хорроркор", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Южный хип-хоп", false));
            arraySpinner_podjanr.add(new CheckBoxClassInit("Грайм", false));
        }
        return arraySpinner_podjanr;
    } //Метка поджанр
    private ArrayList<CheckBoxClassInit> Types_(){
        arraySpiner_create_list = new ArrayList<CheckBoxClassInit>();
        arraySpiner_create_list.add(new CheckBoxClassInit("По исполнителю", false));
        arraySpiner_create_list.add(new CheckBoxClassInit("По жанру", false));
        arraySpiner_create_list.add(new CheckBoxClassInit("По поджанру", false));
        arraySpiner_create_list.add(new CheckBoxClassInit("По языку вокала", false));
        arraySpiner_create_list.add(new CheckBoxClassInit("По настроению", false));
        return arraySpiner_create_list;
    } //Метка формирования плейлиста
    private ArrayList<CheckBoxClassInit> Types_value(int tubus){

        try {
            List<String> temp;
            arraySpinner = new ArrayList<CheckBoxClassInit>();
            Set<String> set;
            switch (tubus) {
                case 0:
                    temp = new ArrayList<>();
                    for (Track t : CollectionTrackList) {
                        temp.add(t.getArtist());
                        if (t.getArtist2() != ".")
                            temp.add(t.getArtist2());
                        if (t.getArtist3() != ".")
                            temp.add(t.getArtist3());
                        if (t.getArtist4() != ".")
                            temp.add(t.getArtist4());
                        if (t.getArtist5() != ".")
                            temp.add(t.getArtist5());
                        if (t.getArtist6() != ".")
                            temp.add(t.getArtist6());
                    }
                    set = new HashSet<String>(temp);
                    for (String t : set) {
                        arraySpinner.add(new CheckBoxClassInit(t, false));
                    }
                    break;
                case 1:
                    temp = new ArrayList<>();
                    for (Track t : CollectionTrackList) {
                        temp.add(t.getJanr());
                    }
                    set = new HashSet<String>(temp);
                    for (String t : set) {
                        arraySpinner.add(new CheckBoxClassInit(t, false));
                    }
                    break;
                case 2:
                    temp = new ArrayList<>();
                    for (Track t : CollectionTrackList) {
                        temp.add(t.getPodJanr());
                    }
                    set = new HashSet<String>(temp);
                    for (String t : set) {
                        arraySpinner.add(new CheckBoxClassInit(t, false));
                    }
                    break;
                case 3:
                    temp = new ArrayList<>();
                    for (Track t : CollectionTrackList) {
                        temp.add(t.getMetka1());
                    }
                    set = new HashSet<String>(temp);
                    for (String t : set) {
                        arraySpinner.add(new CheckBoxClassInit(t, false));
                    }
                    break;
                case 4:
                    temp = new ArrayList<>();
                    for (Track t : CollectionTrackList) {
                        temp.add(t.getMetka2());
                    }
                    set = new HashSet<String>(temp);
                    for (String t : set) {
                        arraySpinner.add(new CheckBoxClassInit(t, false));
                    }
                    break;
            }
        }
        catch (Exception e){
            Log.d(LOG_TAG, "Ошибка в Types_value: "+e.getMessage());
        }
        return arraySpinner;
    }
    private ArrayList<CheckBoxClassInit> Types_Nastr(){
        arrayNastroenie_list = new ArrayList<CheckBoxClassInit>();
        arrayNastroenie_list.add(new CheckBoxClassInit("Безмятежность", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Бодрость", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Вдохновение", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Возмущение", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Волнение", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Воодушевление", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Грусть", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Драйв", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Злость", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Интерес", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Ирония", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Ликование", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Надежда", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Надежда", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Напряжение", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Негодование", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Нежность", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Нетерпение", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Обида", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Озорство", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Печаль", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Подавленность", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Порыв", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Предвкушение", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Радость", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Скорбь", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Спокойствие", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Стремление", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Тревога", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Удовлетворенность", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Умиротворение", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Упорство", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Энергичность", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Энтузиазм", false));
        arrayNastroenie_list.add(new CheckBoxClassInit("Ярость", false));
        return arrayNastroenie_list;
    } //Метка Настроения
    private ArrayList<CheckBoxClassInit> Types_Lang_value(){
        Language = new ArrayList<CheckBoxClassInit>();
        Language.add(new CheckBoxClassInit("Русский язык", false));
        Language.add(new CheckBoxClassInit("Английский язык", false));
        Language.add(new CheckBoxClassInit("Немецкий язык", false));
        Language.add(new CheckBoxClassInit("Португальский язык", false));
        Language.add(new CheckBoxClassInit("Французский язык", false));
        Language.add(new CheckBoxClassInit("Испанский язык", false));
        Language.add(new CheckBoxClassInit("Арабский язык", false));
        Language.add(new CheckBoxClassInit("Китайский язык", false));
        //Language.add(new CheckBoxClassInit("", false));
        return Language;
    } //Метка языка
    private ArrayList<CheckBoxClassInit> Types_Janr_value(){
        Janr_list = new ArrayList<CheckBoxClassInit>();
        Janr_list.add(new CheckBoxClassInit("Народная музыка", false));
        Janr_list.add(new CheckBoxClassInit("Духовная музыка", false));
        Janr_list.add(new CheckBoxClassInit("Классическая музыка", false));
        Janr_list.add(new CheckBoxClassInit("Фолк-музыка", false));
        Janr_list.add(new CheckBoxClassInit("Кантри", false));
        Janr_list.add(new CheckBoxClassInit("Латиноамериканская музыка", false));
        Janr_list.add(new CheckBoxClassInit("Блюз", false));
        Janr_list.add(new CheckBoxClassInit("Ритм-н-блюз", false));
        Janr_list.add(new CheckBoxClassInit("Джаз", false));
        Janr_list.add(new CheckBoxClassInit("Электронная музыка", false));
        Janr_list.add(new CheckBoxClassInit("Рок", false));
        Janr_list.add(new CheckBoxClassInit("Шансон", false));
        Janr_list.add(new CheckBoxClassInit("Романс", false));
        Janr_list.add(new CheckBoxClassInit("Авторская песня", false));
        Janr_list.add(new CheckBoxClassInit("Поп", false));
        Janr_list.add(new CheckBoxClassInit("Хип-хоп", false));
        return Janr_list;
    } //Метка Жанр

    public class SplashActivity extends MainActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
    public class MusicIntentReceiver extends BroadcastReceiver {
        final String LOG_TAG = "myLogs";
        boolean ut = false;
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                    int state = intent.getIntExtra("state", -1);
                    switch (state) {
                        case 0:
                            if(ut) {
                                musicSrv.pausePlayer();
                                Log.d(LOG_TAG, "Наушники отключены");
                                ut = false ;
                            }
                            break;
                        case 1:
                            musicSrv.go();
                            players();
                            Log.d(LOG_TAG, "Наушники подключены");
                            ut = true;
                            break;
                        default:
                            Log.d(LOG_TAG, "Неизвестное состояние");
                    }
                }
            }catch (Exception e){
                Log.d(LOG_TAG, "ОШИБКА С ГАРНИТУРОЙ");
            }
        }

    }


}

