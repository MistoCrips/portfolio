﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model.LD
{
    public class Photo_ : INotifyPropertyChanged //подгрузчик фотографий
    {
        public event PropertyChangedEventHandler PropertyChanged;
        BitmapImage photo1 ;
        BitmapImage photo2;
        BitmapImage photo3;
        BitmapImage photo4;
        BitmapImage photo5;
        BitmapImage photo6;
        public BitmapImage Date1 //Фото
        {
            get { return photo1; }
            set
            {
                if (photo1 == value)
                    return;
                photo1 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date1"));
            }
        }
        public BitmapImage Date2 //Автобиография
        {
            get { return photo2; }
            set
            {
                if (photo2 == value)
                    return;
                photo2 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date2"));
            }
        }
        public BitmapImage Date3 //Диплом
        {
            get { return photo3; }
            set
            {
                if (photo3 == value)
                    return;
                photo3 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date3"));
            }
        }
        public BitmapImage Date4 //Аттестационный лист
        {
            get { return photo4; }
            set
            {
                if (photo4 == value)
                    return;
                photo4 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date4"));
            }
        }
        public BitmapImage Date5 //Инн
        {
            get { return photo5; }
            set
            {
                if (photo5 == value)
                    return;
                photo5 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date5"));
            }
        }
        public BitmapImage Date6 //Снилс
        {
            get { return photo6; }
            set
            {
                if (photo6 == value)
                    return;
                photo6 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date6"));
            }
        }
        public byte[] getJPGFromImageControl(BitmapImage imageC) // Получение из изображения массива байт
        {
            MemoryStream memStream = new MemoryStream();
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(imageC));
            encoder.Save(memStream);
            return memStream.ToArray();
        }
        public BitmapImage LoadImage(byte[] imageData) //получение из массива байт изображения
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }
        public string ByteArrayToString(byte[] ba) //Перевод массива байт в строку
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
        public byte[] StringToByteArray(String hex) //перевод строки в массив байт
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
        public Photo_() { }
        public void ToXml(string LOGIN)
        {
            string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\ph\\" + LOGIN + ".xml";
            XDocument doc;
            if (File.Exists(fileName) == false)
            {
                doc = new XDocument(
                new XElement("base",
                    new XElement("track",
                        new XAttribute("ph1", ByteArrayToString(getJPGFromImageControl(Date1))),
                        new XAttribute("ph2", ByteArrayToString(getJPGFromImageControl(Date2))),
                        new XAttribute("ph3", ByteArrayToString(getJPGFromImageControl(Date3))),
                        new XAttribute("ph4", ByteArrayToString(getJPGFromImageControl(Date4))),
                        new XAttribute("ph5", ByteArrayToString(getJPGFromImageControl(Date5))),
                        new XAttribute("ph6", ByteArrayToString(getJPGFromImageControl(Date6))))));
                doc.Save(fileName);
            }
            else
            {
                doc = XDocument.Load(fileName);
                XElement track = new XElement("track");
                track.Add(new XAttribute("ph1", ByteArrayToString(getJPGFromImageControl(Date1))));
                track.Add(new XAttribute("ph2", ByteArrayToString(getJPGFromImageControl(Date2))));
                track.Add(new XAttribute("ph3", ByteArrayToString(getJPGFromImageControl(Date3))));
                track.Add(new XAttribute("ph4", ByteArrayToString(getJPGFromImageControl(Date4))));
                track.Add(new XAttribute("ph5", ByteArrayToString(getJPGFromImageControl(Date5))));
                track.Add(new XAttribute("ph6", ByteArrayToString(getJPGFromImageControl(Date6))));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }
        public Photo_(XElement element)
        {
            if (element.Attribute("ph1").Value != null) Date1 = LoadImage(StringToByteArray(element.Attribute("ph1").Value));
            if (element.Attribute("ph2").Value != null) Date2 = LoadImage(StringToByteArray(element.Attribute("ph2").Value));
            if (element.Attribute("ph3").Value != null) Date3 = LoadImage(StringToByteArray(element.Attribute("ph3").Value));
            if (element.Attribute("ph4").Value != null) Date4 = LoadImage(StringToByteArray(element.Attribute("ph4").Value));
            if (element.Attribute("ph5").Value != null) Date5 = LoadImage(StringToByteArray(element.Attribute("ph5").Value));
            if (element.Attribute("ph6").Value != null) Date6 = LoadImage(StringToByteArray(element.Attribute("ph6").Value));
        }
    }
}
