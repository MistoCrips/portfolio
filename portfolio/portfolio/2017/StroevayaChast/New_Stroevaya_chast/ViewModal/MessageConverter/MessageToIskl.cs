﻿using New_Stroevaya_chast.Modal.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stroevaya_chast.ViewModal.MessageConverter
{
    public class MessageToIskl
    {
        public ObservableCollection<OpenLoadIskl> server_iskl(string message)
        {
            ObservableCollection<OpenLoadIskl> list = new ObservableCollection<OpenLoadIskl>();
            int j = 1; OpenLoadIskl p = new OpenLoadIskl();
            string[] word = message.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < word.Length; i++)
            {
                if (j > 8) j = 1;
                if (j == 1)
                {
                    p = new OpenLoadIskl();
                    p.Lnumber = word[i];
                    j++; continue;
                }
                if (j == 2)
                {
                    p.FIO = word[i];
                    j++; continue;
                }
                if (j == 3)
                {
                    p.Types = word[i];
                    j++; continue;
                }
                if (j == 4)
                {
                    p.Zvanie = word[i];
                    j++; continue;
                }
                if (j == 5)
                {
                    p.Date = word[i];
                    j++; continue;
                }
                if (j == 6)
                {
                    p.Pr = word[i];
                    j++; continue;
                }
                if (j == 7)
                {
                    p.Date_pr = word[i];
                    j++; continue;
                }
                if (j == 8)
                {
                    p.What_pr = word[i];
                    list.Add(p);
                    j++; continue;
                }
            }
            int a = 1;
            foreach (OpenLoadIskl i in list)
            {
                i.Id = a++;
            }
            return list;
        }
    }
}
