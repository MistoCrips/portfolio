﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace New_Stroevaya_chast.Modal.Model
{
    public class Naryad
    {
        int id_ = 0;
        string type_naryad = ".";
        string podr = ".";
        string dolg = ".";
        string lnumber = ".";
        string zvanie = ".";
        string fio = ".";
        string types = ".";
        string date1 = ".";
        string key = ".";
        public string Type_naryad
        {
            get { return type_naryad; }
            set
            {
                if (type_naryad == value)
                    return;
                type_naryad = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types_naryad"));
            }
        }
        public string Podr
        {
            get { return podr; }
            set
            {
                if (podr == value)
                    return;
                podr = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Podr"));
            }
        }
        public string Dolg
        {
            get { return dolg; }
            set
            {
                if (dolg == value)
                    return;
                dolg = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Dolg"));
            }
        }
        public string Key
        {
            get { return key; }
            set
            {
                if (key == value)
                    return;
                key = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Key"));
            }
        }
        public string Lnumber
        {
            get { return lnumber; }
            set
            {
                if (lnumber == value)
                    return;
                lnumber = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Lnumber"));
            }
        }
        public string Zvanie
        {
            get { return zvanie; }
            set
            {
                if (zvanie == value)
                    return;
                zvanie = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Zvanie"));
            }
        }
        public string FIO
        {
            get { return fio; }
            set
            {
                if (fio == value)
                    return;
                fio = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FIO"));
            }
        }
        public string Types
        {
            get { return types; }
            set
            {
                if (types == value)
                    return;
                types = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public string Date
        {
            get { return date1; }
            set
            {
                if (date1 == value)
                    return;
                date1 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date"));
            }
        }
        public int Id
        {
            get { return id_; }
            set
            {
                if (id_ == value)
                    return;
                id_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Id"));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }
}
