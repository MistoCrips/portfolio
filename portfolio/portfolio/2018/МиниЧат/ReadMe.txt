�������: "��� �����������"

� ��������� ����� �������� ��������� �������������� �����. � ����� �� �������� ���������� �����, ����������� ����� � 
��������. ��� �� ����������� �������� � ������� � ��������. ��������� ������� �� ���� ���������, ��������� ����� �����
��������� �����. ������� ������. ���������� ������� ��� ��� ������������� ����� ����� ������������. 
��� ���������� ������������ ������ MVVM (Model - View - ViewModel).

1. Model - ��� ������� ���������� ������� � ��������� ������ � ������������ (���, ip �����); �������, ��������� � ����������
��������� � �������� ���������, � ����� � �������� ��������. ������� ����� �� �������� � �������� � ������ � ������ ������ ����
MS SQL, My SQL, ������� ����������� ����� � ���� ������ � xml �����.

2. View - ����������� ���� ��� ����� ���������, ���� ��� ������ ��������� � ������ ������������ �������������.

3. ViewModel - �� ���������� ������������. 