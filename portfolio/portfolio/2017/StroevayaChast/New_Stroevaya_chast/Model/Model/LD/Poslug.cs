﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model.LD
{
    public class Poslug : INotifyPropertyChanged  /// Послужной лист
    {
        public event PropertyChangedEventHandler PropertyChanged;
        string chast_ = "", podr_ = "", dolgnost_ = "", vus_ = "", name_pricaz_ = "", nomber_pricaz_ = "", date_pricaz_ = "";
        public Poslug() { }
        public Poslug(XElement element)
        {
            if (element.Attribute("chast_").Value != null) Chast_ = element.Attribute("chast_").Value;
            if (element.Attribute("podr_").Value != null) Podr_ = element.Attribute("podr_").Value;
            if (element.Attribute("dolgnost_").Value != null) Dolgnost_ = element.Attribute("dolgnost_").Value;
            if (element.Attribute("vus_").Value != null) VUS_ = element.Attribute("vus_").Value;
            if (element.Attribute("name_pricaz").Value != null) Name_pricaz = element.Attribute("name_pricaz").Value;
            if (element.Attribute("nomber_pricaz").Value != null) Nomber_pricaz = element.Attribute("nomber_pricaz").Value;
            if (element.Attribute("date_pricaz").Value != null) Date_pricaz = element.Attribute("date_pricaz").Value;
        }

        public string Chast_
        {
            get { return chast_; }
            set
            {
                if (chast_ == value)
                    return;
                chast_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("chast_"));
            }
        }
        public string Podr_
        {
            get { return podr_; }
            set
            {
                if (podr_ == value)
                    return;
                podr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("podr_"));
            }
        }
        public string Dolgnost_
        {
            get { return dolgnost_; }
            set
            {
                if (dolgnost_ == value)
                    return;
                dolgnost_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("dolgnost_"));
            }
        }
        public string VUS_
        {
            get { return vus_; }
            set
            {
                if (vus_ == value)
                    return;
                vus_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("vus_"));
            }
        }
        public string Name_pricaz
        {
            get { return name_pricaz_; }
            set
            {
                if (name_pricaz_ == value)
                    return;
                name_pricaz_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("name_pricaz"));
            }
        }
        public string Nomber_pricaz
        {
            get { return nomber_pricaz_; }
            set
            {
                if (nomber_pricaz_ == value)
                    return;
                nomber_pricaz_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("nomber_pricaz"));
            }
        }
        public string Date_pricaz
        {
            get { return date_pricaz_; }
            set
            {
                if (date_pricaz_ == value)
                    return;
                date_pricaz_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("date_pricaz"));
            }
        }
        public void ToXml(string LOGIN)
        {
            string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + LOGIN + ".xml";
            XDocument doc;
            if (File.Exists(fileName) == false)
            {
                doc = new XDocument(
                new XElement("base",
                    new XElement("track_pos",
                        new XAttribute("chast_", Chast_),
                        new XAttribute("podr_", Podr_),
                        new XAttribute("dolg_", Dolgnost_),
                        new XAttribute("vus_", VUS_),
                        new XAttribute("name_pricaz", Name_pricaz),
                        new XAttribute("nomber_pricaz", Nomber_pricaz),
                        new XAttribute("date_pricaz", Date_pricaz))));
                doc.Save(fileName);
            }
            else
            {
                doc = XDocument.Load(fileName);
                XElement track = new XElement("track_pos");
                track.Add(new XAttribute("chast_", Chast_));
                track.Add(new XAttribute("podr_", Podr_));
                track.Add(new XAttribute("dolg_", Dolgnost_));
                track.Add(new XAttribute("vus_", VUS_));
                track.Add(new XAttribute("name_pricaz", Name_pricaz));
                track.Add(new XAttribute("nomber_pricaz", Nomber_pricaz));
                track.Add(new XAttribute("date_pricaz", Date_pricaz));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }

    }//конец послужного
}
