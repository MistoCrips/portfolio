package com.example.user.proba;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class PlayListAdapter extends BaseAdapter {
    ArrayList<Playlist> PlayerList;
    private LayoutInflater songInf;

    public PlayListAdapter(Context c,  ArrayList<Playlist> A){
        songInf= LayoutInflater.from(c);
        PlayerList = A;
    }

    @Override
    public int getCount() {
        return PlayerList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GridLayout songLay = (GridLayout)songInf.inflate
                (R.layout.playlistslayout, parent, false);
        TextView Name_playlist = (TextView)songLay.findViewById(R.id.Name_playlist);
        TextView Key_song_playlist = (TextView)songLay.findViewById(R.id.Key_song_playlist);
        final Button MenuButtonPlaylist = (Button) songLay.findViewById(R.id.MenuButtonPlaylist);
        Playlist currSong = PlayerList.get(position);
        MenuButtonPlaylist.setTag(position);
        //get title and artist strings
        //titleBox.setText(currSong.getArtist()+" - "+currSong.getTitle());
        Name_playlist.setText(currSong.getNames());
        Key_song_playlist.setText("Количество песен: "+currSong.getKeys());
        songLay.setTag(position);
        return songLay;
    }
}
