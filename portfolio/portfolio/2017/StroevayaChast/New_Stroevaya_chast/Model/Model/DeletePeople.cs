﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model
{
    public class OpenLoadDel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public OpenLoadDel() { }
        public string ColToString()
        {
            Sbor_Del();
            string S = "";
            foreach (OpenLoadDel N in Del_List)
            {
                S += N.Lnumber + ">" + N.FIO + ">" + N.Types + ">" + N.Zvanie + ">" + N.Date + ">" + N.Punkt + ">" + N.Podpunkt + ">" + N.Pr + ">" + N.Date_pr + ">" + N.What_pr + ">";
            }
            return S;
        }
        ObservableCollection<OpenLoadDel> Del_List = new ObservableCollection<OpenLoadDel>();
        private string last_podr = ".";
        private string last_dolg = ".";
        private string lnumber = ".";
        private string fio = ".";
        private string zvanie = ".";
        private string types = ".";
        private string date_ = ".";
        private string p_ = ".";
        private string pp_ = ".";
        private string pr_ = ".";
        private string date_pr_ = ".";
        private string what_pr_ = ".";
        private string osn_ = ".";
        private int id_ = 0;
        public int Id
        {
            get { return id_; }
            set
            {
                if (id_ == value)
                    return;
                id_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Id"));
            }
        }
        public string Pr
        {
            get { return pr_; }
            set
            {
                if (pr_ == value)
                    return;
                pr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Pr"));
            }
        }
        public string Osn
        {
            get { return osn_; }
            set
            {
                if (osn_ == value)
                    return;
                osn_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Osn"));
            }
        }
        public string Date_pr
        {
            get { return date_pr_; }
            set
            {
                if (date_pr_ == value)
                    return;
                date_pr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date_pr"));
            }
        }
        public string What_pr
        {
            get { return what_pr_; }
            set
            {
                if (what_pr_ == value)
                    return;
                what_pr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("What_pr"));
            }
        }
        public string Punkt
        {
            get { return p_; }
            set
            {
                if (p_ == value)
                    return;
                p_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Punkt"));
            }
        }
        public string Podpunkt
        {
            get { return pp_; }
            set
            {
                if (pp_ == value)
                    return;
                pp_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Podpunkt"));
            }
        }
        public string Lnumber
        {
            get { return lnumber; }
            set
            {
                if (lnumber == value)
                    return;
                lnumber = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Lnumber"));
            }
        }
        public string FIO
        {
            get { return fio; }
            set
            {
                if (fio == value)
                    return;
                fio = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FIO"));
            }
        }
        public string Zvanie
        {
            get { return zvanie; }
            set
            {
                if (zvanie == value)
                    return;
                zvanie = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Zvanie"));
            }
        }
        public string Types
        {
            get { return types; }
            set
            {
                if (types == value)
                    return;
                types = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public string Dolg
        {
            get { return last_dolg; }
            set
            {
                if (last_dolg == value)
                    return;
                last_dolg = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Dolg"));
            }
        }
        public string Podr
        {
            get { return last_podr; }
            set
            {
                if (last_podr == value)
                    return;
                last_podr = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Podr"));
            }
        }
        public string Date
        {
            get { return date_; }
            set
            {
                if (date_ == value)
                    return;
                date_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date"));
            }
        }
        public OpenLoadDel(XElement element)
        {
            Lnumber = element.Attribute("lnumber").Value;
            FIO = element.Attribute("fio").Value;
            Zvanie = element.Attribute("tek_zvanie").Value;
            Types = element.Attribute("type").Value;
            Date = element.Attribute("date_").Value;
            Punkt = element.Attribute("punkt").Value;
            Podpunkt = element.Attribute("podpunkt").Value;
            Pr = element.Attribute("pr").Value;
            Date_pr = element.Attribute("date_pr").Value;
            What_pr = element.Attribute("what_pr").Value;
            Osn = element.Attribute("osn").Value;
        }
        public ObservableCollection<OpenLoadDel> Sbor_Del()
        {
            string filename = Environment.CurrentDirectory + "\\Recources\\idp\\d\\51460.xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                Del_List.Add(new OpenLoadDel(el));
            }
            return Del_List;
        }
    }//Загрузка уволенных
}
