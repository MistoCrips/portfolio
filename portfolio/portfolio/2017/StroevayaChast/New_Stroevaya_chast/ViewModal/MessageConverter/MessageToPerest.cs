﻿using New_Stroevaya_chast.Modal.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stroevaya_chast.ViewModal.MessageConverter
{
    public class MessageToPerest
    {
        public ObservableCollection<Perest> server_perest(string message)
        {
            ObservableCollection<Perest> list = new ObservableCollection<Perest>();
            int j = 1; Perest p = new Perest();
            string[] word = message.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < word.Length; i++)
            {
                if (j > 10) j = 1;
                if (j == 1)
                {
                    p = new Perest();
                    p.Lnumber = word[i];
                    j++; continue;
                }
                if (j == 2)
                {
                    p.FIO = word[i];
                    j++; continue;
                }
                if (j == 3)
                {
                    p.Types = word[i];
                    j++; continue;
                }
                if (j == 4)
                {
                    p.Zvanie = word[i];
                    j++; continue;
                }
                if (j == 5)
                {
                    p.Old_dolg = word[i];
                    j++; continue;
                }
                if (j == 6)
                {
                    p.Old_podr = word[i];
                    j++; continue;
                }
                if (j == 7)
                {
                    p.New_dolg = word[i];
                    j++; continue;
                }
                if (j == 8)
                {
                    p.New_podr = word[i];
                    j++; continue;
                }
                if (j == 9)
                {
                    p.VUS = word[i];
                    j++; continue;
                }
                if (j == 10)
                {
                    p.Kod = word[i];
                    list.Add(p);
                    j++; continue;
                }
            }
            int a = 1;
            foreach (Perest i in list)
            {
                i.Id = a++;
            }
            return list;
        }
    }
}
