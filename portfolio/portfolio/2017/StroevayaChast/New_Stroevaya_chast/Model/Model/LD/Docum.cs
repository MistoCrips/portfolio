﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model.LD
{
    public class Docum : INotifyPropertyChanged ///Документ 
    {
        public event PropertyChangedEventHandler PropertyChanged;
        string kod_ = ".";
        string ser_ = ".";
        string nomber_ = ".";
        string who_ = ".";
        string date_ = ".";
        public Docum() { }
        public Docum(XElement element)
        {
            if (element.Attribute("kod").Value != null) Kod = element.Attribute("kod").Value;
            if (element.Attribute("seriya").Value != null) Seriya = element.Attribute("seriya").Value;
            if (element.Attribute("nomber").Value != null) Nomber = element.Attribute("nomber").Value;
            if (element.Attribute("who_vidal").Value != null) Who_vidal = element.Attribute("who_vidal").Value;
            if (element.Attribute("date_vid").Value != null) Date_vid = element.Attribute("date_vid").Value;
        }
        public string Kod
        {
            get { return kod_; }
            set
            {
                if (kod_ == value)
                    return;
                kod_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Kod"));
            }
        }
        public string Seriya
        {
            get { return ser_; }
            set
            {
                if (ser_ == value)
                    return;
                ser_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Ser"));
            }
        }
        public string Nomber
        {
            get { return nomber_; }
            set
            {
                if (nomber_ == value)
                    return;
                nomber_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Nomber"));
            }
        }
        public string Who_vidal
        {
            get { return who_; }
            set
            {
                if (who_ == value)
                    return;
                who_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Who"));
            }
        }
        public string Date_vid
        {
            get { return date_; }
            set
            {
                if (date_ == value)
                    return;
                date_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date"));
            }
        }
        public void ToXml(string LOGIN)
        {
            string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + LOGIN + ".xml";
            XDocument doc;
            if (File.Exists(fileName) == false)
            {
                doc = new XDocument(
                new XElement("base",
                    new XElement("track_dm",
                        new XAttribute("kod", Kod),
                        new XAttribute("seriya", Seriya),
                        new XAttribute("nomber", Nomber),
                        new XAttribute("who_vidal", Who_vidal),
                        new XAttribute("date_vid", Date_vid))));
                doc.Save(fileName);
            }
            else
            {
                doc = XDocument.Load(fileName);
                XElement track = new XElement("track_dm");
                track.Add(new XAttribute("kod", Kod));
                track.Add(new XAttribute("seriya", Seriya));
                track.Add(new XAttribute("nomber", Nomber));
                track.Add(new XAttribute("who_vidal", Who_vidal));
                track.Add(new XAttribute("date_vid", Date_vid));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }

    }
}
