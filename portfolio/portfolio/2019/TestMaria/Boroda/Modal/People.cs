﻿using Boroda.ViewModal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using System.Xml.Linq;
using Xunit;

namespace Boroda.Modal
{
    public class People : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private int id_ = 0;
        private static int counter_ = 0;
        private string first_name_ = "";
        private string last_name_ = "";
        private string middle_name_ = "";
        private string address_ = "";
        private string nomber_ = "";
        private DateTime date_ = DateTime.Parse("01.01.2019");
        private string interval = "";

        public People(XElement element)
        {
            first_name_ = element.Attribute("first_name_").Value;
            last_name_ = element.Attribute("last_name_").Value;
            middle_name_ = element.Attribute("middle_name_").Value;
            address_ = element.Attribute("address_").Value;
            nomber_ = element.Attribute("nomber_").Value;
            interval = element.Attribute("interval").Value;
            try
            {
                date_ = DateTime.Parse(element.Attribute("date_").Value);
            }
            catch (Exception e)
            {
                date_ = DateTime.Parse("01.01.2019");
            }         
            ID = ++ counter_;
        }
        public void RemovePeople(string Key)
        {
            string fileName = Environment.CurrentDirectory + "\\people.xml";
            XDocument doc;
            doc = XDocument.Load(fileName);
            doc.Descendants().Where(e => e.Attributes().Any(a => a.Value == Key)).Remove();
            doc.Save(fileName);
        }
        public People()
        {
            id_ = ++ counter_;
        }
        public int ID
        {
            get { return id_; }
            set
            {
                if (id_ == value)
                    return;
                id_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ID"));
            }
        }
        public string First_name
        {
            get { return first_name_; }
            set
            {
                if (first_name_ == value)
                    return;
                first_name_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("First_name"));
            }
        }
        public string Last_name
        {
            get { return last_name_; }
            set
            {
                if (last_name_ == value)
                    return;
                last_name_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Last_name"));
            }
        }
        public string Middle_name
        {
            get { return middle_name_; }
            set
            {
                if (middle_name_ == value)
                    return;
                middle_name_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Middle_name"));
            }
        }
        public string FIO
        {
            get { return last_name_ + " "+ first_name_ + " " + middle_name_; }

        }
        public string Nomber
        {
            get { return nomber_; }
            set
            {
                if (nomber_ == value)
                    return;
                nomber_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Nomber"));
            }
        }
        public string Interval
        {
            get { return interval; }
            set
            {
                if (interval == value)
                    return;
                interval = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Interval"));
            }
        }
        public string Address
        {
            get { return address_; }
            set
            {
                if (address_ == value)
                    return;
                address_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Address"));
            }
        }
        public DateTime Date_
        {
            get { return date_; }
            set
            {
                if (date_ == value)
                    return;
                date_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date_"));
            }
        }
        public void ToXml()
        {
            XDocument doc;
            if (File.Exists(Environment.CurrentDirectory + "\\people.xml") == false)
            {
                string fileName = Environment.CurrentDirectory + "\\people.xml";
                doc = new XDocument(
                            new XElement("base",
                             new XElement("track",
                                new XAttribute("first_name_", first_name_),
                                new XAttribute("last_name_", last_name_),
                                new XAttribute("middle_name_", middle_name_),
                                new XAttribute("address_", address_),
                                new XAttribute("nomber_", nomber_),
                                new XAttribute("date_", date_),
                                new XAttribute("interval", interval),
                                new XAttribute("key", last_name_ + "_" + first_name_ + "_" + middle_name_ + "_" + address_),
                                new XAttribute("id_", id_))));
                doc.Save(fileName);
            }
            else
            {
                string fileName = Environment.CurrentDirectory + "\\people.xml";
                doc = XDocument.Load(fileName);
                XElement track = new XElement("track");
                track.Add(new XAttribute("first_name_", first_name_));
                track.Add(new XAttribute("last_name_", last_name_));
                track.Add(new XAttribute("middle_name_", middle_name_));
                track.Add(new XAttribute("address_", address_));
                track.Add(new XAttribute("nomber_", nomber_));
                track.Add(new XAttribute("date_", date_));
                track.Add(new XAttribute("interval", interval));
                track.Add(new XAttribute("key", last_name_ + "_" + first_name_ + "_" + middle_name_ + "_" + address_));
                track.Add(new XAttribute("id_", id_));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }
        
    }
 
    
}