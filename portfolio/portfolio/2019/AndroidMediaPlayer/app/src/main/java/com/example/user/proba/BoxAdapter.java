package com.example.user.proba;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;

public class BoxAdapter extends BaseAdapter {
    private Context ctx;
    private ArrayList<CheckBoxClassInit> songs;
    private LayoutInflater songInf;

    public BoxAdapter(Context c, ArrayList<CheckBoxClassInit> theSongs){
        ctx = c;
        songs=theSongs;
        songInf=LayoutInflater.from(c);
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //map to song layout
        //if(convertView == null){
        //    LayoutInflater lInflater = (LayoutInflater)ctx.getSystemService(
        //            Activity.LAYOUT_INFLATER_SERVICE);

        //    convertView = lInflater.inflate(R.layout.itemcheckboxlist, null);
        //}
        LinearLayout songLay = (LinearLayout)songInf.inflate
                (R.layout.itemcheckboxlist, parent, false);
        TextView namedView = (TextView) songLay.findViewById(R.id.CBNamed);
        TextView sizeView = (TextView) songLay.findViewById(R.id.CBValue);

        CheckBoxClassInit currSong = songs.get(position);
        namedView.setText(currSong.getNamed_());
        sizeView.setText(String.valueOf(currSong.getSized_()));
        songLay.setTag(position);
        // присваиваем чекбоксу обработчик
        //cbBuy.setOnCheckedChangeListener(myCheckChangeList);
        // пишем позицию
        //cbBuy.setTag(position);
        // заполняем данными из товаров: в корзине или нет
        //cbBuy.setChecked(p.isValue_());
        return songLay;
    }

    ArrayList<CheckBoxClassInit> getBox() {
        ArrayList<CheckBoxClassInit> box = new ArrayList<CheckBoxClassInit>();
        for (CheckBoxClassInit p : songs) {
            if (p.isValue_())
                box.add(p);
        }
        return box;
    }

    CheckBoxClassInit getProduct(int position) {
        return ((CheckBoxClassInit) getItem(position));
    }

    CompoundButton.OnCheckedChangeListener myCheckChangeList = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            getProduct((Integer) buttonView.getTag()).setValue_(isChecked);
        }
    };
}
