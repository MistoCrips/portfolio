﻿using New_Stroevaya_chast.Modal.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stroevaya_chast.ViewModal.MessageConverter
{
    public class MessageToPerechen
    {
        static ObservableCollection<Perechen> Btgr;
        static ObservableCollection<Perechen> Og;
        static ObservableCollection<Perechen> Woman;
        static ObservableCollection<Perechen> Perec;
        static ObservableCollection<Perechen> Secret;

        public ObservableCollection<Perechen> BTGR
        {
            get { return Btgr; }
            set
            {
                Btgr = value;
            }
        }
        public ObservableCollection<Perechen> OG
        {
            get { return Og; }
            set
            {
                Og = value;
            }
        }
        public ObservableCollection<Perechen> WOMAN
        {
            get { return Woman; }
            set
            {
                Woman = value;
            }
        }
        public ObservableCollection<Perechen> PERECH
        {
            get { return Perec; }
            set
            {
                Perec = value;
            }
        }
        public ObservableCollection<Perechen> SECRET
        {
            get { return Secret; }
            set
            {
                Secret = value;
            }
        }
        public MessageToPerechen(string message)
        {
            Btgr = new ObservableCollection<Perechen>();
            Og = new ObservableCollection<Perechen>();
            Woman = new ObservableCollection<Perechen>();
            Perec = new ObservableCollection<Perechen>();
            Secret = new ObservableCollection<Perechen>();
            server_perechen(message);
        }

        public void server_perechen(string message)
        {
            int ij = 1;
            Perechen p = new Perechen();
            int j = 1;
            string[] word = message.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < word.Length; i++)
            {
                if (word[i] == "Btgr ")
                {
                    j = 2; continue;
                }
                if (word[i] == " Og ")
                {
                    j = 3; continue;
                }
                if (word[i] == "Woman ")
                {
                    j = 4; continue;
                }
                if (word[i] == "Perec ")
                {
                    j = 5; continue;
                }
                if (word[i] == "Secret ")
                {
                    j = 6; continue;
                }
                switch (j)
                {
                    case 2:
                        {
                            if (ij == 1)
                            {
                                p = new Perechen();
                                p.id = int.Parse(word[i]);
                                ij++; continue;
                            }
                            if (ij == 2)
                            {
                                p.value = word[i];
                                Btgr.Add(p);
                                ij = 1; continue;
                            }
                            break;
                        }
                    case 3:
                        {
                            if (ij == 1)
                            {
                                p = new Perechen();
                                p.id = int.Parse(word[i]);
                                ij++; continue;
                            }
                            if (ij == 2)
                            {
                                p.value = word[i];
                                Og.Add(p);
                                ij = 1; continue;
                            }
                            break;
                        }
                    case 4:
                        {
                            if (ij == 1)
                            {
                                p = new Perechen();
                                p.id = int.Parse(word[i]);
                                ij++; continue;
                            }
                            if (ij == 2)
                            {
                                p.value = word[i];
                                Woman.Add(p);
                                ij = 1; continue;
                            }
                            break;
                        }
                    case 5:
                        {
                            if (ij == 1)
                            {
                                p = new Perechen();
                                p.id = int.Parse(word[i]);
                                ij++; continue;
                            }
                            if (ij == 2)
                            {
                                p.value = word[i];
                                Perec.Add(p);
                                ij = 1; continue;
                            }
                            break;
                        }
                    case 6:
                        {
                            if (ij == 1)
                            {
                                p = new Perechen();
                                p.id = int.Parse(word[i]);
                                ij++; continue;
                            }
                            if (ij == 2)
                            {
                                p.value = word[i];
                                Secret.Add(p);
                                ij = 1; continue;
                            }
                            break;
                        }
                }
            }
           
        }
        

    }
}
