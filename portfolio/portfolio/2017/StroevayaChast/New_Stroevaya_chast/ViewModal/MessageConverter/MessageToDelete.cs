﻿using New_Stroevaya_chast.Modal.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stroevaya_chast.ViewModal.MessageConverter
{
    public class MessageToDelete
    {
        public ObservableCollection<OpenLoadDel> server_del(string message)
        {
            ObservableCollection<OpenLoadDel> list = new ObservableCollection<OpenLoadDel>();
            int j = 1; OpenLoadDel p = new OpenLoadDel();
            string[] word = message.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < word.Length; i++)
            {
                if (j > 10) j = 1;
                if (j == 1)
                {
                    p = new OpenLoadDel();
                    p.Lnumber = word[i];
                    j++; continue;
                }
                if (j == 2)
                {
                    p.FIO = word[i];
                    j++; continue;
                }
                if (j == 3)
                {
                    p.Types = word[i];
                    j++; continue;
                }
                if (j == 4)
                {
                    p.Zvanie = word[i];
                    j++; continue;
                }
                if (j == 5)
                {
                    p.Date = word[i];
                    j++; continue;
                }
                if (j == 6)
                {
                    p.Punkt = word[i];
                    j++; continue;
                }
                if (j == 7)
                {
                    p.Podpunkt = word[i];
                    j++; continue;
                }
                if (j == 8)
                {
                    p.Pr = word[i];
                    j++; continue;
                }
                if (j == 9)
                {
                    p.Date_pr = word[i];
                    j++; continue;
                }
                if (j == 10)
                {
                    p.What_pr = word[i];
                    list.Add(p);
                    j++; continue;
                }
            }
            int a = 1;
            foreach (OpenLoadDel i in list)
            {
                i.Id = a++;
            }
            return list;
        }
    }
}
