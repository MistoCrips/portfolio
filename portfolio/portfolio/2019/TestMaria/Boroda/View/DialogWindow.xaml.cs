﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Boroda.View
{
    /// <summary>
    /// Логика взаимодействия для DialogWindow.xaml
    /// </summary>
    public partial class DialogWindow : Window
    {
        public DialogWindow()
        {
            InitializeComponent();
        }
        public void DialogWindow1(string mes, string color)
        {
            Mes.Text = mes;
            if(color == "red")
            {
                Mes.Foreground = Brushes.Red;
            }
            if (color == "green")
            {
                Mes.Foreground = Brushes.Green;
            }

        }
    }
}
