﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model.LD
{
    public class family : INotifyPropertyChanged // Список семьи
    {
        public event PropertyChangedEventHandler PropertyChanged;
        string name_ = ".";
        string types_ = ".";
        string brd_ = ".";
        public family() { }
        public family(XElement element)
        {
            if (element.Attribute("name").Value != null) Name = element.Attribute("name").Value;
            if (element.Attribute("type").Value != null) types = element.Attribute("type").Value;
            if (element.Attribute("brd").Value != null) Brd = element.Attribute("brd").Value;
        }
        public string Name
        {
            get { return name_; }
            set
            {
                if (name_ == value)
                    return;
                name_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
            }
        }
        public string types
        {
            get { return types_; }
            set
            {
                if (types_ == value)
                    return;
                types_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public string Brd
        {
            get { return brd_; }
            set
            {
                if (brd_ == value)
                    return;
                brd_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Brd"));
            }
        }
        public void ToXml(string LOGIN)
        {
            string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + LOGIN + ".xml";
            XDocument doc;
            if (File.Exists(fileName) == false)
            {
                doc = new XDocument(
                new XElement("base",
                    new XElement("track_fm",
                        new XAttribute("name", Name),
                        new XAttribute("type", types),
                        new XAttribute("brd", Brd))));
                doc.Save(fileName);
            }
            else
            {
                doc = XDocument.Load(fileName);
                XElement track = new XElement("track_fm");
                track.Add(new XAttribute("name", Name));
                track.Add(new XAttribute("type", types));
                track.Add(new XAttribute("brd", Brd));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }
    }
}
