﻿using New_Stroevaya_chast.Modal.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stroevaya_chast.ViewModal.MessageConverter
{
    public class MessageToUbitie
    {
        public ObservableCollection<Ub> server_ub(string message)
        {
            ObservableCollection<Ub> List = new ObservableCollection<Ub>();
            int j = 1;
            Ub t = new Ub();
            string[] word = message.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < word.Length; i++)
            {

                if (j > 21)
                {
                    j = 1;
                }
                if (j == 1)
                {
                    t = new Ub();
                    t.Cel = word[i];
                    j++;
                    continue;
                }
                if (j == 2)
                {
                    t.Date3 = word[i]; j++; continue;
                }
                if (j == 3)
                {
                    t.Date_preb = word[i]; j++; continue;
                }
                if (j == 4)
                {
                    t.Date_ub = word[i]; j++; continue;
                }
                if (j == 5)
                {
                    t.Dolg = word[i]; j++; continue;
                }
                if (j == 6)
                {
                    t.FIO = word[i]; j++; continue;
                }
                if (j == 7)
                {
                    t.Lnumber = word[i]; j++; continue;
                }
                if (j == 8)
                {
                    t.Mesto = word[i]; j++; continue;
                }
                if (j == 9)
                {
                    t.Osn = word[i]; j++; continue;
                }
                if (j == 10)
                {
                    t.Podr = word[i]; j++; continue;
                }
                if (j == 11)
                {
                    t.Types = word[i]; j++; continue;
                }
                if (j == 12)
                {
                    t.Types_ub = word[i]; j++; continue;
                }
                if (j == 13)
                {
                    t.VPD_ST1 = word[i]; j++; continue;
                }
                if (j == 14)
                {
                    t.VPD_ST2 = word[i]; j++; continue;
                }
                if (j == 15)
                {
                    t.VPD_ST3 = word[i]; j++; continue;
                }
                if (j == 16)
                {
                    t.VPD_ST4 = word[i]; j++; continue;
                }
                if (j == 17)
                {
                    t.VPD_ST5 = word[i]; j++; continue;
                }
                if (j == 18)
                {
                    t.VPD_ST6 = word[i]; j++; continue;
                }
                if (j == 19)
                {
                    t.VPD_ST7 = word[i]; j++; continue;
                }
                if (j == 20)
                {
                    t.Vrio = word[i]; j++; continue;
                }
                if (j == 21)
                {
                    t.Zvanie = word[i]; j++;
                    List.Add(t);
                }
            }
            return List;
        }
    }
}
