﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestSuperBrands.Models;

namespace TestSuperBrands.Controllers
{
    public class CategoriesController : Controller
    {
        private CategoriesContext db = new CategoriesContext();
        /// <summary>
        /// Работа с КАТЕГОРИЯМИ
        /// </summary>
        /// <returns></returns>
        // GET: Categories
        public async Task<ActionResult> IndexCategories()
        {
            DateTime dt = DateTime.Parse("01.01.1900");
            return View(await db.Categories.Where(p => p.DeleteTime == dt).ToListAsync());
        }

        // GET: Categories/CreateCategories
        public ActionResult CreateCategories()
        {
            return View();
        }

        // POST: Categories/CreateCategories
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateCategories([Bind(Include = "id,Name")] Categories categories)
        {
            DateTime dt = DateTime.Today;
            categories.CreateTime = dt;
            categories.DeleteTime = DateTime.Parse("01.01.1900");
            if (ModelState.IsValid)
            {
                db.Categories.Add(categories);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(categories);
        }

        // GET: Categories/EditCategories/5
        public async Task<ActionResult> EditCategories(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categories categories = await db.Categories.FindAsync(id);
            if (categories == null)
            {
                return HttpNotFound();
            }
            return View(categories);
        }

        // POST: Categories/EditCategories/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCategories([Bind(Include = "id,Name,CreateTime")] Categories categories)
        {
            if (ModelState.IsValid)
            {
                db.Entry(categories).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(categories);
        }

        // GET: Categories/DeleteCategories/5
        public async Task<ActionResult> DeleteCategories(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categories categories = await db.Categories.FindAsync(id);
            if (categories == null)
            {
                return HttpNotFound();
            }
            return View(categories);
        }

        // POST: Categories/DeleteCategories/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmedCategories(int id)
        {
            Categories categories = await db.Categories.FindAsync(id);
            categories.DeleteTime = DateTime.Today;
            if (ModelState.IsValid)
            {
                db.Entry(categories).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }









        /// <summary>
        /// Работа с ПРОДУКТОМ
        /// </summary>
        /// <param name="product"></param>

        public async Task<ActionResult> IndexProduct()
        {
            DateTime dt = DateTime.Parse("01.01.1900");
            return View(await db.Products.Where(p => p.DeleteTime == dt).ToListAsync());
        }


        // GET: Categories/CreateProduct
        public async Task<ActionResult> CreateProduct([Bind(Include = "SelectedCategories")] SelectedListModel product)
        {
            int id = int.Parse(product.SelectedCategories);
            DateTime dt = DateTime.Parse("01.01.1900");
            ICollection<Attributes> attribute = await db.Attributes.Where(p => p.DeleteTime == dt).Where(p=>p.Cat_Id == id).ToListAsync();
            Products pr = new Products();
            for (int i = 0; i < attribute.Count; i++)
                pr.Values.Add(new AttributeValues() { Attributes_Id = id, Attributes = attribute.ElementAt(i) });
            pr.Categories_Id = id;
            ViewBag.Attribute = attribute;
            return View(pr);
        }

        // POST: Categories/CreateProduct
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateProduct([Bind(Include = "id,Name,Categories_Id,Values")] Products product)
        {
            DateTime dt = DateTime.Parse("01.01.1900");
            ICollection<Attributes> attribute = await db.Attributes.Where(p => p.DeleteTime == dt).Include(p => p.Values).ToListAsync();
            foreach(var item in product.Values)
            {
                item.CreateTime = DateTime.Today;
                item.DeleteTime = DateTime.Parse("01.01.1900");
            }
            product.CreateTime = DateTime.Today;
            product.DeleteTime = DateTime.Parse("01.01.1900");
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(product);
        }

        // GET: Categories/EditProduct/5
        public async Task<ActionResult> EditProduct(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DateTime dt = DateTime.Parse("01.01.1900");
            IEnumerable<AttributeValues> attributeValue = await db.AttributeValues.Where(p => p.DeleteTime == dt).Include(p => p.Attributes).ToListAsync();
            Products products = await db.Products
                .Where(p => p.DeleteTime == dt)
                .Where(p=>p.id == id)
                .Include(p => p.Values)
                .Include(p => p.Categories)
                .FirstOrDefaultAsync();
            if (products == null)
            {
                return HttpNotFound();
            }
            ICollection<Attributes> attributes = await db.Attributes.Where(p => p.Cat_Id == products.Categories_Id).ToListAsync();
            foreach(var att in attributes)
            {
                bool q = products.Values.Any(p=>p.Attributes == att); //attributes.Any(p => p.Name == prod.Attributes.Name);
                if (!q)
                    products.Values.Add(new AttributeValues() { Attributes = att, Attributes_Id = att.id, Products_Id = products.id, CreateTime = DateTime.Today, DeleteTime = DateTime.Parse("01.01.1900"), Products = products });
            }
            return View(products);
        }

        // POST: Categories/EditProduct/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditProduct([Bind(Include = "id,Name,Categories_Id,DeleteTime,CreateTime,Values")] Products product)
        {
            if (ModelState.IsValid)
            {
                foreach (var item in product.Values)
                    if (item.id != 0)
                        db.Entry(item).State = EntityState.Modified;
                    else
                        db.AttributeValues.Add(item);
                db.Entry(product).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(product);
        }

        // GET: Categories/DeleteProduct/5
        public async Task<ActionResult> DeleteProduct(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Categories/DeleteProduct/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmedProduct(int id)
        {
            Products product = await db.Products.FindAsync(id);
            product.DeleteTime = DateTime.Today;
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }










        /// <summary>
        /// Работа с АТТРИБУТОМ
        /// </summary>
        /// <param name="attributes"></param>

        public async Task<ActionResult> IndexAttributes()
        {
            DateTime dt = DateTime.Parse("01.01.1900");
            return View(await db.Attributes.Where(p => p.DeleteTime == dt).ToListAsync());
        }

        // GET: Categories/CreateAttributes
        public async Task<ActionResult> CreateAttributes()
        {
            DateTime dt = DateTime.Parse("01.01.1900");
            IEnumerable<Categories> categories = await db.Categories.Where(p => p.DeleteTime == dt).ToListAsync();
            SelectedListModel SLM = new SelectedListModel();
            SLM.Categories = new SelectList(categories.Select(e => new { CategoriesId = e.id, CategoriesName = e.Name }), "CategoriesId", "CategoriesName");
            return View(new Tuple<Attributes, SelectedListModel>(new Attributes(), SLM));
        }

        // POST: Categories/CreateAttributes
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAttributes([Bind(Include = "id,Name")] Attributes attribute, [Bind(Include = "SelectedCategories")] SelectedListModel list)
        {
            DateTime dt = DateTime.Today;
            attribute.CreateTime = dt;
            attribute.DeleteTime = DateTime.Parse("01.01.1900");
            attribute.Cat_Id = int.Parse(list.SelectedCategories); 
            if (ModelState.IsValid)
            {
                db.Attributes.Add(attribute);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(attribute);
        }

        // GET: Categories/EditAttributes/5
        public async Task<ActionResult> EditAttributes(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attributes attributes = await db.Attributes.FindAsync(id);
            if (attributes == null)
            {
                return HttpNotFound();
            }
            return View(attributes);
        }

        // POST: Categories/EditAttributes/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAttributes([Bind(Include = "id,Name")] Attributes attributes)
        {
            
            if (ModelState.IsValid)
            {
                db.Entry(attributes).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(attributes);
        }

        // GET: Categories/DeleteAttributes/5
        public async Task<ActionResult> DeleteAttributes(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attributes attributes = await db.Attributes.FindAsync(id);
            if (attributes == null)
            {
                return HttpNotFound();
            }
            return View(attributes);
        }

        // POST: Categories/DeleteAttributes/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmedAttributes(int id)
        {
            Attributes attributes = await db.Attributes.FindAsync(id);
            attributes.DeleteTime = DateTime.Today;
            if (ModelState.IsValid)
            {
                db.Entry(attributes).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }











        /// <summary>
        /// Работа со ЗНАЧЕНИЕМ АТТРИБУТОВ
        /// </summary>
        /// <param name="attributeValues"></param>

        // GET: Categories/DeleteAttributes/5
        public async Task<ActionResult> DeleteAttributeValues(int? id, int? idd)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttributeValues attributeValues = await db.AttributeValues.FindAsync(id);
            if (attributeValues == null)
            {
                return HttpNotFound();
            }
            return View(new Tuple<AttributeValues, int>(attributeValues, (int)idd));
        }

        // GET: Categories/DeleteAttributeValues/5
        public async Task<ActionResult> DeleteAttributeValue(int id, int idd)
        {
            AttributeValues attributeValues = await db.AttributeValues.FindAsync(id);
            attributeValues.DeleteTime = DateTime.Today;
            if (ModelState.IsValid)
            {
                db.Entry(attributeValues).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("EditProduct/" + idd);
            }
            return RedirectToAction("Index");
        }











        public async Task<ActionResult> CreateProductCategories()
        {
            DateTime dt = DateTime.Parse("01.01.1900");
            IEnumerable<Categories> categories = await db.Categories.Where(p => p.DeleteTime == dt).ToListAsync();
            SelectedListModel SLM = new SelectedListModel();
            SLM.Categories = new SelectList(categories.Select(e => new { CategoriesId = e.id, CategoriesName = e.Name }), "CategoriesId", "CategoriesName");
            return View(SLM);
        }




        public ActionResult Edit()
        {
            return View();
        }



        public async Task<ActionResult> InfoProduct(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DateTime dt = DateTime.Parse("01.01.1900");
            IEnumerable<AttributeValues> attributeValue = await db.AttributeValues.Where(p => p.DeleteTime == dt).Include(p => p.Attributes).ToListAsync();
            Products products = await db.Products
                .Where(p => p.DeleteTime == dt)
                .Where(p=>p.id == id)
                .Include(p => p.Values)
                .Include(p => p.Categories)
                .FirstOrDefaultAsync();
            return View(products);
        }

        public async Task<ActionResult> Index(string name)
        {
            DateTime dt = DateTime.Parse("01.01.1900");
            if (name == null)
            {
                
                ICollection<Products> product = await db.Products.Where(p => p.DeleteTime == dt).OrderBy(e => e.CreateTime).ToListAsync();
                ICollection<Categories> category = await db.Categories.Where(p => p.DeleteTime == dt).ToListAsync();
                return View(new Tuple<ICollection<Categories>, ICollection<Products>> (category, product));
            }
            else
            {
                ICollection<Products> product = await db.Products
                    .Where(p => p.DeleteTime == dt)
                    .Where(e => e.Categories.Name == name)
                    .OrderBy(e => e.CreateTime).ToListAsync();
                ICollection<Categories> category = await db.Categories.Where(p => p.DeleteTime == dt).ToListAsync();
                return View(new Tuple<ICollection<Categories>, ICollection<Products>>(category, product));
            }
            
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
