﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace МиниЧат.Modal
{
    public class Messager
    {
        public string Name { get; set; }
        public string Mes { get; set; }
        public string IP { get; set; }
        public string Text { get; set; }
        public Messager()
        {

        }
        public Messager(XElement el)
        {
            if (el.Attribute("name").Value != "") Name = el.Attribute("name").Value;
            if (el.Attribute("mes").Value != "") Mes = el.Attribute("mes").Value;
            if (el.Attribute("ip_").Value != "") IP = el.Attribute("ip_").Value;
            if (el.Attribute("assnova_").Value != "") Text = el.Attribute("assnova_").Value;

        }

        public void ToXml()
        {
            XDocument doc;
            if (File.Exists(Environment.CurrentDirectory + "\\Recources\\dannie.xml") == false)
            {
                string fileName = Environment.CurrentDirectory + "\\Recources\\dannie.xml";
                doc = new XDocument(
                            new XElement("base",
                             new XElement("track",
                                new XAttribute("name", Name),
                                new XAttribute("mes", Mes),
                                new XAttribute("ip_", IP),
                                new XAttribute("assnova_", Text))));
                doc.Save(fileName);
            }
            else
            {
                string fileName = Environment.CurrentDirectory + "\\Recources\\dannie.xml";
                doc = XDocument.Load(fileName);
                XElement track = new XElement("track");
                track.Add(new XAttribute("name", Name));
                track.Add(new XAttribute("mes", Mes));
                track.Add(new XAttribute("ip_", IP));
                track.Add(new XAttribute("assnova_", Text));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }

    }
    
}
