﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model
{
    public class OpenLoad : INotifyPropertyChanged //Штат
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public OpenLoad()
        {
        }
        public OpenLoad(XElement element)
        {
            Lnumber = element.Attribute("lnumber").Value;
            Podr = element.Attribute("podr").Value;
            Dolgnost = element.Attribute("dolg").Value;
            Vus = element.Attribute("vus").Value;
            Kod = element.Attribute("kod").Value;
            Shtat_type = element.Attribute("shtat_type").Value;
            if (int.Parse(element.Attribute("id").Value) > Id)
            {
                Id = int.Parse(element.Attribute("id").Value);
            }
        }
        ObservableCollection<OpenLoad> Shtat_List = new ObservableCollection<OpenLoad>();

        #region Обязательные переменные
        private int id = 1;
        int counter_ = 1;
        private string podr = "";
        private string podr1 = "";
        private string podr2 = "";
        private string podr3 = "";
        private string podr4 = "";
        private string dolgnost = "";
        private string shtat_type = "";
        private string vus = "";
        private string kod = "";
        private string lnumber = "";
        #endregion
        #region переменные для сборки
        private string zvanie = "";
        private string fio = "";
        private string types = "";
        private string otryv_status = "";
        private string otryv_primech = "";
        private string otryv_pricaz = "";
        private string btgr_ = ".";
        private string og_ = ".";
        private string woman_ = ".";
        private string perec_ = ".";
        private string secret_ = ".";
        #endregion
        #region конструтора (обязательные)
        public int Counter
        {
            get { return counter_; }
            set
            {
                if (counter_ == value)
                    return;
                counter_ = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Counter"));
            }
        }
        public string Lnumber
        {
            get { return lnumber; }
            set
            {
                if (lnumber == value)
                    return;
                lnumber = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Lnumber"));
            }
        }
        public string Podr
        {
            get { return podr; }
            set
            {
                if (podr == value)
                    return;
                podr = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Podr"));
            }
        }
        public string Podr1
        {
            get { return podr1; }
            set
            {
                if (podr1 == value)
                    return;
                podr1 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Podr1"));
            }
        }
        public string Podr2
        {
            get { return podr2; }
            set
            {
                if (podr2 == value)
                    return;
                podr2 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Podr2"));
            }
        }
        public string Podr3
        {
            get { return podr3; }
            set
            {
                if (podr3 == value)
                    return;
                podr3 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Podr3"));
            }
        }
        public string Podr4
        {
            get { return podr4; }
            set
            {
                if (podr4 == value)
                    return;
                podr4 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Podr4"));
            }
        }
        public string Dolgnost
        {
            get { return dolgnost; }
            set
            {
                if (dolgnost == value)
                    return;
                dolgnost = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Dolgnost"));
            }
        }
        public string Shtat_type
        {
            get { return shtat_type; }
            set
            {
                if (shtat_type == value)
                    return;
                shtat_type = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Shtat_type"));
            }
        }
        public string Vus
        {
            get { return vus; }
            set
            {
                if (vus == value)
                    return;
                vus = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Vus"));
            }
        }
        public string Kod
        {
            get { return kod; }
            set
            {
                if (kod == value)
                    return;
                kod = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Kod"));
            }
        }
        #endregion
        #region конструктора (для сборки)
        public string Zvanie
        {
            get { return zvanie; }
            set
            {
                if (zvanie == value)
                    return;
                zvanie = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Zvanie"));
            }
        }
        public string Fio
        {
            get { return fio; }
            set
            {
                if (fio == value)
                    return;
                fio = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Fio"));
            }
        }
        public string Types
        {
            get { return types; }
            set
            {
                if (types == value)
                    return;
                types = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public string Otryv_status
        {
            get { return otryv_status; }
            set
            {
                if (otryv_status == value)
                    return;
                otryv_status = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Otryv_status"));
            }
        }
        public string Otryv_primech
        {
            get { return otryv_primech; }
            set
            {
                if (otryv_primech == value)
                    return;
                otryv_primech = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Otryv_primech"));
            }
        }
        public string Otryv_pricaz
        {
            get { return otryv_pricaz; }
            set
            {
                if (otryv_pricaz == value)
                    return;
                otryv_pricaz = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Otryv_pricaz"));
            }
        }
        public int Id
        {
            get { return id; }
            set
            {
                if (id == value)
                    return;
                id = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Id"));
            }
        }
        #endregion
        #region Перечни
        public string BTGR
        {
            get { return btgr_; }
            set
            {
                if (btgr_ == value)
                    return;
                btgr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("BTGR"));
            }
        } //Батальонно-тактические группы
        public string OG //Оперативная группа
        {
            get { return og_; }
            set
            {
                if (og_ == value)
                    return;
                og_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("OG"));
            }
        }
        public string Woman //Должности замещаемые военнослужащими женского пола
        {
            get { return woman_; }
            set
            {
                if (woman_ == value)
                    return;
                woman_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Woman"));
            }
        }
        public string Perec //Должности подлежащие укомплектованию поеннослужащими по контракту
        {
            get { return perec_; }
            set
            {
                if (perec_ == value)
                    return;
                perec_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Perec"));
            }
        }
        public string Secret //Должности предусматривающие работу с государственной тайной
        {
            get { return secret_; }
            set
            {
                if (secret_ == value)
                    return;
                secret_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Secret"));
            }
        }
        #endregion
        public string OpenLoad1()
        {
            string Message;
            ObservableCollection<OpenLoad> List = new ObservableCollection<OpenLoad>();
            List = sbor();
            Message = Mass_shtat(List);
            return Message;

        }
        
        public ObservableCollection<OpenLoad> sbor()
        {
            string filename = Environment.CurrentDirectory + "\\Recources\\Shtat\\51460.xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                Shtat_List.Add(new OpenLoad(el));
            }
            foreach (OpenLoad P in Shtat_List)
            {
                if (P.Lnumber != "0")
                {
                    if (File.Exists(Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + P.Lnumber + ".xml") == true)
                    {
                        filename = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + P.Lnumber + ".xml";
                        doc = XDocument.Load(filename);
                        foreach (XElement el in doc.Root.Elements("track_dan"))
                        {
                            P.fio = el.Attribute("fio").Value;
                            P.types = el.Attribute("types").Value;
                        }
                        foreach (XElement el in doc.Root.Elements("track_zv"))
                        {
                            P.Zvanie = el.Attribute("name").Value;
                        }
                        P.Otryv_pricaz = "0";
                        P.Otryv_primech = "0";
                        P.Otryv_status = "0";
                        foreach (XElement el in doc.Root.Elements("track_otr"))
                        {
                            if (el.Attribute("date3").Value == ".")
                            {
                                //P.Otryv_pricaz = el.Attribute("pr").Value;
                                if (el.Attribute("cel").Value != ".")
                                    P.Otryv_primech += el.Attribute("cel").Value;
                                if (el.Attribute("date1").Value != ".")
                                    P.Otryv_primech += "_с_" + el.Attribute("date1").Value;
                                if (el.Attribute("date2").Value != ".")
                                    P.Otryv_primech += "_по_" + el.Attribute("date2").Value;
                                if (el.Attribute("ms").Value != ".")
                                    P.Otryv_primech += "_с выездом_" + el.Attribute("ms").Value;
                                if (el.Attribute("osn").Value != ".")
                                    P.Otryv_primech += "_Основание:_" + el.Attribute("osn").Value;
                                if (el.Attribute("vrio").Value != ".")
                                    P.Otryv_primech += "_Временно исполняет обязанности:_" + el.Attribute("vrio").Value;
                                P.Otryv_status = el.Attribute("type1").Value;
                            }
                            else
                            {
                                P.Otryv_pricaz = "0";
                                P.Otryv_primech = "0";
                                P.Otryv_status = "0";
                            }
                        }
                    }
                    else
                    if (File.Exists(Environment.CurrentDirectory + "\\Recources\\Archive\\LD\\" + P.Lnumber + ".xml") == true)
                    {
                        File.Move(Environment.CurrentDirectory + "\\Recources\\Archive\\LD\\" + P.Lnumber + ".xml", Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + P.Lnumber + ".xml");
                        filename = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + P.Lnumber + ".xml";
                        doc = XDocument.Load(filename);
                        foreach (XElement el in doc.Root.Elements("track_dan"))
                        {
                            P.fio = el.Attribute("fio").Value;
                            P.types = el.Attribute("types").Value;
                        }
                        foreach (XElement el in doc.Root.Elements("track_zv"))
                        {
                            P.Zvanie = el.Attribute("name").Value;
                        }
                        foreach (XElement el in doc.Root.Elements("track_otr"))
                        {
                            if (el.Attribute("data_preb").Value == "0")
                            {
                                P.Otryv_pricaz = el.Attribute("pr").Value;
                                P.Otryv_primech = el.Attribute("mesto").Value + "_с_" + el.Attribute("s_data").Value + "_" + el.Attribute("cel_").Value;
                                P.Otryv_status = el.Attribute("types_").Value;
                            }
                            else
                            {
                                P.Otryv_pricaz = "0";
                                P.Otryv_primech = "0";
                                P.Otryv_status = "0";
                            }
                        }
                    }
                }
                else
                {
                    P.fio = "-В-";
                    P.types = "0";
                    P.zvanie = "0";
                    P.Otryv_pricaz = "0";
                    P.Otryv_primech = "0";
                    P.Otryv_status = "0";
                }

            }
            return Shtat_List;
        }
        public string Mass_shtat(ObservableCollection<OpenLoad> List)
        {
            string s = "";
            foreach (OpenLoad P in List)
            {
                s = s + "|" + P.id + "|" + P.Podr + "|" + P.dolgnost + "|" + P.Shtat_type + "|" + P.Vus + "|" + P.Kod + "|" + P.lnumber + "|" + P.Zvanie + "|" + P.Fio + "|" + P.Types + "|" + P.Otryv_status + "|" + P.Otryv_primech + "|" + P.Otryv_pricaz;
            }
            //shtat = Encoding.Default.GetBytes(s);
            return s;
        }
        public OpenLoad Search(int num)
        {
            ObservableCollection<OpenLoad> List = new ObservableCollection<OpenLoad>();
            List = sbor();
            foreach (OpenLoad P in List)
            {
                if (P.id == num)
                {
                    return P;
                }
            }
            OpenLoad Pi = new OpenLoad();
            return Pi;
        }
    }//штат
}
