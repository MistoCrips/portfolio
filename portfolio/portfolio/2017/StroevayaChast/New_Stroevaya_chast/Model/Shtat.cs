﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal
{

    
    public class List_new
    {
        List<string> list_Peres = new List<string>(); //Переведенные
        List<string> list_Del = new List<string>(); //уволенные
        List<string> list_Perev = new List<string>(); //Переведенные
        List<string> list_Iskl = new List<string>(); //Исключенные
        List<string> list_Naryad = new List<string>(); // Наряд
        List<string> list_Izmen = new List<string>(); //Изменения
        List<string> list_LD = new List<string>(); //Личное дело
        public string Name { get; set; }
        public string message()
        {
            string message_ = "Lists>Peres>";
            DirectoryInfo dir = new DirectoryInfo(Environment.CurrentDirectory + "\\Recources\\Active\\peres");
            foreach (var item in dir.GetFiles())
            {
                list_Peres.Add(item.Name);
            }
            dir = new DirectoryInfo(Environment.CurrentDirectory + "\\Recources\\Active\\LD");
            foreach (var item in dir.GetFiles())
            {
                list_LD.Add(item.Name);
            }
            dir = new DirectoryInfo(Environment.CurrentDirectory + "\\Recources\\Active\\izm");
            foreach (var item in dir.GetFiles())
            {
                list_Izmen.Add(item.Name);
            }
            dir = new DirectoryInfo(Environment.CurrentDirectory + "\\Recources\\idp\\i\\archive");
            foreach (var item in dir.GetFiles())
            {
                list_Iskl.Add(item.Name);
            }
            dir = new DirectoryInfo(Environment.CurrentDirectory + "\\Recources\\idp\\d\\archive");
            foreach (var item in dir.GetFiles())
            {
                list_Del.Add(item.Name);
            }
            dir = new DirectoryInfo(Environment.CurrentDirectory + "\\Recources\\idp\\p\\archive");
            foreach (var item in dir.GetFiles())
            {
                list_Perev.Add(item.Name);
            }
            dir = new DirectoryInfo(Environment.CurrentDirectory + "\\Recources\\nd");
            foreach (var item in dir.GetFiles())
            {
                list_Naryad.Add(item.Name);
            }
            foreach (string i in list_Peres)
                message_ +=  i + ">";
            message_ += "LD>";
            foreach (string i in list_LD)
                message_ += i + ">";
            message_ += "Izm>";
            foreach (string i in list_Izmen)
                message_ += i + ">";
            message_ += "Iskl>";
            foreach (string i in list_Iskl)
                message_ += i + ">";
            message_ += "Del>";
            foreach (string i in list_Del)
                message_ += i + ">";
            message_ += "Perev>";
            foreach (string i in list_Perev)
                message_ += i + ">";
            message_ += "ND>";
            foreach (string i in list_Naryad)
                message_ += i + ">";
            return message_;
        }
    }
    

}
