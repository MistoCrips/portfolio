﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stroevaya_chast.ViewModal
{
    public class DateClass
    {

        public string DateAll(DateTime dt)
        {
            string date_ = "";
            date_ = Date(dt);
            return date_;

        }
        public string DateToday()
        {
            string data = "";
            DateTime dt = DateTime.Today;
            data = Date(dt);
            return data;
        }
        public string Date(DateTime dt)
        {
            string data = "";
            if (dt.Day < 10)
            { data = "0" + dt.Day.ToString(); }
            else
            { data = dt.Day.ToString(); }
            if (dt.Month < 10)
            { data += ".0" + dt.Month.ToString(); }
            else
            { data += "." + dt.Month.ToString(); }
            data += "." + dt.Year.ToString();

            return data;
        }
    }
}
