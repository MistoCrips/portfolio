﻿using System.Windows.Input;

namespace МиниЧат.Modal
{
    public interface IDelegateCommand : ICommand
    {
        void RaiseCanExecuteChanged();
    }
}