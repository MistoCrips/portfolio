﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model
{
    public class OpenLoadPerevod : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public OpenLoadPerevod() { }
        public string ColToString()
        {
            Sbor_Del();
            string S = "";
            foreach (OpenLoadPerevod N in Perev_List)
            {
                S += N.Lnumber + ">" + N.FIO + ">" + N.Types + ">" + N.Zvanie + ">" + N.Date + ">" + N.Chast + ">" + N.Gorod + ">" + N.Osn + ">" + N.Pr + ">" + N.Date_pr + ">" + N.What_pr + ">";
            }
            return S;
        }
        ObservableCollection<OpenLoadPerevod> Perev_List = new ObservableCollection<OpenLoadPerevod>();
        private string lnumber = ".";
        private string fio = ".";
        private string zvanie = ".";
        private string types = ".";
        private string date_ = ".";
        private string dolg_ = ".";
        private string podr_ = ".";
        private string chast_ = ".";
        private string gorod_ = ".";
        private string osn_ = ".";
        private string pr_ = ".";
        private string date_pr_ = ".";
        private string what_pr_ = ".";
        private int id_ = 0;
        public int Id
        {
            get { return id_; }
            set
            {
                if (id_ == value)
                    return;
                id_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Id"));
            }
        }
        public string Pr
        {
            get { return pr_; }
            set
            {
                if (pr_ == value)
                    return;
                pr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Pr"));
            }
        }
        public string Date_pr
        {
            get { return date_pr_; }
            set
            {
                if (date_pr_ == value)
                    return;
                date_pr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date_pr"));
            }
        }
        public string What_pr
        {
            get { return what_pr_; }
            set
            {
                if (what_pr_ == value)
                    return;
                what_pr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("What_pr"));
            }
        }
        public string Chast
        {
            get { return chast_; }
            set
            {
                if (chast_ == value)
                    return;
                chast_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Chast"));
            }
        }
        public string Gorod
        {
            get { return gorod_; }
            set
            {
                if (gorod_ == value)
                    return;
                gorod_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Gorod"));
            }
        }
        public string Osn
        {
            get { return osn_; }
            set
            {
                if (osn_ == value)
                    return;
                osn_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Osn"));
            }
        }
        public string Dolg
        {
            get { return dolg_; }
            set
            {
                if (dolg_ == value)
                    return;
                dolg_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Dolg"));
            }
        }
        public string Podr
        {
            get { return podr_; }
            set
            {
                if (podr_ == value)
                    return;
                podr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Podr"));
            }
        }
        public string Lnumber
        {
            get { return lnumber; }
            set
            {
                if (lnumber == value)
                    return;
                lnumber = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Lnumber"));
            }
        }
        public string FIO
        {
            get { return fio; }
            set
            {
                if (fio == value)
                    return;
                fio = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FIO"));
            }
        }
        public string Zvanie
        {
            get { return zvanie; }
            set
            {
                if (zvanie == value)
                    return;
                zvanie = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Zvanie"));
            }
        }
        public string Types
        {
            get { return types; }
            set
            {
                if (types == value)
                    return;
                types = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public string Date
        {
            get { return date_; }
            set
            {
                if (date_ == value)
                    return;
                date_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date"));
            }
        }
        public OpenLoadPerevod(XElement element)
        {
            Lnumber = element.Attribute("lnumber").Value;
            FIO = element.Attribute("fio").Value;
            Zvanie = element.Attribute("tek_zvanie").Value;
            Types = element.Attribute("type").Value;
            Date = element.Attribute("date_").Value;
            Chast = element.Attribute("chast").Value;
            Gorod = element.Attribute("gorod").Value;
            Osn = element.Attribute("osn").Value;
            Pr = element.Attribute("pr").Value;
            Date_pr = element.Attribute("date_pr").Value;
            What_pr = element.Attribute("what_pr").Value;
        }
        public ObservableCollection<OpenLoadPerevod> Sbor_Del()
        {
            string filename = Environment.CurrentDirectory + "\\Recources\\idp\\p\\51460.xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                Perev_List.Add(new OpenLoadPerevod(el));
            }
            return Perev_List;
        }
    }//Загрузка переведенных
}
