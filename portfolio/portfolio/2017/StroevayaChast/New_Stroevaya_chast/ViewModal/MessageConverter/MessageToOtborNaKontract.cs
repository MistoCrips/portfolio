﻿using New_Stroevaya_chast.Modal.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stroevaya_chast.ViewModal.MessageConverter
{
    public class MessageToOtborNaKontract
    {
        static ObservableCollection<Otbor_na_kontr> otob;
        static ObservableCollection<Otbor_na_kontr> pered;
        static ObservableCollection<Otbor_na_kontr> sost;
        static ObservableCollection<Otbor_na_kontr> otkaz;

        public ObservableCollection<Otbor_na_kontr> OTOBR //Отобранные (проходят мероприятия по заключению первого контракта) 
        {
            get { return otob; }
            set
            {
                otob = value;
            }
        }
        public ObservableCollection<Otbor_na_kontr> PERED //Переданные в проект приказа СС ЗМО РФ
        {
            get { return pered; }
            set
            {
                pered = value;
            }
        }
        public ObservableCollection<Otbor_na_kontr> SOST //Подписан приказ СС ЗМО РФ
        {
            get { return sost; }
            set
            {
                sost = value;
            }
        }
        public ObservableCollection<Otbor_na_kontr> OTKAZ //Отказавшиеся от заключения контракта, после написания рапортов
        {
            get { return otkaz; }
            set
            {
                otkaz = value;
            }
        }

        public MessageToOtborNaKontract(string message)
        {
            int types = 0;
            int j = 1;
            Otbor_na_kontr t = new Otbor_na_kontr();
            string[] word = message.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < word.Length; i++)
            {
                if (word[i] == "O1")
                {
                    types = 1;
                    j = 1;
                    continue;
                }
                if (word[i] == "O2")
                {
                    types = 2;
                    j = 1;
                    continue;
                }
                if (word[i] == "O3")
                {
                    types = 3;
                    j = 1;
                    continue;
                }
                if (word[i] == "O4")
                {
                    types = 4;
                    j = 1;
                    continue;
                }
                switch (types)
                {
                    case 1:
                        {
                            if (j == 1)
                            {
                                t = new Otbor_na_kontr();
                                if (word[i] == "true") t.Blank = true; else t.Blank = false;
                                j++;
                                break;
                            }
                            if (j == 2)
                            {
                                t.Date1 = word[i];
                                j++;
                                break;
                            }
                            if (j == 3)
                            {
                                t.Date2 = word[i];
                                j++;
                                break;
                            }
                            if (j == 4)
                            {
                                t.Date3 = word[i];
                                j++;
                                break;
                            }
                            if (j == 5)
                            {
                                t.Date4 = word[i];
                                j++;
                                break;
                            }
                            if (j == 6)
                            {
                                t.Date_ic = word[i];
                                j++;
                                break;
                            }
                            if (j == 7)
                            {
                                t.Dolg = word[i];
                                j++;
                                break;
                            }
                            if (j == 8)
                            {
                                t.FIO = word[i];
                                j++;
                                break;
                            }
                            if (j == 9)
                            {
                                if (word[i] == "true") t.FIZO = true; else t.FIZO = false; j++; break;
                            }
                            if (j == 10)
                            {
                                if (word[i] == "true") t.Ic = true; else t.Ic = false; j++; break;
                            }
                            if (j == 11)
                            {
                                t.Lnumber = word[i];
                                j++;
                                break;
                            }
                            if (j == 12)
                            {
                                t.Podr = word[i];
                                j++;
                                break;
                            }
                            if (j == 13)
                            {
                                t.Primec = word[i];
                                j++;
                                break;
                            }
                            if (j == 14)
                            {
                                if (word[i] == "true") t.Prof = true; else t.Prof = false; j++; break;
                            }
                            if (j == 15)
                            {
                                if (word[i] == "true") t.Raport = true; else t.Raport = false; j++; break;
                            }
                            if (j == 16)
                            {
                                t.Types = word[i]; j++;
                                break;
                            }
                            if (j == 17)
                            {
                                if (word[i] == "true") t.Vvk = true; else t.Vvk = false; j++; break;
                            }
                            if (j == 18)
                            {
                                t.Zvanie = word[i];
                                otob.Add(t); j = 1;
                                break;
                            }
                            break;
                        }
                    case 2:
                        {
                            if (j == 1)
                            {
                                t = new Otbor_na_kontr();
                                if (word[i] == "true") t.Blank = true; else t.Blank = false; j++; break;
                            }
                            if (j == 2)
                            {
                                t.Date1 = word[i]; j++;
                                break;
                            }
                            if (j == 3)
                            {
                                t.Date2 = word[i]; j++;
                                break;
                            }
                            if (j == 4)
                            {
                                t.Date3 = word[i]; j++;
                                break;
                            }
                            if (j == 5)
                            {
                                t.Date4 = word[i]; j++;
                                break;
                            }
                            if (j == 6)
                            {
                                t.Date_ic = word[i]; j++;
                                break;
                            }
                            if (j == 7)
                            {
                                t.Dolg = word[i]; j++;
                                break;
                            }
                            if (j == 8)
                            {
                                t.FIO = word[i]; j++;
                                break;
                            }
                            if (j == 9)
                            {
                                if (word[i] == "true") t.FIZO = true; else t.FIZO = false; j++; break;
                            }
                            if (j == 10)
                            {
                                if (word[i] == "true") t.Ic = true; else t.Ic = false; j++; break;
                            }
                            if (j == 11)
                            {
                                t.Lnumber = word[i]; j++;
                                break;
                            }
                            if (j == 12)
                            {
                                t.Podr = word[i]; j++;
                                break;
                            }
                            if (j == 13)
                            {
                                t.Primec = word[i]; j++;
                                break;
                            }
                            if (j == 14)
                            {
                                if (word[i] == "true") t.Prof = true; else t.Prof = false; j++; break;
                            }
                            if (j == 15)
                            {
                                if (word[i] == "true") t.Raport = true; else t.Raport = false; j++; break;
                            }
                            if (j == 16)
                            {
                                t.Types = word[i]; j++;
                                break;
                            }
                            if (j == 17)
                            {
                                if (word[i] == "true") t.Vvk = true; else t.Vvk = false; j++; break;
                            }
                            if (j == 18)
                            {
                                t.Zvanie = word[i]; j = 1;
                                pered.Add(t);
                                break;
                            }
                            break;
                        }
                    case 3:
                        {
                            if (j == 1)
                            {
                                t = new Otbor_na_kontr();
                                if (word[i] == "true") t.Blank = true; else t.Blank = false; j++; break;
                            }
                            if (j == 2)
                            {
                                t.Date1 = word[i]; j++;
                                break;
                            }
                            if (j == 3)
                            {
                                t.Date2 = word[i]; j++;
                                break;
                            }
                            if (j == 4)
                            {
                                t.Date3 = word[i]; j++;
                                break;
                            }
                            if (j == 5)
                            {
                                t.Date4 = word[i]; j++;
                                break;
                            }
                            if (j == 6)
                            {
                                t.Date_ic = word[i]; j++;
                                break;
                            }
                            if (j == 7)
                            {
                                t.Dolg = word[i]; j++;
                                break;
                            }
                            if (j == 8)
                            {
                                t.FIO = word[i]; j++;
                                break;
                            }
                            if (j == 9)
                            {
                                if (word[i] == "true") t.FIZO = true; else t.FIZO = false; j++; break;
                            }
                            if (j == 10)
                            {
                                if (word[i] == "true") t.Ic = true; else t.Ic = false; j++; break;
                            }
                            if (j == 11)
                            {
                                t.Lnumber = word[i]; j++;
                                break;
                            }
                            if (j == 12)
                            {
                                t.Podr = word[i]; j++;
                                break;
                            }
                            if (j == 13)
                            {
                                t.Primec = word[i]; j++;
                                break;
                            }
                            if (j == 14)
                            {
                                if (word[i] == "true") t.Prof = true; else t.Prof = false; j++; break;
                            }
                            if (j == 15)
                            {
                                if (word[i] == "true") t.Raport = true; else t.Raport = false; j++; break;
                            }
                            if (j == 16)
                            {
                                t.Types = word[i]; j++;
                                break;
                            }
                            if (j == 17)
                            {
                                if (word[i] == "true") t.Vvk = true; else t.Vvk = false; j++; break;
                            }
                            if (j == 18)
                            {
                                t.Zvanie = word[i]; j = 1;
                                sost.Add(t);
                                break;
                            }
                            break;
                        }
                    case 4:
                        {
                            if (j == 1)
                            {
                                t = new Otbor_na_kontr();
                                if (word[i] == "true") t.Blank = true; else t.Blank = false; j++; break;
                            }
                            if (j == 2)
                            {
                                t.Date1 = word[i]; j++;
                                break;
                            }
                            if (j == 3)
                            {
                                t.Date2 = word[i]; j++;
                                break;
                            }
                            if (j == 4)
                            {
                                t.Date3 = word[i]; j++;
                                break;
                            }
                            if (j == 5)
                            {
                                t.Date4 = word[i]; j++;
                                break;
                            }
                            if (j == 6)
                            {
                                t.Date_ic = word[i]; j++;
                                break;
                            }
                            if (j == 7)
                            {
                                t.Dolg = word[i]; j++;
                                break;
                            }
                            if (j == 8)
                            {
                                t.FIO = word[i]; j++;
                                break;
                            }
                            if (j == 9)
                            {
                                if (word[i] == "true") t.FIZO = true; else t.FIZO = false; j++; break;
                            }
                            if (j == 10)
                            {
                                if (word[i] == "true") t.Ic = true; else t.Ic = false; j++; break;
                            }
                            if (j == 11)
                            {
                                t.Lnumber = word[i]; j++;
                                break;
                            }
                            if (j == 12)
                            {
                                t.Podr = word[i]; j++;
                                break;
                            }
                            if (j == 13)
                            {
                                t.Primec = word[i]; j++;
                                break;
                            }
                            if (j == 14)
                            {
                                if (word[i] == "true") t.Prof = true; else t.Prof = false; j++; break;
                            }
                            if (j == 15)
                            {
                                if (word[i] == "true") t.Raport = true; else t.Raport = false; j++; break;
                            }
                            if (j == 16)
                            {
                                t.Types = word[i]; j++;
                                break;
                            }
                            if (j == 17)
                            {
                                if (word[i] == "true") t.Vvk = true; else t.Vvk = false; j++; break;
                            }
                            if (j == 18)
                            {
                                t.Zvanie = word[i]; j = 1;
                                otkaz.Add(t);
                                break;
                            }
                            break;
                        }
                }
            }
        }
    }
}
