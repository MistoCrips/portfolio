﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model
{
    public class OpenLoadKotel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        int id_ = 0;
        private string lnumber = ".";
        private string types = ".";
        private string podr = ".";
        private string fio = ".";
        ObservableCollection<OpenLoadKotel> Kotel_List = new ObservableCollection<OpenLoadKotel>();
        public string Lnumber
        {
            get { return lnumber; }
            set
            {
                if (lnumber == value)
                    return;
                lnumber = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Lnumber"));
            }
        }
        public int Id
        {
            get { return id_; }
            set
            {
                if (Id == value)
                    return;
                Id = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Id"));
            }
        }
        public string Podr
        {
            get { return podr; }
            set
            {
                if (podr == value)
                    return;
                podr = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Podr"));
            }
        }
        public string FIO
        {
            get { return fio; }
            set
            {
                if (fio == value)
                    return;
                fio = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FIO"));
            }
        }
        public string Types
        {
            get { return types; }
            set
            {
                if (types == value)
                    return;
                types = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public OpenLoadKotel()
        {

        }
        public OpenLoadKotel(XElement element)
        {
            Lnumber = element.Attribute("lnumber").Value;
        }
        public ObservableCollection<OpenLoadKotel> Sbor_Del()
        {
            string filename = Environment.CurrentDirectory + "\\Recources\\dokum\\kotel.xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                Kotel_List.Add(new OpenLoadKotel(el));
            }
            return Kotel_List;
        }


    }//Учет питающихся в столовой по-фамильно
}
