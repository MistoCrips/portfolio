﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestSuperBrands.Models
{
    public class Products
    {
        [Key]
        public int id { get; set; }

        public string Name { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreateTime { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DeleteTime { get; set; }

        [ForeignKey("Categories")]
        public int Categories_Id { get; set; }

        public Categories Categories { get; set; }

        public ICollection<AttributeValues> Values { get; set; }

        public Products()
        {
            Values = new List<AttributeValues>();
        }
    }
}