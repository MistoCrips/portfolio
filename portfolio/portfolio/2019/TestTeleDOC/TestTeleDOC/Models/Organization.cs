﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TestTeleDOC.Models
{
    public class Organization
    {
        [Key]
        public int OrganizationId { get; set; } 
        public string Name { get; set; }


        [RegularExpression(@"(\d{12})|(\d{10})", ErrorMessage = "Допускается только числовой ввод. Для юридических лиц в колличестве 10 символов, для физических лиц в колличестве 12 символов")]
        public string INN { get; set; }
        public ICollection<Founder> Founders { get; set; }
        public Organization()
        {
            Founders = new List<Founder>();
        }

    }
    public class OrganizationContext : DbContext
    {
        public DbSet<Founder> Founders { get; set; }
        public DbSet<Organization> Organizations { get; set; }
    }
}