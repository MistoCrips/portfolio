﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;


namespace New_Stroevaya_chast.Modal.Model
{
    public class Config_client : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        string ln_ = ".";
        string fio_ = ".";
        int level_ = 0;
        int num_ = 0;
        string status_ = ".";
        string login_ = ".";
        string pass_ = ".";
        string type_ = ".";

        public string LN
        {
            get { return ln_; }
            set
            {
                if (ln_ == value)
                    return;
                ln_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("LN"));
            }
        }
        public string FIO
        {
            get { return fio_; }
            set
            {
                if (fio_ == value)
                    return;
                fio_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FIO"));
            }
        }
        public int Level
        {
            get { return level_; }
            set
            {
                if (level_ == value)
                    return;
                level_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Level"));
            }
        }
        public string Type_dop
        {
            get { return type_; }
            set
            {
                if (type_ == value)
                    return;
                type_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Type_dop"));
            }
        }
        public string Status
        {
            get { return status_; }
            set
            {
                if (status_ == value)
                    return;
                status_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Status"));
            }
        }
        public string Login
        {
            get { return login_; }
            set
            {
                if (login_ == value)
                    return;
                login_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Login"));
            }
        }
        public string Pass
        {
            get { return pass_; }
            set
            {
                if (pass_ == value)
                    return;
                pass_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Pass"));
            }
        }
        public int Num
        {
            get { return num_; }
            set
            {
                if (num_ == value)
                    return;
                num_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Num"));
            }
        }
        public void ToXml()
        {

            XDocument doc;
            if (File.Exists(Environment.CurrentDirectory + "\\Recources\\Profile\\" + Login + ".xml") == false)
            {
                string fileName = Environment.CurrentDirectory + "\\Recources\\Profile\\" + Login + ".xml";
                doc = new XDocument(
                            new XElement("base",
                             new XElement("track",
                                new XAttribute("lnumber", LN),
                                new XAttribute("login", Login),
                                new XAttribute("pass", Pass),
                                new XAttribute("type_bottom", Level))));
                doc.Save(fileName);
            }
            else
            {
                string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\Profile\\" + Login + ".xml";
                doc = XDocument.Load(fileName);
                doc.Descendants().Where(e => e.Name == "track").Remove();
                XElement track = new XElement("track");
                track.Add(new XAttribute("lnumber", LN));
                track.Add(new XAttribute("login", Login));
                track.Add(new XAttribute("pass", Pass));
                track.Add(new XAttribute("type_bottom", Level));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }
        public ObservableCollection<Config_client> Sbor()
        {
            ObservableCollection<Config_client> t = new ObservableCollection<Config_client>();
            DirectoryInfo dir = new DirectoryInfo(Environment.CurrentDirectory + "\\Recources\\Profile");
            string filename = Environment.CurrentDirectory + "\\Recources\\Profile.xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                Config_client n = new Config_client();
                n.LN = el.Attribute("lnumber").Value;
                n.Login = el.Attribute("login").Value;
                n.Pass = el.Attribute("pass").Value;
                n.Level = int.Parse(el.Attribute("type_bottom").Value);
                if (n.Level == 7)
                    n.Type_dop = "комплектование";
                if (n.Level == 9)
                    n.Type_dop = "служба войск";
                if (n.Level == 8)
                    n.Type_dop = "АХЧ";
                if (n.Level == 3)
                    n.Type_dop = "кадры по офицерам";
                if (n.Level == 4)
                    n.Type_dop = "кадры по прапорщикам";
                if (n.Level == 5)
                    n.Type_dop = "кадры по сержантам";
                if (n.Level == 6)
                    n.Type_dop = "кадры по солдатам";
                n.Status = "Не подключен";
                if (File.Exists(filename = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + n.LN + ".xml"))
                {
                    doc = XDocument.Load(filename);
                    foreach (XElement ele in doc.Root.Elements("track_dan"))
                    {
                        n.FIO = ele.Attribute("fio").Value;
                    }
                }
                else n.FIO = "Не опознан";
                t.Add(n);
            }
            return t;
        }
    }
}
