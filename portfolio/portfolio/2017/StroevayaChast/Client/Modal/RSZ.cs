﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using New_Stroevaya_chast.Modal.Model;

namespace Client.Modal
{
    public class RSZ
    {
        public ObservableCollection<lic_rsz> RSZZ(ObservableCollection<OpenLoad> list_shtat,string name, string Level)
        {
            New_schet(list_shtat,name, Level);
            Pods_otryv();
            return MList;
        }

        ObservableCollection<lic_rsz> MList = new ObservableCollection<lic_rsz>();
        int sht_of = 0,
        sht_prap = 0,
        sht_serg = 0,
        sht_sold = 0,
        sht_vsego = 0;

        int spisok_of = 0,
            spisok_prap = 0,
            spisok_serg_vsego = 0,
            spisok_serg_kontr = 0,
            spisok_serg_sroch = 0,
            spisok_sold_vsego = 0,
            spisok_sold_kontr = 0,
            spisok_sold_sroch = 0,
            spisok_serg_sold_vsego = 0,
            spisok_serg_sold_kontr = 0,
            spisok_serg_sold_sold = 0,
            spisok_vsego = 0,
            spisok_ukompl = 0;

        int lico_of = 0,
            lico_prap = 0,
            lico_serg_vsego = 0,
            lico_serg_kontr = 0,
            lico_serg_sroch = 0,
            lico_sold_vsego = 0,
            lico_sold_kontr = 0,
            lico_sold_sroch = 0,
            lico_serg_sold_vsego = 0,
            lico_serg_sold_kontr = 0,
            lico_serg_sold_sold = 0,
            lico_vsego = 0;

        int Med = 0,
            Gos = 0,
            Naryad = 0,
            Otp = 0,
            Kom = 0,
            Dr_pr = 0,
            Arest = 0,
            SOCH = 0;

        public void New_schet(ObservableCollection<OpenLoad> list_shtat,string name, string Level)
        {
            for (int i = 0; i < list_shtat.Count; i++)
            {
                string[] word1 = list_shtat[i].Podr.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                list_shtat[i].Podr1 = word1[0];
                if (word1.Length == 1) { list_shtat[i].Podr2 = "управ"; } else { list_shtat[i].Podr2 = word1[1]; }
                if (word1.Length == 3) { list_shtat[i].Podr3 = word1[2]; }
                if (word1.Length == 4) { list_shtat[i].Podr4 = word1[3]; }
            }
            var list = list_shtat.Select(e => e.Podr1).Distinct();

            if(Level == "1")
                foreach (string s in list)
                {
                    //Здесь создание нового экземпляра MList
                    MList.Add(Podr_scet(list_shtat, s, "1"));
                    //Saving MList
                }
            if (Level == "2")
            {
                List<string> List1 = new List<string>();
                var list2 = list_shtat.Select(e => e.Podr2).Distinct();
                foreach (string n in list2)
                    List1.Add(n);
                foreach (string n in list)
                {
                    if (n != name) continue;
                    //Здесь создание нового экземпляра MList
                    for (int i = 0; i < list_shtat.Count; i++)
                    {
                        
                        if (list_shtat[i].Podr1 != n)
                        {
                            List1.Remove(list_shtat[i].Podr2);
                        }
                    }
                    foreach (string s in List1)
                    {
                        MList.Add(Podr_scet(list_shtat, s, "2"));
                    }
                }
            }
                if (Level == "3")
                {
                    List<string> List1 = new List<string>();
                var list2 = list_shtat.Select(e => e.Podr2).Distinct();
                foreach (string n in list2)
                    List1.Add(n);
                    List<string> List2 = new List<string>();
                    var list3 = list_shtat.Select(e => e.Podr3).Distinct();
                    foreach (string n in list3)
                    List2.Add(n);
                    foreach (string n in list)
                    {
                    if (n != name) continue;
                    //Здесь создание нового экземпляра MList
                    for (int i = 0; i < list_shtat.Count; i++)
                        {
                            if (list_shtat[i].Podr1 != n)
                            {
                                List1.Remove(list_shtat[i].Podr2);
                            }
                        }
                        // save MList
                        foreach (string p in List1)
                        {
                            //Здесь создание нового экземпляра MList
                            for (int i = 0; i < list_shtat.Count; i++)
                            {
                                if (list_shtat[i].Podr2 != p)
                                {
                                    List2.Remove(list_shtat[i].Podr2);
                                }
                            }
                            foreach (string s in List2)
                            {
                                MList.Add(Podr_scet(list_shtat, s, "3"));
                            }
                            // save MList
                        }
                    }
            }


        }
        OpenLoad tmp = new OpenLoad();
        public void RSZ_( string message, int num) 
        {
            
        }
        public void Pods_otryv() //функция для формирования итога
        {
            lic_rsz t = new lic_rsz();
            t.Podr = "Итого";
            foreach (lic_rsz m in MList)
            {
                t.lico_of += m.lico_of;
                t.lico_prap += m.lico_prap;
                t.lico_serg_kontr += m.lico_serg_kontr;
                t.lico_serg_priz += m.lico_serg_priz;
                t.lico_serg_sold_kontr += m.lico_serg_sold_kontr;
                t.lico_serg_sold_priz += m.lico_serg_sold_priz;
                t.lico_serg_sold_vsego += m.lico_serg_sold_vsego;
                t.lico_serg_vsego += m.lico_serg_vsego;
                t.lico_sold_kontr += m.lico_sold_kontr;
                t.lico_sold_priz += m.lico_sold_priz;
                t.lico_sold_vsego += m.lico_sold_vsego;
                t.lico_vsego += m.lico_vsego;
                t.ots_arest += m.ots_arest;
                t.ots_dr_pr += m.ots_dr_pr;
                t.ots_hospital += m.ots_hospital;
                t.ots_kom += m.ots_kom;
                t.ots_med += m.ots_med;
                t.ots_naryad += m.ots_naryad;
                t.ots_otpusk += m.ots_otpusk;
                t.ots_pol += m.ots_pol;
                t.ots_soch += m.ots_soch;
                t.ots_vsego += m.ots_vsego;
                t.shtat_of += m.shtat_of;
                t.shtat_prap += m.shtat_prap;
                t.shtat_ser += m.shtat_ser;
                t.shtat_sold += m.shtat_sold;
                t.shtat_vsego += m.shtat_vsego;
                t.spisok_of += m.spisok_of;
                t.spisok_prap += m.spisok_prap;
                t.spisok_serg_kontr += m.spisok_serg_kontr;
                t.spisok_serg_priz += m.spisok_serg_priz;
                t.spisok_serg_sold_kontr += m.spisok_serg_sold_kontr;
                t.spisok_serg_sold_priz += m.spisok_serg_sold_priz;
                t.spisok_serg_sold_vsego += m.spisok_serg_sold_vsego;
                t.spisok_serg_vsego += m.spisok_serg_vsego;
                t.spisok_sold_kontr += m.spisok_sold_kontr;
                t.spisok_sold_priz += m.spisok_sold_priz;
                t.spisok_sold_vsego += m.spisok_sold_vsego;
                t.spisok_vsego += m.spisok_vsego;
                if (t.spisok_vsego != 0)
                    t.spisok_ukompl = t.spisok_vsego / t.shtat_vsego * 100;
                else
                    t.spisok_ukompl = 0;
            }
            MList.Add(t);
        }
        public lic_rsz Podr_scet(ObservableCollection<OpenLoad> list_shtat, string message, string level)
        {
            lic_rsz dt_ = new lic_rsz();
            if (level == "1")
            {
                
                dt_.Podr = message;
                for (int i = 0; i < list_shtat.Count; i++)
                {
                    if (list_shtat[i].Podr1 == message)
                    {


                        if (list_shtat[i].Shtat_type == "О")
                        {
                            sht_of++;
                            if (list_shtat[i].Fio != "-В-")
                            {
                                spisok_of++;
                                if (list_shtat[i].Otryv_status != ".")
                                {
                                    if (list_shtat[i].Otryv_status == "госпиталь")
                                    {
                                        Gos++;
                                    }
                                    if (list_shtat[i].Otryv_status == "наряд")
                                    {
                                        Naryad++;
                                    }
                                    if (list_shtat[i].Otryv_status == "арест")
                                    {
                                        Arest++;
                                    }
                                    if (list_shtat[i].Otryv_status == "полигон")
                                    {
                                        Kom++;

                                    }
                                    if (list_shtat[i].Otryv_status == "СОЧ")
                                    {
                                        SOCH++;
                                    }
                                    if (list_shtat[i].Otryv_status == "другие причины")
                                    {
                                        Dr_pr++;
                                    }
                                    if (list_shtat[i].Otryv_status == "командировка")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "мед. рота")
                                    {
                                        Med++;
                                    }
                                    if (list_shtat[i].Otryv_status == "отпуск")
                                    {
                                        Otp++;
                                    }
                                }
                                else lico_of++;
                            }



                        }
                        else
                        if (list_shtat[i].Shtat_type == "пр-к")
                        {
                            sht_prap++;
                            if (list_shtat[i].Fio != "-В-")
                            {
                                spisok_prap++;
                                if (list_shtat[i].Otryv_status != ".")
                                {
                                    if (list_shtat[i].Otryv_status == "госпиталь")
                                    {
                                        Gos++;
                                    }
                                    if (list_shtat[i].Otryv_status == "наряд")
                                    {
                                        Naryad++;
                                    }
                                    if (list_shtat[i].Otryv_status == "арест")
                                    {
                                        Arest++;
                                    }
                                    if (list_shtat[i].Otryv_status == "полигон")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "СОЧ")
                                    {
                                        SOCH++;
                                    }
                                    if (list_shtat[i].Otryv_status == "другие причины")
                                    {
                                        Dr_pr++;
                                    }
                                    if (list_shtat[i].Otryv_status == "командировка")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "мед. рота")
                                    {
                                        Med++;
                                    }
                                    if (list_shtat[i].Otryv_status == "отпуск")
                                    {
                                        Otp++;
                                    }
                                }
                                else lico_prap++;
                            }


                        }
                        else
                        if (list_shtat[i].Shtat_type == "сер")
                        {
                            sht_serg++;
                            if (list_shtat[i].Fio != "-В-")
                            {
                                if (list_shtat[i].Types == "контр" || list_shtat[i].Types == "жен")
                                {
                                    spisok_serg_kontr++;
                                }
                                else spisok_serg_sroch++;
                                if (list_shtat[i].Otryv_status != ".")
                                {
                                    if (list_shtat[i].Otryv_status == "госпиталь")
                                    {
                                        Gos++;
                                    }
                                    if (list_shtat[i].Otryv_status == "наряд")
                                    {
                                        Naryad++;
                                    }
                                    if (list_shtat[i].Otryv_status == "арест")
                                    {
                                        Arest++;
                                    }
                                    if (list_shtat[i].Otryv_status == "полигон")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "СОЧ")
                                    {
                                        SOCH++;
                                    }
                                    if (list_shtat[i].Otryv_status == "другие причины")
                                    {
                                        Dr_pr++;
                                    }
                                    if (list_shtat[i].Otryv_status == "командировка")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "мед. рота")
                                    {
                                        Med++;
                                    }
                                    if (list_shtat[i].Otryv_status == "отпуск")
                                    {
                                        Otp++;
                                    }
                                }
                                else if (list_shtat[i].Types == "контр" || list_shtat[i].Types == "жен")
                                {
                                    lico_serg_kontr++;
                                }
                                else lico_serg_sroch++;
                            }


                        }
                        else
                        if (list_shtat[i].Shtat_type == "ряд")
                        {
                            sht_sold++;
                            if (list_shtat[i].Fio != "-В-")
                            {
                                if (list_shtat[i].Types == "контр" || list_shtat[i].Types == "жен")
                                {
                                    spisok_sold_kontr++;
                                }
                                else spisok_sold_sroch++;
                                if (list_shtat[i].Otryv_status != ".")
                                {
                                    if (list_shtat[i].Otryv_status == "госпиталь")
                                    {
                                        Gos++;
                                    }
                                    if (list_shtat[i].Otryv_status == "наряд")
                                    {
                                        Naryad++;
                                    }
                                    if (list_shtat[i].Otryv_status == "арест")
                                    {
                                        Arest++;
                                    }
                                    if (list_shtat[i].Otryv_status == "полигон")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "СОЧ")
                                    {
                                        SOCH++;
                                    }
                                    if (list_shtat[i].Otryv_status == "другие причины")
                                    {
                                        Dr_pr++;
                                    }
                                    if (list_shtat[i].Otryv_status == "командировка")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "мед. рота")
                                    {
                                        Med++;
                                    }
                                    if (list_shtat[i].Otryv_status == "отпуск")
                                    {
                                        Otp++;
                                    }
                                }
                                else if (list_shtat[i].Types == "контр" || list_shtat[i].Types == "жен")
                                {
                                    lico_sold_kontr++;
                                }
                                else lico_sold_sroch++;
                            }


                        }
                    }

                }

                //dt_.Podr = podr2;
                //штат
                dt_.shtat_of = sht_of;
                dt_.shtat_prap = sht_prap;
                dt_.shtat_ser = sht_serg;
                dt_.shtat_sold = sht_sold;
                dt_.shtat_vsego = sht_of + sht_prap + sht_serg + sht_sold;
                //список
                dt_.spisok_of = spisok_of;
                dt_.spisok_prap = spisok_prap;
                dt_.spisok_serg_kontr = spisok_serg_kontr;
                dt_.spisok_serg_priz = spisok_serg_sroch;
                dt_.spisok_serg_vsego = spisok_serg_sroch + spisok_serg_kontr;

                dt_.spisok_sold_kontr = spisok_sold_kontr;
                dt_.spisok_sold_priz = spisok_sold_sroch;
                dt_.spisok_sold_vsego = spisok_sold_sroch + spisok_sold_kontr;

                dt_.spisok_serg_sold_kontr = spisok_serg_kontr + spisok_sold_kontr;
                dt_.spisok_serg_sold_priz = spisok_serg_sroch + spisok_sold_sroch;
                dt_.spisok_serg_sold_vsego = spisok_serg_kontr + spisok_sold_kontr + spisok_serg_sroch + spisok_sold_sroch;
                dt_.spisok_vsego = dt_.spisok_sold_vsego + dt_.spisok_serg_vsego + dt_.spisok_of + dt_.spisok_prap;
                if (dt_.spisok_vsego != 0)
                    dt_.spisok_ukompl = dt_.spisok_vsego / dt_.shtat_vsego * 100;
                else dt_.spisok_ukompl = 0;
                //Лицо
                dt_.lico_of = lico_of;
                dt_.lico_prap = lico_prap;

                dt_.lico_serg_kontr = lico_serg_kontr;
                dt_.lico_serg_priz = lico_serg_sroch;
                dt_.lico_serg_vsego = lico_serg_kontr + lico_serg_sroch;

                dt_.lico_sold_kontr = lico_sold_kontr;
                dt_.lico_sold_priz = lico_sold_sroch;
                dt_.lico_sold_vsego = lico_sold_kontr + lico_sold_sroch;

                dt_.lico_serg_sold_kontr = lico_serg_kontr + lico_sold_kontr;
                dt_.lico_serg_sold_priz = lico_serg_sroch + lico_sold_sroch;
                dt_.lico_serg_sold_vsego = lico_serg_kontr + lico_sold_kontr + lico_serg_sroch + lico_sold_sroch;
                dt_.lico_vsego = dt_.lico_serg_vsego + dt_.lico_sold_vsego + dt_.lico_of + dt_.lico_prap;

                dt_.ots_arest = Arest;
                dt_.ots_dr_pr = Dr_pr;
                dt_.ots_hospital = Gos;
                dt_.ots_kom = Kom;
                dt_.ots_med = Med;
                dt_.ots_naryad = Naryad;
                dt_.ots_otpusk = Otp;
                dt_.ots_soch = SOCH;
                dt_.ots_vsego = dt_.ots_arest + dt_.ots_dr_pr + dt_.ots_hospital + dt_.ots_kom + dt_.ots_med + dt_.ots_naryad + dt_.ots_otpusk + dt_.ots_soch;


                sht_of = 0;
                sht_prap = 0;
                sht_serg = 0;
                sht_sold = 0;
                sht_vsego = 0;

                spisok_of = 0;
                spisok_prap = 0;
                spisok_serg_vsego = 0;
                spisok_serg_kontr = 0;
                spisok_serg_sroch = 0;
                spisok_sold_vsego = 0;
                spisok_sold_kontr = 0;
                spisok_sold_sroch = 0;
                spisok_serg_sold_vsego = 0;
                spisok_serg_sold_kontr = 0;
                spisok_serg_sold_sold = 0;
                spisok_vsego = 0;
                spisok_ukompl = 0;

                lico_of = 0;
                lico_prap = 0;
                lico_serg_vsego = 0;
                lico_serg_kontr = 0;
                lico_serg_sroch = 0;
                lico_sold_vsego = 0;
                lico_sold_kontr = 0;
                lico_sold_sroch = 0;
                lico_serg_sold_vsego = 0;
                lico_serg_sold_kontr = 0;
                lico_serg_sold_sold = 0;
                lico_vsego = 0;

                Med = 0;
                Gos = 0;
                Naryad = 0;
                Otp = 0;
                Kom = 0;
                Dr_pr = 0;
                Arest = 0;
                SOCH = 0;
            }
            else
                if(level == "2")
            {
                dt_.Podr = message;
                for (int i = 0; i < list_shtat.Count; i++)
                {
                    if (list_shtat[i].Podr2 == message)
                    {


                        if (list_shtat[i].Shtat_type == "О")
                        {
                            sht_of++;
                            if (list_shtat[i].Fio != "-В-")
                            {
                                spisok_of++;
                                if (list_shtat[i].Otryv_status != ".")
                                {
                                    if (list_shtat[i].Otryv_status == "госпиталь")
                                    {
                                        Gos++;
                                    }
                                    if (list_shtat[i].Otryv_status == "наряд")
                                    {
                                        Naryad++;
                                    }
                                    if (list_shtat[i].Otryv_status == "арест")
                                    {
                                        Arest++;
                                    }
                                    if (list_shtat[i].Otryv_status == "полигон")
                                    {
                                        Kom++;

                                    }
                                    if (list_shtat[i].Otryv_status == "СОЧ")
                                    {
                                        SOCH++;
                                    }
                                    if (list_shtat[i].Otryv_status == "другие причины")
                                    {
                                        Dr_pr++;
                                    }
                                    if (list_shtat[i].Otryv_status == "командировка")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "мед. рота")
                                    {
                                        Med++;
                                    }
                                    if (list_shtat[i].Otryv_status == "отпуск")
                                    {
                                        Otp++;
                                    }
                                }
                                else lico_of++;
                            }



                        }
                        else
                        if (list_shtat[i].Shtat_type == "пр-к")
                        {
                            sht_prap++;
                            if (list_shtat[i].Fio != "-В-")
                            {
                                spisok_prap++;
                                if (list_shtat[i].Otryv_status != ".")
                                {
                                    if (list_shtat[i].Otryv_status == "госпиталь")
                                    {
                                        Gos++;
                                    }
                                    if (list_shtat[i].Otryv_status == "наряд")
                                    {
                                        Naryad++;
                                    }
                                    if (list_shtat[i].Otryv_status == "арест")
                                    {
                                        Arest++;
                                    }
                                    if (list_shtat[i].Otryv_status == "полигон")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "СОЧ")
                                    {
                                        SOCH++;
                                    }
                                    if (list_shtat[i].Otryv_status == "другие причины")
                                    {
                                        Dr_pr++;
                                    }
                                    if (list_shtat[i].Otryv_status == "командировка")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "мед. рота")
                                    {
                                        Med++;
                                    }
                                    if (list_shtat[i].Otryv_status == "отпуск")
                                    {
                                        Otp++;
                                    }
                                }
                                else lico_prap++;
                            }


                        }
                        else
                        if (list_shtat[i].Shtat_type == "сер")
                        {
                            sht_serg++;
                            if (list_shtat[i].Fio != "-В-")
                            {
                                if (list_shtat[i].Types == "контр" || list_shtat[i].Types == "жен")
                                {
                                    spisok_serg_kontr++;
                                }
                                else spisok_serg_sroch++;
                                if (list_shtat[i].Otryv_status != ".")
                                {
                                    if (list_shtat[i].Otryv_status == "госпиталь")
                                    {
                                        Gos++;
                                    }
                                    if (list_shtat[i].Otryv_status == "наряд")
                                    {
                                        Naryad++;
                                    }
                                    if (list_shtat[i].Otryv_status == "арест")
                                    {
                                        Arest++;
                                    }
                                    if (list_shtat[i].Otryv_status == "полигон")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "СОЧ")
                                    {
                                        SOCH++;
                                    }
                                    if (list_shtat[i].Otryv_status == "другие причины")
                                    {
                                        Dr_pr++;
                                    }
                                    if (list_shtat[i].Otryv_status == "командировка")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "мед. рота")
                                    {
                                        Med++;
                                    }
                                    if (list_shtat[i].Otryv_status == "отпуск")
                                    {
                                        Otp++;
                                    }
                                }
                                else if (list_shtat[i].Types == "контр" || list_shtat[i].Types == "жен")
                                {
                                    lico_serg_kontr++;
                                }
                                else lico_serg_sroch++;
                            }


                        }
                        else
                        if (list_shtat[i].Shtat_type == "ряд")
                        {
                            sht_sold++;
                            if (list_shtat[i].Fio != "-В-")
                            {
                                if (list_shtat[i].Types == "контр" || list_shtat[i].Types == "жен")
                                {
                                    spisok_sold_kontr++;
                                }
                                else spisok_sold_sroch++;
                                if (list_shtat[i].Otryv_status != ".")
                                {
                                    if (list_shtat[i].Otryv_status == "госпиталь")
                                    {
                                        Gos++;
                                    }
                                    if (list_shtat[i].Otryv_status == "наряд")
                                    {
                                        Naryad++;
                                    }
                                    if (list_shtat[i].Otryv_status == "арест")
                                    {
                                        Arest++;
                                    }
                                    if (list_shtat[i].Otryv_status == "полигон")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "СОЧ")
                                    {
                                        SOCH++;
                                    }
                                    if (list_shtat[i].Otryv_status == "другие причины")
                                    {
                                        Dr_pr++;
                                    }
                                    if (list_shtat[i].Otryv_status == "командировка")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "мед. рота")
                                    {
                                        Med++;
                                    }
                                    if (list_shtat[i].Otryv_status == "отпуск")
                                    {
                                        Otp++;
                                    }
                                }
                                else if (list_shtat[i].Types == "контр" || list_shtat[i].Types == "жен")
                                {
                                    lico_sold_kontr++;
                                }
                                else lico_sold_sroch++;
                            }


                        }
                    }

                }

                //dt_.Podr = podr2;
                //штат
                dt_.shtat_of = sht_of;
                dt_.shtat_prap = sht_prap;
                dt_.shtat_ser = sht_serg;
                dt_.shtat_sold = sht_sold;
                dt_.shtat_vsego = sht_of + sht_prap + sht_serg + sht_sold;
                //список
                dt_.spisok_of = spisok_of;
                dt_.spisok_prap = spisok_prap;
                dt_.spisok_serg_kontr = spisok_serg_kontr;
                dt_.spisok_serg_priz = spisok_serg_sroch;
                dt_.spisok_serg_vsego = spisok_serg_sroch + spisok_serg_kontr;

                dt_.spisok_sold_kontr = spisok_sold_kontr;
                dt_.spisok_sold_priz = spisok_sold_sroch;
                dt_.spisok_sold_vsego = spisok_sold_sroch + spisok_sold_kontr;

                dt_.spisok_serg_sold_kontr = spisok_serg_kontr + spisok_sold_kontr;
                dt_.spisok_serg_sold_priz = spisok_serg_sroch + spisok_sold_sroch;
                dt_.spisok_serg_sold_vsego = spisok_serg_kontr + spisok_sold_kontr + spisok_serg_sroch + spisok_sold_sroch;
                dt_.spisok_vsego = dt_.spisok_sold_vsego + dt_.spisok_serg_vsego + dt_.spisok_of + dt_.spisok_prap;
                if (dt_.spisok_vsego != 0)
                    dt_.spisok_ukompl = dt_.spisok_vsego / dt_.shtat_vsego * 100;
                else dt_.spisok_ukompl = 0;
                //Лицо
                dt_.lico_of = lico_of;
                dt_.lico_prap = lico_prap;

                dt_.lico_serg_kontr = lico_serg_kontr;
                dt_.lico_serg_priz = lico_serg_sroch;
                dt_.lico_serg_vsego = lico_serg_kontr + lico_serg_sroch;

                dt_.lico_sold_kontr = lico_sold_kontr;
                dt_.lico_sold_priz = lico_sold_sroch;
                dt_.lico_sold_vsego = lico_sold_kontr + lico_sold_sroch;

                dt_.lico_serg_sold_kontr = lico_serg_kontr + lico_sold_kontr;
                dt_.lico_serg_sold_priz = lico_serg_sroch + lico_sold_sroch;
                dt_.lico_serg_sold_vsego = lico_serg_kontr + lico_sold_kontr + lico_serg_sroch + lico_sold_sroch;
                dt_.lico_vsego = dt_.lico_serg_vsego + dt_.lico_sold_vsego + dt_.lico_of + dt_.lico_prap;

                dt_.ots_arest = Arest;
                dt_.ots_dr_pr = Dr_pr;
                dt_.ots_hospital = Gos;
                dt_.ots_kom = Kom;
                dt_.ots_med = Med;
                dt_.ots_naryad = Naryad;
                dt_.ots_otpusk = Otp;
                dt_.ots_soch = SOCH;
                dt_.ots_vsego = dt_.ots_arest + dt_.ots_dr_pr + dt_.ots_hospital + dt_.ots_kom + dt_.ots_med + dt_.ots_naryad + dt_.ots_otpusk + dt_.ots_soch;


                sht_of = 0;
                sht_prap = 0;
                sht_serg = 0;
                sht_sold = 0;
                sht_vsego = 0;

                spisok_of = 0;
                spisok_prap = 0;
                spisok_serg_vsego = 0;
                spisok_serg_kontr = 0;
                spisok_serg_sroch = 0;
                spisok_sold_vsego = 0;
                spisok_sold_kontr = 0;
                spisok_sold_sroch = 0;
                spisok_serg_sold_vsego = 0;
                spisok_serg_sold_kontr = 0;
                spisok_serg_sold_sold = 0;
                spisok_vsego = 0;
                spisok_ukompl = 0;

                lico_of = 0;
                lico_prap = 0;
                lico_serg_vsego = 0;
                lico_serg_kontr = 0;
                lico_serg_sroch = 0;
                lico_sold_vsego = 0;
                lico_sold_kontr = 0;
                lico_sold_sroch = 0;
                lico_serg_sold_vsego = 0;
                lico_serg_sold_kontr = 0;
                lico_serg_sold_sold = 0;
                lico_vsego = 0;

                Med = 0;
                Gos = 0;
                Naryad = 0;
                Otp = 0;
                Kom = 0;
                Dr_pr = 0;
                Arest = 0;
                SOCH = 0;
            }
            else
                if(level == "3")
            {
                dt_.Podr = message;
                for (int i = 0; i < list_shtat.Count; i++)
                {
                    if (list_shtat[i].Podr2 == message)
                    {


                        if (list_shtat[i].Shtat_type == "О")
                        {
                            sht_of++;
                            if (list_shtat[i].Fio != "-В-")
                            {
                                spisok_of++;
                                if (list_shtat[i].Otryv_status != ".")
                                {
                                    if (list_shtat[i].Otryv_status == "госпиталь")
                                    {
                                        Gos++;
                                    }
                                    if (list_shtat[i].Otryv_status == "наряд")
                                    {
                                        Naryad++;
                                    }
                                    if (list_shtat[i].Otryv_status == "арест")
                                    {
                                        Arest++;
                                    }
                                    if (list_shtat[i].Otryv_status == "полигон")
                                    {
                                        Kom++;

                                    }
                                    if (list_shtat[i].Otryv_status == "СОЧ")
                                    {
                                        SOCH++;
                                    }
                                    if (list_shtat[i].Otryv_status == "другие причины")
                                    {
                                        Dr_pr++;
                                    }
                                    if (list_shtat[i].Otryv_status == "командировка")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "мед. рота")
                                    {
                                        Med++;
                                    }
                                    if (list_shtat[i].Otryv_status == "отпуск")
                                    {
                                        Otp++;
                                    }
                                }
                                else lico_of++;
                            }



                        }
                        else
                        if (list_shtat[i].Shtat_type == "пр-к")
                        {
                            sht_prap++;
                            if (list_shtat[i].Fio != "-В-")
                            {
                                spisok_prap++;
                                if (list_shtat[i].Otryv_status != ".")
                                {
                                    if (list_shtat[i].Otryv_status == "госпиталь")
                                    {
                                        Gos++;
                                    }
                                    if (list_shtat[i].Otryv_status == "наряд")
                                    {
                                        Naryad++;
                                    }
                                    if (list_shtat[i].Otryv_status == "арест")
                                    {
                                        Arest++;
                                    }
                                    if (list_shtat[i].Otryv_status == "полигон")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "СОЧ")
                                    {
                                        SOCH++;
                                    }
                                    if (list_shtat[i].Otryv_status == "другие причины")
                                    {
                                        Dr_pr++;
                                    }
                                    if (list_shtat[i].Otryv_status == "командировка")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "мед. рота")
                                    {
                                        Med++;
                                    }
                                    if (list_shtat[i].Otryv_status == "отпуск")
                                    {
                                        Otp++;
                                    }
                                }
                                else lico_prap++;
                            }


                        }
                        else
                        if (list_shtat[i].Shtat_type == "сер")
                        {
                            sht_serg++;
                            if (list_shtat[i].Fio != "-В-")
                            {
                                if (list_shtat[i].Types == "контр" || list_shtat[i].Types == "жен")
                                {
                                    spisok_serg_kontr++;
                                }
                                else spisok_serg_sroch++;
                                if (list_shtat[i].Otryv_status != ".")
                                {
                                    if (list_shtat[i].Otryv_status == "госпиталь")
                                    {
                                        Gos++;
                                    }
                                    if (list_shtat[i].Otryv_status == "наряд")
                                    {
                                        Naryad++;
                                    }
                                    if (list_shtat[i].Otryv_status == "арест")
                                    {
                                        Arest++;
                                    }
                                    if (list_shtat[i].Otryv_status == "полигон")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "СОЧ")
                                    {
                                        SOCH++;
                                    }
                                    if (list_shtat[i].Otryv_status == "другие причины")
                                    {
                                        Dr_pr++;
                                    }
                                    if (list_shtat[i].Otryv_status == "командировка")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "мед. рота")
                                    {
                                        Med++;
                                    }
                                    if (list_shtat[i].Otryv_status == "отпуск")
                                    {
                                        Otp++;
                                    }
                                }
                                else if (list_shtat[i].Types == "контр" || list_shtat[i].Types == "жен")
                                {
                                    lico_serg_kontr++;
                                }
                                else lico_serg_sroch++;
                            }


                        }
                        else
                        if (list_shtat[i].Shtat_type == "ряд")
                        {
                            sht_sold++;
                            if (list_shtat[i].Fio != "-В-")
                            {
                                if (list_shtat[i].Types == "контр" || list_shtat[i].Types == "жен")
                                {
                                    spisok_sold_kontr++;
                                }
                                else spisok_sold_sroch++;
                                if (list_shtat[i].Otryv_status != ".")
                                {
                                    if (list_shtat[i].Otryv_status == "госпиталь")
                                    {
                                        Gos++;
                                    }
                                    if (list_shtat[i].Otryv_status == "наряд")
                                    {
                                        Naryad++;
                                    }
                                    if (list_shtat[i].Otryv_status == "арест")
                                    {
                                        Arest++;
                                    }
                                    if (list_shtat[i].Otryv_status == "полигон")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "СОЧ")
                                    {
                                        SOCH++;
                                    }
                                    if (list_shtat[i].Otryv_status == "другие причины")
                                    {
                                        Dr_pr++;
                                    }
                                    if (list_shtat[i].Otryv_status == "командировка")
                                    {
                                        Kom++;
                                    }
                                    if (list_shtat[i].Otryv_status == "мед. рота")
                                    {
                                        Med++;
                                    }
                                    if (list_shtat[i].Otryv_status == "отпуск")
                                    {
                                        Otp++;
                                    }
                                }
                                else if (list_shtat[i].Types == "контр" || list_shtat[i].Types == "жен")
                                {
                                    lico_sold_kontr++;
                                }
                                else lico_sold_sroch++;
                            }


                        }
                    }

                }

                //dt_.Podr = podr2;
                //штат
                dt_.shtat_of = sht_of;
                dt_.shtat_prap = sht_prap;
                dt_.shtat_ser = sht_serg;
                dt_.shtat_sold = sht_sold;
                dt_.shtat_vsego = sht_of + sht_prap + sht_serg + sht_sold;
                //список
                dt_.spisok_of = spisok_of;
                dt_.spisok_prap = spisok_prap;
                dt_.spisok_serg_kontr = spisok_serg_kontr;
                dt_.spisok_serg_priz = spisok_serg_sroch;
                dt_.spisok_serg_vsego = spisok_serg_sroch + spisok_serg_kontr;

                dt_.spisok_sold_kontr = spisok_sold_kontr;
                dt_.spisok_sold_priz = spisok_sold_sroch;
                dt_.spisok_sold_vsego = spisok_sold_sroch + spisok_sold_kontr;

                dt_.spisok_serg_sold_kontr = spisok_serg_kontr + spisok_sold_kontr;
                dt_.spisok_serg_sold_priz = spisok_serg_sroch + spisok_sold_sroch;
                dt_.spisok_serg_sold_vsego = spisok_serg_kontr + spisok_sold_kontr + spisok_serg_sroch + spisok_sold_sroch;
                dt_.spisok_vsego = dt_.spisok_sold_vsego + dt_.spisok_serg_vsego + dt_.spisok_of + dt_.spisok_prap;
                if (dt_.spisok_vsego != 0)
                    dt_.spisok_ukompl = dt_.spisok_vsego / dt_.shtat_vsego * 100;
                else dt_.spisok_ukompl = 0;
                //Лицо
                dt_.lico_of = lico_of;
                dt_.lico_prap = lico_prap;

                dt_.lico_serg_kontr = lico_serg_kontr;
                dt_.lico_serg_priz = lico_serg_sroch;
                dt_.lico_serg_vsego = lico_serg_kontr + lico_serg_sroch;

                dt_.lico_sold_kontr = lico_sold_kontr;
                dt_.lico_sold_priz = lico_sold_sroch;
                dt_.lico_sold_vsego = lico_sold_kontr + lico_sold_sroch;

                dt_.lico_serg_sold_kontr = lico_serg_kontr + lico_sold_kontr;
                dt_.lico_serg_sold_priz = lico_serg_sroch + lico_sold_sroch;
                dt_.lico_serg_sold_vsego = lico_serg_kontr + lico_sold_kontr + lico_serg_sroch + lico_sold_sroch;
                dt_.lico_vsego = dt_.lico_serg_vsego + dt_.lico_sold_vsego + dt_.lico_of + dt_.lico_prap;

                dt_.ots_arest = Arest;
                dt_.ots_dr_pr = Dr_pr;
                dt_.ots_hospital = Gos;
                dt_.ots_kom = Kom;
                dt_.ots_med = Med;
                dt_.ots_naryad = Naryad;
                dt_.ots_otpusk = Otp;
                dt_.ots_soch = SOCH;
                dt_.ots_vsego = dt_.ots_arest + dt_.ots_dr_pr + dt_.ots_hospital + dt_.ots_kom + dt_.ots_med + dt_.ots_naryad + dt_.ots_otpusk + dt_.ots_soch;


                sht_of = 0;
                sht_prap = 0;
                sht_serg = 0;
                sht_sold = 0;
                sht_vsego = 0;

                spisok_of = 0;
                spisok_prap = 0;
                spisok_serg_vsego = 0;
                spisok_serg_kontr = 0;
                spisok_serg_sroch = 0;
                spisok_sold_vsego = 0;
                spisok_sold_kontr = 0;
                spisok_sold_sroch = 0;
                spisok_serg_sold_vsego = 0;
                spisok_serg_sold_kontr = 0;
                spisok_serg_sold_sold = 0;
                spisok_vsego = 0;
                spisok_ukompl = 0;

                lico_of = 0;
                lico_prap = 0;
                lico_serg_vsego = 0;
                lico_serg_kontr = 0;
                lico_serg_sroch = 0;
                lico_sold_vsego = 0;
                lico_sold_kontr = 0;
                lico_sold_sroch = 0;
                lico_serg_sold_vsego = 0;
                lico_serg_sold_kontr = 0;
                lico_serg_sold_sold = 0;
                lico_vsego = 0;

                Med = 0;
                Gos = 0;
                Naryad = 0;
                Otp = 0;
                Kom = 0;
                Dr_pr = 0;
                Arest = 0;
                SOCH = 0;
            }
            return dt_;

        }


    }
    public class lic_rsz:INotifyPropertyChanged 
    {
        public lic_rsz()
        {

        }
        public event PropertyChangedEventHandler PropertyChanged;
        string podr_ = ".";
        int shtat1 = 0;
        int shtat2 = 0;
        int shtat3 = 0;
        int shtat4 = 0;
        int shtat5 = 0;

        int spisok1 = 0;
        int spisok2 = 0;
        int spisok3 = 0;
        int spisok4 = 0;
        int spisok5 = 0;
        int spisok6 = 0;
        int spisok7 = 0;
        int spisok8 = 0;
        int spisok9 = 0;
        int spisok10 = 0;
        int spisok11 = 0;
        int spisok12 = 0;
        int spisok13 = 0;

        int lico1 = 0;
        int lico2 = 0;
        int lico3 = 0;
        int lico4 = 0;
        int lico5 = 0;
        int lico6 = 0;
        int lico7 = 0;
        int lico8 = 0;
        int lico9 = 0;
        int lico10 = 0;
        int lico11 = 0;
        int lico12 = 0;

        int ots1 = 0;
        int ots2 = 0;
        int ots3 = 0;
        int ots4 = 0;
        int ots5 = 0;
        int ots6 = 0;
        int ots7 = 0;
        int ots8 = 0;
        int ots9 = 0;
        int ots10 = 0;
        public string Podr
        {
            get { return podr_; }
            set
            {
                if (podr_ == value)
                    return;
                podr_ = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Podr"));
            }
        }
        /// <summary>
        /// /////////////////
        /// </summary>
        public int shtat_of
        {
            get { return shtat1; }
            set
            {
                if (shtat1 == value)
                    return;
                shtat1 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("shtat1"));
            }
        }
        public int shtat_prap
        {
            get { return shtat2; }
            set
            {
                if (shtat2 == value)
                    return;
                shtat2 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("shtat2"));
            }
        }
        public int shtat_ser
        {
            get { return shtat3; }
            set
            {
                if (shtat3 == value)
                    return;
                shtat3 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("shtat3"));
            }
        }
        public int shtat_sold
        {
            get { return shtat4; }
            set
            {
                if (shtat4 == value)
                    return;
                shtat4 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("shtat4"));
            }
        }
        public int shtat_vsego
        {
            get { return shtat5; }
            set
            {
                if (shtat5 == value)
                    return;
                shtat5 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("shtat5"));
            }
        }
        ////////////////////////////////////////
        public int spisok_of
        {
            get { return spisok1; }
            set
            {
                if (spisok1 == value)
                    return;
                spisok1 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("spisok1"));
            }
        }
        public int spisok_prap
        {
            get { return spisok2; }
            set
            {
                if (spisok2 == value)
                    return;
                spisok2 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("spisok2"));
            }
        }
        public int spisok_serg_vsego
        {
            get { return spisok3; }
            set
            {
                if (spisok3 == value)
                    return;
                spisok3 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("spisok3"));
            }
        }
        public int spisok_serg_kontr
        {
            get { return spisok4; }
            set
            {
                if (spisok4 == value)
                    return;
                spisok4 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("spisok4"));
            }
        }
        public int spisok_serg_priz
        {
            get { return spisok5; }
            set
            {
                if (spisok5 == value)
                    return;
                spisok5 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("spisok5"));
            }
        }
        public int spisok_sold_vsego
        {
            get { return spisok6; }
            set
            {
                if (spisok6 == value)
                    return;
                spisok6 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("spisok6"));
            }
        }
        public int spisok_sold_kontr
        {
            get { return spisok7; }
            set
            {
                if (spisok7 == value)
                    return;
                spisok7 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("spisok7"));
            }
        }
        public int spisok_sold_priz
        {
            get { return spisok8; }
            set
            {
                if (spisok8 == value)
                    return;
                spisok8 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("spisok8"));
            }
        }
        public int spisok_serg_sold_vsego
        {
            get { return spisok9; }
            set
            {
                if (spisok9 == value)
                    return;
                spisok9 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("spisok9"));
            }
        }
        public int spisok_serg_sold_kontr
        {
            get { return spisok10; }
            set
            {
                if (spisok10 == value)
                    return;
                spisok10 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("spisok10"));
            }
        }
        public int spisok_serg_sold_priz
        {
            get { return spisok11; }
            set
            {
                if (spisok11 == value)
                    return;
                spisok11 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("spisok11"));
            }
        }
        public int spisok_vsego
        {
            get { return spisok12; }
            set
            {
                if (spisok12 == value)
                    return;
                spisok12 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("spisok12"));
            }
        }
        public int spisok_ukompl
        {
            get { return spisok13; }
            set
            {
                if (spisok13 == value)
                    return;
                spisok13 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("spisok13"));
            }
        }

        //////////////////////////////////////

        public int lico_of
        {
            get { return lico1; }
            set
            {
                if (lico1 == value)
                    return;
                lico1 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("lico1"));
            }
        }
        public int lico_prap
        {
            get { return lico2; }
            set
            {
                if (lico2 == value)
                    return;
                lico2 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("lico2"));
            }
        }
        public int lico_serg_vsego
        {
            get { return lico3; }
            set
            {
                if (lico3 == value)
                    return;
                lico3 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("lico3"));
            }
        }
        public int lico_serg_kontr
        {
            get { return lico4; }
            set
            {
                if (lico4 == value)
                    return;
                lico4 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("lico4"));
            }
        }
        public int lico_serg_priz
        {
            get { return lico5; }
            set
            {
                if (lico5 == value)
                    return;
                lico5 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("lico5"));
            }
        }
        public int lico_sold_vsego
        {
            get { return lico6; }
            set
            {
                if (lico6 == value)
                    return;
                lico6 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("lico6"));
            }
        }
        public int lico_sold_kontr
        {
            get { return lico7; }
            set
            {
                if (lico7 == value)
                    return;
                lico7 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("lico7"));
            }
        }
        public int lico_sold_priz
        {
            get { return lico8; }
            set
            {
                if (lico8 == value)
                    return;
                lico8 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("lico8"));
            }
        }
        public int lico_serg_sold_vsego
        {
            get { return lico9; }
            set
            {
                if (lico9 == value)
                    return;
                lico9 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("lico9"));
            }
        }
        public int lico_serg_sold_kontr
        {
            get { return lico10; }
            set
            {
                if (lico10 == value)
                    return;
                lico10 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("lico10"));
            }
        }
        public int lico_serg_sold_priz
        {
            get { return lico11; }
            set
            {
                if (lico11 == value)
                    return;
                lico11 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("lico11"));
            }
        }
        public int lico_vsego
        {
            get { return lico12; }
            set
            {
                if (lico12 == value)
                    return;
                lico12 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("lico12"));
            }
        }
        
        /////////////////////////////////////////////
        public int ots_naryad
        {
            get { return ots1; }
            set
            {
                if (ots1 == value)
                    return;
                ots1 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ots1"));
            }
        }
        public int ots_med
        {
            get { return ots2; }
            set
            {
                if (ots2 == value)
                    return;
                ots2 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ots2"));
            }
        }
        public int ots_hospital
        {
            get { return ots3; }
            set
            {
                if (ots3 == value)
                    return;
                ots3 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ots3"));
            }
        }
        public int ots_kom
        {
            get { return ots4; }
            set
            {
                if (ots4 == value)
                    return;
                ots4 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ots4"));
            }
        }
        public int ots_pol
        {
            get { return ots5; }
            set
            {
                if (ots5 == value)
                    return;
                ots5 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ots5"));
            }
        }
        public int ots_otpusk
        {
            get { return ots6; }
            set
            {
                if (ots6 == value)
                    return;
                ots6 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ots6"));
            }
        }
        public int ots_arest
        {
            get { return ots7; }
            set
            {
                if (ots7 == value)
                    return;
                ots7 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ots7"));
            }
        }
        public int ots_soch
        {
            get { return ots8; }
            set
            {
                if (ots8 == value)
                    return;
                ots8 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ots8"));
            }
        }
        public int ots_dr_pr
        {
            get { return ots9; }
            set
            {
                if (ots9 == value)
                    return;
                ots9 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ots9"));
            }
        }
        public int ots_vsego
        {
            get { return ots10; }
            set
            {
                if (ots10 == value)
                    return;
                ots10 = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ots10"));
            }
        }
        string Data()
        {
            string n;
            DateTime dt = DateTime.Today;
            if (dt.Day == 1 || dt.Day == 2 || dt.Day == 3 || dt.Day == 4 || dt.Day == 5 || dt.Day == 6 || dt.Day == 7 || dt.Day == 8 || dt.Day == 9)
                n = "0" + dt.Day.ToString();
            else n = dt.Day.ToString();
            if (dt.Month == 1 || dt.Month == 2 || dt.Month == 3 || dt.Month == 4 || dt.Month == 5 || dt.Month == 6 || dt.Month == 7 || dt.Month == 8 || dt.Month == 9)
                n += "0" + dt.Month.ToString();
            else n += dt.Month.ToString();
            n += dt.Year.ToString();
            return n;
        }
        public void ToXml(string level, string name1)
        {
            string name = "";
            if(level == "1")
            {
                name = "51460";
            }
            else
                if(level == "2" || level == "3")
            {
                name = name1;
            }
            
            XDocument doc;
            if (File.Exists(Environment.CurrentDirectory + "\\Recources\\Active\\rsz\\" + name + "_" + Data() + ".xml") == false)
            {
                string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\rsz\\" + name + "_" + Data() + ".xml";
                doc = new XDocument(
                            new XElement("base",
                             new XElement("track",
                                new XAttribute("podr", Podr),
                                new XAttribute("shtat1", shtat_of),
                                new XAttribute("shtat2", shtat_prap),
                                new XAttribute("shtat3", shtat_ser),
                                new XAttribute("shtat4", shtat_sold),
                                new XAttribute("shtat5", shtat_vsego),

                                new XAttribute("spisok1", spisok_of),
                                new XAttribute("spisok2", spisok_prap),
                                new XAttribute("spisok3", spisok_serg_vsego),
                                new XAttribute("spisok4", spisok_serg_kontr),
                                new XAttribute("spisok5", spisok_serg_priz),
                                new XAttribute("spisok6", spisok_sold_vsego),
                                new XAttribute("spisok7", spisok_sold_kontr),
                                new XAttribute("spisok8", spisok_sold_priz),
                                new XAttribute("spisok9", spisok_serg_sold_vsego),
                                new XAttribute("spisok10", spisok_serg_sold_kontr),
                                new XAttribute("spisok11", spisok_serg_sold_priz),
                                new XAttribute("spisok12", spisok_vsego),
                                new XAttribute("spisok13", spisok_ukompl),

                                new XAttribute("lico1", lico_of),
                                new XAttribute("lico2", lico_prap),
                                new XAttribute("lico3", lico_serg_vsego),
                                new XAttribute("lico4", lico_serg_kontr),
                                new XAttribute("lico5", lico_serg_priz),
                                new XAttribute("lico6", lico_sold_vsego),
                                new XAttribute("lico7", lico_sold_kontr),
                                new XAttribute("lico8", lico_sold_priz),
                                new XAttribute("lico9", lico_serg_sold_vsego),
                                new XAttribute("lico10", lico_serg_sold_kontr),
                                new XAttribute("lico11", lico_serg_sold_priz),
                                new XAttribute("lico12", lico_vsego),

                                new XAttribute("ots1", ots_arest),
                                new XAttribute("ots2", ots_dr_pr),
                                new XAttribute("ots3", ots_hospital),
                                new XAttribute("ots4", ots_kom),
                                new XAttribute("ots5", ots_med),
                                new XAttribute("ots6", ots_naryad),
                                new XAttribute("ots7", ots_otpusk),
                                new XAttribute("ots8", ots_pol),
                                new XAttribute("ots9", ots_soch),
                                new XAttribute("ots10", ots_vsego))));
                doc.Save(fileName);
            }
            else
            {

                string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\rsz\\"+ name + "_" + Data() + ".xml";
                if(File.Exists(fileName) == true)
                    File.Delete(fileName);
                ToXml(level, name1);
            }
        }

        public lic_rsz(XElement element)
        {
            Podr = element.Attribute("podr").Value;
            shtat_of = int.Parse(element.Attribute("shtat1").Value);
            shtat_prap = int.Parse(element.Attribute("shtat2").Value);
            shtat_ser = int.Parse(element.Attribute("shtat3").Value);
            shtat_sold = int.Parse(element.Attribute("shtat4").Value);
            shtat_vsego = int.Parse(element.Attribute("shtat5").Value);

            spisok_of = int.Parse(element.Attribute("spisok1").Value);
            spisok_prap = int.Parse(element.Attribute("spisok2").Value);
            spisok_serg_vsego = int.Parse(element.Attribute("spisok3").Value);
            spisok_serg_kontr = int.Parse(element.Attribute("spisok4").Value);
            spisok_serg_priz = int.Parse(element.Attribute("spisok5").Value);
            spisok_sold_vsego = int.Parse(element.Attribute("spisok6").Value);
            spisok_sold_kontr = int.Parse(element.Attribute("spisok7").Value);
            spisok_sold_priz = int.Parse(element.Attribute("spisok8").Value);
            spisok_serg_sold_vsego = int.Parse(element.Attribute("spisok9").Value);
            spisok_serg_sold_kontr = int.Parse(element.Attribute("spisok10").Value);
            spisok_serg_sold_priz = int.Parse(element.Attribute("spisok11").Value);
            spisok_vsego = int.Parse(element.Attribute("spisok12").Value);
            spisok_ukompl = int.Parse(element.Attribute("spisok13").Value);

            lico_of = int.Parse(element.Attribute("lico1").Value);
            lico_prap = int.Parse(element.Attribute("lico2").Value);
            lico_serg_vsego = int.Parse(element.Attribute("lico3").Value);
            lico_serg_kontr = int.Parse(element.Attribute("lico4").Value);
            lico_serg_priz = int.Parse(element.Attribute("lico5").Value);
            lico_sold_vsego = int.Parse(element.Attribute("lico6").Value);
            lico_sold_kontr = int.Parse(element.Attribute("lico7").Value);
            lico_sold_priz = int.Parse(element.Attribute("lico8").Value);
            lico_serg_sold_vsego = int.Parse(element.Attribute("lico9").Value);
            lico_serg_sold_kontr = int.Parse(element.Attribute("lico10").Value);
            lico_serg_sold_priz = int.Parse(element.Attribute("lico11").Value);
            lico_vsego = int.Parse(element.Attribute("lico12").Value);

            ots_arest = int.Parse(element.Attribute("ots1").Value);
            ots_dr_pr = int.Parse(element.Attribute("ots2").Value);
            ots_hospital = int.Parse(element.Attribute("ots3").Value);
            ots_kom = int.Parse(element.Attribute("ots4").Value);
            ots_med = int.Parse(element.Attribute("ots5").Value);
            ots_naryad = int.Parse(element.Attribute("ots6").Value);
            ots_otpusk = int.Parse(element.Attribute("ots7").Value);
            ots_pol = int.Parse(element.Attribute("ots8").Value);
            ots_soch = int.Parse(element.Attribute("ots9").Value);
            ots_vsego = int.Parse(element.Attribute("ots10").Value);
        }
    }

}
