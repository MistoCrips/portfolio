﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model
{
    public class OpenLoadZaShtat : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public OpenLoadZaShtat() { }
        public string ColToString()
        {
            Sbor_AnShtat();
            string S = "";
            foreach (OpenLoadZaShtat N in AnShtat_List)
            {
                S = S + "|" + N.Lnumber + "|" + N.FIO + "|" + N.Types + "|" + N.Zvanie + "|" + N.Last_dolg + "|" + N.Last_podr;
            }
            return S;
        }
        ObservableCollection<OpenLoadZaShtat> AnShtat_List = new ObservableCollection<OpenLoadZaShtat>();
        int id = 0;
        private string lnumber = "";
        private string fio = "";
        private string zvanie = "";
        private string types = "";
        private string last_podr = ".";
        private string last_dolg = ".";
        public int Id
        {
            get { return id; }
            set
            {
                if (id == value)
                    return;
                id = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Id"));
            }
        }
        public string Lnumber
        {
            get { return lnumber; }
            set
            {
                if (lnumber == value)
                    return;
                lnumber = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Lnumber"));
            }
        }
        public string Last_podr
        {
            get { return last_podr; }
            set
            {
                if (last_podr == value)
                    return;
                last_podr = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Last_podr"));
            }
        }
        public string Last_dolg
        {
            get { return last_dolg; }
            set
            {
                if (last_dolg == value)
                    return;
                last_dolg = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Last_dolg"));
            }
        }
        public string FIO
        {
            get { return fio; }
            set
            {
                if (fio == value)
                    return;
                fio = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FIO"));
            }
        }
        public string Zvanie
        {
            get { return zvanie; }
            set
            {
                if (zvanie == value)
                    return;
                zvanie = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Zvanie"));
            }
        }
        public string Types
        {
            get { return types; }
            set
            {
                if (types == value)
                    return;
                types = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public OpenLoadZaShtat(XElement element)
        {
            Lnumber = element.Attribute("lnumber").Value;
            FIO = element.Attribute("fio").Value;
            Zvanie = element.Attribute("tek_zvanie").Value;
            Types = element.Attribute("type").Value;
            Last_dolg = element.Attribute("last_dolg").Value;
            Last_podr = element.Attribute("last_podr").Value;
        }
        public ObservableCollection<OpenLoadZaShtat> Sbor_AnShtat()
        {
            string filename = Environment.CurrentDirectory + "\\Recources\\AnShtat\\51460.xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                AnShtat_List.Add(new OpenLoadZaShtat(el));
            }
            return AnShtat_List;
        }
        public void RemovePeople()
        {
            string fileName = Environment.CurrentDirectory + "\\Recources\\AnShtat\\51460.xml";
            XDocument doc;
            doc = XDocument.Load(fileName);
            doc.Descendants().Where(e => e.Name == Lnumber).Remove();
            doc.Save(fileName);
        }
        public void ToXml()
        {
            string fileName = Environment.CurrentDirectory + "\\Recources\\AnShtat\\51460.xml";
            XDocument doc;
            if (File.Exists(Environment.CurrentDirectory + "\\Recources\\AnShtat\\51460.xml") == false)
            {
                
                doc = new XDocument(
                            new XElement("base",
                             new XElement("track",
                                new XAttribute("lnumber", Lnumber),
                                new XAttribute("fio", FIO),
                                new XAttribute("tek_zvanie", Zvanie),
                                new XAttribute("type", Types),
                                new XAttribute("last_dolg", Last_dolg),
                                new XAttribute("last_podr", Last_podr))));
                doc.Save(fileName);
            }
            else
            {
                doc = XDocument.Load(fileName);
                XElement track = new XElement("track");
                track.Add(new XAttribute("lnumber", Lnumber));
                track.Add(new XAttribute("fio", FIO));
                track.Add(new XAttribute("tek_zvanie", Zvanie));
                track.Add(new XAttribute("type", Types));
                track.Add(new XAttribute("last_dolg", Last_dolg));
                track.Add(new XAttribute("last_podr", Last_podr));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }

    }//Загрузка заштатников
}
