﻿using System.Windows.Input;

namespace New_Stroevaya_chast.Model.Command
{
    public interface IDelegateCommand : ICommand
    {
        void RaiseCanExecuteChanged();
    }
}
