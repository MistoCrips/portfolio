﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model.LD
{
    public class Vizhivanie : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        string mesto_ = ".";
        string per_ = ".";
        string osn_ = ".";
        public Vizhivanie() { }
        public Vizhivanie(XElement element)
        {
            if (element.Attribute("mesto").Value != null) Mesto = element.Attribute("mesto").Value;
            if (element.Attribute("period").Value != null) Period = element.Attribute("period").Value;
            if (element.Attribute("osnovanie").Value != null) Osnovanie = element.Attribute("osnovanie").Value;
        }
        public string Mesto
        {
            get { return mesto_; }
            set
            {
                if (mesto_ == value)
                    return;
                mesto_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Mesto"));
            }
        }
        public string Period
        {
            get { return per_; }
            set
            {
                if (per_ == value)
                    return;
                per_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Per"));
            }
        }
        public string Osnovanie
        {
            get { return osn_; }
            set
            {
                if (osn_ == value)
                    return;
                osn_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Osn"));
            }
        }
        public void ToXml(string LOGIN)
        {
            string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + LOGIN + ".xml";
            XDocument doc;
            if (File.Exists(fileName) == false)
            {
                doc = new XDocument(
                new XElement("base",
                    new XElement("track_viz",
                        new XAttribute("mesto", Mesto),
                        new XAttribute("period", Period),
                        new XAttribute("osnovanie", Osnovanie))));
                doc.Save(fileName);
            }
            else
            {
                doc = XDocument.Load(fileName);
                doc.Descendants().Where(e => e.Name == "track_viz").Remove();
                XElement track = new XElement("track_viz");
                track.Add(new XAttribute("mesto", Mesto));
                track.Add(new XAttribute("period", Period));
                track.Add(new XAttribute("osnovanie", Osnovanie));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }
    }
}
