﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestTeleDOC.Models;

namespace TestTeleDOC.Controllers
{
    public class OrganizationsController : Controller
    {
        private OrganizationContext db = new OrganizationContext();

        // GET: Organizations
        public ActionResult Index()
        {
            var organization = db.Organizations.Include(p => p.Founders).ToList();
            return View(organization);
        }

       
        // GET: Organizations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Organizations/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrganizationId,Name,INN,Founders")] Organization organization)
        {
            if (ModelState.IsValid && organization.INN != null)
            {
                db.Organizations.Add(organization);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(organization);
        }

        // GET: Organizations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Organization> list = db.Organizations.Include(p => p.Founders).ToList();
            Organization organization = db.Organizations.Find(id);
            var founderlist = db.Founders.Include(p => p.Organization).ToList();
            if (organization == null)
            {
                return HttpNotFound();
            }
            ICollection<Founder> td = new List<Founder>();
            foreach (Founder item in founderlist)
            {
                if (id != null)
                {
                    if (item.OrganizationId != id)
                        td.Add(item);
                }
                else
                    if (item.OrganizationId == id)
                    td.Add(item);
            }
            var tuple = Tuple.Create(organization, td);
            return View(tuple);
        }

        // POST: Organizations/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrganizationId,Name,INN")] Organization organization)
        {
            if (ModelState.IsValid)
            {
                db.Entry(organization).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(organization);
        }

        // GET: Organizations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Organization organization = db.Organizations.Find(id);
            if (organization == null)
            {
                return HttpNotFound();
            }
            return View(organization);
        }

        // POST: Organizations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Organization organization = db.Organizations.Find(id);
            db.Organizations.Remove(organization);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Organizations/Delete/5
        public ActionResult DeleteFounder(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Founder founder = db.Founders.Find(id);
            if (founder == null)
            {
                return HttpNotFound();
            }
            return View(founder);
        }

        // POST: Organizations/Delete/5
        [HttpPost, ActionName("DeleteFounder")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteFouderSave(int id)
        {
            Founder founder = db.Founders.Find(id);
            db.Founders.Remove(founder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult ListFounder()
        {
            var founderlist = db.Founders.Include(p => p.Organization).ToList();
            return View(founderlist);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult CreateFouders()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateFouders([Bind(Include = "Lastname,Firstname,Middlename")] Founder founder)
        {
            if (ModelState.IsValid)
            {
                db.Founders.Add(founder);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(founder);
        }
        // GET: Organizations/FounderListCreate/5

        public ActionResult FounderListCreate(int? id, int? idd)
        {
            if(id != null && idd != null)
            {
                Founder fouder = db.Founders.Find(id);
                Organization organization = db.Organizations.Find(idd);
                organization.Founders.Add(fouder);
                db.Entry(organization).State = EntityState.Modified;
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        public ActionResult FouderDeleteList(int? id, int? idd)
        {
            if (id != null && idd != null)
            {
                Founder fouder = db.Founders.Find(id);
                Organization organization = db.Organizations.Find(idd);
                organization.Founders.Remove(fouder);
                db.Entry(organization).State = EntityState.Modified;
                db.SaveChanges();
            }
            
            return RedirectToAction("Index");
        }
    }
}
