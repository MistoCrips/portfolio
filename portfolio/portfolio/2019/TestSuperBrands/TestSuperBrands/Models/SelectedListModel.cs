﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestSuperBrands.Models
{
    public class SelectedListModel
    {
        public int SelectedOrderId { get; set; }
        public SelectList Categories { get; set; }
        public string SelectedCategories { get; set; }
    }
}