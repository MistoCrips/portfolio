﻿using New_Stroevaya_chast.Modal;
using Client.ViewModal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;
using New_Stroevaya_chast.Modal.Model;
using New_Stroevaya_chast.Modal.Model.LD;

namespace Client.View
{
    /// <summary>
    /// Логика взаимодействия для Client_osn.xaml
    /// </summary>
    public partial class Client_osn : Window
    {
        TCP T = new TCP();
        private string text1;
        private string text2;

        public Client_osn(string text1, string text2)
        {
            this.text1 = text1;
            this.text2 = text2;
            T.Connect();
            T.SendMessage("log|" + text1 + "|" + text2);
            InitializeComponent();
            close_stolb();
        }
        void close_stolb()
        {
            MenuItem t;
            for(int i=0; i < Shtat.Columns.Count; i++)
            {
                t = new MenuItem();
                t.Header = Shtat.Columns[i].Header;
                t.Click += close_stolb1; 
                Close_stolb.Items.Add(t);
                t = new MenuItem();
                t.Header = Shtat.Columns[i].Header;
                t.Click += create_stolb1;
                Create_stolb.Items.Add(t);
            }
        }
        void create_stolb1(object sender, RoutedEventArgs e)
        {
            MenuItem t = (MenuItem)sender;
            if (t.Header.ToString() == "№ п/п")
                Shtat.Columns[0].Visibility = Visibility.Visible;
            if (t.Header.ToString() == "Подразделение")
                Shtat.Columns[1].Visibility = Visibility.Visible;
            if (t.Header.ToString() == "Подр1")
                Shtat.Columns[2].Visibility = Visibility.Visible;
            if (t.Header.ToString() == "Подр2")
                Shtat.Columns[3].Visibility = Visibility.Visible;
            if (t.Header.ToString() == "Штат")
                Shtat.Columns[4].Visibility = Visibility.Visible;
            if (t.Header.ToString() == "Должность")
                Shtat.Columns[5].Visibility = Visibility.Visible;
            if (t.Header.ToString() == "Звание")
                Shtat.Columns[6].Visibility = Visibility.Visible;
            if (t.Header.ToString() == "Личный номер")
                Shtat.Columns[7].Visibility = Visibility.Visible;
            if (t.Header.ToString() == "ФИО")
                Shtat.Columns[8].Visibility = Visibility.Visible;
            if (t.Header.ToString() == "Период службы")
                Shtat.Columns[9].Visibility = Visibility.Visible;
            if (t.Header.ToString() == "Причина отсутствия")
                Shtat.Columns[10].Visibility = Visibility.Visible;
            if (t.Header.ToString() == "Место положения")
                Shtat.Columns[11].Visibility = Visibility.Visible;
            if (t.Header.ToString() == "Приказ")
                Shtat.Columns[12].Visibility = Visibility.Visible;
        }
        void close_stolb1(object sender, RoutedEventArgs e)
        {
            MenuItem t = (MenuItem)sender;
            if (t.Header.ToString() == "№ п/п")
                Shtat.Columns[0].Visibility = Visibility.Hidden;
            if (t.Header.ToString() == "Подразделение")
                Shtat.Columns[1].Visibility = Visibility.Hidden;
            if (t.Header.ToString() == "Подр1")
                Shtat.Columns[2].Visibility = Visibility.Hidden;
            if (t.Header.ToString() == "Подр2")
                Shtat.Columns[3].Visibility = Visibility.Hidden;
            if (t.Header.ToString() == "Штат")
                Shtat.Columns[4].Visibility = Visibility.Hidden;
            if (t.Header.ToString() == "Должность")
                Shtat.Columns[5].Visibility = Visibility.Hidden;
            if (t.Header.ToString() == "Звание")
                Shtat.Columns[6].Visibility = Visibility.Hidden;
            if (t.Header.ToString() == "Личный номер")
                Shtat.Columns[7].Visibility = Visibility.Hidden;
            if (t.Header.ToString() == "ФИО")
                Shtat.Columns[8].Visibility = Visibility.Hidden;
            if (t.Header.ToString() == "Период службы")
                Shtat.Columns[9].Visibility = Visibility.Hidden;
            if (t.Header.ToString() == "Причина отсутствия")
                Shtat.Columns[10].Visibility = Visibility.Hidden;
            if (t.Header.ToString() == "Место положения")
                Shtat.Columns[11].Visibility = Visibility.Hidden;
            if (t.Header.ToString() == "Приказ")
                Shtat.Columns[12].Visibility = Visibility.Hidden;

        }


    }

}
