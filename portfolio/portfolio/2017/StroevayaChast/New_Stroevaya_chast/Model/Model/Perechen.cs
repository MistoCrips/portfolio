﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model
{
    public class Perechen : INotifyPropertyChanged
    {
        public Perechen()
        {

        }
        public event PropertyChangedEventHandler PropertyChanged;
        int id_ = 0;
        string value_ = ".";
        public string value
        {
            get { return value_; }
            set
            {
                if (value_ == value)
                    return;
                value_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("value"));
            }
        }
        public int id
        {
            get { return id_; }
            set
            {
                if (id_ == value)
                    return;
                id_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("id"));
            }
        }
        public Perechen(XElement element)
        {
            if (int.Parse(element.Attribute("id").Value) > id)
            {
                id = int.Parse(element.Attribute("id").Value);
            }
            value = element.Attribute("value").Value;

        }

    }
}
