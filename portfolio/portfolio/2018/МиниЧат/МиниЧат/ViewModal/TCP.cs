using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Xml.Linq;
using �������.Modal;
using System.Windows.Input;
using System.Threading.Tasks;

namespace �������.ViewModal
{
    public class VM : INotifyPropertyChanged
    {
        Messager Last_message;
        static bool Minimazed_;
        static int server_= 1, client_=1;
        string NameS, _ip_client, _ip_server;
        Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;
        #region ���������
        static ObservableCollection<Messager> list_shtat = new ObservableCollection<Messager>();
        public ObservableCollection<Messager> Humans
        {
            get { return list_shtat;  }
            set { list_shtat = value; }
        }
        static ObservableCollection<Peoples> list_people = new ObservableCollection<Peoples>();
        public ObservableCollection<Peoples> People
        {
            get { return list_people;  }
            set { list_people = value; }
        }
        private void OnPropertychanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        #region ������ - �������
        private IDelegateCommand allCommand;
        public IDelegateCommand AllCommand
        {
            get
            {
                return allCommand ??
                    (allCommand = new DelegateCommand(o => {
                        int counter = int.Parse((string)o);
                        switch (counter)
                        {
                            case 1:
                                if(server_ == 1)
                                {
                                    
                                    StartServer();
                                    server_ = 2;
                                    
                                }
                                else
                                {
                                    Stop();
                                    server_ = 1;
                                }    
                                break;
                            case 2:
                                if (client_ == 1)
                                {
                                    if (server_ == 2) return;
                                    Connect();
                                    SendMessage("�����������>" + _ip_client + ">" + NameS);
                                    client_ = 2;
                                }
                                else
                                {
                                    SendMessage("����������_������>" + _ip_client + ">" + NameS);
                                    CloseClient();
                                    client_ = 1;
                                }
                                break;
                        }

                    }));
            }
        }
        private IDelegateCommand sendMessageCommand;
        public IDelegateCommand SendMessageCommand
        {
            get
            {
                return sendMessageCommand ??
                    (sendMessageCommand = new DelegateCommand(o => {
                        string mes = (string)o;
                        
                        DateTime dt = DateTime.Now;
                        Messager m = new Messager();
                        m.IP = _ip_client;
                        m.Mes = mes;
                        m.Name = NameS;
                        m.Text = "[" + dt.Hour + ":" + dt.Minute + ":" + dt.Second + "]�: " + mes;
                        list_shtat.Add(m);
                        if (server_ == 2)
                            SendToClients2(_ip_client + ">" + mes + ">" + NameS, -1);
                        if (client_ == 2)
                            SendMessage(_ip_client+">"+mes+">"+NameS);
                    }));
            }
        }
        #endregion
        #region ������
        TcpClient _tcp�lient = new TcpClient();
        NetworkStream ns;
        bool _stopNetwork;
        #region �������������� ����� ������� ������

        public void Minimazed (bool t)
        {
            Minimazed_ = t;
            Last_message = new Messager();
        }
        // ������� ����������� � �������
        void ip()
        {         
            string fileName = Environment.CurrentDirectory + "\\Recources\\config.xml";
            XDocument doc = XDocument.Load(fileName);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                _ip_server = el.Attribute("value").Value;
            }
            foreach (XElement el in doc.Root.Elements("Name"))
            {
                NameS = el.Attribute("value").Value;
            }
            foreach (XElement el in doc.Root.Elements("Ip"))
            {
                _ip_client = el.Attribute("value").Value;
            }
        }

        public void Connect()
        {
            try
            {

                ip();
                _tcp�lient.Connect(_ip_server, 15000);

                ns = _tcp�lient.GetStream();
                
                Thread th = new Thread(ReceiveRun_client);
                th.Start();
                if (File.Exists(Environment.CurrentDirectory + "\\Recources\\dannie.xml") == true)
                {
                    string fileName = Environment.CurrentDirectory + "\\Recources\\dannie.xml";
                    XDocument doc = XDocument.Load(fileName);
                    foreach (XElement el in doc.Root.Elements("track"))
                    {
                        list_shtat.Add(new Messager(el));
                    }
                }
            }
            catch
            {
                ErrorSound();
            }
        }
        public void CloseClient()
        {
            SendMessage("����������_������>" + _ip_client + ">" + NameS);
            if (ns != null) ns.Close();
            if (_tcp�lient != null) _tcp�lient.Close();
            
            _stopNetwork = true;

        }

        // �������� ��������� � ����������� ������,
        // ��������� ����� ��������� �����������
        // �� ������� ��������� ������������ ���������� ����������. 
        public void SendMessage(string Text_cap)
        {
                if (ns != null)
                {
                    byte[] buffer = Encoding.Default.GetBytes(Text_cap);
                    ns.Write(buffer, 0, buffer.Length);
                }                   
        }

        public void ADD()
        {
            if (Minimazed_)
            {
                Application.Current.Dispatcher.InvokeAsync(async () =>
                {
                    Window W = new Window();
                    TextBlock TB = new TextBlock();
                    TB.Text = Last_message.Text;
                    W.Width = 300;
                    W.Height = 100;
                    double screenHeight = SystemParameters.FullPrimaryScreenHeight;
                    double screenWidth = SystemParameters.FullPrimaryScreenWidth;

                    W.WindowStyle = WindowStyle.None;
                    W.Top = (screenHeight - W.Height);
                    W.Left = (screenWidth - W.Width);

                    W.Name = "���������";
                    W.Title = "�������� ��������� �� " + Last_message.Name;
                    Grid A = new Grid();
                    W.Content = A;
                    A.Children.Add(TB);
                    W.Show();
                    await Task.Delay(2000);
                    W.Close();
                });
                
            }

        }
        // ���� ���������� ���������,
        // ����������� � ��������� ������.
        void ReceiveRun_client()
        {
            
            while (true)
            {
                try
                {
                    string s = null;
                    while (ns.DataAvailable == true)
                    {
                        // ����������� ������������ ������� ������ ������.
                        byte[] buffer = new byte[_tcp�lient.Available];

                        ns.Read(buffer, 0, buffer.Length);
                        s += Encoding.Default.GetString(buffer);
                    }

                    if (s != null)
                    {
                        ShowReceiveMessage(s);
                    }


                    // ����������� ������� ��� �������� �������� ����������.
                    // ��������� ������.
                    Thread.Sleep(100);
                }
                catch
                {
                    ErrorSound();
                }

                if (_stopNetwork == true) break;
            }
            
        }
        #endregion
        #region ���������� � ������� ������

        // ��� ������� � ��������� �������� ������� �����  �� ������ �������
        delegate void UpdateReceiveDisplayDelegate(string message);
        void ShowReceiveMessage(string message) //�������� ���������
        {
            string[] wor = message.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
            if(wor[0] == "�����������")
            {
                Peoples t = new Peoples();
                t.IP = wor[1];
                t.NamePeople = wor[2];          
                _dispatcher.Invoke(new Action(() =>
                {
                    list_people.Add(t);
                }));

            }else
            if (wor[0] == "����������_������")
            {
                _tcp�lient.Close();
                MessageBox.Show(" �������� ���������� � ��������. ������������� ������� ����������");
                Environment.Exit(0);
            }else
            if (wor[0] == "������������")
            {
                Peoples tmp = new Peoples();
                for (int i = 1; i < wor.Length; i++)
                {
                    if (i%2 == 1)
                    {
                        tmp = new Peoples();
                        tmp.NamePeople = wor[i];
                        continue;
    
                }
                    if (i%2 == 0)
                    {
                        bool b = true;
                        tmp.IP = wor[i];
                        foreach (Peoples p in list_people)
                            if (p.NamePeople == tmp.NamePeople)
                                b=false;
                        if (b)
                        {
                            _dispatcher.Invoke(new Action(() =>
                            {
                                list_people.Add(tmp);
                            }));
                        }
                        continue;
                    }
                }
            }

            else
            {
                DateTime T = DateTime.Now;
                Last_message = new Messager();
                Last_message.IP = wor[0];
                Last_message.Mes = wor[1];
                Last_message.Name = wor[2];
                Last_message.Text = "["+T.Hour+":"+ T.Minute+ ":"+T.Second+ "]"+ wor[2] + ": " + wor[1];
                _dispatcher.Invoke(new Action(() =>
                {
                    list_shtat.Add(Last_message);
                }));
                Last_message.ToXml();
                ADD();
            }
        }



        // �������� ���������� � ������������� ������.
        void ErrorSound()
        {
            Console.Beep(2000, 80);
            Console.Beep(3000, 120);
        }

        #endregion
        #endregion
        #region ������
        TcpListener _server;
        const int MAXNUMCLIENTS = 15;
        TcpClient[] clients = new TcpClient[MAXNUMCLIENTS];
        int _countClient = 0;

        public event PropertyChangedEventHandler PropertyChanged;
        #region �������������� ����� ������� ������

        public void StartServer()
        {
            if (_server == null)
            {
                try
                {
                    ip();
                    Peoples t = new Peoples();
                    t.NamePeople = NameS;
                    t.IP = _ip_client;
                    list_people.Add(t);

                    _stopNetwork = false;
                    _countClient = 0;

                    int port = 15000;
                    _server = new TcpListener(IPAddress.Any, port);
                    _server.Start();


                    Thread acceptThread = new Thread(AcceptClients);
                    acceptThread.Start();

                    if (File.Exists(Environment.CurrentDirectory + "\\Recources\\dannie.xml") == true)
                    {
                        string fileName = Environment.CurrentDirectory + "\\Recources\\dannie.xml";
                        XDocument doc = XDocument.Load(fileName);
                        foreach (XElement el in doc.Root.Elements("track"))
                        {
                            list_shtat.Add(new Messager(el));
                        }
                    }
                }
                catch
                {
                    SaveError("Start Server");
                    ErrorSound();
                }
            }
        }


        public void Stop()
        {
            SendToClients2("����������_������", -1);
            StopServer();
        }

        // �������������� ��������� ������� � ���������� �������.
        public void StopServer()
        {
            if (_server != null)
            {
                
                _server.Stop();
                _server = null;
                _stopNetwork = true;

                for (int i = 0; i < MAXNUMCLIENTS; i++)
                {
                    if (clients[i] != null) clients[i].Close();
                }
            }
        }

        void AcceptClients()
        {
            while (true)
            {
                try
                {
                    this.clients[_countClient] = _server.AcceptTcpClient();
                    Thread readThread = new Thread(ReceiveRun);
                    readThread.Start(_countClient);
                    _countClient++;
                }
                catch
                {
                    SaveError("AcceptClients");
                    ErrorSound();
                }
                if (_countClient == MAXNUMCLIENTS || _stopNetwork == true)
                {
                    break;
                }

            }
        }

        //������������� ��� ��������
        void MashrutZapros(string message_, int num)
        {
            DateTime T = DateTime.Now;
            string[] wor = message_.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);

            if (wor[0] == "�����������")
            {
                Peoples t = new Peoples();
                t.IP = wor[1];
                t.NamePeople = wor[2];
                t.Counter = num;
                foreach (Peoples p in list_people)
                    if (p.NamePeople == t.NamePeople)
                        return;
                _dispatcher.Invoke(new Action(() =>
                {
                    list_people.Add(t);
                }));
                SendToClients2(Message_create(list_people), -1);
            }
            else
         if (wor[0] == "����������_������")
            {
                foreach(Peoples p in list_people)
                {
                    if(p.NamePeople == wor[2])
                    {                       
                        _dispatcher.Invoke(new Action(() =>
                        {
                            list_people.Remove(p);
                        }));
                        break;
                    }
                }
                SendToClients2(Message_create(list_people), num);
            }
            else
            {
                SendToClients2(message_, num);
                Messager tmp = new Messager();
                tmp.IP = wor[0];
                tmp.Mes = wor[1];
                tmp.Name = wor[2];
                tmp.Text = "[" + T.Hour + ":" + T.Minute + ":" + T.Second + "]" + wor[2] + ": " + wor[1];
                _dispatcher.Invoke(new Action(() =>
                {
                    list_shtat.Add(tmp);
                }));
            }

        }

        public string Message_create(ObservableCollection<Peoples> t)
        {
            string text = "������������";
            foreach (Peoples a in t)
            {
                text += ">" + a.NamePeople + ">" + a.IP + ">" ;
    
        }
            return text;

        }
        public void SendToClients2(string text, int skipindex)
        {
            for (int i = 0; i < clients.Count(); i++)
            {
                if (clients[i] != null)
                {
                    if (i == skipindex) continue;
                    NetworkStream ns = clients[i].GetStream();
                    byte[] myReadBuffer = Encoding.Default.GetBytes(text);
                    ns.BeginWrite(myReadBuffer, 0, myReadBuffer.Length,
                                                                 new AsyncCallback(AsyncSendCompleted), ns);
                    continue;

                }
            }
        }
        public void AsyncSendCompleted(IAsyncResult ar)
        {
            try
            {
                NetworkStream ns = (NetworkStream)ar.AsyncState;
                ns.EndWrite(ar);
            }
            catch
            {
                SaveError("ReceiveRun");
                ErrorSound();
            }
        }


        // ���������� ��������� �� ������� � ������������ ����������� 
        // ��������� ������ ��������
        void ReceiveRun(object num)
        {
            while (true)
            {
                try
                {
                    string s = null;
                    NetworkStream ns = clients[(int)num].GetStream();
                    while (ns.DataAvailable == true)
                    {
                        byte[] buffer = new byte[clients[(int)num].Available];
                        ns.Read(buffer, 0, buffer.Length);
                        s += Encoding.Default.GetString(buffer);
                        MashrutZapros(s, (int)num);
                    }
                    Thread.Sleep(100);
                }
                catch
                {
                    // ���������� ��������� ����������
                    SaveError("ReceiveRun");
                    ErrorSound();
                }


                if (_stopNetwork == true) break;

            }
        }

        #endregion
        #region ������������ ������� ������



        // ������� ������� � �������� ����� labelCountClient �� ���������������� ������.
        protected delegate void UpdateClientsDisplayDelegate();

        // �������� ���������� � ������������� ������.


        void SaveError(string message)
        {
            string fileName = Environment.CurrentDirectory + "\\Recources\\error.xml";
            XDocument doc;
            if (File.Exists(fileName) == false)
            {
                doc = new XDocument(
                new XElement("base",
                    new XElement("track",
                        new XAttribute("Error-", message)
                       )));
                doc.Save(fileName);
            }
            else
            {
                doc = XDocument.Load(fileName);
                XElement track = new XElement("track");
                track.Add(new XAttribute("Error-", message));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }
        #endregion
        #endregion
    }
}

