package com.example.user.proba;

public class CheckBoxClassInit {
    private int id_ = 0;
    private String named_  = ".";
    private boolean value_ = false;
    private int sized_ = 0;

    CheckBoxClassInit(String NAME, boolean VALUE){
        named_ = NAME;
        value_ = VALUE;
    }

    public int getId_() {
        return id_;
    }

    public void setId_(int ID) {
        this.id_ = ID;
    }

    public String getNamed_() {
        return named_;
    }

    public boolean isValue_() {
        return value_;
    }

    public void setNamed_(String NAMED) {
        this.named_ = NAMED;
    }

    public void setValue_(boolean VALUE) {
        this.value_ = VALUE;
    }

    public int getSized_() {
        return sized_;
    }

    public void setSized_(int sized_) {
        this.sized_ = sized_;
    }
}
