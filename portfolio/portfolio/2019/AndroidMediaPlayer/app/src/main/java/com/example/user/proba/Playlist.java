package com.example.user.proba;

import android.util.Log;

import java.util.ArrayList;

public class Playlist {
    static private long counter = 0;
    private long id_ = 0;
    private String name;
    private String Zvan_;
    private String name_table;
    private int key_;
    ArrayList<Track> Song = new ArrayList<Track>();
    final String LOG_TAG = "myLogs";

    public Playlist(String Namelist, ArrayList<Track> Songs){
        id_ = counter++;
        name = Namelist;
        //Log.d(LOG_TAG,"Размер передаваемой коллекции"+ Songs.size());
        Song = Songs;
        key_ = Song.size();


    }

    public  long getID(){ return id_; }
    public  String getNames(){return name; }
    public  String getZvan(){return Zvan_; }
    public  String getName_table(){return name_table; }
    public  int getKeys(){return key_; }
    public  ArrayList<Track>  getSongs(){return Song; }

    public  long setID(long values){
        id_ = values;
        return id_; }
    public  String setNames(String values){
        name = values;
        return name; }
    public  String setZvan(String values){
        Zvan_ = values;
        return Zvan_; }

    public void setName_table(String name_table) {
        this.name_table = name_table;
    }

    public  int setKeys(int values){
        key_ = values;
        return key_; }
    public  ArrayList<Track> setSongs(ArrayList<Track> values){
        Song = values;
        return Song; }
}
