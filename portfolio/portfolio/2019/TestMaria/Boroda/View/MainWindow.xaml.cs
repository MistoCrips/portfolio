﻿using Boroda.ViewModal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Boroda
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Close_create(object sender, RoutedEventArgs e)
        {
            tb_limit.Text = "";
            tb_date.Text = "";
            tb_city.Text = "";
            TB_nomber.Text = "";
            TB_address.Text = "";
            TB_middle_name.Text = "";
            TB_first_name.Text = "";
            TB_last_name.Text = "";
            tb_date_mer.Text = "";
        }
    }
}
