package com.example.user.proba;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class SongAdapterActiveList extends BaseAdapter {
    private ArrayList<Track> songs;
    private LayoutInflater songInf;
    final String LOG_TAG = "myLogs";

    public SongAdapterActiveList(Context c, ArrayList<Track> theSongs){
        songs=theSongs;
        songInf=LayoutInflater.from(c);
    }

    public SongAdapterActiveList(ArrayList<Track> theSongs) {
        songs=theSongs;

    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        //map to song layout
        GridLayout songLay = (GridLayout)songInf.inflate
                (R.layout.song_active, parent, false);
        //get title and artist views
        TextView songView = (TextView)songLay.findViewById(R.id.song_title);
        TextView artistView = (TextView)songLay.findViewById(R.id.song_artist);
        final Button addButton = (Button) songLay.findViewById(R.id.addButton);
        //TextView titleBox = (TextView)songLay.findViewById(R.id.TitleTrack);
        //get song using position
        Track currSong = songs.get(position);
        //get title and artist strings
        //titleBox.setText(currSong.getArtist()+" - "+currSong.getTitle());
        addButton.setTag(position);

        //addButton.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View v) {
        //        Log.d(LOG_TAG, "Нажат "+position+" элемент" );
        //        addButton.setTag(position);
        //    }
        //});
        songView.setText(currSong.getTitle());
        artistView.setText(currSong.getArtist());
        //set position as tag
        songLay.setTag(position);
        return songLay;
    }

}
