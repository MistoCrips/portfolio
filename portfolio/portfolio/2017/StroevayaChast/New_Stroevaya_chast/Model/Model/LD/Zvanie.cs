﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model.LD
{
    public class zvanie : INotifyPropertyChanged
    {
        int nomber_ = 0;
        string name = ".";
        string date = ".";
        string what = ".";
        int pricaz = 0;

        public zvanie() { }
        public int Nomber
        {
            get { return nomber_; }
            set
            {
                if (nomber_ == value)
                    return;
                nomber_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Nomber"));
            }
        }
        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                    return;
                name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
            }
        }
        public string Date
        {
            get { return date; }
            set
            {
                if (date == value)
                    return;
                date = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date"));
            }
        }
        public string What
        {
            get { return what; }
            set
            {
                if (what == value)
                    return;
                what = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("What"));
            }
        }
        public int Pr
        {
            get { return pricaz; }
            set
            {
                if (pricaz == value)
                    return;
                pricaz = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Pr"));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public zvanie(XElement element)
        {
            if (element.Attribute("name").Value != null) Name = element.Attribute("name").Value;
            if (element.Attribute("date").Value != null) Date = element.Attribute("date").Value;
            if (element.Attribute("what").Value != null) What = element.Attribute("what").Value;

            if (int.Parse(element.Attribute("pr").Value) != 0)
            {
                Pr = int.Parse(element.Attribute("pr").Value);
            }
        }
        public void ToXml(string LOGIN)
        {
            string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + LOGIN + ".xml";
            XDocument doc;
            if (File.Exists(fileName) == false)
            {
                doc = new XDocument(
                new XElement("base",
                    new XElement("track_zv",
                        new XAttribute("name", Name),
                        new XAttribute("date", Date),
                        new XAttribute("what", What),
                        new XAttribute("pr", Pr))));
                doc.Save(fileName);
            }
            else
            {
                doc = XDocument.Load(fileName);
                XElement track = new XElement("track_zv");
                track.Add(new XAttribute("name", Name));
                track.Add(new XAttribute("date", Date));
                track.Add(new XAttribute("what", What));
                track.Add(new XAttribute("pr", Pr));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }
    }//конец звания
}
