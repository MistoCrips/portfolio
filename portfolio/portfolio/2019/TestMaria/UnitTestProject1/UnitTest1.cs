﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Boroda.ViewModal;
using System.Linq;
using Boroda.Modal;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        bool bt;
        ViewModals v = new ViewModals();
        [TestMethod]
        public void TestCreatePeople() //Тест добавления клиента в базу
        {
            Assert.IsTrue(Rab(1, new string[] { "Семов", "Николай", "Владимирович", "Якутск", "11111111111" })); 
        }
        [TestMethod]
        public void TestCreateLimit() // Тест добавления лимита
        {
            Limits l = new Limits();
            l.Interval_min = 10;
            l.Interval_max = 12;
            Assert.IsTrue(Rab(0, new object[] { "Саратов", "28.06.2019", "3", l }));
            
        }

        [TestMethod]
        public void TestCreateDateLimitToPeople() // Тест распределения клиенту даты в пределах лимита
        {
            Limits l = new Limits();
            l.Interval_min = 0;
            l.Interval_max = 0;
            Rab(0, new object[] { "Якутск", "28.06.2019", "3", l });
            Rab(1, new string[] { "Семов", "Николай", "Владимирович", "Якутск", "11111111111" });
            v.people_date = v.List_people.Last();
            v.Lim1 = v.Limits_list.Last();
            Assert.IsTrue(Rab(2, new object[] { "28.06.2019", l }));
        }

        public bool Rab(int num, object o)
        {
            return bt = v.Obrabotka(num, o);
        }
    }
}
