﻿using Client.Modal;
using New_Stroevaya_chast.Modal.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Xml.Linq;
using System.Windows.Documents;
using System.Windows.Controls;
using System.Data;
using New_Stroevaya_chast.Modal;
using New_Stroevaya_chast.Modal.Model.LD;
using New_Stroevaya_chast.ViewModal;
using Stroevaya_chast.ViewModal;
using Stroevaya_chast.ViewModal.MessageConverter;
using System.ComponentModel;
using Client.Modal.Command;
using Microsoft.Win32;
using System.Windows.Media.Imaging;
using Client.View;
using System.Runtime.CompilerServices;

namespace Client.ViewModal
{
    public class TCP : INotifyPropertyChanged
    {
        #region Переменные
        Recrut recrut = new Recrut();
        DateClass dateClass = new DateClass();
        MessageToZashtat messageToZashtat = new MessageToZashtat();
        MessageToPerevod messageToPerevod = new MessageToPerevod();
        MessageToUbitie messageToUbitie = new MessageToUbitie();
        MessageToDelete messageToDelete = new MessageToDelete();
        MessageToIskl messageToIskl = new MessageToIskl();
        MessageToPerest messageToPerest = new MessageToPerest();
        public List<string> FIO_VRIO = new List<string>();
        static ObservableCollection<Izmen> _izm_list = new ObservableCollection<Izmen>();
        bool gg = false;
        static int Dop = 0; //Допуск пользователя
        bool _condition = true;

        private void NotifyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #region ШТАТ
        static ObservableCollection<OpenLoad> list_shtat = new ObservableCollection<OpenLoad>();

        public ObservableCollection<OpenLoad> List_shtat
        {
            get
            {
                return list_shtat;
            }
            set { list_shtat = value; NotifyChanged(); }
        }

        #endregion
        #region ЗАШТАТ
        static ObservableCollection<OpenLoadZaShtat> list_Anshtat = new ObservableCollection<OpenLoadZaShtat>();
        public ObservableCollection<OpenLoadZaShtat> List_AnShtat
        {
            get { return list_Anshtat; }
            set { list_Anshtat = value; NotifyChanged(); }
        }
        static ObservableCollection<OpenLoadZaShtat> list_Anshtat2 = new ObservableCollection<OpenLoadZaShtat>();
        public ObservableCollection<OpenLoadZaShtat> List_AnShtat2
        {
            get { return list_Anshtat2; }
            set { list_Anshtat2 = value; NotifyChanged(); }
        }
        #endregion
        #region ПЕРЕВОД
        static ObservableCollection<OpenLoadPerevod> list_perev = new ObservableCollection<OpenLoadPerevod>();
        static ObservableCollection<OpenLoadPerevod> list_perev_ne_iskl = new ObservableCollection<OpenLoadPerevod>();
        public ObservableCollection<OpenLoadPerevod> List_Perev
        {
            get { return list_perev; }
            set { list_perev = value; NotifyChanged(); }
        }
        #endregion
        #region УВОЛЕННЫЕ
        static ObservableCollection<OpenLoadDel> list_del = new ObservableCollection<OpenLoadDel>();
        static ObservableCollection<OpenLoadDel> list_del_ne_iskl = new ObservableCollection<OpenLoadDel>();
        public ObservableCollection<OpenLoadDel> list_Del
        {
            get { return list_del; }
            set { list_del = value; NotifyChanged(); }
        }
        #endregion
        #region ИСКЛЮЧЕННЫЕ
        static ObservableCollection<OpenLoadIskl> list_iskl = new ObservableCollection<OpenLoadIskl>();
        public ObservableCollection<OpenLoadIskl> list_Iskl
        {
            get { return list_iskl; }
            set { list_iskl = value; NotifyChanged(); }
        }
        #endregion
        #region ПЕРЕСТАНОВКИ
        static ObservableCollection<List_new> list_perest = new ObservableCollection<List_new>();
        public ObservableCollection<List_new> list_Perest
        {
            get { return list_perest; }
            set { list_perest = value; NotifyChanged(); }
        }

        #endregion
        #region Перестановочка
        static ObservableCollection<Perest> list_Per = new ObservableCollection<Perest>();
        static ObservableCollection<Perest> list_Per_tek = new ObservableCollection<Perest>();
        public ObservableCollection<Perest> List_Per
        {
            get { return list_Per; }
            set { list_Per = value; }
        }
        #endregion
        #region РСЗ
        static ObservableCollection<Modal.lic_rsz> list_rsz_lic = new ObservableCollection<Modal.lic_rsz>();
        public ObservableCollection<Modal.lic_rsz> list_RSZ_lic
        {
            get { return list_rsz_lic; }
            set { list_rsz_lic = value; NotifyChanged(); }
        }
        #endregion
        #region ВЫБОРКА
        static ObservableCollection<OpenLoad> list_Vibor = new ObservableCollection<OpenLoad>();
        public ObservableCollection<OpenLoad> List_Vibor
        {
            get { return list_Vibor; }
            set { list_Vibor = value; NotifyChanged(); }
        }
        #endregion
        #region Наряд
        static ObservableCollection<Naryad> list_Naryad = new ObservableCollection<Naryad>();
        public ObservableCollection<Naryad> List_Naryad
        {
            get { return list_Naryad; }
            set { list_Naryad = value; NotifyChanged(); }
        }
        #endregion
        #region Отобранные на контракт
        //Отобранные
        static ObservableCollection<Otbor_na_kontr> list_Otob = new ObservableCollection<Otbor_na_kontr>();
        public ObservableCollection<Otbor_na_kontr> List_Otob
        {
            get { return list_Otob; }
            set { list_Otob = value; NotifyChanged(); }
        }
        //Переданные
        static ObservableCollection<Otbor_na_kontr> list_Pered = new ObservableCollection<Otbor_na_kontr>();
        public ObservableCollection<Otbor_na_kontr> List_Pered
        {
            get { return list_Pered; }
            set { list_Pered = value; }
        }

        //Состоялись
        static ObservableCollection<Otbor_na_kontr> list_Sost = new ObservableCollection<Otbor_na_kontr>();
        public ObservableCollection<Otbor_na_kontr> List_Sost
        {
            get { return list_Sost; }
            set { list_Sost = value; }
        }

        //Отказ
        static ObservableCollection<Otbor_na_kontr> list_Otkaz = new ObservableCollection<Otbor_na_kontr>();
        public ObservableCollection<Otbor_na_kontr> List_Otkaz
        {
            get { return list_Otkaz; }
            set { list_Otkaz = value; }
        }
        #endregion
        #region Котел
        static ObservableCollection<OpenLoadKotel> list_Kotel1 = new ObservableCollection<OpenLoadKotel>();
        public ObservableCollection<OpenLoadKotel> List_Kotel1
        {
            get { return list_Kotel1; }
            set { list_Kotel1 = value; NotifyChanged(); }
        }

        static ObservableCollection<Kotel> list_Kotel2 = new ObservableCollection<Kotel>();
        public ObservableCollection<Kotel> List_Kotel2
        {
            get { return list_Kotel2; }
            set { list_Kotel2 = value; }
        }
        #endregion
        #region LD
        static ObservableCollection<zvanie> ld_zvanie = new ObservableCollection<zvanie>();

        public ObservableCollection<zvanie> LD_Zvanie
        {
            get
            {
                return ld_zvanie;
            }
            set { ld_zvanie = value; NotifyChanged(); }
        }
        static ObservableCollection<Poslug> ld_poslug = new ObservableCollection<Poslug>();

        public ObservableCollection<Poslug> LD_Poslug
        {
            get
            {

                return ld_poslug;
            }
            set { ld_poslug = value; NotifyChanged(); }
        }
        static ObservableCollection<family> ld_fam = new ObservableCollection<family>();

        public ObservableCollection<family> LD_Fam
        {
            get
            {
                return ld_fam;
            }
            set { ld_fam = value; NotifyChanged(); }
        }
        static ObservableCollection<Nagrad> ld_nag = new ObservableCollection<Nagrad>();

        public ObservableCollection<Nagrad> LD_Nag
        {
            get
            {
                return ld_nag;
            }
            set { ld_nag = value; NotifyChanged(); }
        }
        static ObservableCollection<Docum> ld_doc = new ObservableCollection<Docum>();
        static ObservableCollection<Docum> ld_doc1 = new ObservableCollection<Docum>();
        public ObservableCollection<Docum> LD_Doc
        {
            get
            {
                return ld_doc;
            }
            set { ld_doc = value; NotifyChanged(); }
        }
        public ObservableCollection<Docum> LD_Doc1
        {
            get
            {

                return ld_doc1;
            }
            set { ld_doc1 = value; NotifyChanged(); }
        }
        static ObservableCollection<Kontrakt> ld_kon = new ObservableCollection<Kontrakt>();

        public ObservableCollection<Kontrakt> LD_Kon
        {
            get
            {
                return ld_kon;
            }
            set { ld_kon = value; NotifyChanged(); }
        }
        public Naznach LD_Naz = new Naznach();

        static Dannie LD_Dan = new Dannie();
        public Dannie LD_DAN
        {
            get
            {
                return LD_Dan;
            }
            set { LD_Dan = value; NotifyChanged(); }
        }
        public PriemDel LD_PD = new PriemDel();
        public Vizhivanie LD_Viz = new Vizhivanie();
        #endregion
        #region УБЫТИЕ
        static ObservableCollection<Ub> list_ub = new ObservableCollection<Ub>();
        public ObservableCollection<Ub> List_ub
        {
            get { return list_ub; }
            set { list_ub = value; NotifyChanged(); }
        }
        #endregion
        #region УБЫТИЕ
        static ObservableCollection<Ub> list_preb = new ObservableCollection<Ub>();
        public ObservableCollection<Ub> List_preb
        {
            get { return list_preb; }
            set { list_preb = value; NotifyChanged(); }
        }
        #endregion
        #region Списки
        static ObservableCollection<List_new> l_ld = new ObservableCollection<List_new>();
        public ObservableCollection<List_new> L_LD
        {
            get { return l_ld; }
            set { l_ld = value; NotifyChanged(); }
        }
        static ObservableCollection<List_new> l_izm = new ObservableCollection<List_new>();
        public ObservableCollection<List_new> L_IZM
        {
            get { return l_izm; }
            set { l_izm = value; NotifyChanged(); }
        }
        static ObservableCollection<List_new> l_iskl = new ObservableCollection<List_new>();
        public ObservableCollection<List_new> L_ISKL
        {
            get { return l_iskl; }
            set { l_iskl = value; NotifyChanged(); }
        }
        static ObservableCollection<List_new> l_del = new ObservableCollection<List_new>();
        public ObservableCollection<List_new> L_DEL
        {
            get { return l_del; }
            set { l_del = value; NotifyChanged(); }
        }
        static ObservableCollection<List_new> l_perev = new ObservableCollection<List_new>();
        public ObservableCollection<List_new> L_PEREV
        {
            get { return l_perev; }
            set { l_perev = value; NotifyChanged(); }
        }
        static ObservableCollection<List_new> l_nd = new ObservableCollection<List_new>();
        public ObservableCollection<List_new> L_ND
        {
            get { return l_nd; }
            set { l_nd = value; NotifyChanged(); }
        }
        #endregion
        #region ПЕРЕЧНИ
        ObservableCollection<Perechen> Btgr = new ObservableCollection<Perechen>();
        ObservableCollection<Perechen> Og = new ObservableCollection<Perechen>();
        ObservableCollection<Perechen> Woman = new ObservableCollection<Perechen>();
        ObservableCollection<Perechen> Perec = new ObservableCollection<Perechen>();
        ObservableCollection<Perechen> Secret = new ObservableCollection<Perechen>();
        static ObservableCollection<Perechen> list_Btgr = new ObservableCollection<Perechen>();

        public ObservableCollection<Perechen> List_Btgr
        {
            get
            {
                if (list_Btgr == null)
                {

                }
                return list_Btgr;
            }
            set { list_Btgr = value; }
        }
        static ObservableCollection<Perechen> list_Og = new ObservableCollection<Perechen>();

        public ObservableCollection<Perechen> List_Og
        {
            get
            {
                if (list_Og == null)
                {

                }
                return list_Og;
            }
            set { list_Og = value; }
        }
        static ObservableCollection<Perechen> list_Woman = new ObservableCollection<Perechen>();

        public ObservableCollection<Perechen> List_Woman
        {
            get
            {
                if (list_Woman == null)
                {

                }
                return list_Woman;
            }
            set { list_Woman = value; }
        }
        static ObservableCollection<Perechen> list_Perec = new ObservableCollection<Perechen>();

        public ObservableCollection<Perechen> List_Perec
        {
            get
            {
                if (list_Perec == null)
                {

                }
                return list_Perec;
            }
            set { list_Perec = value; }
        }
        static ObservableCollection<Perechen> list_Secret = new ObservableCollection<Perechen>();

        public ObservableCollection<Perechen> List_Secret
        {
            get
            {
                if (list_Secret == null)
                {

                }
                return list_Secret;
            }
            set { list_Secret = value; }
        }
        #endregion
        #region Команда
        static ObservableCollection<Komand_ld> list_komand = new ObservableCollection<Komand_ld>();

        public ObservableCollection<Komand_ld> List_komand
        {
            get
            {

                return list_komand;
            }
            set { list_komand = value; NotifyChanged(); }
        }

        #endregion
        #endregion

        // Объект содержащий рабочий сокет клиентского приложения
        TcpClient _tcpСlient = new TcpClient();

        // Объект сетевого потока для приема и отправки сообщений
        NetworkStream ns;

        // Флаг для остановки потоков и завершения сетевой работы приложения
        bool _stopNetwork;

        #region Функциональная часть Сетевая работа

        // Попытка подключения к серверу
        string ip()
        {
            string s = ".";
            string fileName = Environment.CurrentDirectory + "\\Recources\\config.xml";
            XDocument doc = XDocument.Load(fileName);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                s = el.Attribute("value").Value;
            }
            return s;
        }
        public void Connect()
        {
            try
            {

                _tcpСlient.Connect(ip(), 15000);
                ns = _tcpСlient.GetStream();
                Thread th = new Thread(ReceiveRun);
                th.Start();
            }
            catch
            {
                ErrorSound();
            }
        }
        public void CloseClient()
        {
            if (ns != null) ns.Close();
            if (_tcpСlient != null) _tcpСlient.Close();

            _stopNetwork = true;

        }

        // Отправка сообщений в блокирующем режиме,
        // поскольку обмен короткими сообщениями
        // не вызовет заметного блокирования интерфейса приложения. 
        public void SendMessage(string Text_cap)
        {
            DispatcherHelper.Initialize();
            ThreadPool.QueueUserWorkItem(
                obj =>
                {
                    while (_condition)
                    {
                        if (ns != null)
                        {
                            byte[] buffer = Encoding.Default.GetBytes(Text_cap);
                            ns.Write(buffer, 0, buffer.Length);
                            Thread.Sleep(500);
                        }
                    }
                });
        }


        // Цикл извлечения сообщений,
        // запускается в отдельном потоке.
        void ReceiveRun()
        {
            while (true)
            {
                try
                {
                    string s = null;
                    while (ns.DataAvailable == true)
                    {
                        // Определение необходимого размера буфера приема.
                        byte[] buffer = new byte[_tcpСlient.Available];

                        ns.Read(buffer, 0, buffer.Length);
                        s += Encoding.Default.GetString(buffer);
                    }

                    if (s != null)
                    {
                        ShowReceiveMessage(s);

                        //MessageBox.Show(s);
                        //s = String.Empty;
                    }


                    // Вынужденная строчка для экономия ресурсов процессора.
                    // Неизящный способ.
                    Thread.Sleep(100);
                }
                catch
                {
                    ErrorSound();
                }

                if (_stopNetwork == true) break;
            }
        }
        #endregion
        #region Информации о сетевой работе

        // Код доступа к свойствам объектов главной формы  из других потоков
        delegate void UpdateReceiveDisplayDelegate(string message);
        //Принятие сообщений
        void ShowReceiveMessage(string message)
        {

            string[] wor = message.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
            if (wor.Count() > 1)
                if (wor[0] == "LD1")
                {
                    _Pered_ld(wor[1]);
                }
            string[] world = message.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            if (world[0] == "izm")
            {
                Izmen T = new Izmen
                {
                    Types = world[1],
                    Row = int.Parse(world[2]),
                    Name_table = world[3],
                    Old_value = world[4],
                    New_value = world[5],
                    Col = world[6]
                };
                Izm_d(T);
                _izm_list.Add(T);
            }
            if (world[0] == "OpenFile")
            {
                if (world[1] == "Переведенные")
                {
                    list_perev = new ObservableCollection<OpenLoadPerevod>();
                    list_perev = messageToPerevod.server_perev(world[2]);
                }
                if (world[1] == "Уволенные")
                {
                    list_del = new ObservableCollection<OpenLoadDel>();
                    list_del = messageToDelete.server_del(world[2]);
                }
                if (world[1] == "Исключенные")
                {
                    list_iskl = new ObservableCollection<OpenLoadIskl>();
                    list_iskl = messageToIskl.server_iskl(world[2]);
                }
                if (world[1] == "Перестановка")
                {
                    list_Per = new ObservableCollection<Perest>();
                    list_Per = messageToPerest.server_perest(world[2]);
                }
            }
            string[] word = message.Split(new char[] { '*' }, StringSplitOptions.RemoveEmptyEntries);

            if (word.Length > 1)
            {
                string[] word_dop = word[0].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                Dop = int.Parse(word_dop[1]);
                string[] word_shtat = word[1].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                string[] word_anshtat = word[2].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                string[] word_kotel = word[5].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                create_list();
                list_shtat = recrut.shtat_pol(word_shtat, word_dop[1]);
                list_Anshtat = messageToZashtat.anshtat_pol(word_anshtat);
                list_Anshtat2 = messageToZashtat.Anstat2(list_Anshtat, word_dop[1]);
                MessageToOtborNaKontract messageToOtborNaKontract = new MessageToOtborNaKontract(word[6]);
                list_Otob = messageToOtborNaKontract.OTOBR;
                list_Pered = messageToOtborNaKontract.PERED;
                list_Sost = messageToOtborNaKontract.SOST;
                list_Otkaz = messageToOtborNaKontract.OTKAZ;
                kotel(word_kotel, word_dop[1]);
                spiski(word[7]);
                list_perev = messageToPerevod.server_perev(word[8]);
                list_ub = messageToUbitie.server_ub(word[9]);
                list_del = messageToDelete.server_del(word[3]);
                list_iskl = messageToIskl.server_iskl(word[4]);
                Copy();
                Proverka_otryv();
                MessageToPerechen messageToPerechen = new MessageToPerechen(word[10]);
                Btgr = messageToPerechen.BTGR;
                Og = messageToPerechen.OG;
                Woman = messageToPerechen.WOMAN;
                Perec = messageToPerechen.PERECH;
                Secret = messageToPerechen.SECRET;
                Sopost_perec();
            }
            if (word[0] == "kot-preb")
            {
                Izmen T = new Izmen
                {
                    Types = word[1],
                    Row = int.Parse(word[2]),
                    Name_table = word[3],
                    Old_value = word[4],
                    New_value = word[5],
                    Col = word[6]
                };
                preb_kotel(T);
            }
        }
        //Копирование коллекций, для возможности открытия ранних файлов, без потери текущего состояния
        void Copy()
        {
            foreach (OpenLoadDel a in list_del)
                list_del_ne_iskl.Add(a);
            foreach (OpenLoadPerevod a in list_perev)
                list_perev_ne_iskl.Add(a);
            foreach (Perest a in list_Per)
                list_Per_tek.Add(a);

        }
        //Соотношение перечней должностей, комплектуемые по определенному признаку с должностями в коллекции ШТАТ
        void Sopost_perec()
        {
            DispatcherHelper.Initialize();
            ThreadPool.QueueUserWorkItem(
                obj =>
                {

                    foreach (OpenLoad a in list_shtat)
                        foreach (Perechen p in Btgr)
                        {
                            if (p.id == a.Id)
                            {
                                if (p.value == ".") a.BTGR = "входит";
                                else
                                    a.BTGR = p.value;
                                break;
                            }
                            else
                            {
                                a.BTGR = ".";
                            }
                        }



                    foreach (OpenLoad a in list_shtat)
                        foreach (Perechen p in Og)
                        {
                            if (p.id == a.Id)
                            {
                                if (p.value == ".") a.OG = "входит";
                                else
                                    a.OG = p.value;
                                break;
                            }
                            else
                            {
                                a.OG = ".";
                            }
                        }

                    foreach (OpenLoad a in list_shtat)
                        foreach (Perechen p in Woman)
                        {
                            if (p.id == a.Id)
                            {
                                if (p.value == ".") a.Woman = "входит";
                                else
                                    a.Woman = p.value;
                                break;
                            }
                            else
                            {
                                a.Woman = ".";
                            }
                        }


                    foreach (OpenLoad a in list_shtat)
                        foreach (Perechen p in Perec)
                        {
                            if (p.id == a.Id)
                            {
                                if (p.value == ".") a.Perec = "входит";
                                else
                                    a.Perec = p.value;
                                break;
                            }
                            else
                            {
                                a.Perec = ".";
                            }
                        }

                    foreach (OpenLoad a in list_shtat)
                        foreach (Perechen p in Secret)
                        {
                            if (p.id == a.Id)
                            {
                                if (p.value == ".") a.Secret = "входит";
                                else
                                    a.Secret = p.value;
                                break;
                            }
                            else
                            {
                                a.Secret = ".";
                            }
                        }
                    Thread.Sleep(500);
                });

        }
        // Возврат значений текущего состояния коллекций, после открытия файлов предыдущихверсий
        public void Vne_iskl(string s)
        {
            if (s == "Переведенные, не исключенные")
            {
                if (list_perev.Count != 0)
                {
                    var b = list_perev.Count;
                    for (int i = 0; i < b; i++)
                    {
                        foreach (OpenLoadPerevod a in list_perev)
                        {
                            list_perev.Remove(a); break;
                        }
                        if (list_perev.Count == 0) break;
                    }

                }
                foreach (OpenLoadPerevod a in list_perev_ne_iskl)
                    list_perev.Add(a);
            }
            if (s == "Уволенные, не исключенные")
            {
                if (list_del.Count != 0)
                {
                    var b = list_del.Count;
                    for (int i = 0; i < b; i++)
                    {
                        foreach (OpenLoadDel a in list_del)
                        {
                            list_del.Remove(a); break;
                        }
                        if (list_del.Count == 0) break;
                    }
                }
                foreach (OpenLoadDel a in list_del_ne_iskl)
                    list_del.Add(a);

            }
            if (s == "Текущая перестановка")
            {
                if (list_Per.Count != 0)
                {
                    var b = list_Per.Count;
                    for (int i = 0; i < b; i++)
                    {
                        foreach (Perest a in list_Per)
                        {
                            list_Per.Remove(a); break;
                        }
                        if (list_Per.Count == 0) break;
                    }
                }
                foreach (Perest a in list_Per_tek)
                    list_Per.Add(a);
                gg = false;
            }
        }
        //Соотношение списка убывших со списком личного состава в коллекции ШТАТ
        void Proverka_otryv()
        {
            foreach (OpenLoad p in list_shtat)
            {
                foreach (Ub i in list_ub)
                {
                    if (p.Lnumber == i.Lnumber)
                    {
                        p.Otryv_status = i.Types_ub;
                        if (i.Cel != ".")
                            p.Otryv_primech += i.Cel;
                        if (i.Date_ub != ".")
                            p.Otryv_primech += "_с_" + i.Date_ub;
                        if (i.Date_preb != ".")
                            p.Otryv_primech += "_по_" + i.Date_preb;
                        if (i.Mesto != ".")
                            p.Otryv_primech += "_с выездом_" + i.Mesto;
                        if (i.Osn != ".")
                            p.Otryv_primech += "_основание:_" + i.Osn;
                        if (i.Vrio != ".")
                            p.Otryv_primech += "_Временно исполняет обязонности:_" + i.Vrio;
                        break;
                    }
                }
            }
        }
        //Формирование списков файлов для просмотра старых версий
        void spiski(string message)
        {
            int t = 0;
            string[] word = message.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 1; i < word.Length; i++)
            {
                if (word[i] == "LD")
                {
                    t = 1;
                    continue;
                }
                if (word[i] == "Izm")
                {
                    t = 2; continue;
                }
                if (word[i] == "Iskl")
                {
                    t = 3; continue;
                }
                if (word[i] == "Del")
                {
                    t = 4; continue;
                }
                if (word[i] == "Perev")
                {
                    t = 5; continue;
                }
                if (word[i] == "ND")
                {
                    t = 6; continue;
                }
                if (word[i] == "Peres")
                {
                    t = 7; continue;
                }
                switch (t)
                {
                    case 1:
                        {
                            List_new tt = new List_new();
                            tt.Name = word[i];
                            l_ld.Add(tt);
                            break;
                        }
                    case 2:
                        {
                            List_new tt = new List_new();
                            tt.Name = word[i];
                            l_izm.Add(tt);
                            break;
                        }
                    case 3:
                        {
                            List_new tt = new List_new();
                            tt.Name = word[i];
                            l_iskl.Add(tt);
                            break;
                        }
                    case 4:
                        {
                            List_new tt = new List_new();
                            tt.Name = word[i];
                            l_del.Add(tt);
                            break;
                        }
                    case 5:
                        {
                            List_new tt = new List_new();
                            tt.Name = word[i];
                            l_perev.Add(tt);
                            break;
                        }
                    case 6:
                        {
                            List_new tt = new List_new();
                            tt.Name = word[i];
                            l_nd.Add(tt);
                            break;
                        }
                    case 7:
                        {
                            List_new tt = new List_new();
                            tt.Name = word[i];
                            list_perest.Add(tt);
                            break;
                        }
                }

            }
            List_new td = new List_new();
            td.Name = "Переведенные, не исключенные";
            l_perev.Add(td);
            td = new List_new();
            td.Name = "Уволенные, не исключенные";
            l_del.Add(td);
        }
        //Отображение изменения, пришедших с сервера от другого пользователя
        public void Izm_d(Izmen t)
        {
            if (t.Types == "Удалить")
            {
                OpenLoadZaShtat p = new OpenLoadZaShtat();
                string[] word = t.Col.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
                p.FIO = word[2];
                p.Last_dolg = word[0];
                p.Last_podr = word[1];
                p.Lnumber = t.Old_value;
                p.Zvanie = word[3];
                p.Types = word[4];
                list_Anshtat.Add(p);
                foreach (OpenLoad i in list_shtat)
                {
                    if (i.Lnumber == p.Lnumber)
                    {
                        i.Lnumber = ".";
                        i.Types = ".";
                        i.Zvanie = ".";
                        i.Otryv_pricaz = ".";
                        i.Otryv_primech = ".";
                        i.Otryv_status = ".";
                        i.Fio = "-В-";
                    }
                }
                kotel2();
            }
            if (t.Types == "Добавить")
            {
                string t1 = "."; string t2 = "."; string t3 = ".";
                string[] word = t.Col.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (Ub p in list_ub)
                {
                    if (p.Lnumber == t.New_value)
                    {
                        t1 = p.Types_ub;
                        if (p.Date_ub != ".")
                            t2 += "_c_" + p.Date_ub;
                        if (p.Date_preb != ".")
                            t2 += "_по_" + p.Date_preb;
                        if (p.Mesto != ".")
                            t2 += "_с выездом в_" + p.Mesto;
                    }
                }
                foreach (OpenLoad p in list_shtat)
                {
                    if (p.Id == t.Row)
                    {
                        p.Lnumber = t.New_value;
                        p.Zvanie = word[2];
                        p.Types = word[1];
                        p.Fio = word[0];
                        p.Otryv_status = t1;
                        p.Otryv_primech = t2;
                        p.Otryv_pricaz = t3;
                    }
                }
                foreach (OpenLoadZaShtat p in list_Anshtat)
                {
                    if (p.Lnumber == t.New_value)
                    {
                        list_Anshtat.Remove(p);
                        break;
                    }
                }
                Proverka_otryv();
                kotel2();
            }
            if (t.Types == "Убытие")
            {
                string[] word = t.New_value.Split(new char[] { '*' }, StringSplitOptions.RemoveEmptyEntries);
                Ub t1 = new Ub();
                t1.Cel = word[0];
                t1.Date3 = word[1];
                t1.Date_preb = word[2];
                t1.Date_ub = word[3];
                t1.Dolg = word[4];
                t1.FIO = word[5];
                t1.Lnumber = word[6];
                t1.Mesto = word[7];
                t1.Osn = word[8];
                t1.Podr = word[9];
                t1.Types = word[10];
                t1.Types_ub = word[11];
                t1.VPD_ST1 = word[12];
                t1.VPD_ST2 = word[13];
                t1.VPD_ST3 = word[14];
                t1.VPD_ST4 = word[15];
                t1.VPD_ST5 = word[16];
                t1.VPD_ST6 = word[17];
                t1.VPD_ST7 = word[18];
                t1.Vrio = word[19];
                t1.Zvanie = word[20];
                list_ub.Add(t1);
                foreach (OpenLoad a in list_shtat)
                {
                    if (a.Lnumber == t1.Lnumber)
                    {
                        a.Otryv_status = word[11];
                        if (t1.Cel != ".")
                            a.Otryv_primech += t1.Cel;
                        if (t1.Date_ub != ".")
                            a.Otryv_primech += "_c_" + t1.Date_ub;
                        if (t1.Date_preb != ".")
                            a.Otryv_primech += "_по_" + t1.Date_preb;
                        if (t1.Mesto != ".")
                            a.Otryv_primech += "_с выездом в_" + t1.Mesto;
                    }
                }
                kotel2();
            }
            if (t.Types == "Пребытие")
            {
                string[] word = t.New_value.Split(new char[] { '*' }, StringSplitOptions.RemoveEmptyEntries);
                Ub t1 = new Ub();
                t1.Cel = word[0];
                t1.Date3 = word[1];
                t1.Date_preb = word[2];
                t1.Date_ub = word[3];
                t1.Dolg = word[4];
                t1.FIO = word[5];
                t1.Lnumber = word[6];
                t1.Mesto = word[7];
                t1.Osn = word[8];
                t1.Podr = word[9];
                t1.Types = word[10];
                t1.Types_ub = word[11];
                t1.VPD_ST1 = word[12];
                t1.VPD_ST2 = word[13];
                t1.VPD_ST3 = word[14];
                t1.VPD_ST4 = word[15];
                t1.VPD_ST5 = word[16];
                t1.VPD_ST6 = word[17];
                t1.VPD_ST7 = word[18];
                t1.Vrio = word[19];
                t1.Zvanie = word[20];
                list_preb.Add(t1);
                foreach (OpenLoad a in list_shtat)
                {
                    if (a.Lnumber == t1.Lnumber)
                    {
                        a.Otryv_status = ".";
                        a.Otryv_primech = ".";
                        a.Otryv_pricaz = ".";
                    }
                }
            }
            if (t.Types == "Отбор на контракт")
            {
                if (t.Name_table == "Отобранные")
                {
                    Otbor_na_kontr tmp = new Otbor_na_kontr();
                    string[] word = t.Col.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
                    if (word[0] == "true") tmp.Blank = true; else if (word[0] == "false") tmp.Blank = false;
                    if (word[7] == "true") tmp.FIZO = true; else if (word[7] == "false") tmp.FIZO = false;
                    if (word[8] == "true") tmp.Ic = true; else if (word[8] == "false") tmp.Ic = false;
                    if (word[12] == "true") tmp.Prof = true; else if (word[12] == "false") tmp.Prof = false;
                    if (word[13] == "true") tmp.Raport = true; else if (word[13] == "false") tmp.Raport = false;
                    if (word[15] == "true") tmp.Vvk = true; else if (word[15] == "false") tmp.Vvk = false;
                    tmp.Date1 = word[1];
                    tmp.Date2 = word[2];
                    tmp.Date3 = word[3];
                    tmp.Date4 = word[4];
                    tmp.Date_ic = word[5];
                    tmp.Dolg = word[6];
                    tmp.FIO = t.New_value;
                    tmp.Lnumber = word[9];
                    tmp.Podr = word[10];
                    tmp.Primec = word[11];
                    tmp.Types = word[14];
                    tmp.Zvanie = word[16];
                    list_Otob.Add(tmp);
                    foreach (Otbor_na_kontr i in list_Otkaz)
                    {
                        if (i.Lnumber == tmp.Lnumber)
                        {
                            list_Otkaz.Remove(i);
                            break;
                        }
                    }
                }
                if (t.Name_table == "Переданные")
                {
                    Otbor_na_kontr tmp = new Otbor_na_kontr();
                    string[] word = t.Col.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
                    if (word[0] == "true") tmp.Blank = true; else if (word[0] == "false") tmp.Blank = false;
                    if (word[7] == "true") tmp.FIZO = true; else if (word[7] == "false") tmp.FIZO = false;
                    if (word[8] == "true") tmp.Ic = true; else if (word[8] == "false") tmp.Ic = false;
                    if (word[12] == "true") tmp.Prof = true; else if (word[12] == "false") tmp.Prof = false;
                    if (word[13] == "true") tmp.Raport = true; else if (word[13] == "false") tmp.Raport = false;
                    if (word[15] == "true") tmp.Vvk = true; else if (word[15] == "false") tmp.Vvk = false;
                    tmp.Date1 = word[1];
                    tmp.Date2 = word[2];
                    tmp.Date3 = word[3];
                    tmp.Date4 = word[4];
                    tmp.Date_ic = word[5];
                    tmp.Dolg = word[6];
                    tmp.FIO = t.New_value;
                    tmp.Lnumber = word[9];
                    tmp.Podr = word[10];
                    tmp.Primec = word[11];
                    tmp.Types = word[14];
                    tmp.Zvanie = word[16];
                    foreach (Otbor_na_kontr i in list_Otob)
                    {
                        if (i.Lnumber == tmp.Lnumber)
                        {
                            list_Otob.Remove(i);
                            break;
                        }
                    }
                }
                if (t.Name_table == "Состоялись")
                {
                    Otbor_na_kontr tmp = new Otbor_na_kontr();
                    string[] word = t.Col.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
                    if (word[0] == "true") tmp.Blank = true; else if (word[0] == "false") tmp.Blank = false;
                    if (word[7] == "true") tmp.FIZO = true; else if (word[7] == "false") tmp.FIZO = false;
                    if (word[8] == "true") tmp.Ic = true; else if (word[8] == "false") tmp.Ic = false;
                    if (word[12] == "true") tmp.Prof = true; else if (word[12] == "false") tmp.Prof = false;
                    if (word[13] == "true") tmp.Raport = true; else if (word[13] == "false") tmp.Raport = false;
                    if (word[15] == "true") tmp.Vvk = true; else if (word[15] == "false") tmp.Vvk = false;
                    tmp.Date1 = word[1];
                    tmp.Date2 = word[2];
                    tmp.Date3 = word[3];
                    tmp.Date4 = word[4];
                    tmp.Date_ic = word[5];
                    tmp.Dolg = word[6];
                    tmp.FIO = t.New_value;
                    tmp.Lnumber = word[9];
                    tmp.Podr = word[10];
                    tmp.Primec = word[11];
                    tmp.Types = word[14];
                    tmp.Zvanie = word[16];
                    foreach (Otbor_na_kontr i in list_Pered)
                    {
                        if (i.Lnumber == tmp.Lnumber)
                        {
                            list_Pered.Remove(i);
                            break;
                        }
                    }
                    foreach (OpenLoad a in list_shtat)
                    {
                        if (a.Lnumber == tmp.Lnumber)
                        {
                            a.Types = "контр";
                        }
                    }
                    foreach (OpenLoadZaShtat a in list_Anshtat)
                    {
                        if (a.Lnumber == tmp.Lnumber)
                        {
                            a.Types = "контр";
                        }
                    }
                    foreach (OpenLoadKotel i in list_Kotel1)
                    {
                        if (i.Lnumber == tmp.Lnumber)
                        {
                            list_Kotel1.Remove(i); break;
                        }
                    }
                    kotel2();
                }
                if (t.Name_table == "Отказ")
                {
                    Otbor_na_kontr tmp = new Otbor_na_kontr();
                    string[] word = t.Col.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
                    if (word[0] == "true") tmp.Blank = true; else if (word[0] == "false") tmp.Blank = false;
                    if (word[7] == "true") tmp.FIZO = true; else if (word[7] == "false") tmp.FIZO = false;
                    if (word[8] == "true") tmp.Ic = true; else if (word[8] == "false") tmp.Ic = false;
                    if (word[12] == "true") tmp.Prof = true; else if (word[12] == "false") tmp.Prof = false;
                    if (word[13] == "true") tmp.Raport = true; else if (word[13] == "false") tmp.Raport = false;
                    if (word[15] == "true") tmp.Vvk = true; else if (word[15] == "false") tmp.Vvk = false;
                    tmp.Date1 = word[1];
                    tmp.Date2 = word[2];
                    tmp.Date3 = word[3];
                    tmp.Date4 = word[4];
                    tmp.Date_ic = word[5];
                    tmp.Dolg = word[6];
                    tmp.FIO = t.New_value;
                    tmp.Lnumber = word[9];
                    tmp.Podr = word[10];
                    tmp.Primec = word[11];
                    tmp.Types = word[14];
                    tmp.Zvanie = word[16];
                    foreach (Otbor_na_kontr i in list_Pered)
                    {
                        if (i.Lnumber == tmp.Lnumber)
                        {
                            list_Pered.Remove(i);
                            break;
                        }
                    }
                    foreach (Otbor_na_kontr i in list_Otob)
                    {
                        if (i.Lnumber == tmp.Lnumber)
                        {
                            list_Otob.Remove(i);
                            break;
                        }
                    }
                    foreach (Otbor_na_kontr i in list_Sost)
                    {
                        if (i.Lnumber == tmp.Lnumber)
                        {
                            list_Sost.Remove(i);
                            break;
                        }
                    }
                }
            }
            if (t.Types == "Котел")
            {
                if (t.Name_table == "Добавить")
                {
                    string[] word = t.Col.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
                    OpenLoadKotel tt = new OpenLoadKotel();
                    tt.Lnumber = t.New_value;
                    tt.Podr = word[1];
                    tt.Types = word[2];
                    tt.FIO = word[0];
                    list_Kotel1.Add(tt);
                    kotel2();
                }
                if (t.Name_table == "Убрать")
                {
                    foreach (OpenLoadKotel a in list_Kotel1)
                    {
                        if (a.Lnumber == t.New_value)
                        {
                            list_Kotel1.Remove(a);
                            break;
                        }
                    }
                }
                if (t.Name_table == "Убытие")
                {
                    foreach (OpenLoadKotel a in list_Kotel1)
                    {
                        if (a.Lnumber == t.New_value)
                        {
                            list_Kotel1.Remove(a);
                            break;
                        }
                    }
                }

            }
        }
        //Перевод сообщения в пофамильный список питающихся в столовой
        private void kotel(string[] word, string dop)
        {
            OpenLoadKotel tmp = new OpenLoadKotel();
            int ij = 1;
            for (int i = 1; i < word.Length; i++)
            {
                int j = i;
                while (j > 4)
                {
                    j -= 4;
                }
                if (j == 1)
                {
                    tmp = new OpenLoadKotel();
                    tmp.Lnumber = word[i];
                    //tmp.Id = ij;
                    ij++;
                }
                if (j == 2)
                {
                    tmp.Podr = word[i];
                }
                if (j == 3)
                {
                    tmp.FIO = word[i];
                }
                if (j == 4)
                {
                    tmp.Types = word[i];
                    list_Kotel1.Add(tmp);
                }
            }
            kotel2();
        }
        //Подсчет количества людей питающихся в столовой
        private void kotel2()
        {
            if (list_Kotel2.Count != 0)
            {
                list_Kotel2.Clear();
            }
            var list = list_shtat.Select(p => p.Podr1).Distinct();
            foreach (string pp in list)
            {
                Kotel tmp = new Kotel();
                tmp.Podr = pp;
                foreach (OpenLoadKotel p in list_Kotel1)
                {
                    if (p.Podr == pp)
                    {
                        tmp.Col++;
                    }
                }

                list_Kotel2.Add(tmp);
            }
            Kotel ter = new Kotel();
            ter.Podr = "Итого";
            foreach (Kotel p in list_Kotel2)
            {
                ter.Col += p.Col;
            }
            list_Kotel2.Add(ter);
        }
        //Очистка выборки
        public void clear_viborka()
        {
            list_Vibor.Clear();
        }
        //Увольнение из ЗАШТАТНИКОВ
        public void Delete_people(OpenLoadDel t)
        {
            if (Dop == 7)
            {
                if (t.Types != "контр" && t.Types != "жен")
                {
                    foreach (OpenLoadZaShtat N in list_Anshtat)
                    {
                        if (t.Lnumber == N.Lnumber)
                        {
                            list_Anshtat.Remove(N);
                            list_Anshtat2.Remove(N);
                            break;
                        }
                    }
                    list_del.Add(t);
                    Izmen td = new Izmen();
                    td.Types = "Увольнение";
                    td.New_value = t.Lnumber + ">" + t.FIO + ">" + t.Zvanie + ">" + t.Types + ">" + t.Dolg + ">" + t.Podr + ">" + t.Date + ">" + t.Punkt + ">" + t.Podpunkt + ">" + t.Osn + ">" + t.Pr + ">" + t.Date_pr + ">" + t.What_pr;
                    SendMessage("izm|" + td.Types + "|" + td.Row + "|" + td.Name_table + "|" + td.Old_value + "|" + td.New_value + "|" + td.Col);
                }
            }
            if (Dop == 5 || Dop == 6)
            {
                if (t.Zvanie == "рядовой" && t.Types == "контр" || t.Zvanie == "рядовой" && t.Types == "жен" || t.Zvanie == "ефрейтор" && t.Types == "контр" || t.Zvanie == "ефрейтор" && t.Types == "жен" || t.Zvanie == "младший сержант" && t.Types == "контр" || t.Zvanie == "младший сержант" && t.Types == "жен" || t.Zvanie == "сержант" && t.Types == "контр" || t.Zvanie == "сержант" && t.Types == "жен" || t.Zvanie == "старший сержант" && t.Types == "контр" || t.Zvanie == "старший сержант" && t.Types == "жен" || t.Zvanie == "старшина" && t.Types == "контр" || t.Zvanie == "старшина" && t.Types == "жен")
                {
                    foreach (OpenLoadZaShtat N in list_Anshtat)
                    {
                        if (t.Lnumber == N.Lnumber)
                        {
                            list_Anshtat.Remove(N);
                            list_Anshtat2.Remove(N); break;
                        }
                    }
                    list_del.Add(t);
                    Izmen td = new Izmen();
                    td.Types = "Увольнение";
                    td.New_value = t.Lnumber + ">" + t.FIO + ">" + t.Zvanie + ">" + t.Types + ">" + t.Dolg + ">" + t.Podr + ">" + t.Date + ">" + t.Punkt + ">" + t.Podpunkt + ">" + t.Osn + ">" + t.Pr + ">" + t.Date_pr + ">" + t.What_pr;
                    SendMessage("izm|" + td.Types + "|" + td.Row + "|" + td.Name_table + "|" + td.Old_value + "|" + td.New_value + "|" + td.Col);
                }
            }
            if (Dop == 4)
            {
                if (t.Zvanie == "старшина" || t.Zvanie == "прапорщик" || t.Zvanie == "старший прапорщик")
                {
                    foreach (OpenLoadZaShtat N in list_Anshtat)
                    {
                        if (t.Lnumber == N.Lnumber)
                        {
                            list_Anshtat.Remove(N);
                            list_Anshtat2.Remove(N); break;
                        }
                    }
                    list_del.Add(t);
                    Izmen td = new Izmen();
                    td.Types = "Увольнение";
                    td.New_value = t.Lnumber + ">" + t.FIO + ">" + t.Zvanie + ">" + t.Types + ">" + t.Dolg + ">" + t.Podr + ">" + t.Date + ">" + t.Punkt + ">" + t.Podpunkt + ">" + t.Osn + ">" + t.Pr + ">" + t.Date_pr + ">" + t.What_pr;
                    SendMessage("izm|" + td.Types + "|" + td.Row + "|" + td.Name_table + "|" + td.Old_value + "|" + td.New_value + "|" + td.Col);
                }
            }
            if (Dop == 3)
            {
                if (t.Zvanie == "лейтенант" || t.Zvanie == "старший лейтенант" || t.Zvanie == "капитан" || t.Zvanie == "майор" || t.Zvanie == "подполковник" || t.Zvanie == "полковник" || t.Zvanie == "генерал-лейтенант")
                {
                    foreach (OpenLoadZaShtat N in list_Anshtat)
                    {
                        if (t.Lnumber == N.Lnumber)
                        {
                            list_Anshtat.Remove(N);
                            list_Anshtat2.Remove(N); break;
                        }
                    }
                    list_del.Add(t);
                    Izmen td = new Izmen();
                    td.Types = "Увольнение";
                    td.New_value = t.Lnumber + ">" + t.FIO + ">" + t.Zvanie + ">" + t.Types + ">" + t.Dolg + ">" + t.Podr + ">" + t.Date + ">" + t.Punkt + ">" + t.Podpunkt + ">" + t.Osn + ">" + t.Pr + ">" + t.Date_pr + ">" + t.What_pr;
                    SendMessage("izm|" + td.Types + "|" + td.Row + "|" + td.Name_table + "|" + td.Old_value + "|" + td.New_value + "|" + td.Col);
                }
            }
        }
        //Перевод из ЗАШТАТНИКОВ
        public void Perevod_people(OpenLoadPerevod t)
        {

            if (Dop == 7)
            {
                if (t.Types != "контр" && t.Types != "жен")
                {

                    list_perev.Add(t);
                    foreach (OpenLoadZaShtat a in list_Anshtat)
                    {
                        if (a.Lnumber == t.Lnumber)
                        {
                            list_Anshtat.Remove(a);
                            list_Anshtat2.Remove(a);
                            break;
                        }
                    }

                    Izmen td = new Izmen();
                    td.Types = "Перевод";
                    td.New_value = t.Lnumber + ">" + t.FIO + ">" + t.Zvanie + ">" + t.Types + ">" + t.Dolg + ">" + t.Podr + ">" + t.Date + ">" + t.Chast + ">" + t.Gorod + ">" + t.Osn + ">" + t.Pr + ">" + t.Date_pr + ">" + t.What_pr;
                    SendMessage("izm|" + td.Types + "|" + td.Row + "|" + td.Name_table + "|" + td.Old_value + "|" + td.New_value + "|" + td.Col);
                }
            }
            if (Dop == 5 || Dop == 6)
            {
                if (t.Zvanie == "рядовой" && t.Types == "контр" || t.Zvanie == "рядовой" && t.Types == "жен" || t.Zvanie == "ефрейтор" && t.Types == "контр" || t.Zvanie == "ефрейтор" && t.Types == "жен" || t.Zvanie == "младший сержант" && t.Types == "контр" || t.Zvanie == "младший сержант" && t.Types == "жен" || t.Zvanie == "сержант" && t.Types == "контр" || t.Zvanie == "сержант" && t.Types == "жен" || t.Zvanie == "старший сержант" && t.Types == "контр" || t.Zvanie == "старший сержант" && t.Types == "жен" || t.Zvanie == "старшина" && t.Types == "контр" || t.Zvanie == "старшина" && t.Types == "жен")
                {
                    list_perev.Add(t);
                    foreach (OpenLoadZaShtat a in list_Anshtat)
                    {
                        if (a.Lnumber == t.Lnumber)
                        {
                            list_Anshtat.Remove(a);
                            list_Anshtat2.Remove(a);
                            break;
                        }
                    }
                    Izmen td = new Izmen();
                    td.Types = "Перевод";
                    td.New_value = t.Lnumber + ">" + t.FIO + ">" + t.Zvanie + ">" + t.Types + ">" + t.Dolg + ">" + t.Podr + ">" + t.Date + ">" + t.Chast + ">" + t.Gorod + ">" + t.Osn + ">" + t.Pr + ">" + t.Date_pr + ">" + t.What_pr;
                    SendMessage("izm|" + td.Types + "|" + td.Row + "|" + td.Name_table + "|" + td.Old_value + "|" + td.New_value + "|" + td.Col);
                }
            }
            if (Dop == 4)
            {
                if (t.Zvanie == "старшина" || t.Zvanie == "прапорщик" || t.Zvanie == "старший прапорщик")
                {
                    list_perev.Add(t);
                    foreach (OpenLoadZaShtat a in list_Anshtat)
                    {
                        if (a.Lnumber == t.Lnumber)
                        {
                            list_Anshtat.Remove(a);
                            list_Anshtat2.Remove(a);
                            break;
                        }
                    }
                    Izmen td = new Izmen();
                    td.Types = "Перевод";
                    td.New_value = t.Lnumber + ">" + t.FIO + ">" + t.Zvanie + ">" + t.Types + ">" + t.Dolg + ">" + t.Podr + ">" + t.Date + ">" + t.Chast + ">" + t.Gorod + ">" + t.Osn + ">" + t.Pr + ">" + t.Date_pr + ">" + t.What_pr;
                    SendMessage("izm|" + td.Types + "|" + td.Row + "|" + td.Name_table + "|" + td.Old_value + "|" + td.New_value + "|" + td.Col);
                }
            }
            if (Dop == 3)
            {
                if (t.Zvanie == "лейтенант" || t.Zvanie == "старший лейтенант" || t.Zvanie == "капитан" || t.Zvanie == "майор" || t.Zvanie == "подполковник" || t.Zvanie == "полковник" || t.Zvanie == "генерал-лейтенант")
                {
                    list_perev.Add(t);
                    foreach (OpenLoadZaShtat a in list_Anshtat)
                    {
                        if (a.Lnumber == t.Lnumber)
                        {
                            list_Anshtat.Remove(a);
                            list_Anshtat2.Remove(a);
                            break;
                        }
                    }
                    Izmen td = new Izmen();
                    td.Types = "Перевод";
                    td.New_value = t.Lnumber + ">" + t.FIO + ">" + t.Zvanie + ">" + t.Types + ">" + t.Dolg + ">" + t.Podr + ">" + t.Date + ">" + t.Chast + ">" + t.Gorod + ">" + t.Osn + ">" + t.Pr + ">" + t.Date_pr + ">" + t.What_pr;
                    SendMessage("izm|" + td.Types + "|" + td.Row + "|" + td.Name_table + "|" + td.Old_value + "|" + td.New_value + "|" + td.Col);
                }
            }
        }
        //Исключение после перевода или увольнения
        public void Iskl_people(OpenLoadIskl t)
        {
            if (Dop == 7)
            {
                if (t.Types != "контр" && t.Types != "жен")
                {

                    list_iskl.Add(t);
                    foreach (OpenLoadPerevod a in list_perev)
                    {
                        if (a.Lnumber == t.Lnumber)
                        {
                            list_perev.Remove(a);
                            break;
                        }
                    }
                    foreach (OpenLoadDel a in list_del)
                    {
                        if (a.Lnumber == t.Lnumber)
                        {
                            list_del.Remove(a);
                            break;
                        }
                    }
                    Izmen td = new Izmen();
                    td.Types = "Исключение";
                    td.New_value = t.Lnumber + ">" + t.FIO + ">" + t.Zvanie + ">" + t.Types + ">" + t.Dolg + ">" + t.Podr + ">" + t.Date + ">" + t.Pr + ">" + t.Date_pr + ">" + t.What_pr;
                    SendMessage("izm|" + td.Types + "|" + td.Row + "|" + td.Name_table + "|" + td.Old_value + "|" + td.New_value + "|" + td.Col);
                }
            }
            if (Dop == 5 || Dop == 6)
            {
                if (t.Zvanie == "рядовой" && t.Types == "контр" || t.Zvanie == "рядовой" && t.Types == "жен" || t.Zvanie == "ефрейтор" && t.Types == "контр" || t.Zvanie == "ефрейтор" && t.Types == "жен" || t.Zvanie == "младший сержант" && t.Types == "контр" || t.Zvanie == "младший сержант" && t.Types == "жен" || t.Zvanie == "сержант" && t.Types == "контр" || t.Zvanie == "сержант" && t.Types == "жен" || t.Zvanie == "старший сержант" && t.Types == "контр" || t.Zvanie == "старший сержант" && t.Types == "жен" || t.Zvanie == "старшина" && t.Types == "контр" || t.Zvanie == "старшина" && t.Types == "жен")
                {
                    list_iskl.Add(t);
                    foreach (OpenLoadPerevod a in list_perev)
                    {
                        if (a.Lnumber == t.Lnumber)
                        {
                            list_perev.Remove(a);
                            break;
                        }
                    }
                    foreach (OpenLoadDel a in list_del)
                    {
                        if (a.Lnumber == t.Lnumber)
                        {
                            list_del.Remove(a);
                            break;
                        }
                    }
                    Izmen td = new Izmen();
                    td.Types = "Исключение";
                    td.New_value = t.Lnumber + ">" + t.FIO + ">" + t.Zvanie + ">" + t.Types + ">" + t.Dolg + ">" + t.Podr + ">" + t.Date + ">" + t.Pr + ">" + t.Date_pr + ">" + t.What_pr;
                    SendMessage("izm|" + td.Types + "|" + td.Row + "|" + td.Name_table + "|" + td.Old_value + "|" + td.New_value + "|" + td.Col);
                }
            }
            if (Dop == 4)
            {
                if (t.Zvanie == "старшина" || t.Zvanie == "прапорщик" || t.Zvanie == "старший прапорщик")
                {
                    list_iskl.Add(t);
                    foreach (OpenLoadPerevod a in list_perev)
                    {
                        if (a.Lnumber == t.Lnumber)
                        {
                            list_perev.Remove(a);
                            break;
                        }
                    }
                    foreach (OpenLoadDel a in list_del)
                    {
                        if (a.Lnumber == t.Lnumber)
                        {
                            list_del.Remove(a);
                            break;
                        }
                    }
                    Izmen td = new Izmen();
                    td.Types = "Исключение";
                    td.New_value = t.Lnumber + ">" + t.FIO + ">" + t.Zvanie + ">" + t.Types + ">" + t.Dolg + ">" + t.Podr + ">" + t.Date + ">" + t.Pr + ">" + t.Date_pr + ">" + t.What_pr;
                    SendMessage("izm|" + td.Types + "|" + td.Row + "|" + td.Name_table + "|" + td.Old_value + "|" + td.New_value + "|" + td.Col);
                }
            }
            if (Dop == 3)
            {
                if (t.Zvanie == "лейтенант" || t.Zvanie == "старший лейтенант" || t.Zvanie == "капитан" || t.Zvanie == "майор" || t.Zvanie == "подполковник" || t.Zvanie == "полковник" || t.Zvanie == "генерал-лейтенант")
                {
                    list_iskl.Add(t);
                    foreach (OpenLoadPerevod a in list_perev)
                    {
                        if (a.Lnumber == t.Lnumber)
                        {
                            list_perev.Remove(a);
                            break;
                        }
                    }
                    foreach (OpenLoadDel a in list_del)
                    {
                        if (a.Lnumber == t.Lnumber)
                        {
                            list_del.Remove(a);
                            break;
                        }
                    }
                    Izmen td = new Izmen();
                    td.Types = "Исключение";
                    td.New_value = t.Lnumber + ">" + t.FIO + ">" + t.Zvanie + ">" + t.Types + ">" + t.Dolg + ">" + t.Podr + ">" + t.Date + ">" + t.Pr + ">" + t.Date_pr + ">" + t.What_pr;
                    SendMessage("izm|" + td.Types + "|" + td.Row + "|" + td.Name_table + "|" + td.Old_value + "|" + td.New_value + "|" + td.Col);
                }
            }
        }
        //СОЗДАНИЕ НАРЯДА
        public void New_Naryad(OpenLoad tmp)
        {
            Naryad t = new Naryad();
            t.FIO = tmp.Fio;
            t.Zvanie = tmp.Zvanie;
            t.Lnumber = tmp.Lnumber;
            t.Dolg = tmp.Dolgnost;
            t.Podr = tmp.Podr;
            if (Dop == 9)
            {
                foreach (Naryad d in list_Naryad)
                {
                    if (d.Lnumber == tmp.Lnumber) return;
                }
                list_Naryad.Add(t);
                Izmen tmpp = new Izmen();
                tmpp.Types = "Добавить наряд";
                tmpp.Name_table = "Наряд";
                tmpp.New_value = t.Lnumber + "/" + t.Podr + "/" + t.Types + "/" + t.FIO + "/" + t.Zvanie + "/" + t.Dolg + "/" + t.Key;
                SendMessage("izm|" + tmpp.Types + "|" + tmpp.Row + "|" + tmpp.Name_table + "|" + tmpp.Old_value + "|" + tmpp.New_value + "|" + tmpp.Col);
            }

        }
        //ФУНКЦИЯ ВЫБОРА ИЗ ШТАТА
        public void Create_viborka(OpenLoad tmp, string num)
        {
            bool q = false;
            if (num == "+")
            {
                foreach (OpenLoad m in list_Vibor)
                {
                    if (tmp.Lnumber != m.Lnumber)
                    {
                        q = true;
                    }
                    else
                    {
                        q = false;
                        return;
                    }
                }
                if (list_Vibor.Count == 0) q = true;
                if (q == true)
                    list_Vibor.Add(tmp);
            }
            else
            {
                if (num == "-")
                {
                    foreach (OpenLoad m in list_Vibor)
                    {
                        if (tmp.Lnumber == m.Lnumber)
                        {
                            list_Vibor.Remove(m);
                            return;
                        }
                    }
                }
            }

        }
        //Формирование листов для ComboBox
        private void create_list()
        {
            _list_Type_vibor = new List<string>();
            _list_Type_vibor.Add("Отбор на контракт");
            _list_Type_vibor.Add("Наряд");

            _otr_grid = new List<string>();
            _otr_grid.Add("Отпуск");
            _otr_grid.Add("Командировка");
            _otr_grid.Add("Мед.рота");
            _otr_grid.Add("Госпиталь");
            _otr_grid.Add("Арест");
            _otr_grid.Add("СОЧ");
            _otr_grid.Add("Другие причины");

            _family_Sl_CB = new List<string>();
            _family_Sl_CB.Add("мама");
            _family_Sl_CB.Add("папа");
            _family_Sl_CB.Add("опекун");
            _family_Sl_CB.Add("муж");
            _family_Sl_CB.Add("жена");
            _family_Sl_CB.Add("сын");
            _family_Sl_CB.Add("дочь");

            _zvan_Sl_CB = new List<string>();
            _zvan_Sl_CB.Add("рядовой");
            _zvan_Sl_CB.Add("ефрейтор");
            _zvan_Sl_CB.Add("младший сержант");
            _zvan_Sl_CB.Add("сержант");
            _zvan_Sl_CB.Add("старший сержант");
            _zvan_Sl_CB.Add("старшина");
            _zvan_Sl_CB.Add("прапорщик");
            _zvan_Sl_CB.Add("старший прапорщик");
            _zvan_Sl_CB.Add("лейтенант");
            _zvan_Sl_CB.Add("старший лейтенант");
            _zvan_Sl_CB.Add("капитан");
            _zvan_Sl_CB.Add("майор");
            _zvan_Sl_CB.Add("подполковник");
            _zvan_Sl_CB.Add("полковник");

            _kontr_Sl_CB = new List<string>();
            _kontr_Sl_CB.Add("Первый");
            _kontr_Sl_CB.Add("Очередной");

            _pol_CB = new List<string>();
            _pol_CB.Add("муж");
            _pol_CB.Add("жен");

            _cB_LD = new List<string>();
            _cB_LD.Add("по контракту");
            _cB_LD.Add("по призыву");

            _cB_ub_Gos = new List<string>();
            _cB_ub_Gos.Add("Хабаровск");
            _cB_ub_Gos.Add("Анастасьевка");
            _cB_ub_Gos.Add("Другой");

            _type_ub_kom = new List<string>();
            _type_ub_kom.Add("Служебная командировка");
            _type_ub_kom.Add("Выездной караул");

            _types_otp = new List<string>();
            _types_otp.Add("Основной");
            _types_otp.Add("Дополнительный");
            _types_otp.Add("Учебный");
            _types_otp.Add("По семейным обстоятельствам");
        }

        // Звуковое оповещение о перехваченной ошибке.
        void ErrorSound()
        {
            Console.Beep(2000, 80);
            Console.Beep(3000, 120);
        }

        #endregion
        #region Набор на контракт
        //Добавление военнослужащего по призыву в компанию по набору на контракт
        public void New_otbor(Otbor_na_kontr tmp)
        {
            if (Dop == 7)
            {
                foreach (Otbor_na_kontr a in list_Otob)
                {
                    if (a.Lnumber == tmp.Lnumber)
                    {
                        MessageBox.Show("Военнослужащий " + tmp.FIO + " уже отобран.");
                        return;
                    }
                }
                foreach (Otbor_na_kontr a in list_Pered)
                {
                    if (a.Lnumber == tmp.Lnumber)
                    {
                        MessageBox.Show("Военнослужащий " + tmp.FIO + " уже отправлен в приказ.");
                        return;
                    }
                }
                foreach (Otbor_na_kontr a in list_Sost)
                {
                    if (a.Lnumber == tmp.Lnumber)
                    {
                        MessageBox.Show("Военнослужащий " + tmp.FIO + " уже контрактник.");
                        return;
                    }
                }
                foreach (Otbor_na_kontr a in list_Otkaz)
                {
                    if (a.Lnumber == tmp.Lnumber)
                    {
                        MessageBox.Show("Военнослужащий " + tmp.FIO + " отказался от контракта, для посторного отбора перейдите во вкладку ОТКАЗ и в сплывающем меню правой кнопки мыши выбирите соответствующий пункт.");
                        return;
                    }
                }
                list_Otob.Add(tmp);
                Izmen T = new Izmen();
                T.Types = "Отбор на контракт";
                T.Name_table = "Отобранные";
                T.New_value = tmp.FIO;
                T.Col = tmp.Blank + ">" + tmp.Date1 + ">" + tmp.Date2 + ">" + tmp.Date3 + ">" + tmp.Date4 + ">" + tmp.Date_ic + ">" + tmp.Dolg + ">" + tmp.FIZO + ">" + tmp.Ic + ">" + tmp.Lnumber + ">" + tmp.Podr + ">" + tmp.Primec + ">" + tmp.Prof + ">" + tmp.Raport + ">" + tmp.Types + ">" + tmp.Vvk + ">" + tmp.Zvanie;
                SendMessage("izm|" + T.Types + "|" + T.Row + "|" + T.Name_table + "|" + T.Old_value + "|" + T.New_value + "|" + T.Col);
            }
        }
        //Передача документов военнослужащего по призыву в проект приказа Статс-секретаря заместителя министра обороны РФ (СС ЗМО РФ)
        public void Pered_vkadry(Otbor_na_kontr tmp)
        {
            tmp.Date2 = dateClass.DateToday();
            list_Pered.Add(tmp);
            Izmen T = new Izmen();
            T.Types = "Отбор на контракт";
            T.Name_table = "Переданные";
            T.New_value = tmp.FIO;
            T.Col = tmp.Blank + ">" + tmp.Date1 + ">" + tmp.Date2 + ">" + tmp.Date3 + ">" + tmp.Date4 + ">" + tmp.Date_ic + ">" + tmp.Dolg + ">" + tmp.FIZO + ">" + tmp.Ic + ">" + tmp.Lnumber + ">" + tmp.Podr + ">" + tmp.Primec + ">" + tmp.Prof + ">" + tmp.Raport + ">" + tmp.Types + ">" + tmp.Vvk + ">" + tmp.Zvanie;
            SendMessage("izm|" + T.Types + "|" + T.Row + "|" + T.Name_table + "|" + T.Old_value + "|" + T.New_value + "|" + T.Col);
            list_Otob.Remove(tmp);
        }
        //Получение приказа СС ЗМО РФ
        public void OK_sost(Otbor_na_kontr tmp)
        {
            tmp.Date3 = dateClass.DateToday();
            list_Sost.Add(tmp);
            Izmen T = new Izmen();
            T.Types = "Отбор на контракт";
            T.Name_table = "Состоялись";
            T.New_value = tmp.FIO;
            T.Col = tmp.Blank + ">" + tmp.Date1 + ">" + tmp.Date2 + ">" + tmp.Date3 + ">" + tmp.Date4 + ">" + tmp.Date_ic + ">" + tmp.Dolg + ">" + tmp.FIZO + ">" + tmp.Ic + ">" + tmp.Lnumber + ">" + tmp.Podr + ">" + tmp.Primec + ">" + tmp.Prof + ">" + tmp.Raport + ">" + tmp.Types + ">" + tmp.Vvk + ">" + tmp.Zvanie;
            SendMessage("izm|" + T.Types + "|" + T.Row + "|" + T.Name_table + "|" + T.Old_value + "|" + T.New_value + "|" + T.Col);
            list_Pered.Remove(tmp);
        }
        //Отказ военнослужащего по призыву проходить военную службу по контракту
        public void OK_otkaz(Otbor_na_kontr tmp, int num)
        {
            tmp.Date4 = dateClass.DateToday();
            list_Otkaz.Add(tmp);
            Izmen T = new Izmen();
            T.Types = "Отбор на контракт";
            T.Name_table = "Отказ";
            T.New_value = tmp.FIO;
            T.Col = tmp.Blank + ">" + tmp.Date1 + ">" + tmp.Date2 + ">" + tmp.Date3 + ">" + tmp.Date4 + ">" + tmp.Date_ic + ">" + tmp.Dolg + ">" + tmp.FIZO + ">" + tmp.Ic + ">" + tmp.Lnumber + ">" + tmp.Podr + ">" + tmp.Primec + ">" + tmp.Prof + ">" + tmp.Raport + ">" + tmp.Types + ">" + tmp.Vvk + ">" + tmp.Zvanie;
            SendMessage("izm|" + T.Types + "|" + T.Row + "|" + T.Name_table + "|" + T.Old_value + "|" + T.New_value + "|" + T.Col);
            switch (num)
            {
                case 1:
                    list_Otob.Remove(tmp);
                    break;
                case 2:
                    list_Pered.Remove(tmp);
                    break;
            };
        }
        //Военнослужащие по призыву, отказавшиеся проходить военную службу по контракту в одном подразделении
        //но изъявили вновь желание проходить военную службу по контракту, но уже в другом подразделении
        //Естественно документы не утеряны и не уничтожены. Военнослужащий продолжает проходить мероприятия
        //предшествующие заключению первого контракта с того места, на котором он остановился перед отказом
        public void OK_Snowa_otbor(Otbor_na_kontr tmp)
        {

            tmp.Date1 = dateClass.DateToday();
            list_Otob.Add(tmp);
            Izmen T = new Izmen();
            T.Types = "Отбор на контракт";
            T.Name_table = "Вновь отобранные";
            T.New_value = tmp.FIO;
            T.Col = tmp.Blank + ">" + tmp.Date1 + ">" + tmp.Date2 + ">" + tmp.Date3 + ">" + tmp.Date4 + ">" + tmp.Date_ic + ">" + tmp.Dolg + ">" + tmp.FIZO + ">" + tmp.Ic + ">" + tmp.Lnumber + ">" + tmp.Podr + ">" + tmp.Primec + ">" + tmp.Prof + ">" + tmp.Raport + ">" + tmp.Types + ">" + tmp.Vvk + ">" + tmp.Zvanie;
            SendMessage("izm|" + T.Types + "|" + T.Row + "|" + T.Name_table + "|" + T.Old_value + "|" + T.New_value + "|" + T.Col);
            list_Otkaz.Remove(tmp);
        }
        #endregion
        #region Работа со штатом
        //Добавление человека в список личного состава
        public void Create_Shtat_People(int id, string lnumber)
        {
            if (gg == true) return;
            Izmen T = new Izmen();
            foreach (OpenLoad N in List_shtat)
            {
                if (N.Id == id)
                {
                    foreach (OpenLoadZaShtat a in List_AnShtat)
                    {
                        if (a.Lnumber == lnumber)
                        {
                            if (Dop == 5 || Dop == 4 || Dop == 6)
                            {
                                if (a.Types != "контр" && a.Types != "жен") return;
                            }
                            if (Dop == 3)
                            {
                                if (a.Types != "О") return;
                            }
                            if (Dop == 7)
                            {
                                if (a.Types == "О" || a.Types == "контр" || a.Types == "жен") return;
                            }
                            if (Dop == 8 || Dop == 9 || Dop == 10) return;
                            T.Types = "Добавить";
                            T.Name_table = "Штатка";
                            T.Row = N.Id;
                            T.Old_value = N.Lnumber;
                            T.New_value = a.Lnumber;
                            T.Col = a.FIO + ">" + a.Types + ">" + a.Zvanie + ">" + N.Vus + ">" + N.Kod + ">" + N.Dolgnost + ">" + N.Podr;

                            N.Lnumber = a.Lnumber;
                            N.Zvanie = a.Zvanie;
                            N.Fio = a.FIO;
                            N.Types = a.Types;

                            foreach (OpenLoadKotel K in list_Kotel1)
                            {
                                if (K.Lnumber == a.Lnumber)
                                {
                                    K.Podr = N.Podr1;
                                    kotel2();
                                }

                            }
                            if (list_Per.Count != 0)
                            {
                                foreach (Perest Z in list_Per)
                                {
                                    if (Z.Lnumber == a.Lnumber)
                                    {
                                        if (Z.Old_dolg != "")
                                        {
                                            if (Z.New_dolg != "")
                                            {
                                                Perest tmp = new Perest();
                                                tmp.FIO = a.FIO;
                                                tmp.Lnumber = a.Lnumber;
                                                tmp.Zvanie = a.Zvanie;
                                                tmp.New_dolg = N.Dolgnost;
                                                tmp.New_podr = N.Podr;
                                                tmp.VUS = N.Vus;
                                                tmp.Kod = N.Kod;
                                                list_Per.Add(tmp);
                                                list_Per_tek.Add(tmp);
                                                break;
                                            }
                                            else
                                            {
                                                Z.New_dolg = N.Dolgnost;
                                                Z.New_podr = N.Podr;
                                                Z.Kod = N.Kod;
                                                Z.VUS = N.Vus;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Perest tmp = new Perest();
                                        tmp.FIO = a.FIO;
                                        tmp.Lnumber = a.Lnumber;
                                        tmp.Zvanie = a.Zvanie;
                                        tmp.New_dolg = N.Dolgnost;
                                        tmp.New_podr = N.Podr;
                                        tmp.VUS = N.Vus;
                                        tmp.Kod = N.Kod;
                                        list_Per.Add(tmp);
                                        list_Per_tek.Add(tmp);
                                        break;
                                    }
                                    // break;
                                }
                            }
                            else
                            {
                                Perest tmp = new Perest();
                                tmp.FIO = a.FIO;
                                tmp.Lnumber = a.Lnumber;
                                tmp.Zvanie = a.Zvanie;
                                tmp.New_dolg = N.Dolgnost;
                                tmp.New_podr = N.Podr;
                                tmp.VUS = N.Vus;
                                tmp.Kod = N.Kod;
                                list_Per.Add(tmp);
                                list_Per_tek.Add(tmp);
                            }



                            List_AnShtat.Remove(a);
                            list_Anshtat2.Remove(a);
                            int ii = 1;
                            foreach (OpenLoadZaShtat p in list_Anshtat2)
                            {
                                p.Id = ii;
                                ii++;
                            }
                            ii = 1;
                            foreach (OpenLoadZaShtat p in list_Anshtat)
                            {
                                p.Id = ii;
                                ii++;
                            }
                            break;
                        }
                    }
                    int i = 1;
                    foreach (OpenLoadZaShtat t in List_AnShtat)
                    {
                        t.Id = i;
                        i++;
                    }
                    break;
                }
            }
            Proverka_otryv();
            SendMessage("izm|" + T.Types + "|" + T.Row + "|" + T.Name_table + "|" + T.Old_value + "|" + T.New_value + "|" + T.Col);
            //Тут мы ставим в штат
            //Тут мы записываем перестановку
            //Тут мы проверяем суточку
            //Тут мы переставляем рсз
            //тут мы записываем весь шлак в формат изменений
            //Тут мы записываем в личное дело

        }
        //Получение личного дела и преобразование строки в экземпляр личного дела военнослужащего
        public void _Pered_ld(string message)
        {
            int j = 0;
            int i = 0;
            try
            {
                LD_Dan = new Dannie();

                if (ld_doc.Count == 0)
                    ld_doc = new ObservableCollection<Docum>();
                else ld_doc.Clear();

                if (ld_doc1.Count == 0)
                    ld_doc1 = new ObservableCollection<Docum>();
                else ld_doc.Clear();

                if (ld_fam.Count == 0)
                    ld_fam = new ObservableCollection<family>();
                else ld_fam.Clear();

                if (ld_kon.Count == 0)
                    ld_kon = new ObservableCollection<Kontrakt>();
                else ld_kon.Clear();

                if (ld_nag.Count == 0)
                    ld_nag = new ObservableCollection<Nagrad>();
                else ld_nag.Clear();
                LD_Naz = new Naznach();
                LD_PD = new PriemDel();
                Photo_ LD_Ph = new Photo_();

                if (ld_poslug.Count == 0)
                    ld_poslug = new ObservableCollection<Poslug>();
                else ld_poslug.Clear();
                LD_Viz = new Vizhivanie();

                if (ld_poslug.Count == 0)
                    ld_zvanie = new ObservableCollection<zvanie>();
                else ld_zvanie.Clear();
                zvanie t_z = new zvanie();
                Docum t_d = new Docum();
                family t_f = new family();
                Kontrakt t_k = new Kontrakt();
                Nagrad t_n = new Nagrad();
                Poslug t_p = new Poslug();
                int types = 0;
                string[] word = message.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                for (i = 0; i < word.Count(); i++)
                {
                    if (word[i] == "Zvanie")
                    {
                        j = 0;
                        types = 1;
                    }
                    if (word[i] == "Poslug")
                    {
                        j = 0;
                        types = 2;
                    }
                    if (word[i] == "Fam")
                    {
                        j = 0;
                        types = 3;
                    }
                    if (word[i] == "Nagrad")
                    {
                        j = 0;
                        types = 4;
                    }
                    if (word[i] == "Foto")
                    {
                        j = 0;
                        types = 5;
                        if (word[i + 1] != ".") _photo = LD_Ph.LoadImage(LD_Ph.StringToByteArray(word[i + 1]));
                        if (word[i + 2] != ".") _autobiographi = LD_Ph.LoadImage(LD_Ph.StringToByteArray(word[i + 2]));
                        if (word[i + 3] != ".") _diplom = LD_Ph.LoadImage(LD_Ph.StringToByteArray(word[i + 3]));
                        if (word[i + 4] != ".") _attest = LD_Ph.LoadImage(LD_Ph.StringToByteArray(word[i + 4]));
                        if (word[i + 5] != ".") _inn = LD_Ph.LoadImage(LD_Ph.StringToByteArray(word[i + 5]));
                        if (word[i + 6] != ".") _snils = LD_Ph.LoadImage(LD_Ph.StringToByteArray(word[i + 6]));
                    }
                    if (word[i] == "Docum")
                    {
                        j = 0;
                        types = 6;
                    }
                    if (word[i] == "Dan")
                    {
                        j = 0;
                        types = 7;
                        if (word[i + 1] != ".") LD_Dan.Lnumber = word[i + 1];
                        if (word[i + 2] != ".") LD_Dan.FIO = word[i + 2];
                        if (word[i + 3] != ".") LD_Dan.Bank_card = word[i + 3];
                        if (word[i + 4] != ".") LD_Dan.Date_brd = word[i + 4];
                        if (word[i + 5] != ".") LD_Dan.Gr_obr = word[i + 5];
                        if (word[i + 6] != ".") LD_Dan.Home_adres = word[i + 6];
                        if (word[i + 7] != ".") LD_Dan.Mesto_brd = word[i + 7];
                        if (word[i + 8] != ".") LD_Dan.Nac = word[i + 8];
                        if (word[i + 9] != ".") LD_Dan.PAC = word[i + 9];
                        if (word[i + 10] != ".") LD_Dan.POL = word[i + 10];
                        if (word[i + 11] != ".") LD_Dan.Vod_prava = word[i + 11];
                        if (word[i + 12] != ".") LD_Dan.Voenkomat = word[i + 12];
                        if (word[i + 13] != ".") LD_Dan.Voen_obr = word[i + 13];
                        if (word[i + 14] != ".") LD_Dan.Types = word[i + 14];
                    }
                    if (word[i] == "Kontr")
                    {
                        j = 0;
                        types = 8;
                    }
                    if (word[i] == "Naznac")
                    {
                        j = 0;
                        types = 9;
                        if (word[i + 1] != ".") LD_Naz.pricaz = word[i + 1];
                        if (word[i + 2] != ".") LD_Naz.who_pricaz = word[i + 2];
                        if (word[i + 3] != ".") LD_Naz.date_pricaz = word[i + 3];
                    }
                    if (word[i] == "Priem")
                    {
                        j = 0;
                        types = 10;
                        if (word[i + 1] != ".") LD_PD.pricaz = word[i + 1];
                        if (word[i + 2] != ".") LD_PD.who_pricaz = word[i + 2];
                        if (word[i + 3] != ".") LD_PD.date_pricaz = word[i + 3];
                    }
                    if (word[i] == "Vizhiv")
                    {
                        j = 0;
                        types = 11;
                        if (word[i + 2] != ".") LD_Viz.Mesto = word[i + 2];
                        if (word[i + 1] != ".") LD_Viz.Period = word[i + 1];
                        if (word[i + 3] != ".") LD_Viz.Osnovanie = word[i + 3];
                    }
                    switch (types)
                    {
                        case 1: // zvanie
                            {
                                if (j == 0)
                                {
                                    j++;
                                    break;
                                }
                                if (j == 1)
                                {
                                    t_z = new zvanie();
                                    if (word[i] != ".") t_z.Name = word[i];
                                    j++; break;
                                }
                                if (j == 2)
                                {
                                    if (word[i] != ".") t_z.Nomber = int.Parse(word[i]);
                                    j++; break;
                                }
                                if (j == 3)
                                {
                                    if (word[i] != ".") t_z.Pr = int.Parse(word[i]);
                                    j++; break;
                                }
                                if (j == 4)
                                {
                                    if (word[i] != ".") t_z.What = word[i];
                                    j++; break;
                                }
                                if (j == 5)
                                {
                                    if (word[i] != ".") t_z.Date = word[i];
                                    j = 1;
                                    ld_zvanie.Add(t_z);
                                }
                                break;
                            }
                        case 2: //poslugnoy
                            {
                                if (j == 0)
                                {
                                    j++;
                                    break;
                                }
                                if (j == 1)
                                {
                                    t_p = new Poslug();
                                    if (word[i] != ".") t_p.Chast_ = word[i];
                                    j++; break;
                                }
                                if (j == 2)
                                {
                                    if (word[i] != ".") t_p.Date_pricaz = word[i];
                                    j++; break;
                                }
                                if (j == 3)
                                {
                                    if (word[i] != ".") t_p.Dolgnost_ = word[i];
                                    j++; break;
                                }
                                if (j == 4)
                                {
                                    if (word[i] != ".") t_p.Name_pricaz = word[i];
                                    j++; break;
                                }
                                if (j == 5)
                                {
                                    if (word[i] != ".") t_p.Nomber_pricaz = word[i];
                                    j++; break;
                                }
                                if (j == 6)
                                {
                                    if (word[i] != ".") t_p.Podr_ = word[i];
                                    j++; break;
                                }
                                if (j == 7)
                                {
                                    if (word[i] != ".") t_p.VUS_ = word[i];
                                    j = 1;
                                    ld_poslug.Add(t_p);
                                }
                                break;
                            }
                        case 3: //family
                            {
                                if (j == 0)
                                {
                                    j++;
                                    break;
                                }
                                if (j == 1)
                                {
                                    t_f = new family();
                                    if (word[i] != ".") t_f.Name = word[i];
                                    j++; break;
                                }
                                if (j == 2)
                                {
                                    if (word[i] != ".") t_f.types = word[i];
                                    j++; break;
                                }
                                if (j == 3)
                                {
                                    if (word[i] != ".") t_f.Brd = word[i];
                                    j = 1;
                                    ld_fam.Add(t_f);
                                }
                                break;
                            }
                        case 4: //nagrad
                            {
                                if (j == 0)
                                {
                                    j++;
                                    break;
                                }
                                if (j == 1)
                                {
                                    t_n = new Nagrad();
                                    if (word[i] != ".") t_n.Type = word[i];
                                    j++; break;
                                }
                                if (j == 2)
                                {
                                    if (word[i] != ".") t_n.Who_pricaz = word[i];
                                    j++; break;
                                }
                                if (j == 3)
                                {
                                    if (word[i] != ".") t_n.Pricaz = word[i];
                                    j++; break;
                                }
                                if (j == 4)
                                {
                                    if (word[i] != ".") t_n.Date_pricaz = word[i];
                                    j++; break;
                                }
                                if (j == 5)
                                {
                                    if (word[i] != ".") t_n.Date_ = word[i];
                                    j = 1;
                                    ld_nag.Add(t_n);
                                }
                                break;
                            }
                        case 6: //docum
                            {
                                if (j == 0)
                                {
                                    j++;
                                    break;
                                }
                                if (j == 1)
                                {
                                    t_d = new Docum();
                                    if (word[i] != ".") t_d.Kod = word[i];
                                    j++; break;
                                }
                                if (j == 2)
                                {
                                    if (word[i] != ".") t_d.Seriya = word[i];
                                    j++; break;
                                }
                                if (j == 3)
                                {
                                    if (word[i] != ".") t_d.Nomber = word[i];
                                    j++; break;
                                }
                                if (j == 4)
                                {
                                    if (word[i] != ".") t_d.Who_vidal = word[i];
                                    j++; break;
                                }
                                if (j == 5)
                                {
                                    if (word[i] != ".") t_d.Date_vid = word[i];
                                    j = 1;
                                    if (t_d.Kod == "01")
                                        ld_doc.Add(t_d);
                                    if (t_d.Kod == "04")
                                        ld_doc1.Add(t_d);
                                }
                                break;
                            }
                        case 8: //kontrakt
                            {
                                if (j == 0)
                                {
                                    j++;
                                    break;
                                }
                                if (j == 1)
                                {
                                    t_k = new Kontrakt();
                                    if (word[i] != ".") t_k.data_zakl = word[i];
                                    j++; break;
                                }
                                if (j == 2)
                                {
                                    if (word[i] != ".") t_k.data_okon = word[i];
                                    j++; break;
                                }
                                if (j == 3)
                                {
                                    if (word[i] != ".") t_k.Date_pricaz = word[i];
                                    j++; break;
                                }
                                if (j == 4)
                                {
                                    if (word[i] != ".") t_k.Nomber_pricaz = word[i];
                                    j++; break;
                                }
                                if (j == 5)
                                {
                                    if (word[i] != ".") t_k.types = word[i];
                                    j++; break;
                                }
                                if (j == 6)
                                {
                                    if (word[i] != ".") t_k.Who_pricaz = word[i];
                                    j = 1;
                                    ld_kon.Add(t_k);
                                }
                                break;
                            }
                    }

                }
            }
            catch { MessageBox.Show("Ошибка на шаге i=" + i + " и шаге j =" + j); }
            MessageBox.Show(LD_DAN.FIO);
        }
        //Выборка для наряда
        public void Viborka()
        {
            if (Dop == 9)
            {
                foreach (OpenLoad B in list_Vibor)
                {
                    Naryad t = new Naryad();
                    t.FIO = B.Fio;
                    t.Lnumber = B.Lnumber;
                    t.Zvanie = B.Zvanie;
                    t.Types = B.Types;
                    t.Podr = B.Podr;
                    t.Dolg = B.Dolgnost;
                    list_Naryad.Add(t);
                }

            }
        }

        //Удаление военнослужащего из штата в заштатники
        public void Del_Shtat_People(int id)
        {
            DispatcherHelper.Initialize();
            ThreadPool.QueueUserWorkItem(
                obj =>
                {
                    while (_condition)
                    {
                        if (gg == true) return;
                        Izmen T = new Izmen();
                        foreach (OpenLoad N in List_shtat)
                        {
                            if (N.Id == id)
                            {
                                if (Dop == 5 || Dop == 4 || Dop == 6)
                                {
                                    if (N.Types != "контр" && N.Types != "жен") return;
                                }
                                if (Dop == 3)
                                {
                                    if (N.Types != "О") return;
                                }
                                if (Dop == 7)
                                {
                                    if (N.Types == "О" || N.Types == "контр" || N.Types == "жен") return;
                                }
                                if (Dop == 8 || Dop == 9 || Dop == 10) return;
                                foreach (OpenLoadKotel K in list_Kotel1)
                                {
                                    if (K.Lnumber == N.Lnumber)
                                    {
                                        K.Podr = ".";
                                        kotel2();
                                    }

                                }

                                T.Types = "Удалить";
                                T.Name_table = "Штатка";
                                T.Row = N.Id;
                                T.Old_value = N.Lnumber;
                                T.New_value = ".";
                                T.Col = N.Dolgnost + ">" + N.Podr + ">" + N.Fio + ">" + N.Zvanie + ">" + N.Types;

                                OpenLoadZaShtat tmp = new OpenLoadZaShtat();
                                tmp.Lnumber = N.Lnumber;
                                tmp.Zvanie = N.Zvanie;
                                tmp.FIO = N.Fio;
                                tmp.Types = N.Types;
                                tmp.Last_dolg = N.Dolgnost;
                                tmp.Last_podr = N.Podr;


                                Perest tmp1 = new Perest();
                                tmp1.FIO = N.Fio;
                                tmp1.Lnumber = N.Lnumber;
                                tmp1.Zvanie = N.Zvanie;
                                tmp1.Types = N.Types;
                                tmp1.Old_dolg = N.Dolgnost;
                                tmp1.Old_podr = N.Podr;


                                N.Zvanie = ".";
                                N.Types = ".";
                                N.Lnumber = ".";
                                N.Fio = "-В-";
                                N.Otryv_status = ".";
                                N.Otryv_primech = ".";
                                N.Otryv_pricaz = ".";
                                int ii = 1;
                                DispatcherHelper.CheckBeginInvokeOnUI(
                                     () =>
                                     {
                                         List_AnShtat.Add(tmp);
                                         List_AnShtat2.Add(tmp);
                                         list_Per.Add(tmp1);
                                         list_Per_tek.Add(tmp1);
                                         foreach (OpenLoadZaShtat p in list_Anshtat2)
                                         {
                                             p.Id = ii;
                                             ii++;
                                         }
                                         ii = 1;
                                         foreach (OpenLoadZaShtat p in list_Anshtat)
                                         {
                                             p.Id = ii;
                                             ii++;
                                         }
                                     });
                                Thread.Sleep(500);

                                break;

                            }
                        }

                        SendMessage("izm|" + T.Types + "|" + T.Row + "|" + T.Name_table + "|" + T.Old_value + "|" + T.New_value + "|" + T.Col);
                        //Тут мы ставим в штат
                        //Тут мы записываем перестановку
                        //Тут мы проверяем суточку
                        //Тут мы переставляем рсз
                        //тут мы записываем весь шлак в формат изменений
                        //Тут мы записываем в личное дело
                    }

                });
        }
        //Новый экземпляр убытия
        //Когда военнослужащий покидает место постоянной дислокации (воинская часть или полигон)
        //считается, что военнослужащий УБЫл. Соответственно, при возвращении считается, что военнослужащий ПРИБЫЛ
        public void new_ub(Ub tmp)
        {
            if (tmp.Date3 != ".")
            {
                Creat_kotel2(tmp, "Пребытие");
                Izmen tmpp = new Izmen();
                tmpp.Types = "Пребытие";
                list_preb.Add(tmp);
                tmpp.New_value = tmp.Cel + "*" + tmp.Date3 + "*" + tmp.Date_preb + "*" + tmp.Date_ub + "*" + tmp.Dolg + "*" + tmp.FIO + "*" + tmp.Lnumber + "*" + tmp.Mesto + "*" + tmp.Osn + "*" + tmp.Podr + "*" + tmp.Types + "*" + tmp.Types_ub + "*" + tmp.VPD_ST1 + "*" + tmp.VPD_ST2 + "*" + tmp.VPD_ST3 + "*" + tmp.VPD_ST4 + "*" + tmp.VPD_ST5 + "*" + tmp.VPD_ST6 + "*" + tmp.VPD_ST7 + "*" + tmp.Vrio + "*" + tmp.Zvanie;
                SendMessage("izm|" + tmpp.Types + "|" + tmpp.Row + "|" + tmpp.Name_table + "|" + tmpp.Old_value + "|" + tmpp.New_value + "|" + tmpp.Col);
                foreach (OpenLoad p in list_shtat)
                {
                    if (p.Lnumber == tmp.Lnumber)
                    {
                        p.Otryv_pricaz = ".";
                        p.Otryv_primech = ".";
                        p.Otryv_status = ".";
                    }
                }
                list_ub.Remove(tmp);
            }//Пребытие
            else
            {
                Creat_kotel2(tmp, "Убытие");
                Izmen tmpp = new Izmen();
                tmpp.Types = "Убытие";
                list_ub.Add(tmp);
                foreach (OpenLoad p in list_shtat)
                {
                    if (p.Lnumber == tmp.Lnumber)
                    {
                        p.Otryv_pricaz = ".";
                        p.Otryv_status = tmp.Types_ub;
                        if (tmp.Cel != ".")
                            p.Otryv_primech += tmp.Cel;
                        if (tmp.Date_ub != ".")
                            p.Otryv_primech += "_с_" + tmp.Date_ub;
                        if (tmp.Date_preb != ".")
                            p.Otryv_primech += "_по_" + tmp.Date_preb;
                        if (tmp.Mesto != ".")
                            p.Otryv_primech += "_с выездом_" + tmp.Mesto;
                        if (tmp.Osn != ".")
                            p.Otryv_primech += "_Основание:_" + tmp.Osn;
                        if (tmp.Vrio != ".")
                            p.Otryv_primech += "_Временно исполняет обязанности:_" + tmp.Vrio;

                    }
                }
                tmpp.New_value = tmp.Cel + "*" + tmp.Date3 + "*" + tmp.Date_preb + "*" + tmp.Date_ub + "*" + tmp.Dolg + "*" + tmp.FIO + "*" + tmp.Lnumber + "*" + tmp.Mesto + "*" + tmp.Osn + "*" + tmp.Podr + "*" + tmp.Types + "*" + tmp.Types_ub + "*" + tmp.VPD_ST1 + "*" + tmp.VPD_ST2 + "*" + tmp.VPD_ST3 + "*" + tmp.VPD_ST4 + "*" + tmp.VPD_ST5 + "*" + tmp.VPD_ST6 + "*" + tmp.VPD_ST7 + "*" + tmp.Vrio + "*" + tmp.Zvanie;
                SendMessage("izm|" + tmpp.Types + "|" + tmpp.Row + "|" + tmpp.Name_table + "|" + tmpp.Old_value + "|" + tmpp.New_value + "|" + tmpp.Col);
            }
        }


        #endregion
        //Проверка: находится ли военнослужащий на котловом довольствии (питается ли в столовой) и при убытии
        //снимать с довольствия, а при возвращении - снова ставить на довольствие
        public void Creat_kotel2(Ub tmp, string D)
        {
            string[] word = tmp.Podr.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            string podr = word[0];
            Izmen izm = new Izmen();

            if (D == "Убытие")
            {
                foreach (OpenLoadKotel t in List_Kotel1)
                {
                    if (t.Lnumber == tmp.Lnumber)
                    {
                        izm.Types = "Котел";
                        izm.Name_table = "Убытие";
                        izm.New_value = tmp.Lnumber;
                        izm.Col = tmp.FIO + ">" + podr + ">" + tmp.Types;
                        List_Kotel1.Remove(t);
                        kotel2();
                        break;
                    }
                }
            }
            if (D == "Пребытие")
            {
                izm.Types = "Котел";
                izm.Name_table = "Пребытие";
                izm.New_value = tmp.Lnumber;
                izm.Col = tmp.FIO + ">" + podr + ">" + tmp.Types;
            }
            SendMessage("izm|" + izm.Types + "|" + izm.Row + "|" + izm.Name_table + "|" + izm.Old_value + "|" + izm.New_value + "|" + izm.Col);

        }
        //Добавить или убрать с котлового довольствия
        public void Creat_kotel(OpenLoad tmp, string D)
        {

            Izmen izm = new Izmen();
            if (D == "Добавить")
            {
                foreach (OpenLoadKotel t in List_Kotel1)
                {
                    if (t.Lnumber == tmp.Lnumber) return;
                }
                izm.Types = "Котел";
                izm.Name_table = "Добавить";
                izm.New_value = tmp.Lnumber;
                izm.Col = tmp.Fio + ">" + tmp.Podr1 + ">" + tmp.Types;
                OpenLoadKotel tmpp = new OpenLoadKotel();
                tmpp.FIO = tmp.Fio;
                tmpp.Lnumber = tmp.Lnumber;
                tmpp.Podr = tmp.Podr1;
                tmpp.Types = tmp.Types;
                list_Kotel1.Add(tmpp);
                kotel2();
            }
            if (D == "Убрать")
            {
                foreach (OpenLoadKotel t in List_Kotel1)
                {
                    if (t.Lnumber == tmp.Lnumber)
                    {
                        izm.Types = "Котел";
                        izm.Name_table = "Убрать";
                        izm.New_value = tmp.Lnumber;
                        izm.Col = tmp.Fio + ">" + tmp.Podr1 + ">" + tmp.Types;
                        List_Kotel1.Remove(t);
                        kotel2();
                        break;
                    }
                }
            }

            SendMessage("izm|" + izm.Types + "|" + izm.Row + "|" + izm.Name_table + "|" + izm.Old_value + "|" + izm.New_value + "|" + izm.Col);

        }

        void preb_kotel(Izmen I)
        {
            string[] word = I.Col.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
            OpenLoadKotel k = new OpenLoadKotel();
            k.FIO = word[0];
            k.Lnumber = I.New_value;
            k.Types = I.Types;
            foreach (OpenLoad p in list_shtat)
            {
                if (p.Lnumber == I.New_value)
                {
                    k.Podr = p.Podr1;
                }
            }
            list_Kotel1.Add(k);
            kotel2();
        }

        public void create_komand(OpenLoadZaShtat p, string s, string s1, string s2)
        {
            Komand_ld l = new Komand_ld();
            l.Date = s;
            l.Old = s1;
            l.Starshiy = s2;
            l.Per_sl = p.Types;
            l.FIO = p.FIO;
            l.LN = p.Lnumber;
            list_komand.Add(l);
        }
        //Сохранение данных о пребытии команды
        //Подобного рода данные нужны для бухгалтерского учета, чтоб не допустить закрепления
        //вещевого имущества военнослужащих по призыву за старшими команд
        public void save_komand()
        {
            if (list_komand.Count == 0)
            {
                MessageBox.Show("Сохранять нечего");
                return;
            }
            else
            {
                Izmen tmpp = new Izmen();
                tmpp.Types = "Добавить команду";
                foreach (Komand_ld k in list_komand)
                {
                    tmpp.Row = k.Id;
                    tmpp.Name_table = k.Old;
                    tmpp.Old_value = k.Starshiy;
                    tmpp.New_value += k.LN + ">" + k.FIO + ">" + k.Per_sl + ">" + k.Date + ">";
                }
                SendMessage("izm|" + tmpp.Types + "|" + tmpp.Row + "|" + tmpp.Name_table + "|" + tmpp.Old_value + "|" + tmpp.New_value + "|" + tmpp.Col);
            }
        }

        private string RD(string w)
        {
            string d = ".";
            string[] word = w.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            if (word[1] == "08" || word[1] == "09" || word[1] == "10" || word[1] == "11" || word[1] == "12")
            {
                d = "2-";
            }
            else
                d = "1-";
            d += word[2];
            return d;
        }




        private IDelegateCommand allCommand;
        public IDelegateCommand AllCommand
        {
            get
            {
                return allCommand ??
                    (allCommand = new DelegateCommand(o =>
                    {
                        int counter = int.Parse((string)o);
                        switch (counter)
                        {
                            case 0://Выход из приложения
                                Environment.Exit(0);
                                break;
                            //Работа с листом ШТАТ
                            case 1: //Добавления человека в ШТАТ (Показ списка заштатников)
                                HasVisibilityViborka = true;
                                HasVisibilityCreateUb = false;
                                HasVisibilityViborka2 = false;
                                break;
                            case 2: //Убрать человека из штата
                                OpenLoad p = SelectedDataGrid_Shtat;
                                Del_Shtat_People(p.Id);
                                break;
                            case 3: //Добавить человека в набор на контракт
                                OpenLoad NN = SelectedDataGrid_Shtat;
                                if (NN.Lnumber == "." && NN.Types != "О" && NN.Types != "контр" && NN.Types != "жен") return;
                                Otbor_na_kontr N = new Otbor_na_kontr();
                                N.Lnumber = NN.Lnumber;
                                N.FIO = NN.Fio;
                                DateTime DT = DateTime.Today;
                                N.Date1 = DT.Day + "." + DT.Month + "." + DT.Year;
                                New_otbor(N);
                                break;
                            case 4: // Временное убытие
                                HasVisibilityViborka = false;
                                HasVisibilityCreateUb = true;
                                HasVisibilityViborka2 = false;
                                break;
                            case 5: //Наряд
                                NN = SelectedDataGrid_Shtat;
                                if (NN.Fio != ".")
                                    New_Naryad(NN);
                                break;
                            case 6: //Поставить человека на котел
                                Creat_kotel(SelectedDataGrid_Shtat, "Добавить");
                                break;
                            case 7: //Снять с котла
                                Creat_kotel(SelectedDataGrid_Shtat, "Убрать");
                                break;
                            case 8: // Открыть личное дело
                                var ShtatLD = SelectedDataGrid_Shtat;
                                SendMessage("Search|" + ShtatLD.Lnumber);
                                break;
                            case 9: //Открыть выборку
                                HasVisibilityViborka = false;
                                HasVisibilityCreateUb = false;
                                HasVisibilityViborka2 = true;
                                break;
                            ///Работа с листом заштатники
                            case 11: //Открыть личное дело
                                var ZaShtatLD = SelectedDataGrid_ZaShtat;
                                SendMessage("Search|" + ZaShtatLD.Lnumber);
                                break;
                            case 12: //Уволить
                                HasVisibilityFormDelete = true;
                                HasVisibilityFormPerevod = false;
                                break;
                            case 13: //Перевести
                                HasVisibilityFormPerevod = true;
                                HasVisibilityFormDelete = false;
                                break;
                            //Работа с листом ПЕРЕВОД
                            case 14: //Открыть личное дело
                                var PerevLD = SelectedDataGrid_Perev;
                                SendMessage("Search|" + PerevLD.Lnumber);
                                break;
                            case 15: //Исключить
                                HasVisibilityFormIskl = true;
                                HasVisibilityFormPerevod = false;
                                HasVisibilityFormDelete = false;
                                break;
                            //Работа с листом УВОЛЕННЫЕ
                            case 16: //Открыть личное дело
                                var DeleteLD = SelectedDataGrid_Delete;
                                SendMessage("Search|" + DeleteLD.Lnumber);
                                break;
                            case 17: //Исключить
                                HasVisibilityFormDelIskl = true;
                                break;
                            //Работа с листом ИСКЛЮЧЕННЫЕ
                            case 18: //Открыть личное дело
                                var IsklLD = SelectedDataGrid_Iskl;
                                SendMessage("Search|" + IsklLD.Lnumber);
                                break;
                            //Работа с листом НАРЯД
                            case 19: //Открыть личное дело
                                var NaryadLD = SelectedDataGrid_Naryad;
                                SendMessage("Search|" + NaryadLD.Lnumber);
                                break;
                            case 20: //Убрать из наряда
                                list_Naryad.Remove(SelectedDataGrid_Naryad);
                                break;
                            case 21: //Сохранить наряд
                                string Mes_naryad = "Naryad";
                                foreach (Naryad nn in list_Naryad)
                                {
                                    Mes_naryad += "|" + nn.Date + "|" + nn.Dolg + "|" + nn.FIO + "|" + nn.Key + "|" + nn.Lnumber + "|" + nn.Podr + "|" + nn.Types + "|" + nn.Type_naryad + "|" + nn.Zvanie;
                                }
                                SendMessage(Mes_naryad);
                                break;
                            case 22: //Кнопка отмены
                                HasVisibilityViborka = false;
                                HasVisibilityViborka2 = false;
                                HasVisibilityCreateUb1 = false;
                                HasVisibilityCreateUb2 = false;
                                HasVisibilityCreateUb3 = false;
                                HasVisibilityCreateUb4 = false;
                                HasVisibilityCreateUb5 = false;
                                HasVisibilityCreateUb6 = false;
                                HasVisibilityCreateUb7 = false;
                                HasVisibilityCreateUb8 = false;
                                HasVisibilityFormPerevod = false;
                                HasVisibilityFormDelete = false;
                                HasVisibilityFormIskl = false;
                                HasVisibilityFormDelIskl = false;
                                HasVisibilityLeftGroup = false;
                                clear_viborka();
                                break;
                            case 23: //передача в кадры
                                Pered_vkadry(SelectedDataGrid_OtobKontr);
                                break;
                            case 24: //Перевод
                                OpenLoadZaShtat zashtat = SelectedDataGrid_ZaShtat;
                                var perevod = new OpenLoadPerevod();
                                perevod.Dolg = zashtat.Last_dolg;
                                perevod.FIO = zashtat.FIO;
                                perevod.Lnumber = zashtat.Lnumber;
                                perevod.Podr = zashtat.Last_podr;
                                perevod.Types = zashtat.Types;
                                perevod.Zvanie = zashtat.Zvanie;
                                if (PerevodChast != "") perevod.Chast = PerevodChast;
                                if (TBPerevodCity != "") perevod.Gorod = TBPerevodCity;
                                if (TBPerevodPrikaz != "") perevod.Pr = TBPerevodPrikaz;
                                if (TBPerevodWhatPrikaz != "") perevod.What_pr = TBPerevodWhatPrikaz;
                                if (TBPerevodOsn != "") perevod.Osn = TBPerevodOsn;
                                perevod.Date = dateClass.Date(DatePerevod);
                                perevod.Date_pr = dateClass.Date(DatePrikazPerevod);
                                Perevod_people(perevod);
                                HasVisibilityFormPerevod = false;
                                break;
                            case 25: //Уволить
                                zashtat = SelectedDataGrid_ZaShtat;
                                var delete = new OpenLoadDel();
                                delete.Dolg = zashtat.Last_dolg;
                                delete.FIO = zashtat.FIO;
                                delete.Lnumber = zashtat.Lnumber;
                                delete.Podr = zashtat.Last_podr;
                                delete.Types = zashtat.Types;
                                delete.Zvanie = zashtat.Zvanie;
                                if (TBDelPunkt != "") delete.Punkt = TBDelPunkt;
                                if (TBDelPodPunkt != "") delete.Podpunkt = TBDelPodPunkt;
                                if (TBDelPrikaz != "") delete.Pr = TBDelPrikaz;
                                if (TBDelWhatPrikaz != "") delete.What_pr = TBDelWhatPrikaz;
                                if (TBDelOsn != "") delete.Osn = TBDelOsn;
                                delete.Date = dateClass.Date(DeleteDate);
                                delete.Date_pr = dateClass.Date(DeletePricazDate);
                                Delete_people(delete);
                                HasVisibilityFormDelete = false;
                                break;
                            case 26: //
                                if (SelectedDataGrid_Shtat.Lnumber != ".") return;
                                foreach (OpenLoadZaShtat ii in list_Anshtat)
                                {
                                    if (ii.FIO == IItems.FIO)
                                    {
                                        Create_Shtat_People(SelectedDataGrid_Shtat.Id, ii.Lnumber);
                                        break;
                                    }
                                }
                                if (!IsSelectedMonoCreate)
                                    HasVisibilityViborka = false;
                                break;
                            case 27: //Добавить в Выборку
                                Create_viborka(SelectedDataGrid_Shtat, "+");
                                break;
                            case 28: //Убрать из выборки
                                Create_viborka(SelectedDataGrid_Shtat, "-");
                                break;
                            case 29: //Добавить куда-то после выборки
                                if (CBSelectedViborka2 == "Отбор на контракт")
                                {
                                    foreach (OpenLoad O in list_Vibor)
                                    {
                                        New_otbor(new Otbor_na_kontr
                                        {
                                            Lnumber = O.Lnumber,
                                            FIO = O.Fio,
                                            Zvanie = O.Zvanie,
                                            Types = O.Types,
                                            Podr = O.Podr1,
                                            Dolg = O.Dolgnost
                                        });
                                    }
                                    list_Vibor = new ObservableCollection<OpenLoad>();

                                }
                                else if (CBSelectedViborka2 == "Наряд")
                                {
                                    foreach (OpenLoad O in list_Vibor)
                                    {
                                        New_Naryad(O);
                                    }
                                    list_Vibor = new ObservableCollection<OpenLoad>();
                                }
                                break;
                            case 30: //Сохранить убытие в отпуск
                                Ub tmp = new Ub();
                                tmp.Dolg = SelectedDataGrid_Shtat.Dolgnost;
                                tmp.FIO = SelectedDataGrid_Shtat.Fio;
                                tmp.Lnumber = SelectedDataGrid_Shtat.Lnumber;
                                tmp.Zvanie = SelectedDataGrid_Shtat.Zvanie;
                                tmp.Types = SelectedDataGrid_Shtat.Types;
                                tmp.Podr = SelectedDataGrid_Shtat.Podr;
                                tmp.Types_ub = "отпуск";
                                tmp.Cel = SelectedTypesOtp + "_за_" + TBYearOtp + "_на_" + TBSrokOtp;
                                tmp.Mesto = TBMestoOtp;
                                tmp.Date_ub = dateClass.Date(Date_date1_otp);
                                tmp.Date_preb = dateClass.Date(Date_date2_otp);
                                tmp.Osn = TBOsnOtp;
                                new_ub(tmp);
                                HasVisibilityCreateUb = false;
                                break;
                            case 31: //Сохранить убытие в командировку
                                tmp = new Ub();
                                tmp.Dolg = SelectedDataGrid_Shtat.Dolgnost;
                                tmp.FIO = SelectedDataGrid_Shtat.Fio;
                                tmp.Lnumber = SelectedDataGrid_Shtat.Lnumber;
                                tmp.Zvanie = SelectedDataGrid_Shtat.Zvanie;
                                tmp.Types_ub = "командировка";
                                tmp.Podr = SelectedDataGrid_Shtat.Podr;
                                tmp.Types = SelectedDataGrid_Shtat.Types;
                                tmp.Date_ub = dateClass.Date(Date_ub_kom);
                                tmp.Mesto = TBMesKom;
                                tmp.Cel = TBCelKom;
                                tmp.Osn = TBOsnKom;
                                new_ub(tmp);
                                HasVisibilityCreateUb = false;
                                break;
                            case 32: //Сохранение убытия в Госпиталь
                                tmp = new Ub();
                                tmp.Dolg = SelectedDataGrid_Shtat.Dolgnost;
                                tmp.FIO = SelectedDataGrid_Shtat.Fio;
                                tmp.Lnumber = SelectedDataGrid_Shtat.Lnumber;
                                tmp.Zvanie = SelectedDataGrid_Shtat.Zvanie;
                                tmp.Types_ub = "госпиталь";
                                tmp.Podr = SelectedDataGrid_Shtat.Podr;
                                tmp.Types = SelectedDataGrid_Shtat.Types;
                                tmp.Mesto = SelectedCBUbGos;
                                tmp.Date_ub = dateClass.Date(Date_gos_ub);
                                tmp.Osn = TBOsnGospital;
                                new_ub(tmp);
                                HasVisibilityCreateUb = false;
                                break;
                            case 33:   //Сохранение убытия в медроту
                                tmp = new Ub();
                                tmp.Dolg = SelectedDataGrid_Shtat.Dolgnost;
                                tmp.FIO = SelectedDataGrid_Shtat.Fio;
                                tmp.Lnumber = SelectedDataGrid_Shtat.Lnumber;
                                tmp.Zvanie = SelectedDataGrid_Shtat.Zvanie;
                                tmp.Types_ub = "мед.рота";
                                tmp.Podr = SelectedDataGrid_Shtat.Podr;
                                tmp.Types = SelectedDataGrid_Shtat.Types;
                                tmp.Date_ub = dateClass.Date(Date_med_ub);
                                tmp.Osn = TBOsnMPP;
                                tmp.Cel = "лечение";
                                tmp.Mesto = "Медицинская рота";
                                new_ub(tmp);
                                HasVisibilityCreateUb = false;
                                break;
                            case 34:   //Сохранение убытия "Арест"
                                tmp = new Ub();
                                tmp.Dolg = SelectedDataGrid_Shtat.Dolgnost;
                                tmp.FIO = SelectedDataGrid_Shtat.Fio;
                                tmp.Lnumber = SelectedDataGrid_Shtat.Lnumber;
                                tmp.Zvanie = SelectedDataGrid_Shtat.Zvanie;
                                tmp.Types_ub = "арест";
                                tmp.Podr = SelectedDataGrid_Shtat.Podr;
                                tmp.Types = SelectedDataGrid_Shtat.Types;
                                tmp.Date_ub = dateClass.Date(Date_are_ub);
                                tmp.Osn = TBArest;
                                new_ub(tmp);
                                HasVisibilityCreateUb = false;
                                break;
                            case 35:   //Сохранение убытия в СОЧ
                                tmp = new Ub();
                                tmp.Dolg = SelectedDataGrid_Shtat.Dolgnost;
                                tmp.FIO = SelectedDataGrid_Shtat.Fio;
                                tmp.Lnumber = SelectedDataGrid_Shtat.Lnumber;
                                tmp.Zvanie = SelectedDataGrid_Shtat.Zvanie;
                                tmp.Types_ub = "СОЧ";
                                tmp.Podr = SelectedDataGrid_Shtat.Podr;
                                tmp.Types = SelectedDataGrid_Shtat.Types;
                                tmp.Date_ub = dateClass.Date(Soch_ub_date);
                                new_ub(tmp);
                                HasVisibilityCreateUb = false;
                                break;
                            case 36:   //Сохранение убытия по другим причинам
                                tmp = new Ub();
                                tmp.Dolg = SelectedDataGrid_Shtat.Dolgnost;
                                tmp.FIO = SelectedDataGrid_Shtat.Fio;
                                tmp.Lnumber = SelectedDataGrid_Shtat.Lnumber;
                                tmp.Zvanie = SelectedDataGrid_Shtat.Zvanie;
                                tmp.Types_ub = "другие причины";
                                tmp.Podr = SelectedDataGrid_Shtat.Podr;
                                tmp.Types = SelectedDataGrid_Shtat.Types;
                                tmp.Cel = TBPichOther;
                                tmp.Date_ub = dateClass.Date(Date_ub_dr);
                                tmp.Osn = TBOsnOther;
                                new_ub(tmp);
                                HasVisibilityCreateUb = false;
                                break;
                            case 37:   //Продолжить заполнение убытия
                                var scetch = CBSelectedOtrGrids;
                                if (scetch == "Отпуск")
                                {
                                    HasVisibilityCreateUb2 = true;
                                }
                                else if (scetch == "Командировка")
                                {
                                    HasVisibilityCreateUb3 = true;
                                }
                                else if (scetch == "Мед.рота")
                                {
                                    HasVisibilityCreateUb5 = true;
                                }
                                else if (scetch == "Госпиталь")
                                {
                                    HasVisibilityCreateUb4 = true;
                                }
                                else if (scetch == "Арест")
                                {
                                    HasVisibilityCreateUb6 = true;
                                }
                                else if (scetch == "СОЧ")
                                {
                                    HasVisibilityCreateUb7 = true;
                                }
                                else if (scetch == "Другие причины")
                                {
                                    HasVisibilityCreateUb8 = true;
                                }
                                break;
                            case 38:   //Выбрать из списка перестановок
                                var peres = LB_filter_perest;
                                var i = new Izmen();
                                i.Types = "Перестановка";
                                i.New_value = peres.Name;
                                SendMessage("OpenFile|" + i.Types + "|" + i.Row + "|" + i.Name_table + "|" + i.Old_value + "|" + i.New_value + "|" + i.Col);
                                break;
                            case 39:   //Выбрать из списка переведенных
                                var perev = LB_filter_perev;
                                if (perev.Name == "Переведенные, не исключенные")
                                {
                                    Vne_iskl("Переведенные, не исключенные");
                                }
                                else
                                {
                                    i = new Izmen();
                                    i.Types = "Переведенные";
                                    i.New_value = perev.Name;
                                    SendMessage("OpenFile|" + i.Types + "|" + i.Row + "|" + i.Name_table + "|" + i.Old_value + "|" + i.New_value + "|" + i.Col);
                                }
                                break;
                            case 40:   //Сохранить исключение после перевода
                                if (TBIsklPrikaz == "")
                                {
                                    MessageBox.Show("Введите номер приказа исключения"); return;
                                }
                                if (TBIsklWhatPrikaz == "")
                                {
                                    MessageBox.Show("Введите чей приказ"); return;
                                }
                                var perevod_piople_iskl = SelectedDataGrid_Perev;
                                var perevod_iskl = new OpenLoadIskl();
                                perevod_iskl.Date = dateClass.Date(IsklDate);
                                perevod_iskl.Date_pr = dateClass.Date(IsklPrikazDate);
                                perevod_iskl.Dolg = perevod_piople_iskl.Dolg;
                                perevod_iskl.FIO = perevod_piople_iskl.FIO;
                                perevod_iskl.Lnumber = perevod_piople_iskl.Lnumber;
                                perevod_iskl.Podr = perevod_piople_iskl.Podr;
                                perevod_iskl.Pr = TBIsklPrikaz;
                                perevod_iskl.Types = perevod_piople_iskl.Types;
                                perevod_iskl.What_pr = TBIsklWhatPrikaz;
                                perevod_iskl.Zvanie = perevod_piople_iskl.Zvanie;
                                Iskl_people(perevod_iskl);
                                HasVisibilityFormIskl = false;
                                break;
                            case 41:   //Отобразить список уволенных
                                var del = LB_filter_del;
                                if (del.Name == "Уволенные, не исключенные")
                                {
                                    Vne_iskl("Уволенные, не исключенные");
                                }
                                else
                                {
                                    i = new Izmen();
                                    i.Types = "Уволенные";
                                    i.New_value = del.Name;
                                    SendMessage("OpenFile|" + i.Types + "|" + i.Row + "|" + i.Name_table + "|" + i.Old_value + "|" + i.New_value + "|" + i.Col);
                                }
                                break;
                            case 42:   //Сохранить исключение после увольнения

                                if (TBDelToIsklPrikaz == "")
                                {
                                    MessageBox.Show("Введите номер приказа исключения"); return;
                                }
                                if (TBDelToIskWhatlPrikaz == "")
                                {
                                    MessageBox.Show("Введите чей приказ"); return;
                                }
                                var del_people = SelectedDataGrid_Delete;
                                OpenLoadIskl iskl_people = new OpenLoadIskl();
                                iskl_people.Date = dateClass.Date(DelIsklDate);
                                iskl_people.Date_pr = dateClass.Date(DelIsklPrikazDate);
                                iskl_people.Dolg = del_people.Dolg;
                                iskl_people.FIO = del_people.FIO;
                                iskl_people.Lnumber = del_people.Lnumber;
                                iskl_people.Podr = del_people.Podr;
                                iskl_people.Pr = TBDelToIsklPrikaz;
                                iskl_people.Types = del_people.Types;
                                iskl_people.What_pr = TBDelToIskWhatlPrikaz;
                                iskl_people.Zvanie = del_people.Zvanie;
                                Iskl_people(iskl_people);
                                HasVisibilityFormDelIskl = false;
                                break;
                            case 43:   //Отобразить список исключенных
                                var iskl = LB_filter_iskl;
                                i = new Izmen();
                                i.Types = "Исключенные";
                                i.New_value = iskl.Name;
                                SendMessage("OpenFile|" + i.Types + "|" + i.Row + "|" + i.Name_table + "|" + i.Old_value + "|" + i.New_value + "|" + i.Col);
                                break;
                            case 44:   //Добавить звание в личное дело
                                var zvan = new zvanie();
                                zvan.Name = SelectedZvanSlCB;
                                zvan.Date = dateClass.Date(ZvaniePrikazDate);
                                try
                                {
                                    if (TBLDCreateZvaniePrikaz != "") zvan.Pr = int.Parse(TBLDCreateZvaniePrikaz);
                                }
                                catch
                                {
                                    MessageBox.Show("Введено не цифровое значение в поле ПРИКАЗ");
                                    return;
                                }
                                if (TBLDCreateZvanieWho != "") zvan.What = TBLDCreateZvanieWho;
                                ld_zvanie.Add(zvan);
                                TBLDCreateZvanieWho = "";
                                TBLDCreateZvaniePrikaz = "";
                                break;
                            case 45:   //Режим отображения личного дела

                                break;
                            case 46:   //Поиск личного дела
                                SendMessage("Search|" + TBLDLnumber);
                                break;
                            case 47:   //Добавить фото в Личное дело
                                OpenFileDialog dlg = new OpenFileDialog();
                                dlg.FileName = "";
                                dlg.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                                    "JPEG (*jpg;*jpeg)|*.jpg;*jpeg|" +
                                    "Portable Network Graphic (*png)|*.png";
                                Nullable<bool> result = dlg.ShowDialog();
                                if (result == true)
                                {

                                    BitmapImage btm = new BitmapImage();
                                    btm.BeginInit();
                                    btm.UriSource = new Uri(dlg.FileName);
                                    btm.CacheOption = BitmapCacheOption.OnLoad;
                                    btm.EndInit();
                                    _photo = new BitmapImage();
                                    _photo = btm;
                                }

                                break;
                            case 48:   //Сохранить прибывшую команду
                                if (TBGroupPreb == "")
                                {
                                    MessageBox.Show("Введите откуда прибыла команда"); return;
                                }
                                if (TBGroupOld == "")
                                {
                                    MessageBox.Show("Введите кто сопровождал команду"); return;
                                }
                                save_komand();
                                break;
                            case 49:   //Отобразить скан биографии
                                Messager m = new Messager(_autobiographi);
                                m.Show();
                                break;
                            case 50:   //Отобразить скан диплома
                                m = new Messager(_diplom);
                                m.Show();
                                break;
                            case 51:   //Отобразить скан аттестационного листа
                                m = new Messager(_attest);
                                m.Show();
                                break;
                            case 52:   //Отобразить скан ИНН
                                m = new Messager(_inn);
                                m.Show();
                                break;
                            case 53:   //Отобразить скан СНИЛС
                                m = new Messager(_snils);
                                m.Show();
                                break;
                            case 54:   //Добавить контракт
                                var Kontr = new Kontrakt();
                                Kontr.data_zakl = dateClass.Date(ZaklKontraktDate);
                                Kontr.data_okon = dateClass.Date(OkonKontraktDate);
                                Kontr.Date_pricaz = dateClass.Date(KontraktPrikazDate);
                                if (TBLDCreateKontraktPrikaz != "") Kontr.Nomber_pricaz = TBLDCreateKontraktPrikaz;
                                Kontr.types = SelectedKontrSlCB;
                                if (TBLDCreateKontraktWho != "") Kontr.Who_pricaz = TBLDCreateKontraktWho;
                                ld_kon.Add(Kontr);
                                ZaklKontraktDate = DateTime.Today;
                                TBLDCreateKontraktPrikaz = "";
                                TBLDCreateKontraktWho = "";
                                break;
                            case 55:   //Отобразить таблицу с контрактами
                                if (HasVisibilityLDTable1)
                                    HasVisibilityLDTable1 = false;
                                else
                                    HasVisibilityLDTable1 = true;
                                break;
                            case 56: //Отобразить таблицу с присвоенными званиями
                                if (HasVisibilityLDTable2)
                                    HasVisibilityLDTable2 = false;
                                else
                                    HasVisibilityLDTable2 = true;
                                break;
                            case 57: //Добавить должность в послужной список
                                Poslug posl = new Poslug();
                                if (TBLDCreatePoslugChast != "") posl.Chast_ = TBLDCreatePoslugChast;
                                if (TBLDCreatePoslugDolg != "") posl.Dolgnost_ = TBLDCreatePoslugDolg;
                                if (TBLDCreatePoslugPodr != "") posl.Podr_ = TBLDCreatePoslugPodr;
                                if (TBLDCreatePoslugVus != "") posl.VUS_ = TBLDCreatePoslugVus;
                                if (TBLDCreatePoslugWho != "") posl.Name_pricaz = TBLDCreatePoslugWho;
                                if (TBLDCreatePoslugPrikaz != "") posl.Nomber_pricaz = TBLDCreatePoslugPrikaz;
                                posl.Date_pricaz = dateClass.Date(PoslugnoiPrikazDate);
                                ld_poslug.Add(posl);
                                TBLDCreatePoslugChast = "";
                                TBLDCreatePoslugDolg = "";
                                TBLDCreatePoslugPodr = "";
                                TBLDCreatePoslugVus = "";
                                TBLDCreatePoslugWho = "";
                                TBLDCreatePoslugPrikaz = "";
                                break;
                            case 58: //Отобразить таблицу с послужным списком
                                if (HasVisibilityLDTable3)
                                    HasVisibilityLDTable3 = false;
                                else
                                    HasVisibilityLDTable3 = true;
                                break;
                            case 59: //Добавить члена семьи в личное дело
                                family fam = new family();
                                if (TBLDCreateFamilyFIO != "") fam.Name = TBLDCreateFamilyFIO;
                                fam.types = SelectedFamilySlCB;
                                fam.Brd = dateClass.Date(FamilyBrdDate);
                                ld_fam.Add(fam);
                                TBLDCreateFamilyFIO = "";
                                break;
                            case 60: //Отобразить таблицу с семьей
                                if (HasVisibilityLDTable4)
                                    HasVisibilityLDTable4 = false;
                                else
                                    HasVisibilityLDTable4 = true;
                                break;
                            case 61: //Загрузить скан биографии
                                dlg = new OpenFileDialog();
                                dlg.FileName = "";
                                dlg.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                                    "JPEG (*jpg;*jpeg)|*.jpg;*jpeg|" +
                                    "Portable Network Graphic (*png)|*.png";
                                result = dlg.ShowDialog();
                                if (result == true)
                                {
                                    BitmapImage btm = new BitmapImage();
                                    btm.BeginInit();
                                    btm.UriSource = new Uri(dlg.FileName);
                                    btm.CacheOption = BitmapCacheOption.OnLoad;
                                    btm.EndInit();
                                    _autobiographi = new BitmapImage();
                                    _autobiographi = btm;
                                }
                                break;
                            case 62: //Загрузить скан диплома
                                dlg = new OpenFileDialog();
                                dlg.FileName = "";
                                dlg.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                                    "JPEG (*jpg;*jpeg)|*.jpg;*jpeg|" +
                                    "Portable Network Graphic (*png)|*.png";
                                result = dlg.ShowDialog();
                                if (result == true)
                                {
                                    BitmapImage btm = new BitmapImage();
                                    btm.BeginInit();
                                    btm.UriSource = new Uri(dlg.FileName);
                                    btm.CacheOption = BitmapCacheOption.OnLoad;
                                    btm.EndInit();
                                    _diplom = new BitmapImage();
                                    _diplom = btm;
                                }
                                break;
                            case 63: //Загрузить скан аттестационного листа
                                dlg = new OpenFileDialog();
                                dlg.FileName = "";
                                dlg.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                                    "JPEG (*jpg;*jpeg)|*.jpg;*jpeg|" +
                                    "Portable Network Graphic (*png)|*.png";
                                result = dlg.ShowDialog();
                                if (result == true)
                                {
                                    BitmapImage btm = new BitmapImage();
                                    btm.BeginInit();
                                    btm.UriSource = new Uri(dlg.FileName);
                                    btm.CacheOption = BitmapCacheOption.OnLoad;
                                    btm.EndInit();
                                    _attest = new BitmapImage();
                                    _attest = btm;
                                }
                                break;
                            case 64: //Загрузить скан ИНН
                                dlg = new OpenFileDialog();
                                dlg.FileName = "";
                                dlg.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                                    "JPEG (*jpg;*jpeg)|*.jpg;*jpeg|" +
                                    "Portable Network Graphic (*png)|*.png";
                                result = dlg.ShowDialog();
                                if (result == true)
                                {
                                    BitmapImage btm = new BitmapImage();
                                    btm.BeginInit();
                                    btm.UriSource = new Uri(dlg.FileName);
                                    btm.CacheOption = BitmapCacheOption.OnLoad;
                                    btm.EndInit();
                                    _inn = new BitmapImage();
                                    _inn = btm;
                                }
                                break;
                            case 65: //Загрузить скан СНИЛС
                                dlg = new OpenFileDialog();
                                dlg.FileName = "";
                                dlg.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                                    "JPEG (*jpg;*jpeg)|*.jpg;*jpeg|" +
                                    "Portable Network Graphic (*png)|*.png";
                                result = dlg.ShowDialog();
                                if (result == true)
                                {
                                    BitmapImage btm = new BitmapImage();
                                    btm.BeginInit();
                                    btm.UriSource = new Uri(dlg.FileName);
                                    btm.CacheOption = BitmapCacheOption.OnLoad;
                                    btm.EndInit();
                                    _snils = new BitmapImage();
                                    _snils = btm;
                                }
                                break;
                            case 66: //Добавить награду в личное дело
                                Nagrad nagr = new Nagrad();
                                if (TBLDCreateNagradType != "") nagr.Type = TBLDCreateNagradType;
                                if (TBLDCreateNagradWho != "") nagr.Who_pricaz = TBLDCreateNagradWho;
                                if (TBLDCreateNagradPrikaz != "") nagr.Pricaz = TBLDCreateNagradPrikaz;
                                nagr.Date_ = dateClass.Date(NagradDate);
                                nagr.Date_pricaz = dateClass.Date(NagradPrikazDate);
                                ld_nag.Add(nagr);
                                TBLDCreateNagradType = "";
                                TBLDCreateNagradWho = "";
                                TBLDCreateNagradPrikaz = "";
                                break;
                            case 67: //отобразить таблицу с наградами
                                if (HasVisibilityLDTable5)
                                    HasVisibilityLDTable5 = false;
                                else
                                    HasVisibilityLDTable5 = true;
                                break;
                            case 68: //Сохранить личное дело
                                LD l = new LD();
                                Photo_ T3 = new Photo_();
                                T3.Date1 = _photo;
                                T3.Date2 = _autobiographi;
                                T3.Date3 = _diplom;
                                T3.Date4 = _attest;
                                T3.Date5 = _inn;
                                T3.Date6 = _snils;

                                if (TBLDCreateFIO != "") LD_Dan.FIO = TBLDCreateFIO;
                                if (TBLDCreateRecvizit != "") LD_Dan.Bank_card = TBLDCreateRecvizit;
                                LD_Dan.Date_brd = dateClass.Date(DateBrdLD);
                                if (TBLDCreateGrObr != "") LD_Dan.Gr_obr = TBLDCreateGrObr;
                                if (TBLDCreateHomeAddress != "") LD_Dan.Home_adres = TBLDCreateHomeAddress;
                                if (TBLDCreateLnumber != "") LD_Dan.Lnumber = TBLDCreateLnumber;
                                if (TBLDCreateMestBrd != "") LD_Dan.Mesto_brd = TBLDCreateMestBrd;
                                if (TBLDCreateNac != "") LD_Dan.Nac = TBLDCreateNac;
                                if (TBLDCreatePACNomber != "") LD_Dan.PAC = TBLDCreatePACNomber;
                                if (SelectedPolCB != "") LD_Dan.POL = SelectedPolCB;
                                if (LD_Kon.Count > 0 && SelectedPolCB == "муж")
                                    LD_Dan.Types = "контр";
                                else if (LD_Kon.Count > 0 && SelectedPolCB == "жен")
                                    LD_Dan.Types = "жен";
                                else
                                {
                                    foreach (zvanie z in LD_Zvanie)
                                        if (z.Name == "рядовой")
                                            LD_Dan.Types = RD(z.Date);
                                }
                                if (TBLDCreateVodUdost != "") LD_Dan.Vod_prava = TBLDCreateVodUdost;
                                if (TBLDCreateVoenkom != "") LD_Dan.Voenkomat = TBLDCreateVoenkom;
                                if (TBLDCreateVoenObr != "") LD_Dan.Voen_obr = TBLDCreateVoenObr;

                                LD_Naz.date_pricaz = dateClass.Date(NaznachDate);
                                if (TBLDCreateNaznachPrikaz != "") LD_Naz.pricaz = TBLDCreateNaznachPrikaz;
                                if (TBLDCreateNaznachWho != "") LD_Naz.who_pricaz = TBLDCreateNaznachWho;

                                LD_PD.date_pricaz = dateClass.Date(PriemDelDate);
                                if (TBLDCreatePriemDelhPrikaz != "") LD_PD.pricaz = TBLDCreatePriemDelhPrikaz;
                                if (TBLDCreatePriemDelhWho != "") LD_PD.who_pricaz = TBLDCreatePriemDelhWho;

                                if (TBLDCreateVigivanieMesto != "") LD_Viz.Mesto = TBLDCreateVigivanieMesto;
                                if (TBLDCreateVigivanieOsn != "") LD_Viz.Osnovanie = TBLDCreateVigivanieOsn;
                                if (TBLDCreateVigivaniePeriod != "") LD_Viz.Period = TBLDCreateVigivaniePeriod;

                                Docum d1 = new Docum();
                                d1.Date_vid = dateClass.Date(PassportDate);
                                d1.Kod = "01";
                                if (TBLDCreatePassportNomber != "") d1.Nomber = TBLDCreatePassportNomber;
                                if (TBLDCreatePassportSerial != "") d1.Seriya = TBLDCreatePassportSerial;
                                if (TBLDCreatePassportWho != "") d1.Who_vidal = TBLDCreatePassportWho;
                                LD_Doc.Add(d1);
                                d1 = new Docum();
                                d1.Date_vid = dateClass.Date(UdostDate);
                                d1.Kod = "04";
                                if (TBLDCreateUdostNomber != "") d1.Nomber = TBLDCreateUdostNomber;
                                if (TBLDCreateUdostSerial != "") d1.Seriya = TBLDCreateUdostSerial;
                                if (TBLDCreateUdostWho != "") d1.Who_vidal = TBLDCreateUdostWho;
                                LD_Doc.Add(d1);

                                string mes = l.Create_people_LD(ld_zvanie, ld_poslug, ld_fam, ld_nag, T3, LD_Doc, LD_Dan, ld_kon, LD_Naz, LD_PD, LD_Viz);
                                SendMessage(mes);
                                break;
                            case 69: //открыть добавление команды
                                if (HasVisibilityLeftGroup)
                                    HasVisibilityLeftGroup = false;
                                else
                                    HasVisibilityLeftGroup = true;
                                break;
                            case 70: //Добавить личное дело
                                HasVisibilityLDBlock1 = true;
                                HasVisibilityNewPhoto = true;
                                HasVisibilityOblusInfo = false;
                                HasVisibilityLDCreate1 = true;
                                break;
                            case 71: //Открыть личное дело из отбор на контракт
                                var OtobLD = SelectedDataGrid_OtobKontr;
                                SendMessage("Search|" + OtobLD.Lnumber);
                                break;
                            case 72: //Отказ от контракта из отбор на контракт
                                OK_otkaz(SelectedDataGrid_OtobKontr, 1);
                                break;
                            case 73: //Открыть личное дело из переданные в кадры
                                var PeredLD = SelectedDataGrid_PeredKontr;
                                SendMessage("Search|" + PeredLD.Lnumber);
                                break;
                            case 74: //Состоялся приказ
                                OK_sost(SelectedDataGrid_PeredKontr);
                                break;
                            case 75: //Отказ из переданные в кадры
                                OK_otkaz(SelectedDataGrid_PeredKontr, 2);
                                break;
                            case 76: //Открыть личное дело из состоявшихся
                                var SostLD = SelectedDataGrid_SostKontr;
                                SendMessage("Search|" + SostLD.Lnumber);
                                break;
                            case 77: //Открыть личное дело отказ от контракта
                                var OtkazLD = SelectedDataGrid_OtkazKontr;
                                SendMessage("Search|" + OtkazLD.Lnumber);
                                break;
                            case 78: //Вернуть в отбор на контракт
                                OK_Snowa_otbor(SelectedDataGrid_OtkazKontr);
                                break;
                            case 79:
                                _hasVisibilityLDCreate1 = false;
                                _hasVisibilityLDCreate2 = true;
                                break;
                            case 80:
                                _hasVisibilityLDCreate2 = false;
                                _hasVisibilityLDCreate3 = true;
                                break;
                            case 81:
                                _hasVisibilityLDCreate3 = false;
                                _hasVisibilityLDCreate4 = true;
                                break;
                            case 82:
                                _hasVisibilityLDCreate4 = false;
                                _hasVisibilityLDCreate5 = true;
                                break;
                            case 83:
                                _hasVisibilityLDCreate5 = false;
                                _hasVisibilityLDCreate6 = true;
                                break;
                            case 84:
                                _hasVisibilityLDCreate6 = false;
                                _hasVisibilityLDCreate7 = true;
                                break;
                            case 85:
                                _hasVisibilityLDCreate7 = false;
                                _hasVisibilityLDCreate8 = true;
                                break;
                            case 86:
                                _hasVisibilityLDCreate8 = false;
                                _hasVisibilityLDCreate9 = true;
                                break;
                            case 87:
                                _hasVisibilityLDCreate9 = false;
                                _hasVisibilityLDCreate10 = true;
                                break;
                        }


                    }));
            }
        }
        private IDelegateCommand addRSZCommand;
        public IDelegateCommand AddRSZCommand
        {
            get
            {
                return addRSZCommand ??
                    (addRSZCommand = new DelegateCommand(o =>
                    {
                        int counter = int.Parse((string)o);
                        Modal.RSZ R = new Client.Modal.RSZ();
                        switch (counter)
                        {
                            case 0:
                                list_rsz_lic = R.RSZZ(list_shtat, "", "1");
                                break;
                            case 1:
                                list_rsz_lic = R.RSZZ(list_shtat, "", "1");
                                break;
                            case 2:
                                list_rsz_lic = R.RSZZ(list_shtat, "управ", "2");
                                break;
                            case 3:
                                list_rsz_lic = R.RSZZ(list_shtat, "1мсб", "2");
                                break;
                            case 4:

                                break;
                            case 5:

                                break;
                            case 6:

                                break;
                            case 7:

                                break;
                            case 8:

                                break;
                            case 9:

                                break;
                            case 10:

                                break;
                            case 11:

                                break;
                            case 12:

                                break;
                            case 13:

                                break;
                            case 14:

                                break;
                            case 15:

                                break;
                            case 16:

                                break;
                            case 17:

                                break;
                            case 18:

                                break;
                            case 19:

                                break;
                            case 20:

                                break;
                            case 21:

                                break;
                            case 22:

                                break;
                            case 23:

                                break;
                            case 24:

                                break;
                            case 25:

                                break;
                            case 26:

                                break;
                            case 27:

                                break;
                            case 28:

                                break;
                            case 29:

                                break;
                            case 30:

                                break;
                            case 31:

                                break;
                            case 32:

                                break;
                            case 33:

                                break;
                            case 34:

                                break;
                        }

                    }));
            }
        }
        #region Visibility
        public event PropertyChangedEventHandler PropertyChanged;
        private static bool _hasVisibilityViborka1; //Выборка заштатников
        public bool HasVisibilityViborka
        {
            get { return _hasVisibilityViborka1; }
            set
            {
                _hasVisibilityViborka1 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityViborka2; //Выборка заштатников
        public bool HasVisibilityViborka2
        {
            get { return _hasVisibilityViborka2; }
            set
            {
                _hasVisibilityViborka2 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityCreateUp; //Убытие (1 экран)
        public bool HasVisibilityCreateUb
        {
            get { return _hasVisibilityCreateUp; }
            set
            {
                _hasVisibilityCreateUp = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityCreateUb; //Убытие (1 экран)
        public bool HasVisibilityCreateUb1
        {
            get { return _hasVisibilityCreateUb; }
            set
            {
                _hasVisibilityCreateUb = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityCreateUb2; //Убытие (1 экран)
        public bool HasVisibilityCreateUb2
        {
            get { return _hasVisibilityCreateUb2; }
            set
            {
                _hasVisibilityCreateUb2 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityCreateUb3; //Убытие (1 экран)
        public bool HasVisibilityCreateUb3
        {
            get { return _hasVisibilityCreateUb3; }
            set
            {
                _hasVisibilityCreateUb3 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityCreateUb4; //Убытие (1 экран)
        public bool HasVisibilityCreateUb4
        {
            get { return _hasVisibilityCreateUb4; }
            set
            {
                _hasVisibilityCreateUb4 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityCreateUb5; //Убытие (1 экран)
        public bool HasVisibilityCreateUb5
        {
            get { return _hasVisibilityCreateUb5; }
            set
            {
                _hasVisibilityCreateUb5 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityCreateUb6; //Убытие (1 экран)
        public bool HasVisibilityCreateUb6
        {
            get { return _hasVisibilityCreateUb6; }
            set
            {
                _hasVisibilityCreateUb6 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityCreateUb7; //Убытие (1 экран)
        public bool HasVisibilityCreateUb7
        {
            get { return _hasVisibilityCreateUb7; }
            set
            {
                _hasVisibilityCreateUb7 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityCreateUb8; //Убытие (1 экран)
        public bool HasVisibilityCreateUb8
        {
            get { return _hasVisibilityCreateUb8; }
            set
            {
                _hasVisibilityCreateUb8 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityFormPerevod; //Форма перевода
        public bool HasVisibilityFormPerevod
        {
            get { return _hasVisibilityFormPerevod; }
            set
            {
                _hasVisibilityFormPerevod = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityFormDelete; //Форма перевода
        public bool HasVisibilityFormDelete
        {
            get { return _hasVisibilityFormDelete; }
            set
            {
                _hasVisibilityFormDelete = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityFormIskl; //Форма исключения после перевода
        public bool HasVisibilityFormIskl
        {
            get { return _hasVisibilityFormIskl; }
            set
            {
                _hasVisibilityFormIskl = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityFormDelIskl; //Форма Исключения после увольнения
        public bool HasVisibilityFormDelIskl
        {
            get { return _hasVisibilityFormDelIskl; }
            set
            {
                _hasVisibilityFormDelIskl = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityFormNewPhoto; //Добавление фото
        public bool HasVisibilityNewPhoto
        {
            get { return _hasVisibilityFormNewPhoto; }
            set
            {
                _hasVisibilityFormNewPhoto = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLeftGroup; //Добавление команды
        public bool HasVisibilityLeftGroup
        {
            get { return _hasVisibilityLeftGroup; }
            set
            {
                _hasVisibilityLeftGroup = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDBlock1; //Форма перевода
        public bool HasVisibilityLDBlock1
        {
            get { return _hasVisibilityLDBlock1; }
            set
            {
                _hasVisibilityLDBlock1 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDTable1; //Таблица контракт ЛД
        public bool HasVisibilityLDTable1
        {
            get { return _hasVisibilityLDTable1; }
            set
            {
                _hasVisibilityLDTable1 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDTable2; //Таблица звания ЛД
        public bool HasVisibilityLDTable2
        {
            get { return _hasVisibilityLDTable2; }
            set
            {
                _hasVisibilityLDTable2 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDTable3; //Таблица послужной ЛД
        public bool HasVisibilityLDTable3
        {
            get { return _hasVisibilityLDTable3; }
            set
            {
                _hasVisibilityLDTable3 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDTable4; //Таблица семья ЛД
        public bool HasVisibilityLDTable4
        {
            get { return _hasVisibilityLDTable4; }
            set
            {
                _hasVisibilityLDTable4 = value;
                NotifyChanged();
            }
        }

        private static bool _hasVisibilityLDTable5; //Таблица награды ЛД
        public bool HasVisibilityLDTable5
        {
            get { return _hasVisibilityLDTable5; }
            set
            {
                _hasVisibilityLDTable5 = value;
                NotifyChanged();
            }
        }
        private static bool _isSelectedMonoCreate;
        public bool IsSelectedMonoCreate
        {
            get { return _isSelectedMonoCreate; }
            set
            {
                _isSelectedMonoCreate = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityOblusInfo;
        public bool HasVisibilityOblusInfo
        {
            get { return _hasVisibilityOblusInfo; }
            set
            {
                _hasVisibilityOblusInfo = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDCreate1;
        public bool HasVisibilityLDCreate1
        {
            get { return _hasVisibilityLDCreate1; }
            set
            {
                _hasVisibilityLDCreate1 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDCreate2;
        public bool HasVisibilityLDCreate2
        {
            get { return _hasVisibilityLDCreate2; }
            set
            {
                _hasVisibilityLDCreate2 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDCreate3;
        public bool HasVisibilityLDCreate3
        {
            get { return _hasVisibilityLDCreate3; }
            set
            {
                _hasVisibilityLDCreate3 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDCreate4;
        public bool HasVisibilityLDCreate4
        {
            get { return _hasVisibilityLDCreate4; }
            set
            {
                _hasVisibilityLDCreate4 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDCreate5;
        public bool HasVisibilityLDCreate5
        {
            get { return _hasVisibilityLDCreate5; }
            set
            {
                _hasVisibilityLDCreate5 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDCreate6;
        public bool HasVisibilityLDCreate6
        {
            get { return _hasVisibilityLDCreate6; }
            set
            {
                _hasVisibilityLDCreate6 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDCreate7;
        public bool HasVisibilityLDCreate7
        {
            get { return _hasVisibilityLDCreate7; }
            set
            {
                _hasVisibilityLDCreate7 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDCreate8;
        public bool HasVisibilityLDCreate8
        {
            get { return _hasVisibilityLDCreate8; }
            set
            {
                _hasVisibilityLDCreate8 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDCreate9;
        public bool HasVisibilityLDCreate9
        {
            get { return _hasVisibilityLDCreate9; }
            set
            {
                _hasVisibilityLDCreate9 = value;
                NotifyChanged();
            }
        }
        private static bool _hasVisibilityLDCreate10;
        public bool HasVisibilityLDCreate10
        {
            get { return _hasVisibilityLDCreate10; }
            set
            {
                _hasVisibilityLDCreate10 = value;
                NotifyChanged();
            }
        }

        #endregion
        #region TextBox's
        private string perevodChast { get; set; }
        public string PerevodChast
        {
            get { return perevodChast; }
            set
            {
                perevodChast = value;
                NotifyChanged();
            }
        }
        private string _tBYearOtp { get; set; }
        public string TBYearOtp
        {
            get { return _tBYearOtp; }
            set
            {
                _tBYearOtp = value;
                NotifyChanged();
            }
        }
        private string _tBMestoOtp { get; set; }
        public string TBMestoOtp
        {
            get { return _tBMestoOtp; }
            set
            {
                _tBMestoOtp = value;
                NotifyChanged();
            }
        }
        private string _tBSrokOtp { get; set; }
        public string TBSrokOtp
        {
            get { return _tBSrokOtp; }
            set
            {
                _tBSrokOtp = value;
                NotifyChanged();
            }
        }
        private string _tBOsnOtp { get; set; }
        public string TBOsnOtp
        {
            get { return _tBOsnOtp; }
            set
            {
                _tBOsnOtp = value;
                NotifyChanged();
            }
        }
        private string _tBMesKom { get; set; }
        public string TBMesKom
        {
            get { return _tBMesKom; }
            set
            {
                _tBMesKom = value;
                NotifyChanged();
            }
        }
        private string _tBCelKom { get; set; }
        public string TBCelKom
        {
            get { return _tBCelKom; }
            set
            {
                _tBCelKom = value;
                NotifyChanged();
            }
        }
        private string _tBOsnKom { get; set; }
        public string TBOsnKom
        {
            get { return _tBOsnKom; }
            set
            {
                _tBOsnKom = value;
                NotifyChanged();
            }
        }
        private string _tBOsnGospital { get; set; }
        public string TBOsnGospital
        {
            get { return _tBOsnGospital; }
            set
            {
                _tBOsnGospital = value;
                NotifyChanged();
            }
        }
        private string _tBOsnMPP { get; set; }
        public string TBOsnMPP
        {
            get { return _tBOsnMPP; }
            set
            {
                _tBOsnMPP = value;
                NotifyChanged();
            }
        }
        private string _tBArest { get; set; }
        public string TBArest
        {
            get { return _tBArest; }
            set
            {
                _tBArest = value;
                NotifyChanged();
            }
        }
        private string _tBPichOther { get; set; }
        public string TBPichOther
        {
            get { return _tBPichOther; }
            set
            {
                _tBPichOther = value;
                NotifyChanged();
            }
        }
        private string _tBOsnOther { get; set; }
        public string TBOsnOther
        {
            get { return _tBOsnOther; }
            set
            {
                _tBOsnOther = value;
                NotifyChanged();
            }
        }
        private string _tBPerevodCity { get; set; }
        public string TBPerevodCity
        {
            get { return _tBPerevodCity; }
            set
            {
                _tBPerevodCity = value;
                NotifyChanged();
            }
        }
        private string _tBPerevodOsn { get; set; }
        public string TBPerevodOsn
        {
            get { return _tBPerevodOsn; }
            set
            {
                _tBPerevodOsn = value;
                NotifyChanged();
            }
        }
        private string _tBPerevodPrikaz { get; set; }
        public string TBPerevodPrikaz
        {
            get { return _tBPerevodPrikaz; }
            set
            {
                _tBPerevodPrikaz = value;
                NotifyChanged();
            }
        }
        private string _tBPerevodWhatPrikaz { get; set; }
        public string TBPerevodWhatPrikaz
        {
            get { return _tBPerevodWhatPrikaz; }
            set
            {
                _tBPerevodWhatPrikaz = value;
                NotifyChanged();
            }
        }
        private string _tBDelPunkt { get; set; }
        public string TBDelPunkt
        {
            get { return _tBDelPunkt; }
            set
            {
                _tBDelPunkt = value;
                NotifyChanged();
            }
        }
        private string _tBDelPodPunkt { get; set; }
        public string TBDelPodPunkt
        {
            get { return _tBDelPodPunkt; }
            set
            {
                _tBDelPodPunkt = value;
                NotifyChanged();
            }
        }
        private string _tBDelOsn { get; set; }
        public string TBDelOsn
        {
            get { return _tBDelOsn; }
            set
            {
                _tBDelOsn = value;
                NotifyChanged();
            }
        }
        private string _tBDelPrikaz { get; set; }
        public string TBDelPrikaz
        {
            get { return _tBDelPrikaz; }
            set
            {
                _tBDelPrikaz = value;
                NotifyChanged();
            }
        }
        private string _tBDelWhatPrikaz { get; set; }
        public string TBDelWhatPrikaz
        {
            get { return _tBDelWhatPrikaz; }
            set
            {
                _tBDelWhatPrikaz = value;
                NotifyChanged();
            }
        }
        private string _tBIsklPrikaz { get; set; }
        public string TBIsklPrikaz
        {
            get { return _tBIsklPrikaz; }
            set
            {
                _tBIsklPrikaz = value;
                NotifyChanged();
            }
        }
        private string _tBIsklWhatPrikaz { get; set; }
        public string TBIsklWhatPrikaz
        {
            get { return _tBIsklWhatPrikaz; }
            set
            {
                _tBIsklWhatPrikaz = value;
                NotifyChanged();
            }
        }
        private string _tBDelToIsklPrikaz { get; set; }
        public string TBDelToIsklPrikaz
        {
            get { return _tBDelToIsklPrikaz; }
            set
            {
                _tBDelToIsklPrikaz = value;
                NotifyChanged();
            }
        }
        private string _tBDelToIskWhatlPrikaz { get; set; }
        public string TBDelToIskWhatlPrikaz
        {
            get { return _tBDelToIskWhatlPrikaz; }
            set
            {
                _tBDelToIskWhatlPrikaz = value;
                NotifyChanged();
            }
        }
        private string _tBLDLnumber { get; set; }
        public string TBLDLnumber
        {
            get { return _tBLDLnumber; }
            set
            {
                _tBLDLnumber = value;
                NotifyChanged();
            }
        }
        private string _tBGroupPreb { get; set; }
        public string TBGroupPreb
        {
            get { return _tBGroupPreb; }
            set
            {
                _tBGroupPreb = value;
                NotifyChanged();
            }
        }
        private string _tBGroupName { get; set; }
        public string TBGroupName
        {
            get { return _tBGroupName; }
            set
            {
                _tBGroupName = value;
                NotifyChanged();
            }
        }
        private string _tBGroupOld { get; set; }
        public string TBGroupOld
        {
            get { return _tBGroupOld; }
            set
            {
                _tBGroupOld = value;
                NotifyChanged();
            }
        }
        private string _tBGroupDate { get; set; }
        public string TBGroupDate
        {
            get { return _tBGroupDate; }
            set
            {
                _tBGroupDate = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateLnumber { get; set; }
        public string TBLDCreateLnumber
        {
            get { return _tBLDCreateLnumber; }
            set
            {
                _tBLDCreateLnumber = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateFIO { get; set; }
        public string TBLDCreateFIO
        {
            get { return _tBLDCreateFIO; }
            set
            {
                _tBLDCreateFIO = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateMestBrd { get; set; }
        public string TBLDCreateMestBrd
        {
            get { return _tBLDCreateMestBrd; }
            set
            {
                _tBLDCreateMestBrd = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateNac { get; set; }
        public string TBLDCreateNac
        {
            get { return _tBLDCreateNac; }
            set
            {
                _tBLDCreateNac = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateGrObr { get; set; }
        public string TBLDCreateGrObr
        {
            get { return _tBLDCreateGrObr; }
            set
            {
                _tBLDCreateGrObr = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateVoenObr { get; set; }
        public string TBLDCreateVoenObr
        {
            get { return _tBLDCreateVoenObr; }
            set
            {
                _tBLDCreateVoenObr = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateRecvizit { get; set; }
        public string TBLDCreateRecvizit
        {
            get { return _tBLDCreateRecvizit; }
            set
            {
                _tBLDCreateRecvizit = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateVodUdost { get; set; }
        public string TBLDCreateVodUdost
        {
            get { return _tBLDCreateVodUdost; }
            set
            {
                _tBLDCreateVodUdost = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateVoenkom { get; set; }
        public string TBLDCreateVoenkom
        {
            get { return _tBLDCreateVoenkom; }
            set
            {
                _tBLDCreateVoenkom = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateHomeAddress { get; set; }
        public string TBLDCreateHomeAddress
        {
            get { return _tBLDCreateHomeAddress; }
            set
            {
                _tBLDCreateHomeAddress = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreatePassportId { get; set; }
        public string TBLDCreatePassportId
        {
            get { return _tBLDCreatePassportId; }
            set
            {
                _tBLDCreatePassportId = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreatePassportSerial { get; set; }
        public string TBLDCreatePassportSerial
        {
            get { return _tBLDCreatePassportSerial; }
            set
            {
                _tBLDCreatePassportSerial = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreatePassportNomber { get; set; }
        public string TBLDCreatePassportNomber
        {
            get { return _tBLDCreatePassportNomber; }
            set
            {
                _tBLDCreatePassportNomber = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreatePassportWho { get; set; }
        public string TBLDCreatePassportWho
        {
            get { return _tBLDCreatePassportWho; }
            set
            {
                _tBLDCreatePassportWho = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateUdostId { get; set; }
        public string TBLDCreateUdostId
        {
            get { return _tBLDCreateUdostId; }
            set
            {
                _tBLDCreateUdostId = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateUdostSerial { get; set; }
        public string TBLDCreateUdostSerial
        {
            get { return _tBLDCreateUdostSerial; }
            set
            {
                _tBLDCreateUdostSerial = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateUdostNomber { get; set; }
        public string TBLDCreateUdostNomber
        {
            get { return _tBLDCreateUdostNomber; }
            set
            {
                _tBLDCreateUdostNomber = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateUdostWho { get; set; }
        public string TBLDCreateUdostWho
        {
            get { return _tBLDCreateUdostWho; }
            set
            {
                _tBLDCreateUdostWho = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateKontraktWho { get; set; }
        public string TBLDCreateKontraktWho
        {
            get { return _tBLDCreateKontraktWho; }
            set
            {
                _tBLDCreateKontraktWho = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateKontraktPrikaz { get; set; }
        public string TBLDCreateKontraktPrikaz
        {
            get { return _tBLDCreateKontraktPrikaz; }
            set
            {
                _tBLDCreateKontraktPrikaz = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateZvanieWho { get; set; }
        public string TBLDCreateZvanieWho
        {
            get { return _tBLDCreateZvanieWho; }
            set
            {
                _tBLDCreateZvanieWho = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateZvaniePrikaz { get; set; }
        public string TBLDCreateZvaniePrikaz
        {
            get { return _tBLDCreateZvaniePrikaz; }
            set
            {
                _tBLDCreateZvaniePrikaz = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreatePoslugChast { get; set; }
        public string TBLDCreatePoslugChast
        {
            get { return _tBLDCreatePoslugChast; }
            set
            {
                _tBLDCreatePoslugChast = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreatePoslugPodr { get; set; }
        public string TBLDCreatePoslugPodr
        {
            get { return _tBLDCreatePoslugPodr; }
            set
            {
                _tBLDCreatePoslugPodr = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreatePoslugDolg { get; set; }
        public string TBLDCreatePoslugDolg
        {
            get { return _tBLDCreatePoslugDolg; }
            set
            {
                _tBLDCreatePoslugDolg = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreatePoslugVus { get; set; }
        public string TBLDCreatePoslugVus
        {
            get { return _tBLDCreatePoslugVus; }
            set
            {
                _tBLDCreatePoslugVus = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreatePoslugWho { get; set; }
        public string TBLDCreatePoslugWho
        {
            get { return _tBLDCreatePoslugWho; }
            set
            {
                _tBLDCreatePoslugWho = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreatePoslugPrikaz { get; set; }
        public string TBLDCreatePoslugPrikaz
        {
            get { return _tBLDCreatePoslugPrikaz; }
            set
            {
                _tBLDCreatePoslugPrikaz = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateFamilyFIO { get; set; }
        public string TBLDCreateFamilyFIO
        {
            get { return _tBLDCreateFamilyFIO; }
            set
            {
                _tBLDCreateFamilyFIO = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreatePACNomber { get; set; }
        public string TBLDCreatePACNomber
        {
            get { return _tBLDCreatePACNomber; }
            set
            {
                _tBLDCreatePACNomber = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateNagradType { get; set; }
        public string TBLDCreateNagradType
        {
            get { return _tBLDCreateNagradType; }
            set
            {
                _tBLDCreateNagradType = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateNagradWho { get; set; }
        public string TBLDCreateNagradWho
        {
            get { return _tBLDCreateNagradWho; }
            set
            {
                _tBLDCreateNagradWho = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateNagradPrikaz { get; set; }
        public string TBLDCreateNagradPrikaz
        {
            get { return _tBLDCreateNagradPrikaz; }
            set
            {
                _tBLDCreateNagradPrikaz = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateNaznachWho { get; set; }
        public string TBLDCreateNaznachWho
        {
            get { return _tBLDCreateNaznachWho; }
            set
            {
                _tBLDCreateNaznachWho = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateNaznachPrikaz { get; set; }
        public string TBLDCreateNaznachPrikaz
        {
            get { return _tBLDCreateNaznachPrikaz; }
            set
            {
                _tBLDCreateNaznachPrikaz = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreatePriemDelhWho { get; set; }
        public string TBLDCreatePriemDelhWho
        {
            get { return _tBLDCreatePriemDelhWho; }
            set
            {
                _tBLDCreatePriemDelhWho = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreatePriemDelhPrikaz { get; set; }
        public string TBLDCreatePriemDelhPrikaz
        {
            get { return _tBLDCreatePriemDelhPrikaz; }
            set
            {
                _tBLDCreatePriemDelhPrikaz = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateVigivanieMesto { get; set; }
        public string TBLDCreateVigivanieMesto
        {
            get { return _tBLDCreateVigivanieMesto; }
            set
            {
                _tBLDCreateVigivanieMesto = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateVigivaniePeriod { get; set; }
        public string TBLDCreateVigivaniePeriod
        {
            get { return _tBLDCreateVigivaniePeriod; }
            set
            {
                _tBLDCreateVigivaniePeriod = value;
                NotifyChanged();
            }
        }
        private string _tBLDCreateVigivanieOsn { get; set; }
        public string TBLDCreateVigivanieOsn
        {
            get { return _tBLDCreateVigivanieOsn; }
            set
            {
                _tBLDCreateVigivanieOsn = value;
                NotifyChanged();
            }
        }
        private string _fIOLD { get; set; }
        public string FIOLD
        {
            get { return _fIOLD; }
            set
            {
                _fIOLD = value;
                NotifyChanged();
            }
        }

        #endregion
        #region DatePicker
        DateTime _datePerevod { get; set; }
        public DateTime DatePerevod
        {
            get
            {
                if (_datePerevod.ToString() == "01.01.0001 0:00:00")
                {
                    _datePerevod = DateTime.Today;
                }
                return _datePerevod;
            }
            set
            {

                _datePerevod = value;
                NotifyChanged();
            }
        }
        DateTime _datePrikazPerevod { get; set; }
        public DateTime DatePrikazPerevod
        {
            get
            {
                if (_datePrikazPerevod.ToString() == "01.01.0001 0:00:00")
                {
                    _datePrikazPerevod = DateTime.Today;
                }
                return _datePrikazPerevod;
            }
            set
            {

                _datePrikazPerevod = value;
                NotifyChanged();
            }
        }
        DateTime _deleteDate { get; set; }
        public DateTime DeleteDate
        {
            get
            {
                if (_deleteDate.ToString() == "01.01.0001 0:00:00")
                {
                    _deleteDate = DateTime.Today;
                }
                return _deleteDate;
            }
            set
            {

                _deleteDate = value;
                NotifyChanged();
            }
        }
        DateTime _deletePricazDate { get; set; }
        public DateTime DeletePricazDate
        {
            get
            {
                if (_deletePricazDate.ToString() == "01.01.0001 0:00:00")
                {
                    _deletePricazDate = DateTime.Today;
                }
                return _deletePricazDate;
            }
            set
            {

                _deletePricazDate = value;
                NotifyChanged();
            }
        }
        DateTime _isklDate { get; set; }
        public DateTime IsklDate
        {
            get
            {
                if (_isklDate.ToString() == "01.01.0001 0:00:00")
                {
                    _isklDate = DateTime.Today;
                }
                return _isklDate;
            }
            set
            {

                _isklDate = value;
                NotifyChanged();
            }
        }
        DateTime _isklPrikazDate { get; set; }
        public DateTime IsklPrikazDate
        {
            get
            {
                if (_isklPrikazDate.ToString() == "01.01.0001 0:00:00")
                {
                    _isklPrikazDate = DateTime.Today;
                }
                return _isklPrikazDate;
            }
            set
            {

                _isklPrikazDate = value;
                NotifyChanged();
            }
        }
        DateTime _delIsklDate { get; set; }
        public DateTime DelIsklDate
        {
            get
            {
                if (_delIsklDate.ToString() == "01.01.0001 0:00:00")
                {
                    _delIsklDate = DateTime.Today;
                }
                return _delIsklDate;
            }
            set
            {

                _delIsklDate = value;
                NotifyChanged();
            }
        }
        DateTime _delIsklPrikazDate { get; set; }
        public DateTime DelIsklPrikazDate
        {
            get
            {
                if (_delIsklPrikazDate.ToString() == "01.01.0001 0:00:00")
                {
                    _delIsklPrikazDate = DateTime.Today;
                }
                return _delIsklPrikazDate;
            }
            set
            {

                _delIsklPrikazDate = value;
                NotifyChanged();
            }
        }
        DateTime _dateBrdLD { get; set; }
        public DateTime DateBrdLD
        {
            get
            {
                if (_dateBrdLD.ToString() == "01.01.0001 0:00:00")
                {
                    _dateBrdLD = DateTime.Today;
                }
                return _dateBrdLD;
            }
            set
            {

                _dateBrdLD = value;
                NotifyChanged();
            }
        }
        DateTime _passportDate { get; set; }
        public DateTime PassportDate
        {
            get
            {
                if (_passportDate.ToString() == "01.01.0001 0:00:00")
                {
                    _passportDate = DateTime.Today;
                }
                return _passportDate;
            }
            set
            {

                _passportDate = value;
                NotifyChanged();
            }
        }
        DateTime _udostDate { get; set; }
        public DateTime UdostDate
        {
            get
            {
                if (_udostDate.ToString() == "01.01.0001 0:00:00")
                {
                    _udostDate = DateTime.Today;
                }
                return _udostDate;
            }
            set
            {

                _udostDate = value;
                NotifyChanged();
            }
        }
        DateTime _zaklKontraktDate { get; set; }
        public DateTime ZaklKontraktDate
        {
            get
            {
                if (_zaklKontraktDate.ToString() == "01.01.0001 0:00:00")
                {
                    _zaklKontraktDate = DateTime.Today;
                }
                return _zaklKontraktDate;
            }
            set
            {

                _zaklKontraktDate = value;
                NotifyChanged();
            }
        }
        DateTime _okonKontraktDate { get; set; }
        public DateTime OkonKontraktDate
        {
            get
            {
                if (_okonKontraktDate.ToString() == "01.01.0001 0:00:00")
                {
                    _okonKontraktDate = DateTime.Today;
                }
                return _okonKontraktDate;
            }
            set
            {

                _okonKontraktDate = value;
                NotifyChanged();
            }
        }
        DateTime _kontraktPrikazDate { get; set; }
        public DateTime KontraktPrikazDate
        {
            get
            {
                if (_kontraktPrikazDate.ToString() == "01.01.0001 0:00:00")
                {
                    _kontraktPrikazDate = DateTime.Today;
                }
                return _kontraktPrikazDate;
            }
            set
            {

                _kontraktPrikazDate = value;
                NotifyChanged();
            }
        }
        DateTime _zvaniePrikazDate { get; set; }
        public DateTime ZvaniePrikazDate
        {
            get
            {
                if (_zvaniePrikazDate.ToString() == "01.01.0001 0:00:00")
                {
                    _zvaniePrikazDate = DateTime.Today;
                }
                return _zvaniePrikazDate;
            }
            set
            {

                _zvaniePrikazDate = value;
                NotifyChanged();
            }
        }
        DateTime _poslugnoiPrikazDate { get; set; }
        public DateTime PoslugnoiPrikazDate
        {
            get
            {
                if (_poslugnoiPrikazDate.ToString() == "01.01.0001 0:00:00")
                {
                    _poslugnoiPrikazDate = DateTime.Today;
                }
                return _poslugnoiPrikazDate;
            }
            set
            {

                _poslugnoiPrikazDate = value;
                NotifyChanged();
            }
        }
        DateTime _familyBrdDate { get; set; }
        public DateTime FamilyBrdDate
        {
            get
            {
                if (_familyBrdDate.ToString() == "01.01.0001 0:00:00")
                {
                    _familyBrdDate = DateTime.Today;
                }
                return _familyBrdDate;
            }
            set
            {

                _familyBrdDate = value;
                NotifyChanged();
            }
        }
        DateTime _nagradDate { get; set; }
        public DateTime NagradDate
        {
            get
            {
                if (_nagradDate.ToString() == "01.01.0001 0:00:00")
                {
                    _nagradDate = DateTime.Today;
                }
                return _nagradDate;
            }
            set
            {

                _nagradDate = value;
                NotifyChanged();
            }
        }
        DateTime _nagradPrikazDate { get; set; }
        public DateTime NagradPrikazDate
        {
            get
            {
                if (_nagradPrikazDate.ToString() == "01.01.0001 0:00:00")
                {
                    _nagradPrikazDate = DateTime.Today;
                }
                return _nagradPrikazDate;
            }
            set
            {

                _nagradPrikazDate = value;
                NotifyChanged();
            }
        }
        DateTime _naznachDate { get; set; }
        public DateTime NaznachDate
        {
            get
            {
                if (_naznachDate.ToString() == "01.01.0001 0:00:00")
                {
                    _naznachDate = DateTime.Today;
                }
                return _naznachDate;
            }
            set
            {

                _naznachDate = value;
                NotifyChanged();
            }
        }
        DateTime _priemDelDate { get; set; }
        public DateTime PriemDelDate
        {
            get
            {
                if (_priemDelDate.ToString() == "01.01.0001 0:00:00")
                {
                    _priemDelDate = DateTime.Today;
                }
                return _priemDelDate;
            }
            set
            {

                _priemDelDate = value;
                NotifyChanged();
            }
        }
        DateTime _date_date2_otp { get; set; }
        public DateTime Date_date2_otp
        {
            get
            {
                if (_date_date2_otp.ToString() == "01.01.0001 0:00:00")
                {
                    _date_date2_otp = DateTime.Today;
                }
                return _date_date2_otp;
            }
            set
            {

                _date_date2_otp = value;
                NotifyChanged();
            }
        }
        DateTime _date_date1_otp { get; set; }
        public DateTime Date_date1_otp
        {
            get
            {
                if (_date_date1_otp.ToString() == "01.01.0001 0:00:00")
                {
                    _date_date1_otp = DateTime.Today;
                }
                return _date_date1_otp;
            }
            set
            {

                _date_date1_otp = value;
                NotifyChanged();
            }
        }
        DateTime _date_ub_kom { get; set; }
        public DateTime Date_ub_kom
        {
            get
            {
                if (_date_ub_kom.ToString() == "01.01.0001 0:00:00")
                {
                    _date_ub_kom = DateTime.Today;
                }
                return _date_ub_kom;
            }
            set
            {

                _date_ub_kom = value;
                NotifyChanged();
            }
        }
        DateTime _date_gos_ub { get; set; }
        public DateTime Date_gos_ub
        {
            get
            {
                if (_date_gos_ub.ToString() == "01.01.0001 0:00:00")
                {
                    _date_gos_ub = DateTime.Today;
                }
                return _date_gos_ub;
            }
            set
            {

                _date_gos_ub = value;
                NotifyChanged();
            }
        }
        DateTime _date_med_ub { get; set; }
        public DateTime Date_med_ub
        {
            get
            {
                if (_date_med_ub.ToString() == "01.01.0001 0:00:00")
                {
                    _date_med_ub = DateTime.Today;
                }
                return _date_med_ub;
            }
            set
            {

                _date_med_ub = value;
                NotifyChanged();
            }
        }
        DateTime _date_are_ub { get; set; }
        public DateTime Date_are_ub
        {
            get
            {
                if (_date_are_ub.ToString() == "01.01.0001 0:00:00")
                {
                    _date_are_ub = DateTime.Today;
                }
                return _date_are_ub;
            }
            set
            {

                _date_are_ub = value;
                NotifyChanged();
            }
        }
        DateTime _soch_ub_date { get; set; }
        public DateTime Soch_ub_date
        {
            get
            {
                if (_soch_ub_date.ToString() == "01.01.0001 0:00:00")
                {
                    _soch_ub_date = DateTime.Today;
                }
                return _soch_ub_date;
            }
            set
            {

                _soch_ub_date = value;
                NotifyChanged();
            }
        }
        DateTime _date_ub_dr { get; set; }
        public DateTime Date_ub_dr
        {
            get
            {
                if (_date_ub_dr.ToString() == "01.01.0001 0:00:00")
                {
                    _date_ub_dr = DateTime.Today;
                }
                return _date_ub_dr;
            }
            set
            {

                _date_ub_dr = value;
                NotifyChanged();
            }
        }




        #endregion
        #region ListBox
        public OpenLoadZaShtat IItems { get; set; } = new OpenLoadZaShtat();
        public OpenLoad IList_vibor2 { get; set; } = new OpenLoad();
        public List_new LB_filter_perest { get; set; } = new List_new();
        public List_new LB_filter_perev { get; set; } = new List_new();
        public List_new LB_filter_del { get; set; } = new List_new();
        public List_new LB_filter_iskl { get; set; } = new List_new();
        #endregion
        #region DataGrid's
        public OpenLoad SelectedDataGrid_Shtat { get; set; }
        public OpenLoadZaShtat SelectedDataGrid_ZaShtat { get; set; }
        public OpenLoadDel SelectedDataGrid_Delete { get; set; }
        public OpenLoadPerevod SelectedDataGrid_Perev { get; set; }
        public OpenLoadIskl SelectedDataGrid_Iskl { get; set; }
        public Naryad SelectedDataGrid_Naryad { get; set; }
        public Otbor_na_kontr SelectedDataGrid_OtobKontr { get; set; }
        public Otbor_na_kontr SelectedDataGrid_PeredKontr { get; set; }
        public Otbor_na_kontr SelectedDataGrid_SostKontr { get; set; }
        public Otbor_na_kontr SelectedDataGrid_OtkazKontr { get; set; }
        #endregion
        #region ComboBox's
        private static List<string> _list_Type_vibor { get; set; }
        public List<string> list_Type_vibor
        {
            get { return _list_Type_vibor; }
            set { _list_Type_vibor = value; NotifyChanged(); }
        }
        private static List<string> _otr_grid { get; set; }
        public List<string> Otr_grids
        {
            get { return _otr_grid; }
            set { _otr_grid = value; NotifyChanged(); }
        }
        private static List<string> _family_Sl_CB { get; set; }
        public List<string> Family_Sl_CB
        {
            get { return _family_Sl_CB; }
            set { _family_Sl_CB = value; NotifyChanged(); }
        }
        private static List<string> _zvan_Sl_CB { get; set; }
        public List<string> Zvan_Sl_CB
        {
            get { return _zvan_Sl_CB; }
            set { _zvan_Sl_CB = value; NotifyChanged(); }
        }
        private static List<string> _kontr_Sl_CB { get; set; }
        public List<string> Kontr_Sl_CB
        {
            get { return _kontr_Sl_CB; }
            set { _kontr_Sl_CB = value; NotifyChanged(); }
        }
        private static List<string> _pol_CB { get; set; }
        public List<string> Pol_CB
        {
            get { return _pol_CB; }
            set { _pol_CB = value; NotifyChanged(); }
        }
        private static List<string> _cB_LD { get; set; }
        public List<string> CB_LD
        {
            get { return _cB_LD; }
            set { _cB_LD = value; NotifyChanged(); }
        }
        private static List<string> _cB_ub_Gos { get; set; }
        public List<string> CB_ub_Gos
        {
            get { return _cB_ub_Gos; }
            set { _cB_ub_Gos = value; NotifyChanged(); }
        }
        private static List<string> _type_ub_kom { get; set; }
        public List<string> Type_ub_kom
        {
            get { return _type_ub_kom; }
            set { _type_ub_kom = value; NotifyChanged(); }
        }
        private static List<string> _types_otp { get; set; }
        public List<string> Types_otp
        {
            get { return _types_otp; }
            set { _types_otp = value; NotifyChanged(); }
        }











        string _cBSelectedViborka2 { get; set; }
        public string CBSelectedViborka2
        {
            get { return _cBSelectedViborka2; }
            set
            {
                _cBSelectedViborka2 = value;
                //MessageBox.Show(_cBSelectedViborka2);
                NotifyChanged();

            }
        }
        string _cBSelectedOtrGrids { get; set; }
        public string CBSelectedOtrGrids
        {
            get { return _cBSelectedOtrGrids; }
            set
            {
                _cBSelectedOtrGrids = value;
                NotifyChanged();

            }
        }
        string _selectedFamilySlCB { get; set; }
        public string SelectedFamilySlCB
        {
            get { return _selectedFamilySlCB; }
            set
            {
                _selectedFamilySlCB = value;
                NotifyChanged();

            }
        }
        string _selectedZvanSlCB { get; set; }
        public string SelectedZvanSlCB
        {
            get { return _selectedZvanSlCB; }
            set
            {
                _selectedZvanSlCB = value;
                NotifyChanged();

            }
        }
        string _selectedKontrSlCB { get; set; }
        public string SelectedKontrSlCB
        {
            get { return _selectedKontrSlCB; }
            set
            {
                _selectedKontrSlCB = value;
                NotifyChanged();

            }
        }
        string _selectedPolCB { get; set; }
        public string SelectedPolCB
        {
            get { return _selectedPolCB; }
            set
            {
                _selectedPolCB = value;
                NotifyChanged();

            }
        }
        string _selectedCBLD { get; set; }
        public string SelectedCBLD
        {
            get { return _selectedCBLD; }
            set
            {
                _selectedCBLD = value;
                NotifyChanged();

            }
        }
        string _selectedCBUbGos { get; set; }
        public string SelectedCBUbGos
        {
            get { return _selectedCBUbGos; }
            set
            {
                _selectedCBUbGos = value;
                NotifyChanged();

            }
        }
        string _selectedTypeUbKom { get; set; }
        public string SelectedTypeUbKom
        {
            get { return _selectedTypeUbKom; }
            set
            {
                _selectedTypeUbKom = value;
                NotifyChanged();

            }
        }
        string _selectedTypesOtp { get; set; }
        public string SelectedTypesOtp
        {
            get { return _selectedTypesOtp; }
            set
            {
                _selectedTypesOtp = value;
                NotifyChanged();

            }
        }






        #endregion
        #region Image
        BitmapImage _photo { get; set; }
        public BitmapImage Photo
        {
            get { return _photo; }
            set
            {
                _photo = value;
                NotifyChanged();
            }
        }
        BitmapImage _autobiographi { get; set; }
        BitmapImage _diplom { get; set; }
        BitmapImage _inn { get; set; }
        BitmapImage _attest { get; set; }
        BitmapImage _snils { get; set; }
        #endregion
    }


}
