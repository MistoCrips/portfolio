package com.example.user.proba;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import android.content.ContentUris;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.PowerManager;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;
import java.util.Random;
import android.app.Notification;
import android.app.PendingIntent;




public class MusicService extends Service implements
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener {
    //private final static int MAX_VOLUME = 100;
    //final float minVolume = 50;
    //final float volume = (float) (1 - (Math.log(MAX_VOLUME - minVolume) / Math.log(MAX_VOLUME)));

    final String LOG_TAG = "myLogs";
    private final IBinder musicBind = new MusicBinder();
    //media player
    private static MediaPlayer player;
    //song list
    private static ArrayList<Track> songs;
    private static Track song;
    //current position
    private static int songPosn, dr, TotalTime;
    public static boolean plaing, loopt, nexting, shuffle;
    private String songTitle = "";
    private Random rand;
    static Uri trackUri;


    public void onCreate(){
        //create the service
        super.onCreate();
        //initialize position
        songPosn=0;
        //Uri trackUri=null;
        //create player
        player = new MediaPlayer();
        //player.setVolume(volume,volume);
        initMusicPlayer();
        plaing = false;
        rand=new Random();
        shuffle=false;
        loopt=false;
        nexting =false;
    }
    public void setShuffle(boolean index){
        if(index == false) shuffle=false;
        else shuffle=true;
    }
    public void initMusicPlayer(){
        try {
            player.setWakeMode(getApplicationContext(),
                    PowerManager.PARTIAL_WAKE_LOCK);
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setOnPreparedListener(this);
            player.setOnCompletionListener(this);
            //player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            //    public void onCompletion(MediaPlayer mp) {
            //        mp.release();
            //    };
            //});
            player.setOnErrorListener(this);
        } catch(IllegalArgumentException e){
            Log.d(LOG_TAG, "Ошибка в initMusicPlayer песни2: " + e.getMessage());
        }
        catch (IllegalStateException e){
            Log.d(LOG_TAG, "Ошибка в initMusicPlayer песни3: " + e.getMessage());
        }

    }
    public void setList(ArrayList<Track> theSongs){
        songs=theSongs;
    }
    public class MusicBinder extends Binder {
        MusicService getService() {
            return MusicService.this;
        }
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }
    @Override
    public boolean onUnbind(Intent intent){
        try {
            player.stop();
            player.release();
        }catch (Exception e){
            Log.d(LOG_TAG, "Ошибка в onUnbind");
        }
        return false;
    }
    public void pauseSong() {
        try {
            player.pause();
            plaing = false;
        }
        catch (Exception e){
            Log.d(LOG_TAG, "Ошибка в pauseSong");
        }
    }
    @Override
    public void onCompletion(MediaPlayer mp) {
        try {
            playNext();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(LOG_TAG, "Ошибка в onCompletion");
        }
    }
    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        try {
            //player = new MediaPlayer();
            //player.setDataSource(getApplicationContext(), trackUri);
            //player.prepare();
            //player.setOnPreparedListener(new MediaPlayer.OnPreparedListener(){
            //    @Override
            //    public void onPrepared(MediaPlayer mp){
            //        mp.start();
            //    }
            //});
            mp.reset();
        }
        catch (Exception e){
            Log.d(LOG_TAG, "Ошибка в onError");
        }
        return false;
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onPrepared(MediaPlayer mp) {
        //start playback
        try {
            mp.start();
            //Intent notIntent = new Intent(this, MainActivity.class);
            //notIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //PendingIntent pendInt = PendingIntent.getActivity(this, 0,
            //        notIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            //Notification.Builder builder = new Notification.Builder(this);
            //builder.setContentIntent(pendInt)
            //        .setSmallIcon(R.drawable.ic_launcher_background)
            //        .setTicker(songTitle)
            //        .setOngoing(true)
            //        .setContentTitle("Playing")
            //        .setContentText(songTitle);
            //Notification not = builder.build();
            //startForeground(1, not);
        }
        catch (Exception e){
            Log.d(LOG_TAG, "Ошибка в onPrepared");
        }
    }
    public void loop(boolean Index){
        loopt = Index;
        if(loopt==true) player.setLooping(true);
        else player.setLooping(false);
    }
    public void setSong(int songIndex){
        songPosn=songIndex;
    }
    public String SetTitle(){
        return songTitle;
    }
    public boolean GetPlaing(){
        return plaing;
    }
    public int setTime(){
        return player.getDuration();
    }


    public void playSong() throws IOException {
        try {
            player.reset();
            Track SongsPlay = songs.get(songPosn);
            Log.d(LOG_TAG, "Тут эта песня: "+songPosn + " Название: "+ SongsPlay.getTitle());
            long currSong = SongsPlay.getID();
            trackUri = ContentUris.withAppendedId(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, currSong);
            //Log.d(LOG_TAG, "Тут эта песня1: "+songPosn);
            try{
                player.setDataSource(getApplicationContext(), trackUri);
                player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                player.setOnPreparedListener(this);
                player.prepare();
                //player.prepareAsync();
                songTitle=SongsPlay.getTitle();
            }
            catch(IOException e){
                Toast.makeText(this, "mp3 не найден", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
                Log.d(LOG_TAG, "Ошибка в воспроизведении песни1: "+ trackUri+ ". Ошибка: " + e.getMessage());
                player.reset();
                player = new MediaPlayer();
                player.setDataSource(song.getURL());
                //player.setDataSource(getApplicationContext(), trackUri);
                player.prepare();
                player.setOnPreparedListener(new MediaPlayer.OnPreparedListener(){
                    @Override
                    public void onPrepared(MediaPlayer mp){
                        mp.start();
                    }
                });
            }
            catch(IllegalArgumentException e){
                Log.d(LOG_TAG, "Ошибка в воспроизведении песни2: "+ trackUri+ ". Ошибка: "  + e.getMessage());
                player.reset();
                player = new MediaPlayer();
                player.setDataSource(song.getURL());
                //player.setDataSource(getApplicationContext(), trackUri);
                player.prepare();
                player.setOnPreparedListener(new MediaPlayer.OnPreparedListener(){
                    @Override
                    public void onPrepared(MediaPlayer mp){
                        mp.start();
                    }
                });
            }
            catch (IllegalStateException e){
                Log.d(LOG_TAG, "Ошибка в воспроизведении песни3: "+ trackUri+ ". Ошибка: "  + e.getMessage());
                player.reset();
                player = new MediaPlayer();
                //player.setDataSource(getApplicationContext(), trackUri);
                player.setDataSource(song.getURL());
                player.prepare();
                player.setOnPreparedListener(new MediaPlayer.OnPreparedListener(){
                    @Override
                    public void onPrepared(MediaPlayer mp){
                        mp.start();
                    }
                });
            }
            //go();
            dr = player.getDuration();
            player.setOnCompletionListener(this);
            //nexting = true;
        }catch (Exception e){
            Log.d(LOG_TAG, "Ошибка в воспроизведении песни: " + e.getMessage());
            player.reset();
            player = new MediaPlayer();
            playSong();
        }
     }

    public int getPosn(){
        //if(player.getCurrentPosition() != 0)
        //    nexting = true;
        return player.getCurrentPosition();
    }
    public int getDur(){
        return dr; //player.getDuration();
    }
    public boolean isPng(){
        return player.isPlaying();
    }
    public void pausePlayer(){
        player.pause();
    }
    public void seek(int posn){
        player.seekTo(posn);
    }
    public void go(){
        player.start();
        plaing = true;
    }
    public void playPrev() throws IOException {

        if(songPosn-1<0) {
            songPosn = songs.size() - 1;
            player.reset();
            playSong();
        }else
            {
                player.reset();
                songPosn = songPosn - 1;
                playSong();
            }

    }
    public void playNext() throws IOException {
            if (shuffle) {
                player.reset();
                int newSong = songPosn;
                while (newSong == songPosn) {
                    newSong = rand.nextInt(songs.size());
                }
                songPosn = newSong;

            } else {
                player.reset();
                if(nexting){
                    if (songPosn + 1 >= songs.size()) songPosn = 0;
                    else songPosn = songPosn + 1;
                    nexting = false;
                }
            }
            playSong();
    }
    @Override
    public void onDestroy() {
        player.stop();
        //stopForeground(true);
    }
    public void stop(){
        player.stop();
    }
    public int SongPosn(){
        return songPosn;
    }
    public void Nexting_ ( boolean B){
        nexting = B;
    }

}
