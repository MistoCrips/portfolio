using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using �������.ViewModal;

namespace �������
{
    /// <summary>
    /// ������ �������������� ��� MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private System.Windows.Forms.NotifyIcon m_notifyIcon;
        VM v = new VM();
        bool serv, client;
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = v;
            m_notifyIcon = new System.Windows.Forms.NotifyIcon();
            m_notifyIcon.BalloonTipTitle = "�������";
            m_notifyIcon.BalloonTipText = "��� �������� �������";
            m_notifyIcon.Text = "����������� ���";
            m_notifyIcon.Icon = new System.Drawing.Icon(Environment.CurrentDirectory + "\\Recources\\16.ico");
            m_notifyIcon.Click += new EventHandler(m_notifyIcon_Click);
        }

        private void MinimizedWindow_MouseDown(object sender, MouseButtonEventArgs e)
        {        
            this.WindowState = WindowState.Minimized; //��������
        }

        private WindowState m_storedWindowState = WindowState.Normal;
        void OnStateChanged(object sender, EventArgs args)
        {
            if (WindowState == WindowState.Minimized)
            {
                Hide();
                v.Minimazed(true);
            }
            else
            {
                m_storedWindowState = WindowState;
                v.Minimazed(false);
                
            }
        }
        void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            CheckTrayIcon();
        }
        void m_notifyIcon_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = m_storedWindowState;
            this.Visibility = Visibility.Visible;
            this.ShowInTaskbar = true;
        }
        void CheckTrayIcon()
        {
            ShowTrayIcon(!IsVisible);
        }
        void ShowTrayIcon(bool show)
        {
            if (m_notifyIcon != null)
                m_notifyIcon.Visible = show;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            v.CloseClient();
            v.Stop();
            Environment.Exit(0);
        }

        private void Menu_header_1_Click(object sender, RoutedEventArgs e)
        {
            MenuItem I = (MenuItem)sender;
            if(I.Header.ToString() == "�������� ������" && !client)
            {
                I.Header = "��������� ������";
                serv = true;
            } else
            if (I.Header.ToString() == "��������� ������")
            {
                I.Header = "�������� ������";
                serv = false;
            } else
            if (I.Header.ToString() == "������������" && !serv)
            {
                I.Header = "�����������";
                client = true;
            } else
            if (I.Header.ToString() == "�����������")
            {
                I.Header = "������������";
                client = false;
            }
        }
    }
}
