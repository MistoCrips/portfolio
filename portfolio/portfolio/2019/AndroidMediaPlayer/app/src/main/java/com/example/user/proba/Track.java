package com.example.user.proba;

import android.view.View;
import android.view.View.OnClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Track {
    private long id_ = 0;
    private long counter = 0;
    private String artist = "."; //Исполнитель
    private String artist2 = "."; //Исполнитель2
    private String artist3 = "."; //Исполнитель3
    private String artist4 = "."; //Исполнитель4
    private String artist5 = "."; //Исполнитель5
    private String artist6 = "."; //Исполнитель6
    private String name = ".";   //Название
    private String janr = ".";   //Жанр
    private String podjanr = ".";//Поджанр
    private String urls = ".";   //Адрес местонахождения трека
    private String metka_1 = ".";//Метка 1: Метка Язык вакала
    private String metka_2 = ".";//Метка 2: Метка Вызывает настроение
    private String metka_3 = ".";//Метка 3:
    private String metka_4 = ".";//Метка 4:
    private String metka_5 = ".";//Метка 5:
    private String metka_6 = ".";//Метка 6:
    private String metka_7 = ".";//Метка 7:
    private String metka_8 = ".";//Метка 8:
    private Date date_;//Дата добавления

    public  long getID(){ return id_; }
    public  long getCounter(){ return counter; }
    public  String getArtist(){return artist; }
    public  String getArtist2(){return artist2; }
    public  String getArtist3(){return artist3; }
    public  String getArtist4(){return artist4; }
    public  String getArtist5(){return artist5; }
    public  String getArtist6(){return artist6; }
    public  String getTitle(){ return name; }
    public  String getJanr(){return janr; }
    public  String getPodJanr(){return podjanr; }
    public  String getURL(){return urls; }
    public  String getMetka1(){return metka_1; }
    public  String getMetka2(){return metka_2; }
    public  String getMetka3(){return metka_3; }
    public  String getMetka4(){return metka_4; }
    public  String getMetka5(){return metka_5; }
    public  String getMetka6(){return metka_6; }
    public  String getMetka7(){return metka_7; }
    public  String getMetka8(){return metka_8; }
    public Date getDate(){
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String string_date = sdf.format(date_);
        try {
            date = sdf.parse(string_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public Track(long Id,String NameTrack, String ArtistTrack){
        id_ = Id;
        artist = ArtistTrack;
        name = NameTrack;


    }
    public  long setID(long values){
        id_ = values;
        return id_; }
    public  long setCounter(long values){
        counter = values;
        return counter; }
    public  String setArtist(String values){
        artist = values;
        return artist; }
    public  String setArtist2(String values){
        artist2 = values;
        return artist2; }
    public  String setArtist3(String values){
        artist3 = values;
        return artist3; }
    public  String setArtist4(String values){
        artist4 = values;
        return artist4; }
    public  String setArtist5(String values){
        artist5 = values;
        return artist5; }
    public  String setArtist6(String values){
        artist6 = values;
        return artist6; }
    public  String setTitle(String values){
        name = values;
        return name; }
    public  String setJanr(String values){
        janr = values;
        return janr; }
    public  String setPodJanr(String values){
        podjanr = values;
        return podjanr; }
    public  String setURL(String values){
        urls = values;
        return urls; }
    public  String setMetka1(String values){
        metka_1 = values;
        return metka_1; }
    public  String setMetka2(String values){
        metka_2 = values;
        return metka_2; }
    public  String setMetka3(String values){
        metka_3 = values;
        return metka_3; }
    public  String setMetka4(String values){
        metka_4 = values;
        return metka_4; }
    public  String setMetka5(String values){
        metka_5 = values;
        return metka_5; }
    public  String setMetka6(String values){
        metka_6 = values;
        return metka_6; }
    public  String setMetka7(String values){
        metka_7 = values;
        return metka_7; }
    public  String setMetka8(String values){
        metka_8 = values;
        return metka_8; }
    public Date setDate(Date values)
    {
        date_ = values;
        return date_; }

    public int compareTo(Track o) {
        return getDate().compareTo(o.getDate());
    }

}
