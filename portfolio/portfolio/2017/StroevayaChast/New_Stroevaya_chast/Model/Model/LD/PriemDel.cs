﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model.LD
{
    public class PriemDel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        string who_ = ".";
        string pr_ = ".";
        string date1_ = ".";
        public PriemDel() { }
        public PriemDel(XElement element)
        {
            if (element.Attribute("who_pricaz").Value != null) who_pricaz = element.Attribute("who_pricaz").Value;
            if (element.Attribute("pricaz").Value != null) pricaz = element.Attribute("pricaz").Value;
            if (element.Attribute("date_pricaz").Value != null) date_pricaz = element.Attribute("date_pricaz").Value;
        }
        public string who_pricaz
        {
            get { return who_; }
            set
            {
                if (who_ == value)
                    return;
                who_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Who"));
            }
        }
        public string pricaz
        {
            get { return pr_; }
            set
            {
                if (pr_ == value)
                    return;
                pr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Pr"));
            }
        }
        public string date_pricaz
        {
            get { return date1_; }
            set
            {
                if (date1_ == value)
                    return;
                date1_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date1"));
            }
        }
        public void ToXml(string LOGIN)
        {
            string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + LOGIN + ".xml";
            XDocument doc;
            if (File.Exists(fileName) == false)
            {
                doc = new XDocument(
                new XElement("base",
                    new XElement("track_pr_del",
                        new XAttribute("who_pricaz", who_pricaz),
                        new XAttribute("pricaz", pricaz),
                        new XAttribute("date_pricaz", date_pricaz))));
                doc.Save(fileName);
            }
            else
            {
                doc = XDocument.Load(fileName);
                doc.Descendants().Where(e => e.Name == "track_pr_del").Remove();
                XElement track = new XElement("track_pr_del");
                track.Add(new XAttribute("who_pricaz", who_pricaz));
                track.Add(new XAttribute("pricaz", pricaz));
                track.Add(new XAttribute("date_pricaz", date_pricaz));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }
    }
}
