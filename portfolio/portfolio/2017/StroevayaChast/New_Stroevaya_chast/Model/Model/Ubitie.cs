﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model
{
    public class Ub : INotifyPropertyChanged
    {
        ObservableCollection<Ub> list_ub = new ObservableCollection<Ub>();
        public event PropertyChangedEventHandler PropertyChanged;
        string dolg_ = ".";
        string podr_ = ".";

        string lnumber_ = ".";
        string fio_ = ".";
        string zvanie_ = ".";
        string types_1 = ".";

        string types_2 = ".";
        string mesto_ = ".";
        string cel_ = ".";
        string date1_ = ".";
        string date2_ = ".";
        string date3_ = ".";

        string osn_ = ".";
        string vrio_ = ".";

        string vpd_st1 = ".";
        string vpd_st2 = ".";
        string vpd_st3 = ".";
        string vpd_st4 = ".";
        string vpd_st5 = ".";
        string vpd_st6 = ".";
        string vpd_st7 = ".";
        public string Osn
        {
            get { return osn_; }
            set
            {
                if (osn_ == value)
                    return;
                osn_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Osn"));
            }
        }
        public string Vrio
        {
            get { return vrio_; }
            set
            {
                if (vrio_ == value)
                    return;
                vrio_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Vrio"));
            }
        }
        public string Dolg
        {
            get { return dolg_; }
            set
            {
                if (dolg_ == value)
                    return;
                dolg_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Dolg"));
            }
        }
        public string Podr
        {
            get { return podr_; }
            set
            {
                if (podr_ == value)
                    return;
                podr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Podr"));
            }
        }
        public string Lnumber
        {
            get { return lnumber_; }
            set
            {
                if (lnumber_ == value)
                    return;
                lnumber_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Lnumber"));
            }
        }
        public string FIO
        {
            get { return fio_; }
            set
            {
                if (fio_ == value)
                    return;
                fio_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FIO"));
            }
        }
        public string Zvanie
        {
            get { return zvanie_; }
            set
            {
                if (zvanie_ == value)
                    return;
                zvanie_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Zvanie"));
            }
        }
        public string Types
        {
            get { return types_1; }
            set
            {
                if (types_1 == value)
                    return;
                types_1 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public string Cel
        {
            get { return cel_; }
            set
            {
                if (cel_ == value)
                    return;
                cel_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Cel"));
            }
        }
        public string Mesto
        {
            get { return mesto_; }
            set
            {
                if (mesto_ == value)
                    return;
                mesto_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Mesto"));
            }
        }
        public string Date_ub
        {
            get { return date1_; }
            set
            {
                if (date1_ == value)
                    return;
                date1_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date_ub"));
            }
        }
        public string Date_preb
        {
            get { return date2_; }
            set
            {
                if (date2_ == value)
                    return;
                date2_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date_preb"));
            }
        }
        public string Date3
        {
            get { return date3_; }
            set
            {
                if (date3_ == value)
                    return;
                date3_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date3"));
            }
        }
        public string Types_ub
        {
            get { return types_2; }
            set
            {
                if (types_2 == value)
                    return;
                types_2 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types_ub"));
            }
        }
        public string VPD_ST1
        {
            get { return vpd_st1; }
            set
            {
                if (vpd_st1 == value)
                    return;
                vpd_st1 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("VPD_ST1"));
            }
        }
        public string VPD_ST2
        {
            get { return vpd_st2; }
            set
            {
                if (vpd_st2 == value)
                    return;
                vpd_st2 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("VPD_ST2"));
            }
        }
        public string VPD_ST3
        {
            get { return vpd_st3; }
            set
            {
                if (vpd_st3 == value)
                    return;
                vpd_st3 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("VPD_ST3"));
            }
        }
        public string VPD_ST4
        {
            get { return vpd_st4; }
            set
            {
                if (vpd_st4 == value)
                    return;
                vpd_st4 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("VPD_ST4"));
            }
        }
        public string VPD_ST5
        {
            get { return vpd_st5; }
            set
            {
                if (vpd_st5 == value)
                    return;
                vpd_st5 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("VPD_ST5"));
            }
        }
        public string VPD_ST6
        {
            get { return vpd_st6; }
            set
            {
                if (vpd_st6 == value)
                    return;
                vpd_st6 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("VPD_ST6"));
            }
        }
        public string VPD_ST7
        {
            get { return vpd_st7; }
            set
            {
                if (vpd_st7 == value)
                    return;
                vpd_st7 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("VPD_ST7"));
            }
        }
        public string OpenLoad1()
        {
            string Message;
            ObservableCollection<Ub> List = new ObservableCollection<Ub>();
            List = sbor();
            Message = Mass_shtat(List);
            return Message;

        }
        public Ub() { }
        public Ub(XElement element)
        {
            cel_ = element.Attribute("cel").Value;
            date1_ = element.Attribute("date1").Value;
            dolg_ = element.Attribute("dolg").Value;
            fio_ = element.Attribute("fio").Value;
            lnumber_ = element.Attribute("ln").Value;
            mesto_ = element.Attribute("ms").Value;

            osn_ = element.Attribute("osn").Value;
            podr_ = element.Attribute("podr").Value;
            types_1 = element.Attribute("type1").Value;
            types_2 = element.Attribute("type2").Value;
            vrio_ = element.Attribute("vrio").Value;

            zvanie_ = element.Attribute("zv").Value;
            vpd_st1 = element.Attribute("vp1").Value;
            vpd_st2 = element.Attribute("vp2").Value;
            vpd_st3 = element.Attribute("vp3").Value;
            vpd_st4 = element.Attribute("vp4").Value;

            vpd_st5 = element.Attribute("vp5").Value;
            vpd_st6 = element.Attribute("vp6").Value;
            vpd_st7 = element.Attribute("vp7").Value;

        }
        public ObservableCollection<Ub> sbor()
        {
            string filename = Environment.CurrentDirectory + "\\Recources\\Active\\ub.xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                list_ub.Add(new Ub(el));
            }

            return list_ub;
        }
        public string Mass_shtat(ObservableCollection<Ub> List)
        {
            string s = "";
            foreach (Ub P in List)
            {
                s += P.Cel + ">" + P.Date3 + ">" + P.Date_preb + ">" + P.Date_ub + ">" + P.Dolg + ">" + P.FIO + ">" + P.Lnumber + ">" + P.Mesto + ">" + P.Osn + ">" + P.Podr + ">" + P.Types + ">" + P.Types_ub + ">" + P.VPD_ST1 + ">" + P.VPD_ST2 + ">" + P.VPD_ST3 + ">" + P.VPD_ST4 + ">" + P.VPD_ST5 + ">" + P.VPD_ST6 + ">" + P.VPD_ST7 + ">" + P.Vrio + ">" + P.Zvanie + ">";
            }
            return s;
        }
    }
}
