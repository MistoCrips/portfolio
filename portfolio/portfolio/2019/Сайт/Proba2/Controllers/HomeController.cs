﻿using Proba2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proba2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Меня зовут Леонид.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Текст для страницы Контакты";

            return View();
        }
        public ActionResult FirstWork()
        {
            return View();
        }
        public ActionResult Success()
        {
            return View();
        }
        public ActionResult Error()
        {
            return View();
        }
        public ActionResult CollegePage()
        {
            return View();
        }
        public ActionResult ProjectPage()
        {
            ViewBag.Message = "Здесь Вы можете ознакомиться с проектами, в которых я принемал участие:";
            return View();
        }

        [HttpPost]
        public ActionResult Index(EmailModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    new EmailController().SendEmail(model);

                    return RedirectToAction("Success");
                }
                catch (Exception)
                {
                    return RedirectToAction("Error");
                }
            }
            return View(model);
        }
    }
}