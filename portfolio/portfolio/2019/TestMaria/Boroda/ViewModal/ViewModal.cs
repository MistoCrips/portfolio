﻿using Boroda.Modal;
using Boroda.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Boroda.ViewModal
{
    public class ViewModals : INotifyPropertyChanged
    {
        int types = 1; //1 - xml, 2 - MS SQL
        #region переменные ms sql
        string connectionString;
        SqlDataAdapter adapter_people;
        SqlDataAdapter adapter_limit;
        SqlConnection con;
        DataSet ds_all;
        #endregion
        #region Даты
        private DateTime _dateFact;
        public DateTime DateFact
        {
            get { return _dateFact; }
            set
            {
                if (value != null)
                    _dateFact = value;
                Obrabotka(4, _dateFact);
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("DateFact"));
            }
        }
        #endregion
        #region Команды

        private IDelegateCommand allCommand;
        private IDelegateCommand addLimitCommand;
        private IDelegateCommand addPeopleCommand;
        private IDelegateCommand createPeopleDateCommand;
        private IDelegateCommand delPeopleDateCommand;
        public IDelegateCommand AllCommand
        {
            get
            {
                return allCommand ??
                    (allCommand = new DelegateCommand(o => {
                        int counter = int.Parse((string)o);
                        switch (counter)
                        {
                            case 0:
                                Application.Current.MainWindow.Width = 1050;
                                people_date = new People();
                                people_date = SelectedDataGrid;

                                HasVisibilityGridAddDate = true;
                                HasVisibilityGridAddClient = false;
                                HasVisibilityGridAddLimit = false;
                                Obrabotka(4, _dateFact);
                                break;
                            case 1:
                                Application.Current.MainWindow.Width = 1050;
                                HasVisibilityGridAddDate = false;
                                HasVisibilityGridAddClient = true;
                                HasVisibilityGridAddLimit = false;
                                break;
                            case 2:
                                Application.Current.MainWindow.Width = 1050;
                                HasVisibilityGridAddDate = false;
                                HasVisibilityGridAddClient = false;
                                HasVisibilityGridAddLimit = true;
                                break;
                            case 3:
                                Application.Current.MainWindow.Width = 860;
                                HasVisibilityGridAddDate = false;
                                HasVisibilityGridAddClient = false;
                                HasVisibilityGridAddLimit = false;
                                break;
                            case 4:
                                HasVisibilityGridClients = true;
                                HasVisibilityGridLimit = false;
                                if (Application.Current.MainWindow.Width == 1050)
                                {
                                    if (HasVisibilityGridAddDate == false)
                                    {
                                        HasVisibilityGridAddClient = true;
                                        HasVisibilityGridAddLimit = false;
                                    }
                                }
                                break;
                            case 5:
                                HasVisibilityGridClients = false;
                                HasVisibilityGridLimit = true;
                                if (Application.Current.MainWindow.Width == 1050)
                                {
                                    if (HasVisibilityGridAddDate == false)
                                    {
                                        HasVisibilityGridAddClient = false;
                                        HasVisibilityGridAddLimit = true;
                                    }
                                }
                                break;
                            case 6:
                                Obrabotka(5, o);
                                break;
                        }
                        
                    }));
            }
        }
        public IDelegateCommand AddLimitCommand
        {
            get
            {
                return addLimitCommand ??
                    (addLimitCommand = new DelegateCommand(o => { Obrabotka(0, o); }));
            }
        }
        public IDelegateCommand AddPeopleCommand
        {
            get
            {
                return addPeopleCommand ??
                    (addPeopleCommand = new DelegateCommand(o => { Obrabotka(1, o); }));
            }
        }
        public IDelegateCommand CreatePeopleDateCommand
        {
            get
            {
                return createPeopleDateCommand ??
                    (createPeopleDateCommand = new DelegateCommand(o => {
                        people_date = SelectedDataGrid;
                        Obrabotka(2, o);
                        Application.Current.MainWindow.Width = 860;
                        HasVisibilityGridAddDate = false;
                        HasVisibilityGridAddClient = false;
                        HasVisibilityGridAddLimit = false;
                    }));
            }
        }
        public IDelegateCommand DelPeopleDateCommand
        {
            get
            {
                return delPeopleDateCommand ??
                    (delPeopleDateCommand = new DelegateCommand(o => { Obrabotka(3, o); }));
            }
        }
        
        #endregion
        #region Коллекции
        static ObservableCollection<People> list_people = new ObservableCollection<People>();
        static ObservableCollection<Limits> list_limit = new ObservableCollection<Limits>();
        static ObservableCollection<Limits> list_limit_create = new ObservableCollection<Limits>();
        static ObservableCollection<Limits> limits_list = new ObservableCollection<Limits>();
        public ObservableCollection<Limits> Limits_list {
            get { return limits_list; }
            set {
                limits_list = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Limits_list"));
            }
        }
        public ObservableCollection<People> List_people
        {
            get { return list_people; }
            set { list_people = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("List_people"));
            }
        }
        public ObservableCollection<Limits> List_limit
        {
            get { return list_limit; }
            set
            {
                list_limit = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("List_limit"));
            }
        }
        public ObservableCollection<Limits> List_limit_create {
            get { return list_limit_create; }
            set
            {
                list_limit_create = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("List_limit_create"));
            }
        }
        #endregion
        #region Логические переменные
        private static bool _hasVisibilityGridClients;
        private static bool _hasVisibilityGridLimit;
        private static bool _hasVisibilityGridAddLimit;
        private static bool _hasVisibilityGridAddClient;
        private static bool _hasVisibilityGridAddDate;
        public bool HasVisibilityGridClients
        {
            get { return _hasVisibilityGridClients; }
            set
            {
                _hasVisibilityGridClients = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("HasVisibilityGridClients"));
            }
        }
        
        public bool HasVisibilityGridLimit
        {
            get { return _hasVisibilityGridLimit; }
            set
            {
                _hasVisibilityGridLimit = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("HasVisibilityGridLimit"));
            }
        }
        public bool HasVisibilityGridAddLimit
        {
            get { return _hasVisibilityGridAddLimit; }
            set
            {
                _hasVisibilityGridAddLimit = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("HasVisibilityGridAddLimit"));
            }
        }
        public bool HasVisibilityGridAddClient
        {
            get { return _hasVisibilityGridAddClient; }
            set
            {
                _hasVisibilityGridAddClient = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("HasVisibilityGridAddClient"));
            }
        }
        public bool HasVisibilityGridAddDate
        {
            get { return _hasVisibilityGridAddDate; }
            set
            {
                _hasVisibilityGridAddDate = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("HasVisibilityGridAddDate"));
            }
        }
        #endregion
        #region Мелкие переменные
        public Limits Lim1 = new Limits();
        public People people_date;
        public event PropertyChangedEventHandler PropertyChanged;
        public People SelectedDataGrid { get; set; }
        public Limits SelectedDataGridLimit { get; set; }
        #endregion
        public ViewModals() { Load(); }
        public void Load()
        {
            
            _dateFact = DateTime.Now;

            string fileName;
            switch (types)
            {
                case 1:
                    XDocument X;
                    if (File.Exists(Environment.CurrentDirectory + "\\limit.xml") != false)
                    {
                        fileName = Environment.CurrentDirectory + "\\limit.xml";
                        X = XDocument.Load(fileName);
                        foreach (XElement t in X.Root.Elements("track"))
                        {
                            list_limit.Add(new Limits(t));

                        }
                    }
                    if (File.Exists(Environment.CurrentDirectory + "\\people.xml") != false)
                    {
                        fileName = Environment.CurrentDirectory + "\\people.xml";
                        X = XDocument.Load(fileName);
                        foreach (XElement t in X.Root.Elements("track"))
                        {
                            list_people.Add(new People(t));
                        }
                    }
                    break;
                case 2:
                    MSQL();
                    break;
            }
            
            
            _hasVisibilityGridClients = true;
            _hasVisibilityGridLimit = false;
            limits_list.Add(new Limits(0, 0));
            limits_list.Add(new Limits(10, 12));
            limits_list.Add(new Limits(12, 14));
            limits_list.Add(new Limits(14, 16));
            limits_list.Add(new Limits(16, 18));
            limits_list.Add(new Limits(18, 20));

            
        }
        private void MSQL()
        {
            connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            ds_all = new DataSet();
            string sql = "SELECT * FROM Peoples";
            try
            {
                con = new SqlConnection(connectionString);
                SqlCommand command = new SqlCommand(sql, con);
                adapter_people = new SqlDataAdapter(command);

                adapter_people.InsertCommand = new SqlCommand("sp_Insert_People", con);
                adapter_people.InsertCommand.CommandType = CommandType.StoredProcedure;
                adapter_people.InsertCommand.Parameters.Add(new SqlParameter("@first_name", SqlDbType.NVarChar, 50, "First_name"));
                adapter_people.InsertCommand.Parameters.Add(new SqlParameter("@last_name", SqlDbType.NVarChar, 50, "Last_name"));
                adapter_people.InsertCommand.Parameters.Add(new SqlParameter("@middle_name", SqlDbType.NVarChar, 50, "Middle_name"));
                adapter_people.InsertCommand.Parameters.Add(new SqlParameter("@address", SqlDbType.NVarChar, 50, "Address"));
                adapter_people.InsertCommand.Parameters.Add(new SqlParameter("@nomber", SqlDbType.NVarChar, 50, "Nomber"));
                adapter_people.InsertCommand.Parameters.Add(new SqlParameter("@date_zamer", SqlDbType.NVarChar, 50, "Date_zamer"));
                adapter_people.InsertCommand.Parameters.Add(new SqlParameter("@interval", SqlDbType.NVarChar, 50, "Interval"));
                SqlParameter parametr = adapter_people.InsertCommand.Parameters.Add("@Id", SqlDbType.Int, 0, "Id");
                parametr.Direction = ParameterDirection.Output;

                con.Open();
                DataTable dt = new DataTable();
                adapter_people.Fill(dt);
                ds_all.Tables.Add(dt);
                if (list_people == null)
                    list_people = new ObservableCollection<People>();

                foreach (DataRow dr in ds_all.Tables[0].Rows)
                {
                    string s;
                    if (dr[6].ToString() == "")
                        s = "01.01.2019";
                    else
                        s = dr[6].ToString();
                    list_people.Add(new People
                    {
                        ID = Convert.ToInt32(dr[0].ToString()),
                        First_name = dr[1].ToString(),
                        Last_name = dr[2].ToString(),
                        Middle_name = dr[3].ToString(),
                        Address = dr[4].ToString(),
                        Nomber = dr[5].ToString(),
                        Date_ = DateTime.Parse(s),
                        Interval = dr[7].ToString(),
                    });
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                if (con != null)
                {
                     con.Close();
                }

            }
                    sql = "SELECT * FROM Limit";
                    try
                    {
                        con = new SqlConnection(connectionString);
                        SqlCommand command = new SqlCommand(sql, con);
                        adapter_limit = new SqlDataAdapter(command);

                        adapter_limit.InsertCommand = new SqlCommand("sp_InsertLimits", con);
                        adapter_limit.InsertCommand.CommandType = CommandType.StoredProcedure;
                        adapter_limit.InsertCommand.Parameters.Add(new SqlParameter("@city", SqlDbType.NVarChar, 50, "City"));
                        adapter_limit.InsertCommand.Parameters.Add(new SqlParameter("@date_time", SqlDbType.Date, 0, "Date_time"));
                        adapter_limit.InsertCommand.Parameters.Add(new SqlParameter("@int_min", SqlDbType.Int, 0, "Interval_min"));
                        adapter_limit.InsertCommand.Parameters.Add(new SqlParameter("@int_max", SqlDbType.Int, 0, "Interval_max"));
                        adapter_limit.InsertCommand.Parameters.Add(new SqlParameter("@limits", SqlDbType.Int, 0, "Limits"));
                        SqlParameter parametr = adapter_limit.InsertCommand.Parameters.Add("@Id", SqlDbType.Int, 0, "Id");
                        parametr.Direction = ParameterDirection.Output;

                        con.Open();
                        DataTable dt = new DataTable();
                        adapter_limit.Fill(dt);
                        ds_all.Tables.Add(dt);
                        if (list_limit == null)
                            list_limit = new ObservableCollection<Limits>();

                        foreach (DataRow dr in ds_all.Tables[1].Rows)
                        {
                            int i, ii;
                            if (dr[2].ToString() == "")
                                i = 0;
                            else
                                i = Convert.ToInt32(dr[2].ToString());
                            if (dr[3].ToString() == "")
                                ii = 0;
                            else
                                ii = Convert.ToInt32(dr[3].ToString());
                            list_limit.Add(new Limits
                            {
                                ID = Convert.ToInt32(dr[0].ToString()),
                                Date_ = Convert.ToDateTime(dr[1].ToString()),
                                Interval_min = i,
                                Interval_max = ii,
                                LIMIT = Convert.ToInt32(dr[4].ToString()),
                                City = dr[5].ToString()
                            });

                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                    }
                    finally
                    {
                        if (con != null)
                        {
                            adapter_limit.Dispose();
                        }

                    }
                    
            
            
        }
        private void Message(string mes, string color)
        {
            DialogWindow dialog = new DialogWindow();
            dialog.DialogWindow1(mes, color);            
            dialog.Show();
            Task.Delay(1000);
            dialog.Close();
          
        }
        private void UpdateDB(SqlDataAdapter adapter, int i)
        {
            SqlCommandBuilder comandbuilder = new SqlCommandBuilder(adapter);
            
            if(i == 1)
                adapter.SelectCommand = new SqlCommand("SELECT * FROM Peoples", con);
            adapter.Update(ds_all.Tables[0]);
            if(i == 2)
            {
                adapter.SelectCommand = new SqlCommand("SELECT * FROM Limit", con);
                adapter.Update(ds_all.Tables[1]);
            }
                
        }
        public bool Obrabotka(int num, object o)
        {
            switch (num)
            {
                case 0:
                    var values = (object[])o;
                    Limits p = new Limits();
                    p.City = (string)values[0];
                    p.Date_ = DateTime.Parse((string)values[1]);
                    if ((string)values[2] == "")
                        return false;
                    else
                        p.LIMIT = int.Parse((string)values[2]);
                    Lim1 = (Limits)values[3];
                    p.Interval_min = Lim1.Interval_min;
                    p.Interval_max = Lim1.Interval_max;
                    foreach (Limits t in list_limit)
                    {
                        if (p.City == t.City && p.Date_ == t.Date_ )
                        {
                            if( p.Interval_min == t.Interval_min && p.Interval_max == t.Interval_max)
                            {
                                t.LIMIT += p.LIMIT;
                                return true;
                            }
                            if (t.Interval_min == 0 && t.Interval_max == 0 && p.Interval_min != 0 && p.Interval_max != 0)
                            {
                                t.LIMIT -= p.LIMIT;
                                break;
                            }
                                    
                        }
                    }
                    list_limit.Add(p);
                    if(types == 1)
                        p.ToXml();
                    if(types == 2)
                    {
                        DataRow dr = ds_all.Tables[1].NewRow();
                        dr[0] = p.ID;
                        dr[1] = p.Date_;
                        dr[2] = p.Interval_min;
                        dr[3] = p.Interval_max;
                        dr[4] = p.LIMIT;
                        dr[5] = p.City;
                        ds_all.Tables[1].Rows.Add(dr);
                        UpdateDB(adapter_limit, 2);
                    }
                    return true;
                case 1:
                    values = (object[])o;
                    People p1 = new People();
                    p1.Last_name = (string)values[0];
                    p1.First_name = (string)values[1];
                    p1.Middle_name = (string)values[2];
                    p1.Address = (string)values[3];
                    p1.Nomber = (string)values[4];
                    list_people.Add(p1);
                    if (types == 1)
                        p1.ToXml();
                    if (types == 2)
                    {
                        DataRow dr = ds_all.Tables[0].NewRow();
                        dr[0] = p1.ID;
                        dr[1] = p1.First_name;
                        dr[2] = p1.Last_name;
                        dr[3] = p1.Middle_name;
                        dr[4] = p1.Address;
                        dr[5] = p1.Nomber;
                        ds_all.Tables[0].Rows.Add(dr);
                        UpdateDB(adapter_people, 1);
                    }
                    return true;
                case 2:               
                    int r = 0;
                    values = (object[])o;
                    var d = DateTime.Parse((string)values[0]);
                    Lim1 = new Limits();
                    Lim1 = (Limits)values[1];
                    if (Lim1 == null) return false;
                    foreach (Limits l in list_limit)
                    {
                        if (l.City == people_date.Address)
                        {
                            if (l.Date_ == d)
                            {
                                if (l.Interval_min != 0 && l.Interval_max != 0 && l.Interval_min == Lim1.Interval_min && l.Interval_max == Lim1.Interval_max || Lim1.Interval_min != 0 && Lim1.Interval_max != 0 && l.Interval_min == 0 && l.Interval_max == 0)
                                {
                                    list_people[people_date.ID - 1].Date_ = d;
                                    list_people[people_date.ID - 1].Interval = Lim1.Interval;
                                    if (types == 2) //Иначе, если не пустой редактируем
                                    {
                                        foreach (DataRow dr in ds_all.Tables[0].Rows)
                                        {
                                            if (dr[0].ToString() == people_date.ID.ToString())
                                            {
                                                dr[6] = d;
                                                dr[7] = Lim1.Interval;
                                                UpdateDB(adapter_people, 1);
                                                return true;
                                            }
                                        }
                                    }
                                    l.LIMIT--;
                                    if (l.LIMIT == 0)
                                    {
                                        list_limit.Remove(l);
                                        r = 3;
                                        if (types == 2) //Если Пустой лимит - удаляем
                                        {
                                            foreach (DataRow dr in ds_all.Tables[1].Rows)
                                            {
                                                if (dr[0].ToString() == l.ID.ToString())
                                                {
                                                    dr.Delete();
                                                    UpdateDB(adapter_limit, 2);
                                                    return true;
                                                }
                                            }
                                        }
                                        for (int t = 1; t > list_limit.Count; t++)
                                        {
                                            list_limit[t - 1].ID = t;
                                        }

                                    }else
                                    if (types == 2) //Иначе, если не пустой редактируем
                                    {
                                        foreach (DataRow dr in ds_all.Tables[1].Rows)
                                        {
                                            if (dr[0].ToString() == l.ID.ToString())
                                            {
                                                dr[4] = l.LIMIT;
                                                UpdateDB(adapter_limit, 2);
                                                return true;
                                            }
                                        }
                                    }
                                    r = 3;
                                    Message("Готово", "green");
                                    return true;
                                }
                                else
                                if (Lim1.Interval_min == 0 && Lim1.Interval_max == 0 && l.Interval_min == 0 && l.Interval_max == 0)
                                {
                                    list_people[people_date.ID - 1].Date_ = d;
                                    l.LIMIT--;
                                    if (types == 2) //Иначе, если не пустой редактируем
                                    {
                                        foreach (DataRow dr in ds_all.Tables[0].Rows)
                                        {
                                            if (dr[0].ToString() == people_date.ID.ToString())
                                            {
                                                dr[6] = d;
                                                dr[7] = Lim1.Interval;
                                                UpdateDB(adapter_people, 1);
                                                return true;
                                            }
                                        }
                                    }
                                    if (l.LIMIT == 0)
                                    {
                                        list_limit.Remove(l);
                                        r = 3;
                                        for (int t = 1; t > list_limit.Count; t++)
                                        {
                                            list_limit[t - 1].ID = t;
                                        }
                                        if (types == 2) //Если Пустой лимит - удаляем
                                        {
                                            foreach (DataRow dr in ds_all.Tables[1].Rows)
                                            {
                                                if (dr[0].ToString() == l.ID.ToString())
                                                {
                                                    dr.Delete();
                                                    UpdateDB(adapter_limit, 2);
                                                    return true;
                                                }
                                            }
                                        }
                                    }
                                    else
                                        if (types == 2) //Иначе, если не пустой редактируем
                                    {
                                        foreach (DataRow dr in ds_all.Tables[1].Rows)
                                        {
                                            if (dr[0].ToString() == l.ID.ToString())
                                            {
                                                dr[4] = l.LIMIT;
                                                UpdateDB(adapter_limit, 2);
                                                return true;
                                            }
                                        }
                                    }
                                    Message("Готово", "green");
                                    r = 3;
                                    return true;
                                }
                                else r = 2;
                            }
                            else r = 1;
                        }
                        else r = 0;
                    }
                    switch (r)
                    {
                        case 0:
                            Message("В данном городе лимитов для замеров нет", "red");
                            return false;
                        case 1:
                            Message("В выбранный день лимитов для замеров нет", "red");
                            return false;
                        case 2:
                            Message("В выбранный день, в данный интервал времени, замеров нет", "red");
                            return false;
                    }
                    break;
                case 3:
                    people_date = new People();
                    people_date = SelectedDataGrid;
                    if(types == 1)
                        people_date.RemovePeople(people_date.Last_name + "_" + people_date.First_name + "_" + people_date.Middle_name + "_" + people_date.Address);
                    if (types == 2)
                    {
                        foreach(DataRow dr in ds_all.Tables[0].Rows)
                        {
                            if(dr[0].ToString() == people_date.ID.ToString())
                            {
                                dr.Delete();
                                UpdateDB(adapter_people, 1);
                                return true;
                            }
                        }
                    }
                    return true;
                    
                case 4:
                    DateTime dt = (DateTime)o;
                    if(people_date != null)
                    {
                        List_limit_create = new ObservableCollection<Limits>();
                        var list1 = list_limit.Where(c => c.City == people_date.Address); 
                        var list2 = list1.Where(c => c.Date_ == dt);
                        var list3 = list2.OrderBy(i => i.Interval_min);
                        foreach (Limits l in list3)
                            List_limit_create.Add(l);
                        
                    }
                    else
                    {
                        List_limit_create = new ObservableCollection<Limits>();
                        var list = list_limit.Where(c => c.Date_ == dt);
                        var list2 = list.OrderBy(i => i.Interval_min);
                        foreach (Limits l in list2)
                            List_limit_create.Add(l);
                    }
                    return true;
                case 5:
                    Lim1 = SelectedDataGridLimit;
                    if(types == 1)
                        Lim1.RemovePeople(Lim1.City + "_" + Lim1.Interval_min + "_" + Lim1.Interval_max + "_" + Lim1.DateToStr(Lim1.Date_));
                    if (types == 2)
                    {
                        foreach (DataRow dr in ds_all.Tables[1].Rows)
                        {
                            if (dr[0].ToString() == Lim1.ID.ToString())
                            {
                                dr.Delete();
                                UpdateDB(adapter_limit, 2);
                                return true;
                            }
                        }
                    }
                    return true;
                
            }
            return false;
        }

        
    }
}