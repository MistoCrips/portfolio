﻿using System.Windows.Input;

namespace Client.Modal.Command
{
    public interface IDelegateCommand : ICommand
    {
        void RaiseCanExecuteChanged();
    }
}
