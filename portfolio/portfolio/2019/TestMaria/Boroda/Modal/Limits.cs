﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Boroda.Modal
{
    public class Limits : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private int id_ = 0;
        private static int counter_ = 0;
        private string city_ = "";
        private DateTime date_;
        private string date_str;
        private int limit = 0;
        private int interval_min = 0;
        private int interval_max = 0;

        public Limits(XElement element)
        {
            city_ = element.Attribute("city_").Value;
            limit = int.Parse(element.Attribute("limit").Value);
            if (element.Attribute("interval_min").Value == "")
                interval_min = 0;
            else
                interval_min = int.Parse(element.Attribute("interval_min").Value);
            if (element.Attribute("interval_max").Value == "")
                interval_max = 0;
            else
                interval_max = int.Parse(element.Attribute("interval_max").Value);
            date_ = DateTime.Parse(element.Attribute("date_").Value);
            ID = counter_ ++ ;
        }
        public Limits()
        {
            id_ = counter_++;
        }
        public Limits(int min, int max)
        {
            interval_min = min;
            interval_max = max;
        }
        public int ID
        {
            get { return id_; }
            set
            {
                if (id_ == value)
                    return;
                id_ = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
        }
        public int LIMIT
        {
            get { return limit; }
            set
            {
                if (limit == value)
                    return;
                limit = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("LIMIT"));
            }
        }
        public string City
        {
            get { return city_; }
            set
            {
                if (city_ == value)
                    return;
                city_ = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("City"));
            }
        }
        public DateTime Date_
        {
            get { return date_; }
            set
            {
                if (date_ == value)
                    return;
                date_ = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Date_"));
            }
        }
        public string Date_str
        {
            get { return DateToStr(date_); }
            set
            {
                if (date_str == value)
                    return;
                date_str = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Date_str"));
            }
        }
        public string DateToStr(DateTime dt)
        {
            string data = "";
            if (dt.Day < 10)
            { data = "0" + dt.Day.ToString(); }
            else
            { data = dt.Day.ToString(); }
            if (dt.Month < 10)
            { data += ".0" + dt.Month.ToString(); }
            else
            { data += "." + dt.Month.ToString(); }
            data += "." + dt.Year.ToString();

            return data;
        }
        public int Interval_min
        {
            get { return interval_min; }
            set
            {
                if (interval_min == value)
                    return;
                interval_min = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Interval_min"));
            }
        }
        public int Interval_max
        {
            get { return interval_max; }
            set
            {
                if (interval_max == value)
                    return;
                interval_max = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Interval_max"));
            }
        }
        public string Interval
        {
            get {
                if (interval_min == 0 && interval_max == 0)
                    return "Другое время";
                else
                    return interval_min + " - " + interval_max; }
            set { PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Interval")); }
        }
        public void ToXml()
        {

            XDocument doc;
            if (File.Exists(Environment.CurrentDirectory + "\\limit.xml") == false)
            {
                string fileName = Environment.CurrentDirectory + "\\limit.xml";
                doc = new XDocument(
                            new XElement("base",
                             new XElement("track",
                                new XAttribute("city_", city_),
                                new XAttribute("limit", limit),
                                new XAttribute("interval_min", interval_min),
                                new XAttribute("interval_max", interval_max),
                                new XAttribute("date_", date_),
                                new XAttribute("key", city_ + "_" + interval_min + "_" + interval_max + "_" + DateToStr(date_)),
                                new XAttribute("id_", id_))));
                doc.Save(fileName);
            }
            else
            {
                string fileName = Environment.CurrentDirectory + "\\limit.xml";
                doc = XDocument.Load(fileName);
                XElement track = new XElement("track");
                track.Add(new XAttribute("city_", city_));
                track.Add(new XAttribute("limit", limit));
                track.Add(new XAttribute("interval_min", interval_min));
                track.Add(new XAttribute("interval_max", interval_max));
                track.Add(new XAttribute("date_", date_));
                track.Add(new XAttribute("key", city_ + "_" + interval_min + "_" + interval_max + "_" + DateToStr(date_)));
                track.Add(new XAttribute("id_", id_));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }
        public void RemovePeople(string Key)
        {
            string fileName = Environment.CurrentDirectory + "\\limit.xml";
            XDocument doc= XDocument.Load(fileName);
            doc.Descendants().Where(e => e.Attributes().Any(a => a.Value == Key)).Remove();
            doc.Save(fileName);
        }
    }
}
