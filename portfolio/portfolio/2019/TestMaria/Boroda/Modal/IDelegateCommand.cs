﻿using System.Windows.Input;

namespace Boroda.Modal
{
    public interface IDelegateCommand : ICommand
    {
        void RaiseCanExecuteChanged();
    }
}