﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model.LD
{
    public class Nagrad : INotifyPropertyChanged // Наградной список
    {
        string types_ = ".";
        string date1_ = ".";
        string what_ = ".";
        string pr_ = ".";
        string date2_ = ".";
        public event PropertyChangedEventHandler PropertyChanged;
        public Nagrad() { }
        public Nagrad(XElement element)
        {
            if (element.Attribute("type").Value != null) Type = element.Attribute("type").Value;
            if (element.Attribute("date1_").Value != null) Date_ = element.Attribute("date_").Value;
            if (element.Attribute("what").Value != null) Who_pricaz = element.Attribute("what").Value;
            if (element.Attribute("pr").Value != null) Pricaz = element.Attribute("pr").Value;
            if (element.Attribute("date2_").Value != null) Date_pricaz = element.Attribute("date2_").Value;
        }
        public string Type
        {
            get { return types_; }
            set
            {
                if (types_ == value)
                    return;
                types_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public string Date_
        {
            get { return date1_; }
            set
            {
                if (date1_ == value)
                    return;
                date1_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date1"));
            }
        }
        public string Who_pricaz
        {
            get { return what_; }
            set
            {
                if (what_ == value)
                    return;
                what_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("What"));
            }
        }
        public string Pricaz
        {
            get { return pr_; }
            set
            {
                if (pr_ == value)
                    return;
                pr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Pr"));
            }
        }
        public string Date_pricaz
        {
            get { return date2_; }
            set
            {
                if (date2_ == value)
                    return;
                date2_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date2"));
            }
        }
        public void ToXml(string LOGIN)
        {
            string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + LOGIN + ".xml";
            XDocument doc;
            if (File.Exists(fileName) == false)
            {
                doc = new XDocument(
                new XElement("base",
                    new XElement("track_ng",
                        new XAttribute("type", Type),
                        new XAttribute("date1_", Date_),
                        new XAttribute("what", Who_pricaz),
                        new XAttribute("pr", Pricaz),
                        new XAttribute("date2_", Date_pricaz))));
                doc.Save(fileName);
            }
            else
            {
                doc = XDocument.Load(fileName);
                XElement track = new XElement("track_ng");
                track.Add(new XAttribute("type", Type));
                track.Add(new XAttribute("date1_", Date_));
                track.Add(new XAttribute("what", Who_pricaz));
                track.Add(new XAttribute("pr", Pricaz));
                track.Add(new XAttribute("date2_", Date_pricaz));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }
    }
}
