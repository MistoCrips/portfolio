﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TestSuperBrands.Models
{
    public class Categories
    {
        [Key]
        public int id { get; set; }

        public string Name { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreateTime { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DeleteTime { get; set; }

        public ICollection<Products> Product { get; set; }

        public Categories()
        {
            Product = new List<Products>();
        }
    }
    public class CategoriesContext : DbContext
    {
        public DbSet<AttributeValues> AttributeValues { get; set; }
        public DbSet<Attributes> Attributes { get; set; }
        public DbSet<Products> Products { get;
            set; }
        public DbSet<Categories> Categories { get; set; }
    }

}