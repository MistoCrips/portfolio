﻿
using New_Stroevaya_chast.Modal.Model.LD;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model.LD
{
    public class LD
    {
        string Message = "";
        #region Открытие
        public string Load(string LNumber)
        {
            Message = "LD1>";
            if (open_zv(LNumber) != "")
                Message += "Zvanie" + open_zv(LNumber);
            if (open_pos(LNumber) != "")
                Message += "|Poslug" + open_pos(LNumber);
            if (open_fm(LNumber) != "")
                Message += "|Fam" + open_fm(LNumber);
            if (open_ng(LNumber) != "")
                Message += "|Nagrad" + open_ng(LNumber);
            if (open_ph(LNumber) != "")
                 Message += "|Foto" + open_ph(LNumber);
            if (open_docum(LNumber) != "")
                Message += "|Docum" + open_docum(LNumber);
            if (open_dan(LNumber) != "")
                Message += "|Dan" + open_dan(LNumber);
            if (open_kon(LNumber) != "")
                Message += "|Kontr" + open_kon(LNumber);
            if (open_nazn(LNumber) != "")
                Message += "|Naznac" + open_nazn(LNumber);
            if (open_dela(LNumber) != "")
                Message += "|Priem" + open_dela(LNumber);
            if (open_vizh(LNumber) != "")
                Message += "|Vizhiv" + open_vizh(LNumber);
            if (Message_otr(LNumber) != "")
                Message += "|Otriv" + Message_otr(LNumber);
            return Message ;
        }
        public string Create_people_LD(ObservableCollection<zvanie> Z, ObservableCollection<Poslug> T, ObservableCollection<family> T1, ObservableCollection<Nagrad> T2, Photo_ T3, ObservableCollection<Docum> T4, Dannie T5, ObservableCollection<Kontrakt> T6, Naznach p, PriemDel p1, Vizhivanie p2)
        {
            Message = "nLD>";
            if (Message_zv(Z) != "")
                Message += "Zvanie" + Message_zv(Z);
            if (Message_pos(T) != "")
                Message += "|Poslug" + Message_pos(T);
            if (Message_fm(T1) != "")
                Message += "|Fam" + Message_fm(T1);
            if (Message_ng(T2) != "")
                Message += "|Nagrad" + Message_ng(T2);
            if (Message_ph(T3) != "")
                Message += "|Foto" + Message_ph(T3);
            if (Message_docum(T4) != "")
                Message += "|Docum" + Message_docum(T4);
            if (Message_dan(T5) != "")
                Message += "|Dan" + Message_dan(T5);
            if (Message_kon(T6) != "")
                Message += "|Kontr" + Message_kon(T6);
            if (Message_nazn(p) != "")
                Message += "|Naznac" + Message_nazn(p);
            if (Message_dela(p1) != "")
                Message += "|Priem" + Message_dela(p1);
            if (Message_vizh(p2) != "")
                Message += "|Vizhiv" + Message_vizh(p2);
            return Message;
        }
        #region Загрузка званий
        public string open_zv(string ln)
        {
            ObservableCollection<zvanie> T = Sbor_Zv(ln);
            string s = "";
            foreach(zvanie p in T)
            {
                s += "|" + p.Name + "|" + p.Nomber + "|" + p.Pr + "|" + p.What + "|" + p.Date;
            }
            return s;
        }
        public ObservableCollection<zvanie> Sbor_Zv(string ln)
        {
            ObservableCollection<zvanie> T = new ObservableCollection<zvanie>();
            string filename = Environment.CurrentDirectory  +"\\Recources\\Active\\LD\\" + ln + ".xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track_zv"))
            {
                T.Add(new zvanie(el));
            }
            return T;
        }
        public string Message_zv(ObservableCollection<zvanie> T)
        {

            string s = "";
            foreach (zvanie p in T)
            {
                s += "|" + p.Name + "|" + p.Nomber + "|" + p.Pr + "|" + p.What + "|" + p.Date;
            }
            return s;
        }
        #endregion
        #region Послужной
        public string open_pos(string ln)
        {
            ObservableCollection<Poslug> T = Sbor_Pos(ln);
            string s = "";
            foreach (Poslug p in T)
            {
                s += "|" + p.Chast_ + "|" + p.Date_pricaz + "|" + p.Dolgnost_ + "|" + p.Name_pricaz + "|" + p.Nomber_pricaz + "|" + p.Podr_ + "|" + p.VUS_ ;
            }
            return s;
        }
        public ObservableCollection<Poslug> Sbor_Pos(string ln)
        {
            ObservableCollection<Poslug> T = new ObservableCollection<Poslug>();
            string filename = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + ln + ".xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track_pos"))
            {
                T.Add(new Poslug(el));
            }
            return T;
        }
        public string Message_pos(ObservableCollection<Poslug> T)
        {
            string s = "";
            foreach (Poslug p in T)
            {
                s += "|" + p.Chast_ + "|" + p.Date_pricaz + "|" + p.Dolgnost_ + "|" + p.Name_pricaz + "|" + p.Nomber_pricaz + "|" + p.Podr_ + "|" + p.VUS_;
            }
            return s;
        }
        #endregion
        #region Семья
        public string open_fm(string ln)
        {
            ObservableCollection<family> T = Sbor_fam(ln);
            string s = "";
            foreach (family p in T)
            {
                s += "|"+ p.Name + "|" + p.types + "|" + p.Brd;
            }
            return s;
        }
        public ObservableCollection<family> Sbor_fam(string ln)
        {
            ObservableCollection<family> T = new ObservableCollection<family>();
            string filename = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + ln + ".xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track_fm"))
            {
                T.Add(new family(el));
            }
            return T;
        }
        public string Message_fm(ObservableCollection<family> T)
        {
            string s = "";
            foreach (family p in T)
            {
                s += "|" + p.Name + "|" + p.types + "|" + p.Brd;
            }
            return s;
        }
        #endregion
        #region Награды
        public string open_ng(string ln)
        {
            ObservableCollection<Nagrad> T = Sbor_ng(ln);
            string s = "";
            foreach (Nagrad p in T)
            {
                s += "|" + p.Type + "|" + p.Who_pricaz + "|" + p.Pricaz + "|" + p.Date_pricaz + "|" + p.Date_ ;
            }
            return s;
        }
        public ObservableCollection<Nagrad> Sbor_ng(string ln)
        {
            ObservableCollection<Nagrad> T = new ObservableCollection<Nagrad>();
            string filename = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + ln + ".xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track_ng"))
            {
                T.Add(new Nagrad(el));
            }
            return T;
        }
        public string Message_ng(ObservableCollection<Nagrad> T)
        {
            string s = "";
            foreach (Nagrad p in T)
            {
                s += "|" + p.Type + "|" + p.Who_pricaz + "|" + p.Pricaz + "|" + p.Date_pricaz + "|" + p.Date_;
            }
            return s;
        }
        #endregion
        #region Фото
        public string open_ph(string ln)
        {
            ObservableCollection<Photo_> T = Sbor_ph(ln);
            string s = "";
            foreach (Photo_ p in T)
            {
                s +="|Ph1*"+ p.ByteArrayToString(p.getJPGFromImageControl(p.Date1)) + "|Ph2*" + p.ByteArrayToString(p.getJPGFromImageControl(p.Date2)) + "|Ph3*" + p.ByteArrayToString(p.getJPGFromImageControl(p.Date3)) + "|Ph4*" + p.ByteArrayToString(p.getJPGFromImageControl(p.Date4)) + "|Ph5*" + p.ByteArrayToString(p.getJPGFromImageControl(p.Date5)) + "|Ph6*"+ p.ByteArrayToString(p.getJPGFromImageControl(p.Date6));
            }
            return s;
        }
        public ObservableCollection<Photo_> Sbor_ph(string ln)
        {
            ObservableCollection<Photo_> T = new ObservableCollection<Photo_>();
            string filename = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\ph\\" + ln + ".xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track_ph"))
            {
                T.Add(new Photo_(el));
            }
            return T;
        }
        public string Message_ph(Photo_ p)
        {
            string s = "";
            s += "|Ph1*" + p.ByteArrayToString(p.getJPGFromImageControl(p.Date1)) + "|Ph2*" + p.ByteArrayToString(p.getJPGFromImageControl(p.Date2)) + "|Ph3*" + p.ByteArrayToString(p.getJPGFromImageControl(p.Date3)) + "|Ph4*" + p.ByteArrayToString(p.getJPGFromImageControl(p.Date4)) + "|Ph5*" + p.ByteArrayToString(p.getJPGFromImageControl(p.Date5)) + "|Ph6*" + p.ByteArrayToString(p.getJPGFromImageControl(p.Date6));
            return s;
        }
        
        #endregion
        #region Документы
        public string open_docum(string ln)
        {
            ObservableCollection<Docum> T = Sbor_docum(ln);
            string s = "";
            foreach (Docum p in T)
            {
                s += "|" + p.Kod + "|" + p.Seriya + "|" + p.Nomber + "|" + p.Who_vidal + "|" + p.Date_vid;
            }
            return s;
        }
        public ObservableCollection<Docum> Sbor_docum(string ln)
        {
            ObservableCollection<Docum> T = new ObservableCollection<Docum>();
            string filename = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + ln + ".xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track_dm"))
            {
                T.Add(new Docum(el));
            }
            return T;
        }
        public string Message_docum(ObservableCollection<Docum> T)
        {
            string s = "";
            foreach (Docum p in T)
            {
                s += "|" + p.Kod + "|" + p.Seriya + "|" + p.Nomber + "|" + p.Who_vidal + "|" + p.Date_vid;
            }
            return s;
        }

        #endregion
        #region Данные
        public string open_dan(string ln)
        {
            ObservableCollection<Dannie> T = Sbor_dn(ln);
            string s = "";
            foreach (Dannie p in T)
            {
                s += "|" + p.Lnumber + "|" + p.FIO + "|" + p.Bank_card + "|" + p.Date_brd + "|" + p.Gr_obr + "|"+ p.Home_adres+"|" + p.Mesto_brd + "|" + p.Nac + "|" + p.PAC + "|" + p.POL + "|" + p.Vod_prava + "|" + p.Voenkomat + "|" + p.Voen_obr + "|" + p.Types;
            }
            return s;
        }
        public ObservableCollection<Dannie> Sbor_dn(string ln)
        {
            ObservableCollection<Dannie> T = new ObservableCollection<Dannie>();
            string filename = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + ln + ".xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track_dan"))
            {
                T.Add(new Dannie(el));
            }
            return T;
        }
        public string Message_dan(Dannie T)
        {

            string s = "";
            s += "|" + T.Lnumber + "|" + T.FIO + "|" + T.Bank_card + "|" + T.Date_brd + "|" + T.Gr_obr + "|" + T.Home_adres + "|" + T.Mesto_brd + "|" + T.Nac + "|" + T.PAC + "|" + T.POL + "|" + T.Vod_prava + "|" + T.Voenkomat + "|" + T.Voen_obr + "|" + T.Types;
            return s;
        }
        #endregion
        #region Контракт
        public string open_kon(string ln)
        {
            ObservableCollection<Kontrakt> T = Sbor_kon(ln);
            string s = "";
            foreach (Kontrakt p in T)
            {
                s += "|" + p.data_zakl + "|" + p.data_okon + "|" + p.Date_pricaz + "|" + p.Nomber_pricaz + "|" + p.types + "|" + p.Who_pricaz;
            }
            return s;
        }
        public ObservableCollection<Kontrakt> Sbor_kon(string ln)
        {
            ObservableCollection<Kontrakt> T = new ObservableCollection<Kontrakt>();
            string filename = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + ln + ".xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track_kon"))
            {
                T.Add(new Kontrakt(el));
            }
            return T;
        }
        public string Message_kon(ObservableCollection<Kontrakt> T)
        {
            string s = "";
            foreach (Kontrakt p in T)
            {
                s += "|" + p.data_zakl + "|" + p.data_okon + "|" + p.Date_pricaz + "|" + p.Nomber_pricaz + "|" + p.types + "|" + p.Who_pricaz;
            }
            return s;
        }
        #endregion
        #region Назначение
        public string open_nazn(string ln)
        {
            ObservableCollection<Naznach> T = Sbor_nazn(ln);
            string s = "";
            foreach (Naznach p in T)
            {
                s += "|" + p.pricaz + "|" + p.who_pricaz + "|"  + p.date_pricaz;
            }
            return s;
        }
        public ObservableCollection<Naznach> Sbor_nazn(string ln)
        {
            ObservableCollection<Naznach> T = new ObservableCollection<Naznach>();
            string filename = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + ln + ".xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track_naz"))
            {
                T.Add(new Naznach(el));
            }
            return T;
        }
        public string Message_nazn(Naznach p)
        {
            string s = "";
            s += "|" + p.pricaz + "|" + p.who_pricaz + "|" + p.date_pricaz;
            return s;
        }
        #endregion
        #region Прием дел
        public string open_dela(string ln)
        {
            ObservableCollection<PriemDel> T = Sbor_dela(ln);
            string s = "";
            foreach (PriemDel p in T)
            {
                s += "|" + p.pricaz + "|" + p.who_pricaz + "|" + p.date_pricaz ;
            }
            return s;
        }
        public ObservableCollection<PriemDel> Sbor_dela(string ln)
        {
            ObservableCollection<PriemDel> T = new ObservableCollection<PriemDel>();
            string filename = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + ln + ".xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track_pr_del"))
            {
                T.Add(new PriemDel(el));
            }
            return T;
        }
        public string Message_dela(PriemDel p)
        {
            string s = "";
            s += "|" + p.pricaz + "|" + p.who_pricaz + "|" + p.date_pricaz;
            return s;
        }
        #endregion
        #region Выживайка
        public string open_vizh(string ln)
        {
            ObservableCollection<Vizhivanie> T = Sbor_vizh(ln);
            string s = "";
            foreach (Vizhivanie p in T)
            {
                s += "|" + p.Period + "|" + p.Mesto + "|" + p.Osnovanie;
            }
            return s;
        }
        public ObservableCollection<Vizhivanie> Sbor_vizh(string ln)
        {
            ObservableCollection<Vizhivanie> T = new ObservableCollection<Vizhivanie>();
            string filename = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + ln + ".xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track_viz"))
            {
                T.Add(new Vizhivanie(el));
            }
            return T;
        }
        public string Message_vizh(Vizhivanie p)
        {
            string s = "";
            s += "|" + p.Period + "|" + p.Mesto + "|" + p.Osnovanie;
            return s;
        }
        #endregion
        #region Отрыв
        public string Message_otr(string ln)
        {
            ObservableCollection<Otryv_LD> T = Sbor_otr(ln);
            string s = "";
            foreach (Otryv_LD p in T)
            {
                s += "|" + p.Types_ + "|" + p.S_data + "|" + p.Cel_ + "|" + p.Mesto + "|" + p.Osnovanie + "|"+p.Data_preb;
            }
            return s;
        }
        public ObservableCollection<Otryv_LD> Sbor_otr(string ln)
        {
            ObservableCollection<Otryv_LD> T = new ObservableCollection<Otryv_LD>();
            string filename = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + ln + ".xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track_otr"))
            {
                T.Add(new Otryv_LD(el));
            }
            return T;
        }

        #endregion
        #endregion
        #region Сохранение
        public void Saveing_Zv(string LNumber, zvanie tmp)
        {
            tmp.ToXml(LNumber);
        }
        public void Saving_photo(string LNumber, Photo_ tmp)
        {
            tmp.ToXml(LNumber);
        }
        public void Saveing_Po(string LNumber, Poslug tmp)
        {
            tmp.ToXml(LNumber);
        }
        public void Saveing_Fm(string LNumber, family tmp)
        {
            tmp.ToXml(LNumber);
        }
        public void Saveing_Ng(string LNumber, Nagrad tmp)
        {
            tmp.ToXml(LNumber);
        }
        public void Saveing_Dc(string LNumber, Docum tmp)
        {
            tmp.ToXml(LNumber);
        }
        public void Saveing_Dn(string LNumber, Dannie tmp)
        {
            tmp.ToXml(LNumber);
        }
        public void Saveing_Kn(string LNumber, Kontrakt tmp)
        {
            tmp.ToXml(LNumber);
        }
        public void Saveing_Naz(string LNumber, Naznach tmp)
        {
            tmp.ToXml(LNumber);
        }
        public void Saveing_PD(string LNumber, PriemDel tmp)
        {
            tmp.ToXml(LNumber);
        }
        public void Saveing_Viz(string LNumber, Vizhivanie tmp)
        {
            tmp.ToXml(LNumber);
        }
        public void Saveing_OLD(string LNumber, Otryv_LD tmp)
        {
            tmp.ToXml(LNumber);
        }
        #endregion
    }
}
