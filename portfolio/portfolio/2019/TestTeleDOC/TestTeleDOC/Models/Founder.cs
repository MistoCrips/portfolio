﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestTeleDOC.Models
{
    public class Founder
    {
        public int FounderId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Middlename { get; set; }
        public int? OrganizationId { get; set; }

        public Organization Organization { get; set; }
    }
}