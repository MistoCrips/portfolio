﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model
{
    public class Komand_ld : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        static int counter_ = 0;
        int id_ = 0;
        string ln_ = ".";
        string starshiy_ = ".";
        string old_ = ".";
        string date1_ = ".";
        string fio_ = ".";
        string per_sl = ".";
        public int Counter
        {
            get { return counter_; }
            set
            {
                if (counter_ == value)
                    return;
                counter_ = value;
            }
        }
        public int Id
        {
            get { return id_; }
            set
            {
                id_ = Counter++;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Id"));
            }
        }
        public string LN
        {
            get { return ln_; }
            set
            {
                if (ln_ == value)
                    return;
                ln_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("LN"));
            }
        }
        public string Starshiy
        {
            get { return starshiy_; }
            set
            {
                if (starshiy_ == value)
                    return;
                starshiy_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Starshiy"));
            }
        }
        public string Old
        {
            get { return old_; }
            set
            {
                if (old_ == value)
                    return;
                old_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Old"));
            }
        }
        public string Date
        {
            get { return date1_; }
            set
            {
                if (date1_ == value)
                    return;
                date1_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date"));
            }
        }
        public string FIO
        {
            get { return fio_; }
            set
            {
                if (fio_ == value)
                    return;
                fio_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FIO"));
            }
        }
        public string Per_sl
        {
            get { return per_sl; }
            set
            {
                if (per_sl == value)
                    return;
                per_sl = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Per_sl"));
            }
        }
        public Komand_ld()
        {

        }
        public Komand_ld(XElement element)
        {
            LN = element.Attribute("ln").Value;
            Starshiy = element.Attribute("st").Value;
            Old = element.Attribute("old").Value;
            Date = element.Attribute("date").Value;
            FIO = element.Attribute("fio").Value;
            Per_sl = element.Attribute("per").Value;
            if (int.Parse(element.Attribute("id").Value) > counter_)
            {
                counter_ = int.Parse(element.Attribute("id").Value);
            }
        }


    }
}
