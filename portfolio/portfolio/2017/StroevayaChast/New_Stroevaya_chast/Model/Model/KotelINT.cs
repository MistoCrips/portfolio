﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace New_Stroevaya_chast.Modal.Model
{
    public class Kotel : INotifyPropertyChanged
    {
        string podr_ = ".";
        int col_ = 0;
        public string Podr
        {
            get { return podr_; }
            set
            {
                if (podr_ == value)
                    return;
                podr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Podr"));
            }
        }
        public int Col
        {
            get { return col_; }
            set
            {
                if (col_ == value)
                    return;
                col_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Col"));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }//Котел (Учет питающихся в столовой в цифрах)
}
