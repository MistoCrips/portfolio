﻿using New_Stroevaya_chast.Modal.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stroevaya_chast.ViewModal
{
    public class MessageToZashtat
    {
        public ObservableCollection<OpenLoadZaShtat> anshtat_pol(string[] word)
        {
            ObservableCollection<OpenLoadZaShtat> list = new ObservableCollection<OpenLoadZaShtat>(); 
            OpenLoadZaShtat tmp = new OpenLoadZaShtat();
            int ij = 1;
            for (int i = 1; i < word.Length; i++)
            {

                int j = i;
                while (j > 6)
                {
                    j -= 6;
                }
                if (j == 1)
                {
                    tmp = new OpenLoadZaShtat();
                    tmp.Lnumber = word[i];
                    tmp.Id = ij;
                    ij++;
                }
                if (j == 2)
                {
                    tmp.FIO = word[i];
                }
                if (j == 3)
                {
                    tmp.Types = word[i];
                }
                if (j == 4)
                {
                    tmp.Zvanie = word[i];
                }
                if (j == 5)
                {
                    tmp.Last_dolg = word[i];
                }
                if (j == 6)
                {
                    tmp.Last_podr = word[i];
                    list.Add(tmp);
                }

            }
            return list;
            
        }//Получение заштатников


        public ObservableCollection<OpenLoadZaShtat> Anstat2(ObservableCollection<OpenLoadZaShtat> list, string dop)
        {
            ObservableCollection<OpenLoadZaShtat> list2 = new ObservableCollection<OpenLoadZaShtat>();
            if (dop == "4") //Прапорский по допуску
            {
                foreach (OpenLoadZaShtat t in list)
                {
                    if (t.Zvanie == "старшина" || t.Zvanie == "прапорщик" || t.Zvanie == "старший прапорщик")
                    {
                        list2.Add(t);
                    }
                }
            }
            if (dop == "3") //офицерский по допуску
            {
                foreach (OpenLoadZaShtat t in list)
                {
                    if (t.Zvanie == "лейтенант" || t.Zvanie == "старший лейтенант" || t.Zvanie == "капитан" || t.Zvanie == "майор" || t.Zvanie == "подполковник" || t.Zvanie == "полковник" || t.Zvanie == "генерал-лейтенант" || t.Zvanie == "генерал-майор")
                    {
                        list2.Add(t);
                    }
                }
            }
            if (dop == "5" || dop == "6") //сержанты и солдаты по допуску
            {
                foreach (OpenLoadZaShtat t in list)
                {
                    if (t.Zvanie == "рядовой" && t.Types == "контр" || t.Zvanie == "рядовой" && t.Types == "жен" || t.Zvanie == "ефрейтор" && t.Types == "контр" || t.Zvanie == "ефрейтор" && t.Types == "жен" || t.Zvanie == "младший сержант" && t.Types == "контр" || t.Zvanie == "младший сержант" && t.Types == "жен" || t.Zvanie == "сержант" && t.Types == "контр" || t.Zvanie == "сержант" && t.Types == "жен" || t.Zvanie == "старший сержант" && t.Types == "контр" || t.Zvanie == "старший сержант" && t.Types == "жен" || t.Zvanie == "старшина" && t.Types == "контр" || t.Zvanie == "старшина" && t.Types == "жен")
                    {
                        list2.Add(t);
                    }
                }
            }
            if (dop == "7") //срочники по допуску
            {
                foreach (OpenLoadZaShtat t in list)
                {
                    if (t.Zvanie == "рядовой" && t.Types != "контр" || t.Zvanie == "ефрейтор" && t.Types != "контр" || t.Zvanie == "младший сержант" && t.Types != "контр")
                    {
                        list2.Add(t);
                    }
                }
            }
            return list2;
        }
    }
}
