package com.example.user.proba;

import java.util.ArrayList;

public class Lang {

    private String _id;
    private String _name;
    public  String getID(){return _id; }
    public  String setID(String values){
        _id = values;
        return _id; }
    public  String getNames(){return _name; }
    public  String setNames(String values){
        _name = values;
        return _name; }


    public Lang(String id, String Name ){
        _id = id;
        _name = Name;
    }
}
