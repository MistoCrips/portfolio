﻿using Stroevaya_chast.ViewModal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model
{
    public class Izmen : INotifyPropertyChanged
    {
        private string types_ = ".";
        private string name_table_ = ".";
        private string old_value = ".";
        private int row_ = 0;
        private string col_ = ".";
        private string new_value = ".";
        DateClass dateClass = new DateClass();
        public Izmen()
        {

        }
        public Izmen(XElement el)
        {
            if (el.Attribute("types").Value != ".") Types = el.Attribute("types").Value;
            if (el.Attribute("col").Value != ".") Col = el.Attribute("col").Value;
            if (el.Attribute("row").Value != ".") Row = int.Parse(el.Attribute("row").Value);
            if (el.Attribute("name").Value != ".") Name_table = el.Attribute("name").Value;
            if (el.Attribute("new_value").Value != ".") New_value = el.Attribute("new_value").Value;
            if (el.Attribute("old_value").Value != ".") Old_value = el.Attribute("old_value").Value;
        }

        public string Types
        {
            get { return types_; }
            set
            {
                if (types_ == value)
                    return;
                types_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public string Name_table
        {
            get { return name_table_; }
            set
            {
                if (name_table_ == value)
                    return;
                name_table_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name_table"));
            }
        }
        public string Old_value
        {
            get { return old_value; }
            set
            {
                if (old_value == value)
                    return;
                old_value = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Old_value"));
            }
        }
        public string New_value
        {
            get { return new_value; }
            set
            {
                if (new_value == value)
                    return;
                new_value = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("New_value"));
            }
        }
        public int Row
        {
            get { return row_; }
            set
            {
                if (row_ == value)
                    return;
                row_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Row"));
            }
        }
        public string Col
        {
            get { return col_; }
            set
            {
                if (col_ == value)
                    return;
                col_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Col"));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void ToXmlIzm(string fileName)
        {
            XDocument doc;
            if (File.Exists(fileName) == false)
            {
                doc = new XDocument(
                            new XElement("base",
                             new XElement("track",
                                new XAttribute("types", Types),
                                new XAttribute("col", Col),
                                new XAttribute("row", Row),
                                new XAttribute("name", Name_table),
                                new XAttribute("new_value", New_value),
                                new XAttribute("old_value", Old_value))));
                doc.Save(fileName);
            }
            else
            {
                doc = XDocument.Load(fileName);
                XElement track = new XElement("track");
                track.Add(new XAttribute("types", Types));
                track.Add(new XAttribute("col", Col));
                track.Add(new XAttribute("row", Row));
                track.Add(new XAttribute("name", Name_table));
                track.Add(new XAttribute("new_value", New_value));
                track.Add(new XAttribute("old_value", Old_value));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }
        public void ToXml() //Архивация изменения
        {
            ToXmlIzm(Environment.CurrentDirectory + "\\Recources\\Active\\izm\\izm " + dateClass.DateToday() + ".xml");
        }
        public void ToXml1() //несохраненные - активные изменения
        {
            ToXmlIzm(Environment.CurrentDirectory + "\\Recources\\Active\\izm.xml");
        }
        public void ToXmlPerest(Perest tmp, string message)
        {
            string fileName;
            XDocument doc;

            if (File.Exists(Environment.CurrentDirectory + "\\Recources\\Active\\peres\\peres " + dateClass.DateToday() + ".xml") == false)
            {
                fileName = Environment.CurrentDirectory + "\\Recources\\Active\\peres\\peres " + dateClass.DateToday() + ".xml";
                doc = new XDocument(
                            new XElement("base",
                             new XElement("track",
                                new XAttribute("lnumber", tmp.Lnumber),
                                new XAttribute("new_podr", tmp.New_podr),
                                new XAttribute("old_podr", tmp.Old_podr),
                                new XAttribute("old_dolg", tmp.Old_dolg),
                                new XAttribute("fio", tmp.FIO),
                                new XAttribute("tek_zvanie", tmp.Zvanie),
                                new XAttribute("type", tmp.Types),
                                new XAttribute("vus", tmp.VUS),
                                new XAttribute("kod", tmp.Kod),
                                new XAttribute("new_dolg", tmp.New_dolg))));
                doc.Save(fileName);
            }
            else
            {
                fileName = Environment.CurrentDirectory + "\\Recources\\Active\\peres\\peres " + dateClass.DateToday() + ".xml";
                doc = XDocument.Load(fileName);
                XElement track = new XElement("track");
                track.Add(new XAttribute("lnumber", tmp.Lnumber));
                track.Add(new XAttribute("new_podr", tmp.New_podr));
                track.Add(new XAttribute("old_podr", tmp.Old_podr));
                track.Add(new XAttribute("old_dolg", tmp.Old_dolg));
                track.Add(new XAttribute("fio", tmp.FIO));
                track.Add(new XAttribute("tek_zvanie", tmp.Zvanie));
                track.Add(new XAttribute("type", tmp.Types));
                track.Add(new XAttribute("vus", tmp.VUS));
                track.Add(new XAttribute("kod", tmp.Kod));
                track.Add(new XAttribute("new_dolg", tmp.New_dolg));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
            if (tmp.New_dolg != "." && tmp.New_dolg != "")
            {
                fileName = Environment.CurrentDirectory + "\\Recources\\Shtat\\51460.xml";
                doc = XDocument.Load(fileName);
                foreach (XElement i in doc.Root.Elements("track"))
                {
                    if (int.Parse(i.Attribute("id").Value) == tmp.Id)
                    {
                        i.Attribute("lnumber").Value = tmp.Lnumber;
                    }
                }
                doc.Save(fileName);
                fileName = Environment.CurrentDirectory + "\\Recources\\AnShtat\\51460.xml";
                doc = XDocument.Load(fileName);
                doc.Descendants().Where(e => e.Name == tmp.Lnumber).Remove();
                doc.Save(fileName);
            }
            else
            {
                fileName = Environment.CurrentDirectory + "\\Recources\\Shtat\\51460.xml";
                doc = XDocument.Load(fileName);
                foreach (XElement i in doc.Root.Elements("track"))
                {
                    if (int.Parse(i.Attribute("id").Value) == tmp.Id)
                    {
                        i.Attribute("lnumber").Value = "0";
                    }
                }
                doc.Save(fileName);
                OpenLoadZaShtat t = new OpenLoadZaShtat();
                t.Lnumber = tmp.Lnumber;
                t.FIO = tmp.FIO;
                t.Last_dolg = tmp.Old_dolg;
                t.Last_podr = tmp.Old_podr;
                t.Zvanie = tmp.Zvanie;
                t.Types = tmp.Types;
                t.ToXml();
            }



        }
        public void ToXmlLD(OpenLoad tmp, string mes)
        {
            if (Types == "Добавить")
            {

                XDocument doc;
                if (Types == "Добавить")
                {
                    string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + New_value + ".xml";
                    doc = XDocument.Load(fileName);
                    XElement track = new XElement("P");
                    track.Add(new XAttribute("chast", "51460"));
                    track.Add(new XAttribute("podr", tmp.Podr));
                    track.Add(new XAttribute("dolg", tmp.Dolgnost));
                    track.Add(new XAttribute("vus", tmp.Vus));
                    track.Add(new XAttribute("data", dateClass.DateToday()));
                    doc.Root.Add(track);
                    doc.Save(fileName);
                }
            }
        }
        public void ToXmlUb(Ub tmp, string Types)
        {
            try
            {
                string fileName;
                XDocument doc;
                XElement track;
                if (Types == "Убытие")
                {
                    if (File.Exists(Environment.CurrentDirectory + "\\Recources\\Active\\ub.xml") == false)
                    {
                        fileName = Environment.CurrentDirectory + "\\Recources\\Active\\ub.xml";
                        doc = new XDocument(
                                    new XElement("base",
                                     new XElement("track",
                                        new XAttribute("cel", tmp.Cel),
                                        new XAttribute("date2", tmp.Date_preb),
                                        new XAttribute("date1", tmp.Date_ub),
                                        new XAttribute("dolg", tmp.Dolg),
                                        new XAttribute("fio", tmp.FIO),
                                        new XAttribute("ln", tmp.Lnumber),
                                        new XAttribute("ms", tmp.Mesto),
                                        new XAttribute("osn", tmp.Osn),
                                        new XAttribute("podr", tmp.Podr),
                                        new XAttribute("type1", tmp.Types),
                                        new XAttribute("type2", tmp.Types_ub),
                                        new XAttribute("vrio", tmp.Vrio),
                                        new XAttribute("zv", tmp.Zvanie),
                                        new XAttribute("vp1", tmp.VPD_ST1),
                                        new XAttribute("vp2", tmp.VPD_ST2),
                                        new XAttribute("vp3", tmp.VPD_ST3),
                                        new XAttribute("vp4", tmp.VPD_ST4),
                                        new XAttribute("vp5", tmp.VPD_ST5),
                                        new XAttribute("vp6", tmp.VPD_ST6),
                                        new XAttribute("vp7", tmp.VPD_ST7))));
                        doc.Save(fileName);
                    }
                    else
                    {
                        fileName = Environment.CurrentDirectory + "\\Recources\\Active\\ub.xml";
                        doc = XDocument.Load(fileName);
                        track = new XElement("track");
                        track.Add(new XAttribute("cel", tmp.Cel));
                        track.Add(new XAttribute("date2", tmp.Date_preb));
                        track.Add(new XAttribute("date1", tmp.Date_ub));
                        track.Add(new XAttribute("dolg", tmp.Dolg));
                        track.Add(new XAttribute("fio", tmp.FIO));
                        track.Add(new XAttribute("ln", tmp.Lnumber));
                        track.Add(new XAttribute("ms", tmp.Mesto));
                        track.Add(new XAttribute("osn", tmp.Osn));
                        track.Add(new XAttribute("podr", tmp.Podr));
                        track.Add(new XAttribute("type1", tmp.Types));
                        track.Add(new XAttribute("type2", tmp.Types_ub));
                        track.Add(new XAttribute("vrio", tmp.Vrio));
                        track.Add(new XAttribute("zv", tmp.Zvanie));
                        track.Add(new XAttribute("vp1", tmp.VPD_ST1));
                        track.Add(new XAttribute("vp2", tmp.VPD_ST2));
                        track.Add(new XAttribute("vp3", tmp.VPD_ST3));
                        track.Add(new XAttribute("vp4", tmp.VPD_ST4));
                        track.Add(new XAttribute("vp5", tmp.VPD_ST5));
                        track.Add(new XAttribute("vp6", tmp.VPD_ST6));
                        track.Add(new XAttribute("vp7", tmp.VPD_ST7));
                        doc.Root.Add(track);
                        doc.Save(fileName);
                    }
                    fileName = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + tmp.Lnumber + ".xml";
                    doc = XDocument.Load(fileName);
                    track = new XElement("track_ub");
                    track.Add(new XAttribute("date1", tmp.Date_ub));
                    track.Add(new XAttribute("date2", tmp.Date_preb));
                    track.Add(new XAttribute("cel", tmp.Cel));
                    track.Add(new XAttribute("ms", tmp.Mesto));
                    track.Add(new XAttribute("osn", tmp.Osn));
                    track.Add(new XAttribute("type1", tmp.Types_ub));
                    track.Add(new XAttribute("vrio", tmp.Vrio));
                    track.Add(new XAttribute("date3", tmp.Date3));
                    doc.Root.Add(track);
                    doc.Save(fileName);

                }
                if (tmp.Types_ub == "Пребытие")
                {
                    fileName = Environment.CurrentDirectory + "\\Recources\\Active\\preb.xml";
                    doc = XDocument.Load(fileName);
                    track = new XElement("track");
                    track.Add(new XAttribute("fio", tmp.FIO));
                    track.Add(new XAttribute("ln", tmp.Lnumber));
                    track.Add(new XAttribute("type2", tmp.Types_ub));
                    doc.Root.Add(track);
                    doc.Save(fileName);
                    fileName = Environment.CurrentDirectory + "\\Recources\\Active\\ub.xml";
                    doc = XDocument.Load(fileName);
                    doc.Descendants().Where(e => e.Name == tmp.FIO).Remove();
                    doc.Save(fileName);
                    fileName = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + tmp.Lnumber + ".xml";
                    doc = XDocument.Load(fileName);
                    track = new XElement("track_ub");
                    if (track.Attribute("date3").Value == ".")
                        track.Attribute("date3").Value = tmp.Date3;
                    doc.Save(fileName);
                }
            }
            catch { MessageBox.Show("Ошибка сохранения убытия"); }
        }

        public void Save_izm_shtat(Izmen i)
        {
            string[] word = i.Col.Split(new char[] { '>' }, StringSplitOptions.RemoveEmptyEntries);
            string filename = Environment.CurrentDirectory + "\\Recources\\Shtat\\51460.xml";
            XDocument doc;
            doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                if (el.Attribute("id").Value == i.Row.ToString())
                {

                }
            }
            doc.Save(filename);
        }


    }
}
