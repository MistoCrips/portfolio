﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model
{
    public class Perechni_load
    {
        ObservableCollection<Perechen> Btgr = new ObservableCollection<Perechen>();
        ObservableCollection<Perechen> Og = new ObservableCollection<Perechen>();
        ObservableCollection<Perechen> Woman = new ObservableCollection<Perechen>();
        ObservableCollection<Perechen> Perec = new ObservableCollection<Perechen>();
        ObservableCollection<Perechen> Secret = new ObservableCollection<Perechen>();

        public ObservableCollection<Perechen> sborBtgr()
        {
            string filename = Environment.CurrentDirectory + "\\Recources\\perechni\\btgr.xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                Btgr.Add(new Perechen(el));
            }

            return Btgr;
        }
        public ObservableCollection<Perechen> sborOg()
        {
            string filename = Environment.CurrentDirectory + "\\Recources\\perechni\\og.xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                Og.Add(new Perechen(el));
            }

            return Og;
        }
        public ObservableCollection<Perechen> sborwoman()
        {
            string filename = Environment.CurrentDirectory + "\\Recources\\perechni\\woman.xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                Woman.Add(new Perechen(el));
            }

            return Woman;
        }
        public ObservableCollection<Perechen> sborPerec()
        {
            string filename = Environment.CurrentDirectory + "\\Recources\\perechni\\nabor.xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                Perec.Add(new Perechen(el));
            }

            return Perec;
        }
        public ObservableCollection<Perechen> sborSecret()
        {
            string filename = Environment.CurrentDirectory + "\\Recources\\perechni\\secret.xml";
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                Secret.Add(new Perechen(el));
            }

            return Secret;
        }
        public string Mass_shtat()
        {
            sborBtgr(); sborOg(); sborwoman(); sborPerec(); sborSecret();
            string s = "Btgr > ";
            foreach (Perechen P in Btgr)
            {
                s += P.id + ">" + P.value + ">";
            }
            s += "Og > ";
            foreach (Perechen P in Og)
            {
                s += P.id + ">" + P.value + ">";
            }
            s += "Woman > ";
            foreach (Perechen P in Woman)
            {
                s += P.id + ">" + P.value + ">";
            }
            s += "Perec > ";
            foreach (Perechen P in Perec)
            {
                s += P.id + ">" + P.value + ">";
            }
            s += "Secret > ";
            foreach (Perechen P in Secret)
            {
                s += P.id + ">" + P.value + ">";
            }
            return s;
        }
    }
}
