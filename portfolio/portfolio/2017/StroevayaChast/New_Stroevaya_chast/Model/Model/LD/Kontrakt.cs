﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model.LD
{
    public class Kontrakt : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        string types_ = ".";
        string date1_ = ".";
        string date2_ = ".";
        string who_ = ".";
        string nom_ = ".";
        string date3_ = ".";
        public Kontrakt() { }
        public Kontrakt(XElement element)
        {
            if (element.Attribute("types").Value != null) types = element.Attribute("types").Value;
            if (element.Attribute("data_zakl").Value != null) data_zakl = element.Attribute("data_zakl").Value;
            if (element.Attribute("data_okon").Value != null) data_okon = element.Attribute("data_okon").Value;
            if (element.Attribute("who_pricaz").Value != null) Who_pricaz = element.Attribute("who_pricaz").Value;
            if (element.Attribute("nomber_pricaz").Value != null) Nomber_pricaz = element.Attribute("nomber_pricaz").Value;
            if (element.Attribute("date_pricaz").Value != null) Date_pricaz = element.Attribute("date_pricaz").Value;
        }
        public string types
        {
            get { return types_; }
            set
            {
                if (types_ == value)
                    return;
                types_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public string data_zakl
        {
            get { return date1_; }
            set
            {
                if (date1_ == value)
                    return;
                date1_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date1"));
            }
        }
        public string data_okon
        {
            get { return date2_; }
            set
            {
                if (date2_ == value)
                    return;
                date2_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date2"));
            }
        }
        public string Who_pricaz
        {
            get { return who_; }
            set
            {
                if (who_ == value)
                    return;
                who_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Who"));
            }
        }
        public string Nomber_pricaz
        {
            get { return nom_; }
            set
            {
                if (nom_ == value)
                    return;
                nom_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Nom"));
            }
        }
        public string Date_pricaz
        {
            get { return date3_; }
            set
            {
                if (date3_ == value)
                    return;
                date3_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date3"));
            }
        }
        public void ToXml(string LOGIN)
        {
            string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + LOGIN + ".xml";
            XDocument doc;
            if (File.Exists(fileName) == false)
            {
                doc = new XDocument(
                new XElement("base",
                    new XElement("track_kon",
                        new XAttribute("types", types),
                        new XAttribute("data_zakl", data_zakl),
                        new XAttribute("data_okon", data_okon),
                        new XAttribute("who_pricaz", Who_pricaz),
                        new XAttribute("nomber_pricaz", Nomber_pricaz),
                        new XAttribute("date_pricaz", Date_pricaz))));
                doc.Save(fileName);
            }
            else
            {
                doc = XDocument.Load(fileName);
                doc.Descendants().Where(e => e.Name == "track_kon").Remove();
                XElement track = new XElement("track_kon");
                track.Add(new XAttribute("types", types));
                track.Add(new XAttribute("data_zakl", data_zakl));
                track.Add(new XAttribute("data_okon", data_okon));
                track.Add(new XAttribute("who_pricaz", Who_pricaz));
                track.Add(new XAttribute("nomber_pricaz", Nomber_pricaz));
                track.Add(new XAttribute("date_pricaz", Date_pricaz));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }
    }
}
