﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model.LD
{
    public class Otryv_LD : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        string types_ = ".";
        string cel_ = ".";
        string mesto_ = ".";
        string date1_ = ".";
        string osn_ = ".";
        string date2_ = ".";
        string pr_ = ".";

        public Otryv_LD() { }
        public Otryv_LD(XElement element)
        {
            if (element.Attribute("mesto").Value != null) Mesto = element.Attribute("mesto").Value;
            if (element.Attribute("types_").Value != null) Types_ = element.Attribute("types_").Value;
            if (element.Attribute("cel_").Value != null) Cel_ = element.Attribute("cel_").Value;
            if (element.Attribute("s_data").Value != null) S_data = element.Attribute("s_data").Value;
            if (element.Attribute("data_preb").Value != null) Data_preb = element.Attribute("data_preb").Value;
            if (element.Attribute("osnovanie").Value != null) Osnovanie = element.Attribute("osnovanie").Value;
            if (element.Attribute("pr").Value != null) Osnovanie = element.Attribute("pr").Value;
        }
        public string Types_
        {
            get { return types_; }
            set
            {
                if (types_ == value)
                    return;
                types_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public string Cel_
        {
            get { return cel_; }
            set
            {
                if (cel_ == value)
                    return;
                cel_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Cel"));
            }
        }
        public string Mesto
        {
            get { return mesto_; }
            set
            {
                if (mesto_ == value)
                    return;
                mesto_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Mesto"));
            }
        }
        public string S_data
        {
            get { return date1_; }
            set
            {
                if (date1_ == value)
                    return;
                date1_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date1"));
            }
        }
        public string Osnovanie
        {
            get { return osn_; }
            set
            {
                if (osn_ == value)
                    return;
                osn_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Osn"));
            }
        }
        public string Data_preb
        {
            get { return date2_; }
            set
            {
                if (date2_ == value)
                    return;
                date2_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date2"));
            }
        }
        public string Pr
        {
            get { return pr_; }
            set
            {
                if (pr_ == value)
                    return;
                pr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Pr"));
            }
        }
        public void ToXml(string LOGIN)
        {
            string fileName = Environment.CurrentDirectory + "\\Recources\\Active\\LD\\" + LOGIN + ".xml";
            XDocument doc;
            if (File.Exists(fileName) == false)
            {
                doc = new XDocument(
                new XElement("base",
                    new XElement("track_ub",
                        new XAttribute("mesto", Mesto),
                        new XAttribute("types_", Types_),
                        new XAttribute("cel_", Cel_),
                        new XAttribute("s_data", S_data),
                        new XAttribute("data_preb", Data_preb),
                        new XAttribute("pr", Pr),
                        new XAttribute("osnovanie", Osnovanie))));
                doc.Save(fileName);
            }
            else
            {
                doc = XDocument.Load(fileName);
                doc.Descendants().Where(e => e.Name == "track_ub").Remove();
                XElement track = new XElement("track_ub");
                track.Add(new XAttribute("mesto", Mesto));
                track.Add(new XAttribute("types_", Types_));
                track.Add(new XAttribute("cel_", Cel_));
                track.Add(new XAttribute("s_data", S_data));
                track.Add(new XAttribute("data_preb", Data_preb));
                track.Add(new XAttribute("pr", Pr));
                track.Add(new XAttribute("osnovanie", Osnovanie));
                doc.Root.Add(track);
                doc.Save(fileName);
            }
        }

    }
}
