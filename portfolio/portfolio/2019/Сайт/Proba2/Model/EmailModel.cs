﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proba2.Model
{
    public class EmailModel
    {
        public string Subject { get; set; }
        public string From { get; set; }

        public string To { get { return "megasspo@gmail.com"; } }
        public string Body { get; set; }
    }
}