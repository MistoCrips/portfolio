﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestSuperBrands.Models
{
    public class Attributes
    {
        private int id_ { get; set; }
        [Key]
        public int id {
            get { return id_; }
            set { if (id_ == 0) id_ = value; } }

        public string Name { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreateTime { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DeleteTime { get; set; }

        public int Cat_Id { get; set; }

        public ICollection<AttributeValues> Values { get; set; }

        public Attributes()
        {
            Values = new List<AttributeValues>();
        }
    }

    public class AttributeValues
    {
        [Key]
        public int id { get; set; }

        public string Value { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreateTime { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DeleteTime { get; set; }

        [ForeignKey("Attributes")]
        public int Attributes_Id { get; set; }

        public Attributes Attributes { get; set; }

        [ForeignKey("Products")]
        public int Products_Id { get; set; }

        public Products Products { get; set; }
    }
}