﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace New_Stroevaya_chast.Modal.Model
{
    public class Otbor_na_kontr : INotifyPropertyChanged
    {
        public Otbor_na_kontr()
        { }
        public Otbor_na_kontr(XElement element)
        {
            Podr = element.Attribute("podr").Value;
            Dolg = element.Attribute("dolg").Value;
            Lnumber = element.Attribute("ln").Value;
            FIO = element.Attribute("fio").Value;
            Types = element.Attribute("per").Value;
            Zvanie = element.Attribute("zv").Value;
            Date_ic = element.Attribute("date_ic").Value;
            Date1 = element.Attribute("date1").Value;
            Date2 = element.Attribute("date2").Value;
            Date3 = element.Attribute("date3").Value;

            Date4 = element.Attribute("date4").Value;
            Primec = element.Attribute("prim").Value;
            if (element.Attribute("rap").Value == "true") Raport = true; else if (element.Attribute("rap").Value == "false") Raport = true;
            if (element.Attribute("prof").Value == "true") Prof = true; else if (element.Attribute("prof").Value == "false") Prof = true;
            if (element.Attribute("bla").Value == "true") Blank = true; else if (element.Attribute("bla").Value == "false") Blank = true;
            if (element.Attribute("fiz").Value == "true") FIZO = true; else if (element.Attribute("fiz").Value == "false") FIZO = true;
            if (element.Attribute("vvk").Value == "true") Vvk = true; else if (element.Attribute("vvk").Value == "false") Vvk = true;
            if (element.Attribute("ic").Value == "true") Ic = true; else if (element.Attribute("ic").Value == "false") Ic = true;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        string podr_ = ".";
        string dolg_ = ".";
        string ln_ = ".";
        string fio_ = ".";
        string per_ = ".";
        string zv_ = ".";
        string date_ic = ".";
        string date1 = "."; //ДАта отбора
        string date2 = "."; //Дата передачи пакета документов в проект СС ЗМО РФ
        string date3 = "."; // Дата приказа
        string date4 = "."; // Дата отказа
        string prim_ = ".";

        bool rap_ = false;
        bool prof_ = false;
        bool bla_ = false;
        bool fiz_ = false;
        bool vvk_ = false;
        bool ic_ = false;
        string message = ".";
        public string D1()
        {
            ObservableCollection<Otbor_na_kontr> Ot = new ObservableCollection<Otbor_na_kontr>();
            string filename = Environment.CurrentDirectory + "\\Recources\\nk\\Otob.xml"; //Отобранные
            XDocument doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                Ot.Add(new Otbor_na_kontr(el));
            }
            message = "O1";
            foreach (Otbor_na_kontr i in Ot)
            {
                message += "|" + i.Blank + "|" + i.Date1 + "|" + i.Date2 + "|" + i.Date3 + "|" + i.Date4 + "|" + i.Date_ic + "|" + i.Dolg + "|" + i.FIO + "|" + i.FIZO + "|" + i.Ic + "|" + i.Lnumber + "|" + i.Podr + "|" + i.Primec + "|" + i.Prof + "|" + i.Raport + "|" + i.Types + "|" + i.Vvk + "|" + i.Zvanie;
            }
            Ot = new ObservableCollection<Otbor_na_kontr>();
            filename = Environment.CurrentDirectory + "\\Recources\\nk\\Pered.xml"; //Переданные в приказ СС ЗМО
            doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                Ot.Add(new Otbor_na_kontr(el));
            }
            message += "|O2";
            foreach (Otbor_na_kontr i in Ot)
            {
                message += "|" + i.Blank + "|" + i.Date1 + "|" + i.Date2 + "|" + i.Date3 + "|" + i.Date4 + "|" + i.Date_ic + "|" + i.Dolg + "|" + i.FIO + "|" + i.FIZO + "|" + i.Ic + "|" + i.Lnumber + "|" + i.Podr + "|" + i.Primec + "|" + i.Prof + "|" + i.Raport + "|" + i.Types + "|" + i.Vvk + "|" + i.Zvanie;
            }
            Ot = new ObservableCollection<Otbor_na_kontr>();
            filename = Environment.CurrentDirectory + "\\Recources\\nk\\Sostoy.xml"; //Состоялись
            doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                Ot.Add(new Otbor_na_kontr(el));
            }
            message += "|O3";
            foreach (Otbor_na_kontr i in Ot)
            {
                message += "|" + i.Blank + "|" + i.Date1 + "|" + i.Date2 + "|" + i.Date3 + "|" + i.Date4 + "|" + i.Date_ic + "|" + i.Dolg + "|" + i.FIO + "|" + i.FIZO + "|" + i.Ic + "|" + i.Lnumber + "|" + i.Podr + "|" + i.Primec + "|" + i.Prof + "|" + i.Raport + "|" + i.Types + "|" + i.Vvk + "|" + i.Zvanie;
            }
            Ot = new ObservableCollection<Otbor_na_kontr>();
            filename = Environment.CurrentDirectory + "\\Recources\\nk\\Otkaz.xml"; //Отказ
            doc = XDocument.Load(filename);
            foreach (XElement el in doc.Root.Elements("track"))
            {
                Ot.Add(new Otbor_na_kontr(el));
            }
            message += "|O4";
            foreach (Otbor_na_kontr i in Ot)
            {
                message += "|" + i.Blank + "|" + i.Date1 + "|" + i.Date2 + "|" + i.Date3 + "|" + i.Date4 + "|" + i.Date_ic + "|" + i.Dolg + "|" + i.FIO + "|" + i.FIZO + "|" + i.Ic + "|" + i.Lnumber + "|" + i.Podr + "|" + i.Primec + "|" + i.Prof + "|" + i.Raport + "|" + i.Types + "|" + i.Vvk + "|" + i.Zvanie;
            }
            return message;
        }
        public string Podr
        {
            get { return podr_; }
            set
            {
                if (podr_ == value)
                    return;
                podr_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Podr"));
            }
        }
        public string Dolg
        {
            get { return dolg_; }
            set
            {
                if (dolg_ == value)
                    return;
                dolg_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Dolg"));
            }
        }
        public string Lnumber
        {
            get { return ln_; }
            set
            {
                if (ln_ == value)
                    return;
                ln_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Lnumber"));
            }
        }
        public string Zvanie
        {
            get { return zv_; }
            set
            {
                if (zv_ == value)
                    return;
                zv_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Zvanie"));
            }
        }
        public string FIO
        {
            get { return fio_; }
            set
            {
                if (fio_ == value)
                    return;
                fio_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FIO"));
            }
        }
        public string Types
        {
            get { return per_; }
            set
            {
                if (per_ == value)
                    return;
                per_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Types"));
            }
        }
        public string Date_ic
        {
            get { return date_ic; }
            set
            {
                if (date_ic == value)
                    return;
                date_ic = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date_ic"));
            }
        }
        public string Date1
        {
            get { return date1; }
            set
            {
                if (date1 == value)
                    return;
                date1 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date1"));
            }
        }
        public string Date2
        {
            get { return date2; }
            set
            {
                if (date2 == value)
                    return;
                date2 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date2"));
            }
        }
        public string Date3
        {
            get { return date3; }
            set
            {
                if (date3 == value)
                    return;
                date3 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date3"));
            }
        }
        public string Date4
        {
            get { return date4; }
            set
            {
                if (date4 == value)
                    return;
                date4 = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Date4"));
            }
        }
        public string Primec
        {
            get { return prim_; }
            set
            {
                if (prim_ == value)
                    return;
                prim_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Primec"));
            }
        }
        public bool Ic
        {
            get { return ic_; }
            set
            {
                if (ic_ == value)
                    return;
                ic_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Ic"));
            }
        }
        public bool Vvk
        {
            get { return vvk_; }
            set
            {
                if (vvk_ == value)
                    return;
                vvk_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Vvk"));
            }
        }
        public bool Prof
        {
            get { return prof_; }
            set
            {
                if (prof_ == value)
                    return;
                prof_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Prof"));
            }
        }
        public bool Blank
        {
            get { return bla_; }
            set
            {
                if (bla_ == value)
                    return;
                bla_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Blank"));
            }
        }
        public bool FIZO
        {
            get { return fiz_; }
            set
            {
                if (fiz_ == value)
                    return;
                fiz_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Fizo"));
            }
        }
        public bool Raport
        {
            get { return rap_; }
            set
            {
                if (rap_ == value)
                    return;
                rap_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Raport"));
            }
        }
    }//Отбор на контрактную службу
}
