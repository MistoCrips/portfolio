﻿using New_Stroevaya_chast.Modal.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace New_Stroevaya_chast.ViewModal
{
    public class Recrut //Класс для обработки запросов
    {
        public ObservableCollection<OpenLoad> shtat_pol(string[] word, string dop) //Получение штата
        {
            ObservableCollection<OpenLoad> list = new ObservableCollection<OpenLoad>();
            OpenLoad tmp = new OpenLoad();
            int g = 0;
            for (int i = 1; i < word.Length; i++)
            {

                int j = i;
                while (j > 13)
                {
                    j -= 13;
                }
                if (j == 1)
                {
                    tmp = new OpenLoad();
                    tmp.Id = int.Parse(word[i]);
                    tmp.Counter = int.Parse(word[i]);
                }
                if (j == 2)
                {
                    tmp.Podr = word[i];
                    string[] word1 = word[i].Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    tmp.Podr1 = word1[0];
                    if (word1.Length == 1) { tmp.Podr2 = "управ"; } else { tmp.Podr2 = word1[1]; }
                }
                if (j == 3)
                {
                    tmp.Dolgnost = word[i];
                }
                if (j == 4)
                {
                    tmp.Shtat_type = word[i];
                }
                if (j == 5)
                {
                    tmp.Vus = word[i];
                }
                if (j == 6)
                {
                    tmp.Kod = word[i];
                }
                if (j == 7)
                {
                    if (word[i] == "0") tmp.Lnumber = ".";
                    else
                        tmp.Lnumber = word[i];
                }
                if (j == 8)
                {
                    if (word[i] == "0") tmp.Zvanie = ".";
                    else
                        tmp.Zvanie = word[i];
                }
                if (j == 9)
                {
                    if (word[i] == "0") tmp.Fio = "-В-";
                    else
                        tmp.Fio = word[i];
                }
                if (j == 10)
                {
                    if (word[i] == "0") tmp.Types = ".";
                    else
                        tmp.Types = word[i];
                }
                if (j == 11)
                {
                    if (word[i] == "0") tmp.Otryv_status = ".";
                    else
                        tmp.Otryv_status = word[i];
                }
                if (j == 12)
                {
                    if (word[i] == "0") tmp.Otryv_primech = ".";
                    else
                        tmp.Otryv_primech = word[i];
                }
                if (j == 13)
                {
                    if (word[i] == "0") tmp.Otryv_pricaz = ".";
                    else
                        tmp.Otryv_pricaz = word[i];
                    list.Add(tmp);
                }

            }
            if (dop == "8") //Приказ по допуску
            {
                int ji = 0, nn = 0;
                int N = list.Count;
                for (int i = 0; i < N; i++)
                {
                    if (list[nn].Lnumber == ".")
                    {
                        list.Remove(list[nn]);
                    }
                    else { nn++; }
                }
                foreach (OpenLoad p in list)
                {
                    p.Counter = ji + 1;
                    ji += 1;
                }
            }
            if (dop == "9") //Наряд по допуску
            {
                int ji = 0, nn = 0;
                int N = list.Count;
                for (int i = 0; i < N; i++)
                {
                    if (list[nn].Lnumber == ".")
                    {
                        list.Remove(list[nn]);

                    }
                    else
                    if (list[nn].Otryv_status != ".")
                    {
                        list.Remove(list[nn]);
                    }
                    else { nn++; }
                }
                foreach (OpenLoad p in list)
                {
                    p.Counter = ji + 1;
                    ji += 1;
                }
            }
            if (dop == "4") //Прапорский штат по допуску
            {
                int ji = 0, nn = 0;
                int N = list.Count;
                for (int i = 0; i < N; i++)
                {
                    if (list[nn].Shtat_type != "пр-к")
                    {
                        list.Remove(list[nn]);
                    }
                    else { nn++; }
                }
                foreach (OpenLoad p in list)
                {
                    p.Counter = ji + 1;
                    ji += 1;
                }
            }
            if (dop == "3") //офицерский штат по допуску
            {
                int ji = 0, nn = 0;
                int N = list.Count;
                for (int i = 0; i < N; i++)
                {
                    if (list[nn].Shtat_type != "О")
                    {
                        list.Remove(list[nn]);
                    }
                    else { nn++; }
                }
                foreach (OpenLoad p in list)
                {
                    p.Counter = ji + 1;
                    ji += 1;
                }
            }
            if (dop == "5" || dop == "6") //сер и ряд штат по допуску
            {
                int ji = 0, nn = 0;
                int N = list.Count;
                for (int i = 0; i < N; i++)
                {
                    if (list[nn].Shtat_type != "ряд" && list[nn].Shtat_type != "сер")
                    {
                        list.Remove(list[nn]);
                    }
                    else { nn++; }
                }
                foreach (OpenLoad p in list)
                {
                    p.Counter = ji + 1;
                    ji += 1;
                    if (p.Types != "контр" && p.Types != "жен")
                    {
                        p.Zvanie = ".";
                        p.Types = ".";
                        p.Otryv_pricaz = ".";
                        p.Otryv_primech = ".";
                        p.Otryv_status = ".";
                        p.Fio = "-В-";
                    }
                }
            }
            if (dop == "10") //гражданский штат по допуску
            {
                int ji = 0, nn = 0;
                int N = list.Count;
                for (int i = 0; i < N; i++)
                {
                    if (list[nn].Shtat_type != "г/п")
                    {
                        list.Remove(list[nn]);
                    }
                    else { nn++; }
                }
                foreach (OpenLoad p in list)
                {
                    p.Counter = ji + 1;
                    ji += 1;
                }
            }
            return list;
        }

    }
}
